#ifndef DEVICE_POLICY_MANAGER_H
#define DEVICE_POLICY_MANAGER_H

void v_Update_DPM_State( PD_Port_t *p_Context, Event_t *event );
void v_Device_Policy_Manager_Task( void *args );
void v_Post_DPM_Event( PD_Port_t *p_Port, Event_t *p_Event );

#endif