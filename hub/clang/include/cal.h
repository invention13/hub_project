/** @file cal.h
 *  @brief main Routines associated with calibration and trimming
 *
 *
 *  @author Ian Board
 *  @bug No known bugs.
 */
#ifndef _CAL_Hdevi
#define _CAL_H
   
#define CHIP_REVISION_C0 0x20

void v_Basic_Trims_From_Flash(void);
void v_Apply_Trims_From_Flash(void);
void v_PLL_Calibration( void ) ;
void v_Dump_Trim_Memory(void);
void v_Apply_USB_Fixes(void);
void v_Default_CDR_Setup( void );
void v_Apply_C0_USB_Fixes(void);

#endif