#ifndef PHY_H
#define PHY_H

// ->TYPECCTL1
//#define USBCCTL1  1       - aka TYPE_C_CTL1
// Pull up current
#define SRC_CUR80      0x08
#define SRC_CUR180     0x10
#define SRC_CUR330     0x18
#define MODE_SNK       0x00
#define MODE_SRC_STAY  0x02
#define MODE_SRC_ACC   0x03
#define MODE_DRP       0x04
#define MODE_DRP_ACC   0x05

// ->TYPECCTL2
//#define USBCCTL2     2   - aka TYPE_C_CTL2
#define UNSUP_ACC    0x80
#define TRY_SRC      0x40
#define ATT_SRC      0x20
#define ATT_SNK      0x10
#define ERR_REC      0x08
#define DIS_ST       0x04
#define UNATT_SRC    0x02
#define UNATT_SNK    0x01

// ->TYPECCTL3
//#define USBCCTL3     3   - aka TYPE_C_CTL3
#define VBUS_IGNOR   0x20
#define RST_PHY      0x04
#define PD_DIS       0x02
#define DET_DIS      0x01

// ->CC1CONTROL
//#define CC1_CTL      4   - aka CC1_CONTROL
#define VBUSOK_       0x80
#define PDWN1_        0x08
#define TXE1_         0x04
#define VCONN1_       0x02
#define PUEN1_        0x01

// ->CC2CONTROL
//#define CC2_CTL      5   - aka CC2_CONTROL
#define VBUSOK_       0x80
#define PDWN2_        0x08
#define TXE2_         0x04
#define VCONN2_       0x02
#define PUEN2_        0x01

// ->CCSEL
//#define CC_SEL       6
#define VCON_SWP_OFF 0x08
#define VCON_SWP_ON  0x04
#define CC_SEL2      0x02
#define CC_SEL1      0x01

// ->TYPECSTATUS1
//#define USBC_STAT1   7   - aka Type_C_STATUS1
#define CSM_RDY      0x00
#define CSM_CMPLT    0x80
#define M_CSM_CMPLT  0xc0
#define CSM_GOING    0x40
#define CC_ORIENT1   0x10
#define CC_ORIENT2   0x20
#define M_CSM_RSLT   0x0f
#define SRC_DEF_ATT  0x01
#define SRC_1P5A_ATT 0x02
#define SRC_3A_ATT   0x03
#define M_SRC_ATT    0x03       //added 030292017   
#define SNK_ATT      0x04
#define AUDIO_ATT    0x06

#define TYPECRSLT_SNK_ATT 0x4
#define TYPECDET_CMPLT    0x2

// ->TYPECSTATUS2
//#define USBC_STAT2   8   - aka Type_C_STATUS2
#define VBUS_REQ_     0x20
#define PD_NOT_ALWD_  0x10
#define ADC1_DONE_    0x04
#define OVRTEMP_      0x02
#define SHORT_        0x01

// ->TYPECSTATUS3
//#define USBC_STAT3   9   - aka Type_C_STATUS3
#define TYPEC_ACTIVE 0b10000000

// ->CC1CMP
//#define CC1_CMP      10
#define DET_3A       0x10
#define DET_1P5A     0x08
#define DET_DEF      0x04
#define DET_RD       0x02
#define DET_RA       0x01

// ->CC2CMP
//#define CC2_CMP      11
// Bit defs as per CC1_CMP

// ->CC1STATUS
//#define CC1_STAT     12   - aka CC1_STATUS
#define SRC_RP1      0x10
#define PWR3A_SNK1   0x08
#define PWR1P5A_SNK1 0x04
#define PWRDEF_SNK1  0x02
#define SNKPR1       0x01

// ->CC2STATUS
//#define CC2_STAT     13   - aka CC2_STATUS
#define SRC_RP2      0x10
#define PWR3A_SNK2   0x08
#define PWR1P5A_SNK2 0x04
#define PWRDEF_SNK2  0x02
#define SNKPR2       0x01

// ->ADC1
//#define ADC1         14
#define ADCSTART     0x40

// ->reserved
//#define ADC2         15

// ->AFETRIM1
//#define AFE_TRIM1    16

// ->AFETRIM2
//#define AFE_TRIM2    17

// ->TRIMHCUR (single value)
//#define AFE_TRIM3    18

// ->TST
#define ATEST        19

// ->PWR
//#define POWER        20
#define EN_AFE       0x1

// ->SLICE
//#define SLICE        21   - (20 in otc9108 r0.5)

// ->INTERRUPT1
//#define IRQ1         22   - aka INTERRUPT1
#define I_ADC_DONE1  0x04
#define I_OVRTEMP    0x02
#define I_SHORT      0x01

// ->INTERRUPT2
//#define IRQ2         23   - aka INTERRUPT2
#define I_CC_CHANGE  0x80
#define I_PD_RX      0x40
#define I_PD_HR_RX   0x20
#define I_PD_CR_RX   0x10
#define I_PD_TX_OK   0x08
#define I_PD_TX_FAIL 0x04
#define I_FAST_SWAP  0x02

// ->MASK1
//#define IRQ_MSK1     24   - aka MASK1
#define M_ADC_DONE1  0x04
#define M_OVRTEMP    0x02
#define M_SHORT      0x01

// ->MASK2
//#define IRQ_MSK2       25   - aka MASK2
#define M_CC_CHANGE    0x80
#define M_PD_RX        0x40
#define M_PD_HR_RX     0x20
#define M_PD_CR_RX     0x10
#define M_PD_TX_OK     0x08
#define M_PD_TX_FAIL   0x04
#define M_FAST_SWAP    0x02

// ->PDCFG1
//#define PD_CFG1        26   - aka PD-Setup 1
#define ID_INSERT      0x80
#define VBUS_HIGH_VOLT 0x40
#define GUIDE_TRY_SNK  0x20

// ->PDCFG2
//#define PD_CFG2        27   - aka PD-Setup 2
#define FAST_SWAP_SNK  0x80
#define FAST_SWAP_SRC  0x40
#define NEW_CDR        0x00
#define OLD_CDR        0x20  //Changed. Check email: OT910x IP CDR_VERSION Bit Notification 11/01/18
#define SOP_DB2        0x10
#define SOP_DB1        0x08
#define RX_SOP2        0x04
#define RX_SOP1        0x02
#define RX_SOP0        0x01
#define ALL_SOP        0x1F
#define CDR_VERSION    0

// Deprecated in drop 03292017. For RX use RX_SOP* 
#define SOP2           0x04
#define SOP1           0x02
#define SOP0           0x01


// ->PDCFG3
//#define PD_CFG3        28   - aka PD-Setup 3
// 9108 PD_CFG3 register version conversion
#define revSop0Adr      2
#define specRevSop2(sr) (sr<<6)
#define specRevSop1(sr) (sr<<4)
#define specRevSop0(sr) (sr<<revSop0Adr)
#define M_VERS_SOP2    0xC0
#define M_VERS_SOP1    0x30
#define M_VERS_SOP0    0x0C

// 9109 PD_CFG3 register
#define PR_PROVIDER    0x01
#define PR_CONSUMER    0x00
#define DR_UFP         0x00
#define DR_DFP         0x02
#define M_CFG3_DR      0x02     //added 03292017 
#define M_DATA_ROLE    0x02
#define M_PWR_ROLE     0x01

// ->SHORTPROTECT
//#define SHORT_PROT     29
#define SHORT_RESET    0x80
#define SHORT_TIME     0x40

// ->PDSTATUS
//#define PD_STAT        30   - PD-Status
#define FAST_SWAP      0x80
#define TX_BUSY        0x03
#define TX_ERROR       0x02
#define TX_SUCCESS     0x01
#define M_TX_RESULT    0x07

//#define PDSTATUS_TXRESULT_RESET   0x0
//#define PDSTATUS_TXRESULT_SUCCESS 0x1
//#define PDSTATUS_TXRESULT_ERROR   0x2
//#define PDSTATUS_TXRESULT_BUSY    0x3

// ->RXSTATUS
//#define RX_STAT        31  - aka RX_Status
#define RX_DATA        0x80
#define RX_OVERRUN     0x40
#define RX_CLEAR       0x01

// ->RXINFO
//#define RX_INFO        32   - aka RX_Info
#define M_RX_BYTES     0xF8

// ->TXCOMMAND
//#define TX_CMD         33   - aka TX_Command
#define TX_BIST        0x80
#define TX_HWR_RST     0x60
#define TX_CAB_RST     0x40
#define TX_BUF         0x20
#define TX_NOP         0x00
#define TX_WAIT_RP     0x04
#define TX_START       0x02
#define TXBUF_READY    0x01

// ->TXINFO
//#define TX_INFO        34
#define RETRIES3       (3<<3)
#define RETRIES2       (2<<3)
#define TX_SOP0        1
#define TX_SOP1        2
#define TX_SOP2        3
#define TX_DB_SOP1     4
#define TX_DB_SOP2     5

#endif