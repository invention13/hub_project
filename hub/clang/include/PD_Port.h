#ifndef _PD_PORT_H
#define _PD_PORT_H

#include "timers.h"
#include "queue.h"
#include "event_groups.h"

#include "events.h"

#include "device_manager.h"
#include "port_policy_engine.h"
#include "protocol.h"

#define PD_EVENT_QUEUE_LENGTH 10
#define MAX_SOURCE_OBJECTS 4
#define MAX_SINK_OBJECTS 4

struct PD_Port_tag;

typedef void *(*State_Handler_t)(struct PD_Port_tag *, Event_t *);

 typedef enum 
 { 
   PORT_ROLE_SOURCE, 
   PORT_ROLE_SINK 
 } Power_Role_t;
 
 typedef enum
 {
   DATA_ROLE_UFP,
   DATA_ROLE_DFP
 } Data_Role_t;
 
typedef struct PD_Port_Init_tag
{
    uint8_t *u8_Name;
    Power_Role_t e_Power_Role;
    Data_Role_t e_Data_Role;
    volatile uint32_t *TypeC_Base;

}PD_Port_Init_t;

// Event bits used for synchronization after startup or reset
#define POLICY_ENGINE_READY (1 << 0)
#define PROTOCOL_READY      (1 << 1)
#define DEVICE_MANAGER_READY (1 << 2 )
#define ALL_READY ( POLICY_ENGINE_READY | PROTOCOL_READY | DEVICE_MANAGER_READY )


typedef struct PD_Port_tag
{
    EventGroupHandle_t Startup_Ready;
    
    // Identifier
    uint8_t *u8_Name;
    
    // Port Parameters
    Power_Role_t e_Port_Role;
    Data_Role_t e_Data_Role;
    
    // Value of Rp to announce current capability for implicit contract
    uint8_t Rp;
    
    // Message ID
    uint8_t u8_Message_ID[2]; // One for SOP, and SOP'/SOP''
    
    // Source and sink PDOs
    uint32_t u32_Source_Object[MAX_SOURCE_OBJECTS];
    uint8_t u8_Number_Source_Objects;
    
    uint32_t u32_Sink_Object[MAX_SINK_OBJECTS];
    uint8_t u8_Number_Sink_Objects;
    
    // Component parts - Each represents a thread plus data
    Device_Manager_t Device_Manager;
    Protocol_t  Protocol;
    Port_Policy_Engine_t Port_Policy_Engine;

    // Timers
    TimerHandle_t timer;
    
    // Threads
    TaskHandle_t PPE_Task, 
                 DPM_Task,
                 Protocol_Task;
    
    // To insert into list of active ports
    struct PD_Port_tag *p_Next_Port;
    
    // Private section - may vary with implementation  
    // type C registers
    volatile uint32_t *TypeC_Base;

}PD_Port_t;



void v_Init_PD_Port( PD_Port_Init_t *p_Init, PD_Port_t *p_Port );
void v_Post_Event( PD_Port_t *p_Port, Event_t *p_Event );
void v_Announce_Ready( PD_Port_t *p_Port, uint8_t u8_Bit );
void v_Wait_Until_Ready( PD_Port_t *p_Port, uint8_t u8_Bits );


#endif