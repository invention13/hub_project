/** @file trim_defaults.h
 *  @brief Trim defaults for superior C0 rev
 *  Used if calibration version < 4 - values weren't written to cal
 *  memory by ATE and hard-coded defaults must be used.
 *
 *
 *  @author Ian Board
 *  @bug No known bugs.
 */
#ifndef _CAL_DEFAULTS_H
#define _CAL_DEFAULTS_H

#define DIG_SUPPLY_1p5_DEFAULT 7
#define SQUELCH_DETECTOR_DEFAULT 1
#define DISCONNECT_DETECTOR_DEFAULT 3
#define CDP_TX_VOLTAGE_DEFAULT 4
#define CDR_SYNC_TRIM_DEFAULT 1
#define FSLS_FE_DEFAULT 7
#define FAST_ACQ_DEFAULT 1
#define EXT_REP_SYNC_DEFAULT 1
#define PLL_CP_DEFAULT 0x52
#define PLL_FILTER_R_DEFAULT 0x63
#define PLL_FILTER_C1_DEFAULT 0x73
#define PLL_FILTER_C2_DEFAULT 0x83

#endif