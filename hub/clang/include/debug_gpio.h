#ifndef _DEBUG_H
#define _DEBUG_H

#include <stdint.h>

// This uses the GPIOs associated with the SD card on the Superior Eval board as debug GPIOs.

// GPIO10
// J1206 pin 15
// Port B GPB_1
#define GPIO10_DEBUG (1 << 1)


// GPIO11
// J1206 Pin 13
// Port B GPB_2
#define GPIO11_DEBUG (1 << 2)


// GPIO12
// J1206 Pin 11
// Port B GPB_3
#define GPIO12_DEBUG (1 << 3)


// GPIO13
// J1206 Pin 9
// Port B GPB_4
#define GPIO13_DEBUG (1 << 4)

// GPIO14
// J1206 Pin 7
// Port B GPB_5
#define GPIO14_DEBUG (1 << 5)

// GPIO15
// J1206 Pin 3
// Port B GPB_6
#define GPIO15_DEBUG (1 << 6)

#define DEBUG_GPIOS ( GPIO10_DEBUG | GPIO11_DEBUG | GPIO12_DEBUG | GPIO13_DEBUG | GPIO14_DEBUG | GPIO15_DEBUG )

#if defined(USE_DEBUG_GPIOS) && !defined(NDEBUG)
#define v_Init_Debug_GPIO() _v_Init_Debug_GPIO()
#define v_Set_Debug_GPIO(x, y) _v_Set_Debug_GPIO(x, y)
#else
#define v_Init_Debug_GPIO() (void)0
#define v_Set_Debug_GPIO(x,y) (void)0
#endif

void _v_Set_Debug_GPIO( uint8_t, uint8_t );
void _v_Init_Debug_GPIO(void);

#endif