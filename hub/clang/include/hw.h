#ifndef _HW_H
#define _HW_H

typedef struct Chip_Info_tag
{
  uint32_t u32_Timestamp;
  uint8_t u8_ATE_Loadboard_Number;
  uint16_t u16_Lot_Number;
  uint8_t u8_Chip_Revision;
  uint8_t u8_Chip_Name;
  uint8_t u8_Cal_Version;
  uint32_t u32_ATE_Program_Date_Code;
  uint8_t u8_ATE_Site_Number;
} Chip_Info_t;

void SystemInit(void);
void v_Get_Chip_Info( Chip_Info_t *p_Info );
void v_Delay_us( uint8_t u8_Delay );

#endif