#ifndef DEVICE_POLICY_MANAGER_H
#define DEVICE_POLICY_MANAGER_H

 typedef struct Device_Policy_Manager_tag
 {
     QueueHandle_t Event_Queue;
     void  *p_DPM_State;
 } Device_Manager_t;

#define DEVICE_POLICY_MANAGER_STACK_SIZE 128

void v_Update_DPM_State( void *p_Context, Event_t *event );
void v_Device_Policy_Manager_Task( void *args );
void v_Post_DPM_Event( void *p_Port, Event_t *p_Event );
void v_Init_Device_Manager( void *p_Owning_Port, void  *p_Init );

#endif