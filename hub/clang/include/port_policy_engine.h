#ifndef PORT_POLICY_ENGINE_H
#define PORT_POLICY_ENGINE_H


 typedef struct Port_Policy_Engine_tag
 {
     void  *p_Port;
     QueueHandle_t Event_Queue; 
     void *p_PE_State;
 }Port_Policy_Engine_t;

#define PORT_POLICY_ENGINE_STACK_SIZE 128


void v_Init_Port_Policy_Engine( void *p_Owning_Port, void *p_Init );
void v_Update_PE_State( void *p_Owning_Port, Event_t *event );
void v_Port_Policy_Engine( void *args );
void v_Post_PE_Event( void *p_Owning_Port, Event_t *p_Event );

#endif