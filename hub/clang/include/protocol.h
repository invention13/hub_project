#ifndef PROTOCOL_H
#define PROTOCOL_H


 typedef struct Protocol_tag
 {
     QueueHandle_t Event_Queue;
     void   *p_Protocol_State;
 }Protocol_t;

#define PROTOCOL_STACK_SIZE 128

void v_Init_Protocol( void *p_Port, void *p_Init );
void v_Protocol_Task( void *args );
void v_Post_Protocol_Event( void *p_Port, Event_t *p_Event );

#endif