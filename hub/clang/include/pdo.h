#ifndef DEVICE_POLICY_MGR_H
#define DEVICE_POLICY_MGR_H

#ifdef __ICCARM__
#define IAR_PACK __packed
#define GCC_PACK
#elif __GNUC__
#define IAR_PACK
#define GCC_PACK __attribute__((packed))
#endif


// Source Capability
typedef IAR_PACK struct
{
    unsigned int u10_Max_Current : 10;
    unsigned int u10_Voltage : 10;
    unsigned int u2_Peak_Current : 2;
    unsigned int reserved : 2;
    unsigned int u1_Unchunked_Ext_Msg_Support : 1;
    unsigned int u1_Dual_Role_Data : 1;
    unsigned int u1_USB_Comm_Capable : 1;
    unsigned int u1_Uncontrained_Power : 1;
    unsigned int u1_USB_Suspend_Supported : 1;
    unsigned int u1_Dual_Role : 1;
    unsigned int u2_Type : 2;
} GCC_PACK Fixed_Supply_Source_PDO_t;


typedef IAR_PACK struct
{
    unsigned int u10_Max_Current : 10;
    unsigned int u10_Min_Voltage : 10;
    unsigned int u10_Max_Voltage : 10;
    unsigned int u2_Type : 2;
} GCC_PACK Variable_Supply_Source_PDO_t;

typedef IAR_PACK struct
{
    unsigned int u10_Max_Allowable_Power : 10;
    unsigned int u10_Min_Voltage : 10;
    unsigned int u10_Max_Voltage : 10;
    unsigned int u2_Type : 2;
}GCC_PACK Battery_Supply_Source_PDO_t;

typedef IAR_PACK  struct
{
    unsigned int u7_Max_Current : 7;
    unsigned int reserved1 : 1;
    unsigned int u8_Min_Voltage : 8;
    unsigned int reserved2 : 1;
    unsigned int u8_Max_Voltage : 8;
    unsigned int reserved3 : 2;
    unsigned int u1_PPS_Power_Limited : 1;
    unsigned int u2_Programmable_Power_Supply : 2; // Reserved - should not be used.
    unsigned int u2_Type : 2;
}GCC_PACK Programmable_Power_Supply_Source_PDO_t;

// Sink capability
typedef __packed struct
{
    unsigned int u10_Operational_Current : 10;
    unsigned int u10_Voltage : 10;
    unsigned int reserved : 3;
    unsigned int u2_Fast_Role_Swap_Current : 2;
    unsigned int u1_Dual_Role_Data : 1;
    unsigned int u1_USB_Comm_Capable : 1;
    unsigned int u1_Unconstrained_Power : 1;
    unsigned int u1_Higher_Capability : 1;
    unsigned int u1_Dual_Role_Power : 1;
    unsigned int u2_Type : 2;
}Fixed_Supply_Sink_PDO_t;

typedef __packed struct
{
    unsigned int u10_Operational_Current : 10;
    unsigned int u10_Min_Voltage : 10;
    unsigned int u10_Max_Voltage : 10;
    unsigned int u2_Type : 2;
}Variable_Supply_Sink_PDO_t;

typedef __packed struct
{
    unsigned int u10_Operational_Power : 10;
    unsigned int u10_Min_Voltage : 10;
    unsigned int u10_Max_Voltage : 10;
    unsigned int u2_Type : 2;
}Battery_Supply_Sink_PDO_t;

typedef __packed struct
{
    unsigned int u7_Max_Current : 7;
    unsigned int reserved : 1;
    unsigned int u8_Min_Voltage : 8;
    unsigned int reserved_1 : 1;
    unsigned int u8_Max_Voltage : 8;
    unsigned int reserved_2 : 3;
    unsigned int u2_Programmable_Power_Supply : 2;
    unsigned int u2_Type;
}Programmable_Power_Supply_Aug_Sink_PDO_t;

// Request

typedef __packed struct
{
  unsigned int u10_Max_Operating_Current : 10;
  unsigned int u10_Operating_Current : 10;
  unsigned int reserved : 3;
  unsigned int b_Unchunked_Extended_Messages : 1;
  unsigned int b_No_USB_Suspend : 1;
  unsigned int b_USB_Comm_Capable : 1;
  unsigned int b_Capability_Mismatch : 1;
  unsigned int b_Give_Back_Flag : 1;
  unsigned int u3_Object_Position : 3;
  unsigned int reserved_1 : 1;
}Fixed_Variable_Request_PDO_t;

typedef __packed struct
{
  unsigned int u10_Max_Operating_Power : 10;
  unsigned int u10_Operating_Power : 10;
  unsigned int reserved : 3;
  unsigned int b_Unchunked_Extended_Messages : 1;
  unsigned int b_No_USB_Suspend : 1;
  unsigned int b_USB_Comm_Capable : 1;
  unsigned int b_Capability_Mismatch : 1;
  unsigned int b_Give_Back_Flag : 1;
  unsigned int u3_Object_Position : 3;
  unsigned int reserved_1 : 1;
}Battery_Request_PDO_t;

typedef __packed struct
{
  unsigned int u7_Operating_Current : 7;
  unsigned int reserved : 2;
  unsigned int u11_Output_Voltage : 11;
  unsigned int reserved_1 : 3;
  unsigned int b_Unchunked_Extended_Messages : 1;
  unsigned int b_No_USB_Suspend : 1;
  unsigned int b_USB_Comm_Capable : 1;
  unsigned int b_Capability_Mismatch : 1;
  unsigned int reserved_2 : 1;
  unsigned int u3_Object_Position : 3;
  unsigned int reserved_3 : 1;
}Programmable_Request_PDO_t;


typedef union
{
  Fixed_Supply_Source_PDO_t      fixed;
  Variable_Supply_Source_PDO_t   variable;
  Battery_Supply_Source_PDO_t    battery;
  Programmable_Power_Supply_Source_PDO_t programmable;
  uint32_t            uint32;
}Source_Capability_PDO;



#endif