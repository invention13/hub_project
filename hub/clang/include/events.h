#ifndef EVENTS_H
#define EVENTS_H

typedef enum
{
    ENTRANCE = 0,
    EXIT,
    TIMEOUT_EVENT,

    
    LAST_EVENT
}Event_ID_t;

typedef union
{
    uint32_t uval;
    void * pv_val;
}Event_Data_t;

typedef struct Event_tag
{
    Event_ID_t Event_ID;
    Event_Data_t Event_Data;
}Event_t;

#endif