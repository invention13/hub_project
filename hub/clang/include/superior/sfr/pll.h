/**
 * @copyright 2019 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file pll_sfr.h
 */

/**
 * @brief A structure to represent Special Function Registers for PLL.
 */

#ifndef __PLL_H__
#define __PLL_H__

#include "../clough/clough_core.h"
#include <stdint.h>

// ********************************************************************************************************************
// compiler specific
// ********************************************************************************************************************

// IAR
#ifdef __ICCARM__
  #include "io_macros.h"
#endif

// ********************************************************************************************************************
// register typedefs
// ********************************************************************************************************************

typedef union {
  struct {
      uint8_t  PLLTIPFD                 :  4; /*!< PFD Test Inputs */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  PLLTILPF                 :  2; /*!< LPF Test Inputs */
  };
  uint8_t BYTE;
} PLL_TEST_0_t;

typedef union {
  struct {
      uint8_t PLLTIVCO_8_0             :  8; /*!< VCO Test Inputs[7:0] !!misaligned!! */
  };
  uint8_t BYTE;
} PLL_TEST_1_t;

typedef union {
  struct {
      uint8_t PLLTIVCO_9                :  1; /*!< VCO Test Inputs[8] !!misaligned!! */
      uint8_t                           :  7; /*   (reserved) */
  };
  uint8_t BYTE;
} PLL_TEST_2_t;

typedef union {
  struct {
      uint8_t  PLLTO                    :  2; /*!< PLL Test Output */
      uint8_t  GMT                      :  2;
      uint8_t  OST                      :  4;
  };
  uint8_t BYTE;
} PLL_TEST_3_t;

typedef union {
  struct {
      uint8_t  DATA                     :  4;
      uint8_t  CMD                      :  4;
  };
  uint8_t BYTE;
} PLL_CMD_LFT_0_t;

typedef union {
  struct {
      uint8_t  ZT                       :  4;
      uint8_t  IRT                      :  4;
  };
  uint8_t BYTE;
} PLL_CMD_LFT_1_t;

typedef union {
  struct {
      uint8_t  CPT                      :  3;
      uint8_t  AUTOCAL                  :  1;
      uint8_t  FT                       :  4;
  };
  uint8_t BYTE;
} PLL_CMD_LFT_2_t;

typedef union {
  struct {
      uint8_t  LK                       :  1;
      uint8_t  XTALEN                   :  1; /*!< Xtal En */
      uint8_t  LOCK                     :  1;
      uint8_t  CLKUSABLE                :  1;
      uint8_t  DT                       :  2;
      uint8_t  GODIV                    :  1;
      uint8_t  GOVCO                    :  1;
  };
  uint8_t BYTE;
} PLL_CMD_LFT_3_t;

typedef struct {
    // pll test
    PLL_TEST_0_t TEST0;               // offset: 0x00
    PLL_TEST_1_t TEST1;               // offset: 0x01
    PLL_TEST_2_t TEST2;               // offset: 0x02
    PLL_TEST_3_t TEST3;               // offset: 0x03

    // pll cmd and control register
    PLL_CMD_LFT_0_t CCR0;             // offset: 0x04
    PLL_CMD_LFT_1_t CCR1;             // offset: 0x05
    PLL_CMD_LFT_2_t CCR2;             // offset: 0x06
    PLL_CMD_LFT_3_t CCR3;             // offset: 0x07
} PLL_SFRS_t;

// ********************************************************************************************************************
// SFR memory mapping
// ********************************************************************************************************************

#define _PLL_SFRS   ((volatile PLL_SFRS_t *) (ASIC_7B_BASE + 0x50))


#ifdef __SFR_MAP_ASOBJ__
  // optional: provide as struct
  #ifdef __ICCARM__
    // IAR style
    volatile __no_init PLL_SFRS_t PLL_SFRS   @ (ASIC_7B_BASE + 0x50);
  #else
    // generic style
    #define PLL_SFRS  (* _PLL_SFRS)
  #endif
#else
  // default : provide as struct pointer
  #define PLL_SFRS       _PLL_SFRS
#endif
#endif  // include guard
