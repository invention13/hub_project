/**********************************************************************************************************************
 * @copyright 2017 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file pmu_sfr.h
 *********************************************************************************************************************/

#ifndef __PMU_H__
#define __PMU_H__

#include "../clough/clough_core.h"
#include <stdint.h>
#include "./usbhub.h"


// ********************************************************************************************************************
// compiler specific
// ********************************************************************************************************************

// IAR
#ifdef __ICCARM__
  #include "io_macros.h"
#endif


// ********************************************************************************************************************
// register typedefs
// ********************************************************************************************************************

// clock control / status typedef
typedef union {
  struct {
    uint8_t CKSEL          :  1;  // currently selected clock (read only):
                                  //   0 => PLL
                                  //   1 => RC oscillator
    uint8_t PLLFORCE       :  1;  // force PLL to be used as clock (even if USB hub doesn't request it)
    uint8_t SUSPEND        :  1;  // indication of USB hub suspend status
    uint8_t PLLCLKUSABLE   :  1;  //
    uint8_t XTALENEXTCLOCK :  1;  //
    uint8_t PLLLOCK        :  1;  // Pll lock indication.  read only.
    uint8_t CLKDIV         :  2;  // Clock divider select (not recommended to use):
                                  //   00 => full rate clock
                                  //   01 => %2 clock
                                  //   10 => %4 clock
  };
  uint8_t BYTE;
} PMU_CLKCSR_t;

// reset control / status typedef
typedef union {
  struct {
    uint8_t PMURESETFLAG   :  1; // software reset flag.       read only.
    uint8_t AWDTFLAG       :  1; // watchdog reset flag.       read only.
    uint8_t MCUBORFLAG     :  1; // mcu brown out reset flag.  read only.
    uint8_t BORFLAG        :  1; // asic brown out reset flag. read only.
    uint8_t PORFLAG        :  1; // power on reset flag.       read only.
    uint8_t                :  1;
    uint8_t PMURESET       :  1; // write 1 to force a system reset (power cycle)
    uint8_t PMUSLEEP       :  1; // write 1 to enter deep sleep
  };
  uint8_t BYTE;
} PMU_RSTCSR_t;


// deep sleep wakeup flags
typedef union {
  struct {
    uint8_t                :  4;
    uint8_t VBUSTRP        :  1; // vbus detected
    uint8_t SLEEPTIMERTRP  :  1; // sleep time-out
    uint8_t                :  2;
  };
  uint8_t BYTE;
} PMU_WAKEFLG_t;


// ********************************************************************************************************************
// SFR map typedef
// ********************************************************************************************************************

typedef struct {
  PMU_CLKCSR_t  CLKCSR;            // clock control / status
  PMU_RSTCSR_t  RSTCSR;            // reset control / status
  PMU_WAKEFLG_t WAKEFLG;           // deep sleep wakeup flags
  uint8_t       _NOASSIGN_3;
} PMU_SFRS_t;


// ********************************************************************************************************************
// SFR memory mapping
// ********************************************************************************************************************

#define _PMU   ((volatile PMU_SFRS_t *) (ASIC_7B_BASE + 0x00))


#ifdef __SFR_MAP_ASOBJ__
  // optional: provide as struct
  #ifdef __ICCARM__
    // IAR style
    volatile __no_init PMU_SFRS_t PMU   @ (ASIC_7B_BASE + 0x00);
  #else
    // generic style
    #define PMU  (* _PMU)
  #endif
#else
  // default : provide as struct pointer
  #define PMU       _PMU
#endif


// ********************************************************************************************************************
// convenience functions
// ********************************************************************************************************************

// force the PLL to be enabled, thereby providing the 30MHz clock whether the hub requests it or not.
__STATIC_INLINE void PMU_setPllForceEnable(
  uint8_t ena
) {
  register PMU_CLKCSR_t     clksrc;
  register USBHUB_PORCTRL_t porctrl;

  // hub has control over tha async reset of the PLL.  we need to release it if set and we are supposed to enable.
  if( ena ) {
    porctrl = _USBHUB->PORCTRL;
    if( porctrl.PLLRST ) {
      porctrl.PLLRST   = 0;
      _USBHUB->PORCTRL = porctrl;
    }
  }

  // set the force
  clksrc.BYTE     = 0;
  clksrc.PLLFORCE = ena ? 1 : 0;
  _PMU->CLKCSR     = clksrc;
}


#endif  // include guard
