#ifndef __NVMTRIM_H__
#define __NVMTRIM_H__

#include "../clough/clough_core.h"

// ****************************************************************************
// Superior NVM Trim Area
// ****************************************************************************

#define NVM_TRIM_OFFSET 8
#define NVM_CAL_VERSION         (*(uint8_t *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x00))

typedef struct
{
uint8_t VALUE : 4,              /* Bandgap Trim Value */
        RFU : 4;
} BG_TRIM_TYPE;
#define NVM_BG_TRIM             (*(BG_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x01))

typedef struct
{
  uint8_t VALUE : 7,            /* V2I Trim Value */
          RFU : 1;
} V2I_TRIM_TYPE;
#define NVM_V2I_TRIM            (*(V2I_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x02))

typedef struct
{
  uint8_t VALUE : 6,            /* ICOSC Trim Value */
          RFU : 2;
} ICOSC_TRIM_TYPE;
#define NVM_ICOSC_TRIM          (*(ICOSC_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x03))

typedef struct
{
  uint8_t VALUE : 4,            /* USB 45? Termination Trim Value */
          RFU : 4;
} USB45TERM_TRIM_TYPE;
#define NVM_USB45_TRIM          (*(USB45TERM_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x04))

typedef struct
{
  uint8_t VALUE0  : 1,          /* LPCDWU Bias Resistor/Current */
          VALUE21 : 2,
          RFU : 5;
} LPCDWU_TRIM_TYPE;
#define NVM_LPCDWU_TRIM         (*(LPCDWU_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x05))

typedef struct
{
  uint16_t  VALUE : 12,         /* VPTAT Trim Value */
            RFU : 4;
} VPTAT_TRIM_TYPE;
#define NVM_VPTAT_TRIM          (*(VPTAT_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x06))

typedef struct
{
  uint8_t NA : 4,
          VALUE : 3,            /* Charge Measurement Bias Resistor/Current */
          RFU : 1;
} CMBIAS_TRIM_TYPE;
#define NVM_CMBIAS_TRIM         (*(CMBIAS_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x08))


typedef struct
{
  uint8_t CH01 : 4,             /* Charge Measurement Integration Capacitor/Voltage Gain */
          CH23 : 4;
} CMGAIN_TRIM_TYPE;
#define NVM_CMGAIN_TRIM         (*(CMGAIN_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x09))

typedef struct
{
  uint16_t VALUE : 16;          /* Charge Measurement Gain Error Correction Factor */
} CMERROR_TRIM_TYPE;
#define NVM_CMERROR_TRIM        (*(CMERROR_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x0A))

typedef struct
{
  uint8_t VALUE : 8;            /* Charge Measurement Offset Error Correction Factor */
} CMOFFSET_TRIM_TYPE;
#define NVM_CMOFFSET_TRIM       (*(CMOFFSET_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x0C))


typedef struct
{
  uint8_t VALUE : 3,            /* PLL bias current calibration register. */
          RFU : 5;
} FCETOFFSET_TRIM_TYPE;
#define NVM_FCETOFFSET_TRIM     (*(FCETOFFSET_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x0D))


typedef struct
{
  uint8_t VALUE : 4,            /* PLL bias current calibration register. */
          RFU : 4;
} PLLBIAS_TRIM_TYPE;
#define NVM_PLLBIAS_TRIM        (*(PLLBIAS_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x0E))


typedef struct
{
  uint8_t VALUE : 2,            /* CC Port PullDown Resistance Trim. */
          RFU : 6;
} CCRESISTANCE_TRIM_TYPE;
#define NVM_CCRESISTANCE_TRIM   (*(CCRESISTANCE_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0xF))


typedef struct
{
  uint8_t VALUE : 4,            /* CC Port 330uA trim. */
          RFU : 4;
} CCCURRENT_TRIM_TYPE;
#define NVM_CCCURRENT_TRIM        (*(CCCURRENT_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x10))


typedef struct
{
  uint8_t VALUE : 4,            /* 1.5V Dig Supply Setting */
          RFU : 4;
} V_DIG_SUPPLY_TRIM_TYPE;
#define V_DIG_SUPPLY_TRIM        (*(V_DIG_SUPPLY_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x11))

typedef struct
{
  uint8_t VALUE : 3,            /* Squelch Detector UFP */
          RFU : 5;
} SQUELCH_UFP_TRIM_TYPE;
#define SQUELCH_UFP_TRIM        (*(SQUELCH_UFP_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x12))

typedef struct
{
  uint8_t VALUE : 3,            /* Squelch Detector DFP1 */
          RFU : 5;
} SQUELCH_DFP1_TRIM_TYPE;
#define SQUELCH_DFP1_TRIM       (*(SQUELCH_DFP1_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x13))

typedef struct
{
  uint8_t VALUE : 3,            /* Squelch Detector DFP2 */
          RFU : 5;
} SQUELCH_DFP2_TRIM_TYPE;
#define SQUELCH_DFP2_TRIM       (*(SQUELCH_DFP2_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x14))

typedef struct
{
  uint8_t VALUE : 3,            /* Squelch Detector DFP3 */
          RFU : 5;
} SQUELCH_DFP3_TRIM_TYPE;
#define SQUELCH_DFP3_TRIM        (*(SQUELCH_DFP3_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x15))

typedef struct
{
  uint8_t VALUE : 3,            /* Disconnect Detector Setting UFP */
          RFU : 5;
} DISCONNECT_DET_UFP_TRIM_TYPE;
#define DISCONNECT_DET_UFP_TRIM   (*(DISCONNECT_DET_UFP_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x16))

typedef struct
{
  uint8_t VALUE : 3,            /* Disconnect Detector Setting DFP1 */
          RFU : 5;
} DISCONNECT_DET_DFP1_TRIM_TYPE;
#define DISCONNECT_DET_DFP1_TRIM   (*(DISCONNECT_DET_DFP1_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x17))

typedef struct
{
  uint8_t VALUE : 3,            /* Disconnect Detector Setting DFP2 */
          RFU : 5;
} DISCONNECT_DET_DFP2_TRIM_TYPE;
#define DISCONNECT_DET_DFP2_TRIM   (*(DISCONNECT_DET_DFP2_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x18))

typedef struct
{
  uint8_t VALUE : 3,            /* Disconnect Detector Setting DFP3 */
          RFU : 5;
} DISCONNECT_DET_DFP3_TRIM_TYPE;
#define DISCONNECT_DET_DFP3_TRIM   (*(DISCONNECT_DET_DFP3_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x19))

typedef struct
{
  uint8_t VALUE : 3,            /* CDP Voltage Setting DFP1 */
          RFU : 5;
} CDP_V_DFP1_TRIM_TYPE;
#define CDP_V_DFP1_TRIM       (*(CDP_V_DFP1_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x1a))

typedef struct
{
  uint8_t VALUE : 3,            /* CDP Voltage Setting DFP2 */
          RFU : 5;
} CDP_V_DFP2_TRIM_TYPE;
#define CDP_V_DFP2_TRIM        (*(CDP_V_DFP2_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x1b))

typedef struct
{
  uint8_t VALUE : 3,            /* CDP Voltage Setting DFP3 */
          RFU : 5;
} CDP_V_DFP3_TRIM_TYPE;
#define CDP_V_DFP3_TRIM        (*(CDP_V_DFP3_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x1c))

typedef struct
{
  uint8_t VALUE : 2,            /* CDR Sync Trim */
          RFU : 6;
} CDR_SYNC_TRIM_1_TYPE;
#define CDR_SYNC_TRIM_1        (*(CDR_SYNC_TRIM_1_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x1d))

typedef struct
{
  uint8_t VALUE : 2,            /* CDR Sync Trim */
          RFU : 6;
} CDR_SYNC_TRIM_2_TYPE;
#define CDR_SYNC_TRIM_2        (*(CDR_SYNC_TRIM_2_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x1e))

typedef struct
{
  uint8_t VALUE : 2,            /* CDR Sync Trim */
          RFU : 6;
} CDR_SYNC_TRIM_3_TYPE;
#define CDR_SYNC_TRIM_3       (*(CDR_SYNC_TRIM_3_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x1f))

typedef struct
{
  uint8_t VALUE : 2,            /* CDR Sync Trim */
          RFU : 6;
} CDR_SYNC_TRIM_4_TYPE;
#define CDR_SYNC_TRIM_4        (*(CDR_SYNC_TRIM_4_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x20))

typedef struct
{
  uint8_t VALUE : 4,            /* FSTS Falling Edge Trim UFP */
          RFU : 4;
} FSTS_FE_UFP_TRIM_TYPE;
#define FSTS_FE_UFP_TRIM        (*(FSTS_FE_UFP_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x21))

typedef struct
{
  uint8_t VALUE : 4,            /* FSTS Falling Edge Trim DFP1 */
          RFU : 4;
} FSTS_FE_DFP1_TRIM_TYPE;
#define FSTS_FE_DFP1_TRIM        (*(FSTS_FE_DFP1_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x22))

typedef struct
{
  uint8_t VALUE : 4,            /* FSTS Falling Edge Trim DFP2 */
          RFU : 4;
} FSTS_FE_DFP2_TRIM_TYPE;
#define FSTS_FE_DFP2_TRIM        (*(FSTS_FE_DFP2_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x23))

typedef struct
{
  uint8_t VALUE : 4,            /* FSTS Falling Edge Trim DFP3 */
          RFU : 4;
} FSTS_FE_DFP3_TRIM_TYPE;
#define FSTS_FE_DFP3_TRIM  (*(FSTS_FE_DFP3_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x24))

typedef struct
{
  uint8_t VALUE : 1,            /* Fast Acquistition Enable UFP */
          RFU : 7;
} FAST_ACQ_UFP_TRIM_TYPE;
#define FAST_ACQ_UFP_TRIM        (*(FAST_ACQ_UFP_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x25))

typedef struct
{
  uint8_t VALUE : 1,            /* Fast Acquisition Enable DFP1 */
          RFU : 7;
} FAST_ACQ_DFP1_TRIM_TYPE;
#define FAST_ACQ_DFP1_TRIM        (*(FAST_ACQ_DFP1_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x26))

typedef struct
{
  uint8_t VALUE : 1,            /* Fast Acquisition Enable DFP2 */
          RFU : 7;
} FAST_ACQ_DFP2_TRIM_TYPE;
#define FAST_ACQ_DFP2_TRIM   (*(FAST_ACQ_DFP2_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x27))

typedef struct
{
  uint8_t VALUE : 1,            /* Fast Acquisition Enable DFP3 */
          RFU : 7;
} FAST_ACQ_DFP3_TRIM_TYPE;
#define FAST_ACQ_DFP3_TRIM        (*(FAST_ACQ_DFP3_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x28))

typedef struct
{
  uint8_t VALUE : 2,            /* Extended Repeated Packet Sync Field UFP */
          RFU : 6;
} EXT_RP_SYNC_UFP_TRIM_TYPE;
#define EXT_RP_SYNC_UFP_TRIM  (*(EXT_RP_SYNC_UFP_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x29))

typedef struct
{
  uint8_t VALUE : 2,            /* Extended Repeated Packet Sync Field DFP1 */
          RFU : 6;
} EXT_RP_SYNC_DFP1_TRIM_TYPE;
#define EXT_RP_SYNC_DFP1_TRIM   (*(EXT_RP_SYNC_DFP1_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x2a))

typedef struct
{
  uint8_t VALUE : 2,            /* Extended Repeated Packet Sync Field DFP2 */
          RFU : 6;
} EXT_RP_SYNC_DFP2_TRIM_TYPE;
#define EXT_RP_SYNC_DFP2_TRIM   (*(EXT_RP_SYNC_DFP2_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x2b))

typedef struct
{
  uint8_t VALUE : 2,            /* Extended Repeated Packet Sync Field DFP2 */
          RFU : 6;
} EXT_RP_SYNC_DFP3_TRIM_TYPE;
#define EXT_RP_SYNC_DFP3_TRIM  (*(EXT_RP_SYNC_DFP3_TRIM_TYPE *) (FLASH_ADK_SECTOR + NVM_TRIM_OFFSET + 0x2c))





#endif // include guard
