/**********************************************************************************************************************
 * @copyright 2017 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file usbafe.h
 *********************************************************************************************************************/

#ifndef __USBAFE_H__
#define __USBAFE_H__

#include "../clough/clough_core.h"
#include "../clough/clough_dbg.h"
#include <stdint.h>
#include "usbhub.h"


// ********************************************************************************************************************
// compiler specific
// ********************************************************************************************************************

// IAR
#ifdef __ICCARM__
  #include "io_macros.h"
#endif


// ********************************************************************************************************************
// USB AFE typedefs
// ********************************************************************************************************************

// enable byte
typedef union {
  struct {
    uint8_t BIASENA       :  1;   // bias enable
    uint8_t AFEENA        :  1;   // afe function enable
    uint8_t USBPOL        :  1;   // usb  pin polarity.  set to 1 to swap DP/DM
    uint8_t FCETPOL       :  1;   // fcet pin polarity.  set to 1 to swap DP/DM
    uint8_t OVDENA        :  1;   // over-voltage function enable
    uint8_t OVDAUTO       :  1;   // auto-protect enable for over-voltage
    uint8_t SE0AFEDGLTCH  :  1;   //
    uint8_t FSLSSE0FLTBYP :  1;   //
  };
  uint8_t BYTE;
} USBAFE_ENA_t;


// dft override bank selects
typedef union {
  struct {
    uint8_t TERMCTRL    : 1; // if set then resistor drives are in DFT override
    uint8_t ENACTRL     : 1; // if set then analog component enables are in DFT override
    uint8_t DRVCTRL     : 1; // if set then transmit controls are in DFT override
    uint8_t SUSPENDCTRL : 1; //
    uint8_t DIGLOOPCTRL : 1; //
    uint8_t             : 3;
  };
  uint8_t BYTE;              // offset: 0x08
} USBAFE_DFTSEL_t;

// resistor pulls override
typedef union {
  struct {
    uint8_t DMRPD       :  1; // DM pull down
    uint8_t DPRPD       :  1; // DP pull down
    uint8_t DMRPU       :  2; // DM pull up
    uint8_t DPRPU       :  2; // DP pull up
    uint8_t TERMON      :  1; // HS term enable
    uint8_t PUPSTRDSBL  :  1; // pull up strength select disable
  };
  uint8_t BYTE;
} USBAFE_DFTTERM_t;

// fcet control
typedef union {
  struct {
    uint8_t FCETDCPSHRT      : 1;  // if set, then DP/DM shorted together (DCP)
    uint8_t FCETCDPTXENA     : 1;  // if set, then CDP driver enabled
    uint8_t FCETCDPRXENA     : 1;  // if set, then CDP receiver (comparator) enabled
    uint8_t FCETCDPRXD       : 1;  // CDP receiver output
    uint8_t FCETAPPLRSELDP   : 2;  // DP resistor divider select
    uint8_t FCETAPPLRSELDM   : 2;  // DM resistor divider select
  };
  uint8_t BYTE;
} USBAFE_FCET_t;

// cdr trim
typedef union {
  struct {
    uint8_t CDRIDLMONENA  :  1;   // idle monitor enable (lockup detection)
    uint8_t               :  2;
    uint8_t CDRFLSHTRIM   :  1;   // CDR flushing trim.  controls extra clocks to core logic after packet.
    uint8_t CDRSYNCTRIM   :  2;   // SYNC swallowing trim
    uint8_t CDRDRBLTRIM   :  1;   // EOP dribble trim
    uint8_t CDRTXMSKBYP   :  1;   // if set then receiver disabled during packet transmit
  };
  uint8_t BYTE;                   // offset: 0x06
} USBAFE_CDRTRIM_t;


// ********************************************************************************************************************
// USB AFE SFR map
// ********************************************************************************************************************

typedef struct {
  // enable controls
  USBAFE_ENA_t ENA;              // offset:  0x00

  // R/C calibration trim
  union {
    struct {
       uint8_t RTERMTRIM : 4;    // termination resistor trim
       uint8_t CAPTRIM   : 3;    // capacitor trim
       uint8_t           : 1;
    };
    uint8_t BYTE;
  } RCTRIM;                      // offset:  0x01

  // HS trims
  union {
    struct {
      uint16_t HSSQLCHTRIM : 3;  // squelch threshold
      uint16_t HSDSCNTRIM  : 3;  // disconnect threshold
      uint16_t             : 2;
      uint16_t HSTXCSTRIM  : 4;  // transmit current trim
      uint16_t HSTXRTTRIM  : 4;  //
    };
    uint16_t HWORD;
  } HSTRIM;                      // offset:  0x02 ... 0x03

  // FS/LS trims
  union {
    struct {
      uint16_t FSLSVILTRIM      : 2; // VIL (receive fall threshold) trim
      uint16_t FSLSVIHTRIM      : 2; // VIH (receive rise threshold) trim
      uint16_t FSLSRPTLAT       : 1; // enable repeater TXENA latching
      uint16_t                  : 1;
      uint16_t FSLSRPTDLY       : 2; // repeater delay trim
      uint16_t FSLSSE0FLTPWTRIM : 4; // repeater SE0 timing trim:  pulse width
      uint16_t FSLSSE0FLTFETRIM : 4; // repeater SE0 timing trim:  trailing edge delay
    };
    uint16_t HWORD;                  // offset: 0x04 ... 0x05
  } FSLSTRIM;

  // CDR trims
  USBAFE_CDRTRIM_t CDRTRIM;         // offset: 0x06

  // FCET controls
  USBAFE_FCET_t FCET;               // offset: 0x07

  // DFT bank override enables
  USBAFE_DFTSEL_t DFTSEL;           // offset: 0x08

  uint8_t     _RESERVED_9;     // offset: 0x09
  uint8_t     _RESERTED_A;     // offset: 0x0A

  // DFT read values
  union {
    struct {
      uint8_t FSLSRXD    :  1; // FS/LS receiver output
      uint8_t FSLSRXSE0  :  1; // FS/LS SE0 detector output
      uint8_t FSLSRXDM   :  1; // FS/LS single-ended DP receiver output
      uint8_t FSLSRXDP   :  1; // FS/LS single-ended DM receiver output
      uint8_t HSRXD      :  1; // HS receiver output
      uint8_t HSSQLCH    :  1; // HS squelch detector output
      uint8_t HSDSCN     :  1; // HS disconnect detector output
      uint8_t            :  1; //
    };
    uint8_t BYTE;              // offset: 0x0B
  } DFTREAD;

  // DFT resistor drives bank
  USBAFE_DFTTERM_t DFTTERM;     // offset:  0x0C

  // DFT enable bank
  union {
    struct {
      uint8_t DSCNENA   : 1; // disconnect detector
      uint8_t SQLCHENA  : 1; // squelch detector
      uint8_t SERXENA   : 1; // single ended receivers
      uint8_t FSLSRXENA : 1; // FS/LS differential receiver
      uint8_t HSRXENA   : 1; // HS differential receiver
      uint8_t HSCDRENA  : 1; // CDR
      uint8_t SUSPEND   : 1; //
      uint8_t DIGLOOP   : 1; //
    };
    uint8_t BYTE;               // offset: 0x0D
  } DFTENA;

  // DFT transmitter bank
  union {
    struct {
      uint8_t FSLSTXOE   : 1; // FS/LS output enable
      uint8_t FSLSTXD    : 1; // FS/LS transmit data
      uint8_t FSLSLSMODE : 1; // LS mode select
      uint8_t FSLSTXSE0  : 1; // FS/LS SE0 drive
      uint8_t HSTXENA    : 1; // HS output enable
      uint8_t HSTXSE0N   : 1; // HS SE0 drive
      uint8_t HSTXD      : 1; // FS transmit data
      uint8_t            : 1;
    };
    uint8_t BYTE;                // offset: 0x0E
  } DFTTX;

  uint8_t     _RESERTED_F;       // offset: 0x0F
} USBAFE_SFRS_t;


// AFE CDR phase-related trims
typedef union {
  struct {
    uint8_t FRCPH   : 3;    // phase to force, if PHFORCE is set
    uint8_t         : 1;
    uint8_t PHFORCE : 1;    // if set, then force a particular phase.  for test modes.
    uint8_t PHROT   : 2;    // phase rotation trims.  used for CDR eye centering.
    uint8_t         : 1;
  };

  uint8_t BYTE;
} USBCDRTRIM_PHASE_t;

// AFE CDR delay-related trims
typedef union {
  struct {
    uint8_t ENA     : 1;   // enable delay trimming
    uint8_t         : 3;
    uint8_t RT      : 2;   // rise edge delay
    uint8_t FT      : 2;   // fall edfe delay
  };

  uint8_t BYTE;
} USBCDRTRIM_DLY_t;

// AFE CDR framing-related trims
typedef union {
  struct {
    uint8_t FASTACQ : 1;   // enable fast acquisition for SYNC field
    uint8_t SQLMSK  : 1;   // enable squelch packet masking in latch.
    uint8_t         : 6;
  };

  uint8_t BYTE;
} USBCDRTRIM_FRMR_t;


typedef struct {
  uint8_t            CURPH;         // offset 0x00
  USBCDRTRIM_PHASE_t PHASE;         // offset 0x01
  uint16_t           _RESERVED_3;   // offset 0x02 ... 0x03
  USBCDRTRIM_DLY_t   CLKDLY;        // offset 0x04
  USBCDRTRIM_DLY_t   RXDDLY;        // offset 0x05
  uint8_t            CLKSEL;        // offset 0x06
  USBCDRTRIM_FRMR_t  FRMR;          // offset 0x07
} USBCDRTRIM_PORT_t;


// extra CDR trims extension
typedef struct {
  USBCDRTRIM_PORT_t  UP;            // offset 0x01 ... 0x07
  USBCDRTRIM_PORT_t  DN1;           // offset 0x08 ... 0x0F
  USBCDRTRIM_PORT_t  DN2;           // offset 0x10 ... 0x17
  USBCDRTRIM_PORT_t  DN3;           // offset 0x18 ... 0x1F
} USBCDRTRIM_SFRS_t;



#define _USBAFE_UP  ((volatile USBAFE_SFRS_t*    ) (ASIC_16B_BASE + 0x6000))
#define _USBAFE_DN1 ((volatile USBAFE_SFRS_t*    ) (ASIC_16B_BASE + 0x6010))
#define _USBAFE_DN2 ((volatile USBAFE_SFRS_t*    ) (ASIC_16B_BASE + 0x6020))
#define _USBAFE_DN3 ((volatile USBAFE_SFRS_t*    ) (ASIC_16B_BASE + 0x6030))
#define _USBCDRTRIM ((volatile USBCDRTRIM_SFRS_t*) (ASIC_16B_BASE + 0x6500))


#ifdef __SFR_MAP_ASOBJ__
  // optional: provide as struct
  #ifdef __ICCARM__
    // IAR style
    volatile __no_init USBAFE_SFRS_t     USBAFE_UP  @ (ASIC_16B_BASE + 0x6000);
    volatile __no_init USBAFE_SFRS_t     USBAFE_DN1 @ (ASIC_16B_BASE + 0x6010);
    volatile __no_init USBAFE_SFRS_t     USBAFE_DN2 @ (ASIC_16B_BASE + 0x6020);
    volatile __no_init USBAFE_SFRS_t     USBAFE_DN3 @ (ASIC_16B_BASE + 0x6030);
    volatile __no_init USBCDRTRIM_SFRS_t USBCDRTRIM @ (ASIC_16B_BASE + 0x6500);
  #else
    // generic style
    #define USBAFE_UP  (* _USBAFE_UP)
    #define USBAFE_DN1 (* _USBAFE_DN1)
    #define USBAFE_DN2 (* _USBAFE_DN2)
    #define USBAFE_DN3 (* _USBAFE_DN3)
    #define USBCDRTRIM (* _USBCDRTRIM)
  #endif
#else
  // default : provide as struct pointer
  #define USBAFE_UP  _USBAFE_UP
  #define USBAFE_DN1 _USBAFE_DN1
  #define USBAFE_DN2 _USBAFE_DN2
  #define USBAFE_DN3 _USBAFE_DN3
  #define USBCDRTRIM _USBCDRTRIM
#endif


// ********************************************************************************************************************
// convenience functions
// ********************************************************************************************************************

// enable an AFE
__STATIC_INLINE void USBAFE_enableAFE(
  uint8_t portnum  ,  // port number to enable, 0 for US
  uint8_t swapPins    // whether to swap DP/DM
) {
  volatile USBAFE_SFRS_t* afe;
  USBAFE_ENA_t            tempe;

  // multiplex which AFE
  switch( portnum ) {
    case 0  : afe = _USBAFE_UP;  break;
    case 1  : afe = _USBAFE_DN1; break;
    case 2  : afe = _USBAFE_DN2; break;
    case 3  : afe = _USBAFE_DN3; break;
    default : dbgTrapErr();      return;
  }

  // populate the byte
  tempe.BYTE = 0;
  tempe.BIASENA       = 1;
  tempe.AFEENA        = 1;
  tempe.USBPOL        = swapPins ? 1 : 0;
  tempe.FCETPOL       = swapPins ? 1 : 0;
  tempe.OVDENA        = 1;
  tempe.OVDAUTO       = 1;
  tempe.SE0AFEDGLTCH  = 1;
  tempe.FSLSSE0FLTBYP = 0;

  // write
  afe->ENA.BYTE = tempe.BYTE;
}


// disable an AFE
__STATIC_INLINE void USBAFE_disableAFE(
  uint8_t portnum   // port number to enable, 0 for US
) {
  volatile USBAFE_SFRS_t* afe;
  USBAFE_ENA_t            tempe;

  // multiplex which AFE
  switch( portnum ) {
    case 0  : afe = _USBAFE_UP;  break;
    case 1  : afe = _USBAFE_DN1; break;
    case 2  : afe = _USBAFE_DN2; break;
    case 3  : afe = _USBAFE_DN3; break;
    default : dbgTrapErr();      return;
  }

  // populate the byte
  tempe.BYTE = 0;
  tempe.BIASENA       = 0;
  tempe.AFEENA        = 0;
  tempe.USBPOL        = 0;
  tempe.FCETPOL       = 0;
  tempe.OVDENA        = 0;
  tempe.OVDAUTO       = 0;
  tempe.SE0AFEDGLTCH  = 1;
  tempe.FSLSSE0FLTBYP = 0;

  // write
  afe->ENA.BYTE = tempe.BYTE;
}


// setup CDR trims (and corresponding hub settings) to recommended values
__STATIC_INLINE void USBAFE_setCDRTrims(
  uint8_t portnum   // port number to enable, 0 for US
) {
  volatile USBAFE_SFRS_t*     afe;
  volatile USBCDRTRIM_PORT_t* cdrport;
  volatile USBHUB_PHYTRIM_t*  phy;
  USBAFE_CDRTRIM_t            cdrtrim;
  USBCDRTRIM_FRMR_t           frmtrim;
  USBHUB_PHYTRIM_t            phytrim;

  // multiplex which AFE
  switch( portnum ) {
    case 0  : afe = _USBAFE_UP;  cdrport = &(_USBCDRTRIM->UP ); phy = &(_USBHUB->PHYTRIM.UP ); break;
    case 1  : afe = _USBAFE_DN1; cdrport = &(_USBCDRTRIM->DN1); phy = &(_USBHUB->PHYTRIM.DN1); break;
    case 2  : afe = _USBAFE_DN2; cdrport = &(_USBCDRTRIM->DN2); phy = &(_USBHUB->PHYTRIM.DN2); break;
    case 3  : afe = _USBAFE_DN3; cdrport = &(_USBCDRTRIM->DN3); phy = &(_USBHUB->PHYTRIM.DN3); break;
    default : dbgTrapErr();      return;
  }

  // set afe trims
  cdrtrim.BYTE         = 0;
  cdrtrim.CDRIDLMONENA = 1;  // idle monitor enabled
  cdrtrim.CDRFLSHTRIM  = 0;  // normal flushing
  cdrtrim.CDRSYNCTRIM  = 1;  // registered fast acq
  cdrtrim.CDRDRBLTRIM  = 0;  // shorter dribble
  cdrtrim.CDRTXMSKBYP  = 0;  // mask rx when transmitting
  afe->CDRTRIM = cdrtrim;
  
  //set HS trims
  //Increasing transmitter rise time (from default 9) - During testing 
  //provided slightly improvement in eye opening
  afe->HSTRIM.HSTXRTTRIM = 0x0F;   
  //Setting HS RX Threshold disconnect to optimal value according to simulation
  afe->HSTRIM.HSDSCNTRIM = 0x03; 
  //Setting HS RX Threshold Squelch to optimal value according to simulation
  afe->HSTRIM.HSSQLCHTRIM = 0x01;

  // set extended AFE trims
  frmtrim.BYTE    = 0;
  frmtrim.FASTACQ = 1;
  frmtrim.SQLMSK  = 0;
  cdrport->FRMR = frmtrim;

  // set PHY trims
  phytrim.BYTE = 0;
  phytrim.SORPFLT = 1;  // filter packets without proper SORP
  phytrim.WRAPDET = 1;  // implement over-length FIFO wrapping detector
  phytrim.SYNCEXT = 1;  // create 2 bits of fake sync field
  phytrim.EOI     = 1;  // normal arming depth
  *phy = phytrim;
}

#endif // include guard
