/**
 * @copyright 2019 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file utility_func.h
 */

/**
 * @brief A structure to add convenience functions
 */

#ifndef __UTILITY_FUNC_H__
#define __UTILITY_FUNC_H__

#include <stdint.h>
#include "superior.h"
#include "clough_dbg.h"

// PLL calibration sequence
// **USAGE WARNING**: This function relies on TIMER0 to implement wait periods
// Please ensure that TIMER0 is not being used concurrently for another function while this 
// function is running
static  __INLINE void performPLLCalibration() {
  int32_t  tmout;    // use timer to check for failed actions and timeout the simulation
  uint32_t nvm_pllbias_trim;
  register USBHUB_PORCTRL_t porctrl;
  register PMU_CLKCSR_t     clksrc;

  // clear PLL force if set
  clksrc = _PMU->CLKCSR;
  if ( clksrc.PLLFORCE ) {
    clksrc.PLLFORCE = 0;
    _PMU->CLKCSR = clksrc;
  }

  // Apply the ANAV1P5 to power PLL
  // CRG Registers:  PLL:  PLL_ENV1P5REG=1    
  CRG_SFRS->PLL.PLL_ENV1P5REG = 0x1;

  // Wait 5ms @4MHz for power up
  // **** WARNING **** : Please ensure that TIMER0 is not being used for another function in your program
  TIMER0->CFG &= ~TIMER_CFG_ENA;                         // turn off the general purpose timer
  TIMER0->COUNT = 0;                                     // clear the counter desired counter value
  TIMER0->CFG |= TIMER_CFG_ENA;                          // turn on the general purpose timer. used for timeouts
  tmout = TIMER0->COUNT + (5 * 4000);                    // setup a 5ms timeout using the 4MHz clock
  while( (TIMER0->COUNT - tmout) < 0 ) {}                // wait for the timeout value

  // Deassert PLL_RESET
  // This action results in the PLL being enabled with
  // an autocalibration based on the reset value of OST (0x5)
  porctrl = _USBHUB->PORCTRL;
  if( porctrl.PLLRST ) {
    porctrl.PLLRST   = 0;
    _USBHUB->PORCTRL = porctrl;
  }

  // Wait for PLL to lock
  while ( PMU->CLKCSR.PLLCLKUSABLE != 0x1 ) {}

  // At this point we have a 30MHz clock

  // Disable auto-calibration upon suspend-exit
  PLL_SFRS->CCR0.BYTE = 0x30;    

  // Toggle Suspend to deassert PLL_EN and then assert PLL_EN
  USBHUB->SUSPENDFORCE = 0x1;  
  USBHUB->SUSPENDFORCE = 0x0;

  // At this point we have a 4MHz clock
  
  // Wait time for XTAL to settle
  // **** WARNING **** : Please ensure that TIMER0 is not being used for another function in your program
  TIMER0->CFG &= ~TIMER_CFG_ENA;                         // turn off the general purpose timer
  TIMER0->COUNT = 0;                                     // clear the counter desired counter value
  TIMER0->CFG |= TIMER_CFG_ENA;                          // turn on the general purpose timer. used for timeouts
  tmout = TIMER0->COUNT + (1 * 4000);                    // setup a 1ms timeout using the 4MHz clock
  while( (TIMER0->COUNT - tmout) < 0 ) {}                // wait for the timeout value

  // Grab the OST from NVR
  nvm_pllbias_trim = *(uint8_t*)0x00027F16;
  nvm_pllbias_trim &= 0x0F;
  nvm_pllbias_trim |= 0xB0;
  PLL_SFRS->CCR0.BYTE = nvm_pllbias_trim;  

  // Perform auto-calibration
  PLL_SFRS->CCR0.BYTE = 0xF0;    
 
  // At this point we have a 4MHz clock which transitions to a 30MHz clock
  // once calibration is complete

  // Wait period for the auto-calibration to complete
  // **** WARNING **** : Please ensure that TIMER0 is not being used for another function in your program
  TIMER0->CFG &= ~TIMER_CFG_ENA;                         // turn off the general purpose timer
  TIMER0->COUNT = 0;                                     // clear the counter desired counter value
  TIMER0->CFG |= TIMER_CFG_ENA;                          // turn on the general purpose timer. used for timeouts
  tmout = TIMER0->COUNT + (1 * 30000);                   // setup a 1ms timeout using the 4MHz/30Mhz clock
  while( (TIMER0->COUNT - tmout) < 0 ) {}                // wait for the timeout value

  // Wait for PLL to lock
  while ( PMU->CLKCSR.PLLCLKUSABLE != 0x1 ) {}

  // At this point we have a 30MHz clock
 
  // set the PLL force
  clksrc = _PMU->CLKCSR;
  clksrc.PLLFORCE = 1;
  _PMU->CLKCSR     = clksrc;
}

#endif // include guard
