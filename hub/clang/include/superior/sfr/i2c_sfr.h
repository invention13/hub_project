/**
 * @copyright 2017 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file i2c_sfr.h
 */

#ifndef __I2C_SFR_H__
#define __I2C_SFR_H__

#include <stdint.h>

/**
 * @brief A structure to represent Special Function Registers for I2C.
 */

#define ASIC_7B_BASE     ((uint32_t) 0x50000000)
#define I2C_BASE     (ASIC_7B_BASE + 0x8)
#define I2C_STATUS   ((volatile uint8_t *) (I2C_BASE + 0x00))
#define I2C_CTRL1    ((volatile uint8_t *) (I2C_BASE + 0x01))
#define I2C_CTRL2    ((volatile uint8_t *) (I2C_BASE + 0x02))
#define I2C_DATA     ((volatile uint8_t *) (I2C_BASE + 0x03))
#define I2C_ADDR1    ((volatile uint8_t *) (I2C_BASE + 0x04))
#define I2C_ADDR2    ((volatile uint8_t *) (I2C_BASE + 0x05))
#define I2C_MODE        0x1

#endif /* end of __I2C_SFR_H__ section */

