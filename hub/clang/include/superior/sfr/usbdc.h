/**********************************************************************************************************************
 * @copyright 2017 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file usbdc_sfr.h
 *********************************************************************************************************************/

#ifndef __USBDC_SFR_H__
#define __USBDC_SFR_H__

#include "../clough/clough_core.h"
#include "../clough/clough_dbg.h"
#include <stdint.h>


// ********************************************************************************************************************
// compiler specific
// ********************************************************************************************************************

// IAR
#ifdef __ICCARM__
  #include "io_macros.h"
#endif


// ********************************************************************************************************************
// USB device controller SFR typedefs
// ********************************************************************************************************************

// endpoint 0 control / status register typedef.
typedef union {
  struct {
    uint8_t STALL   :  1;   // if 1, then response to IN/OUT transaction to EP0 will be STALL.  use to
                            //   signal control transaction error to host.  auto-clears on SETUP token arrival.
    uint8_t HSNAK   :  1;   // if 1, then response to status portion of control transaction will be NAK.  use
                            //   to delay response until decision to STALL is made.  auto-sets on SETUP token arrival.
                            //   clear on WRITE.
    uint8_t INBSY   :  1;   // read only IN buffer status:
                            //   sets on packet committed for delivery by write to INBC.
                            //   clears on IN transfer complete.
                            //   software should only interact with the IN endpoint buffer when this bit is zero.
    uint8_t OUTBSY  :  1;   // read only OUT buffer status:
                            //   sets on arrival of SETUP token.
                            //   clears on OUT transfer conplete.
                            //   software should only interact with the OUT endpoint buffer when this bit is zero.
    uint8_t DSTALL  :  1;   // if 1, then response to data stage of control transfer is STALL.  auto-clears
                            //   on arrival of SETUP token.  recommended to set this bit after last data stage
                            //   transfer so that STALL is delivered to host if it transfers more data than
                            //   expected.
    uint8_t         :  2;   // <reserved>
    uint8_t CHGSET  :  1;   // setup data changed flag (autoset, clear on write1)
  };
  uint8_t BYTE;
} USBDC_EP0CS_t;            // size/align:  1/1 byte

// endpoint OUTx config register
typedef union {
  struct {
    uint8_t BUF     : 2;    // number of packet buffers:
                            //   00 => single
                            //   01 => double
                            //   10 => triple
                            //   11 => quad
    uint8_t TYPE    : 2;    // endpoint type:
                            //   01 => isochronous
                            //   10 => bulk transfer
                            //   11 => interrupt
    uint8_t         : 2;
    uint8_t STALL   : 1;    // force STALL response if set
    uint8_t VAL     : 1;    // endpoint valid.  if zero then endpoint responds to nothing.
  };
  uint8_t BYTE;
} USBDC_OUTCON_t;           // size/align:  1/1 byte

// endpoint OUTx control / status register
typedef union {
  struct {
    uint8_t ERR     : 1;    // read only: ISO data sequence error detected.  set if:
                            //   - data packet received was corrupted
                            //   - data packet received, but buffer was full
    uint8_t BUSY    : 1;    // OUT buffer "busy"
    uint8_t NPAK    : 2;    // read only: number of packets currently present in OUT buffer
    uint8_t AUTO    : 1;    // read only: when set, FIFO is allowed to commit packets to the OUT buffer
    uint8_t         : 3;
  };
  uint8_t BYTE;
} USBDC_OUTCS_t;            // size/align:  1/1 byte

// OUT endpoint control
typedef struct {
  uint16_t        BC;       // 0x00 => OUT byte count
  USBDC_OUTCON_t  CON;      // config register
  USBDC_OUTCS_t   CS;       // control / status register
}  USBDC_EPOUT_t;           // size/align:  4/2 byte

//  endpoint INx config register
typedef union {
  struct {
    uint8_t BUF     : 2;    // number of packet buffers:
                            //   00 => single
                            //   01 => double
                            //   10 => triple
                            //   11 => quad
    uint8_t TYPE    : 2;    // endpoint type:
                            //   01 => isochronous
                            //   10 => bulk transfer
                            //   11 => interrupt

    uint8_t ISOD    : 2;    // ISO packets per microframe (HS mode only):
                            //   00 => 1
                            //   01 => 2
                            //   10 => 3
    uint8_t STALL   : 1;    // force STALL response if set
    uint8_t VAL     : 1;    // endpoint valid.  if zero then endpoint responds to nothing.
  };
  uint8_t BYTE;
} USBDC_INCON_t;           // size/align:  1/1 byte

// endpoint INx control / status register
typedef union {
  struct {
    uint8_t ERR     : 1;    // read only: ISO data sequence error detected.  set if:
                            //   - data packet received was corrupted
                            //   - data packet received, but buffer was full
    uint8_t BUSY    : 1;    // IN buffer "busy"
    uint8_t NPAK    : 2;    // read only: number of packets currently present in IN buffer
    uint8_t AUTO    : 1;    // read only: when set, FIFO is allowed to retire packets from the IN buffer
    uint8_t         : 3;
  };
  uint8_t BYTE;
} USBDC_INCS_t;             // size/align:  1/1 byte

// OUT endpoint control
typedef struct {
  uint16_t        BC;       // 0x00 => OUT byte count
  USBDC_INCON_t   CON;      // config register
  USBDC_INCS_t    CS;       // control / status register
}  USBDC_EPIN_t;            // size/align:  4/2 byte

// host controller OUT error byte
typedef union {
  struct {
    uint8_t ERRCNT  : 2;     // error count
    uint8_t ERRTYPE : 3;     // error type:
                             //   000 => no error
                             //   011 => STALL
                             //   100 => timeout
                             //   101 => PID error
    uint8_t RESEND  : 1;     // write only (returns 0 on read).  write 1 to restart aborted transaction.
    uint8_t DOPING  : 1;     // set to send PING instead of OUT.  Bit updates to reflect PING result.
    uint8_t         : 1;
  };
  uint8_t BYTE;
} USBDC_HCOUTERR_t;          // size/align:  1/1 byte

// host controller IN error byte
typedef union {
  struct {
    uint8_t ERRCNT    : 2;   // error count
    uint8_t ERRTYPE   : 3;   // error type:
                             //   000 => no error
                             //   001 => CRC
                             //   010 => data toggle
                             //   011 => STALL
                             //   100 => timeout
                             //   101 => PID error
                             //   110 => data overrun
                             //   111 => data underrun (?)
    uint8_t RESEND    : 1;   // write only (returns 0 on read).  write 1 to restart aborted transaction.
    uint8_t           : 1;
    uint8_t UNDERRIEN : 1;   // set to generate interrupt when packet shorter than max packet.
  };
  uint8_t BYTE;
} USBDC_HCINERR_t;           // size/align:  1/1 byte

// host controller EP0 registers
typedef struct {
  uint8_t          ENDPNR;      // endpoint number to communicate control traffic to
  USBDC_HCOUTERR_t OUTERR;      // error byte
  uint8_t          _RESERVED01;
  USBDC_HCINERR_t  INERR;       // error byte
} USBDC_HCEP0_t;                // size/align:  4/1 byte

// host controller EPOUTx registers
typedef struct {
  uint8_t          ENDPNR;   // endpoint number to communicate with
  USBDC_HCOUTERR_t ERR;      // error byte
} USBDC_HCEPOUT_t;           // size/align:  2/1 byte

// host controller EPINx registers
typedef struct {
  uint8_t          ENDPNR;   // endpoint number to communicate with
  USBDC_HCINERR_t  ERR;      // error byte
} USBDC_HCEPIN_t;            // size/align:  2/1 byte

// usb irq type
typedef union {
  struct {
    uint8_t SUDAV  : 1;      // setup data valid
    uint8_t SOF    : 1;      // start of frame
    uint8_t SUTOK  : 1;      // setup token
    uint8_t SUSP   : 1;      // suspend
    uint8_t URES   : 1;      // line reset
    uint8_t HSPEED : 1;      // switch to HS mode
    uint8_t        : 1;
    uint8_t LPM    : 1;      // link power management
  };
  uint8_t BYTE;
} USBDC_USBIRQ_t;            // size/align:  1/1 byte

// IVECT enumeration
#define USBDC_IVECT_SUDAV      ((uint8_t) 0x00)
#define USBDC_IVECT_SOF        ((uint8_t) 0x04)
#define USBDC_IVECT_SUTOK      ((uint8_t) 0x08)
#define USBDC_IVECT_SUSPEND    ((uint8_t) 0x0C)
#define USBDC_IVECT_USBRESET   ((uint8_t) 0x10)
#define USBDC_IVECT_HSPEED     ((uint8_t) 0x14)
#define USBDC_IVECT_HCOUT0ERR  ((uint8_t) 0x16)
#define USBDC_IVECT_EP0IN      ((uint8_t) 0x18)
#define USBDC_IVECT_HCIN0ERR   ((uint8_t) 0x1A)
#define USBDC_IVECT_EP0OUT     ((uint8_t) 0x1C)
#define USBDC_IVECT_EP0PING    ((uint8_t) 0x20)
#define USBDC_IVECT_HCOUT1ERR  ((uint8_t) 0x22)
#define USBDC_IVECT_EP1IN      ((uint8_t) 0x24)
#define USBDC_IVECT_HCIN1ERR   ((uint8_t) 0x26)
#define USBDC_IVECT_EP1OUT     ((uint8_t) 0x28)
#define USBDC_IVECT_EP1PING    ((uint8_t) 0x2C)
#define USBDC_IVECT_HCOUT2ERR  ((uint8_t) 0x2E)
#define USBDC_IVECT_EP2IN      ((uint8_t) 0x30)
#define USBDC_IVECT_HCIN2ERR   ((uint8_t) 0x32)
#define USBDC_IVECT_EP2OUT     ((uint8_t) 0x34)
#define USBDC_IVECT_EP2PING    ((uint8_t) 0x38)
#define USBDC_IVECT_HCOUT3ERR  ((uint8_t) 0x3A)
#define USBDC_IVECT_EP3IN      ((uint8_t) 0x3C)
#define USBDC_IVECT_HCIN3ERR   ((uint8_t) 0x3E)
#define USBDC_IVECT_EP3OUT     ((uint8_t) 0x40)
#define USBDC_IVECT_EP3PING    ((uint8_t) 0x44)
#define USBDC_IVECT_HCOUT4ERR  ((uint8_t) 0x46)
#define USBDC_IVECT_EP4IN      ((uint8_t) 0x48)
#define USBDC_IVECT_HCIN4ERR   ((uint8_t) 0x4A)
#define USBDC_IVECT_EP4OUT     ((uint8_t) 0x4C)
#define USBDC_IVECT_EP4PING    ((uint8_t) 0x50)
#define USBDC_IVECT_HCOUT5ERR  ((uint8_t) 0x52)
#define USBDC_IVECT_EP5IN      ((uint8_t) 0x54)
#define USBDC_IVECT_HCIN5ERR   ((uint8_t) 0x56)
#define USBDC_IVECT_EP5OUT     ((uint8_t) 0x58)
#define USBDC_IVECT_EP5PING    ((uint8_t) 0x5C)
#define USBDC_IVECT_HCOUT6ERR  ((uint8_t) 0x5E)
#define USBDC_IVECT_EP6IN      ((uint8_t) 0x60)
#define USBDC_IVECT_HCIN6ERR   ((uint8_t) 0x62)
#define USBDC_IVECT_EP6OUT     ((uint8_t) 0x64)
#define USBDC_IVECT_EP6PING    ((uint8_t) 0x68)
#define USBDC_IVECT_HCOUT7ERR  ((uint8_t) 0x6A)
#define USBDC_IVECT_EP7IN      ((uint8_t) 0x6C)
#define USBDC_IVECT_HCIN7ERR   ((uint8_t) 0x6E)
#define USBDC_IVECT_EP7OUT     ((uint8_t) 0x70)
#define USBDC_IVECT_EP7PING    ((uint8_t) 0x74)
#define USBDC_IVECT_HCOUT8ERR  ((uint8_t) 0x76)
#define USBDC_IVECT_EP8IN      ((uint8_t) 0x78)
#define USBDC_IVECT_HCIN8ERR   ((uint8_t) 0x7A)
#define USBDC_IVECT_EP8OUT     ((uint8_t) 0x7C)
#define USBDC_IVECT_EP8PING    ((uint8_t) 0x80)
#define USBDC_IVECT_HCOUT9ERR  ((uint8_t) 0x82)
#define USBDC_IVECT_EP9IN      ((uint8_t) 0x84)
#define USBDC_IVECT_HCIN9ERR   ((uint8_t) 0x86)
#define USBDC_IVECT_EP9OUT     ((uint8_t) 0x88)
#define USBDC_IVECT_EP9PING    ((uint8_t) 0x8C)
#define USBDC_IVECT_HCOUT10ERR ((uint8_t) 0x8E)
#define USBDC_IVECT_EP10IN     ((uint8_t) 0x90)
#define USBDC_IVECT_HCIN10ERR  ((uint8_t) 0x92)
#define USBDC_IVECT_EP10OUT    ((uint8_t) 0x94)
#define USBDC_IVECT_EP10PING   ((uint8_t) 0x98)
#define USBDC_IVECT_HCOUT11ERR ((uint8_t) 0x9A)
#define USBDC_IVECT_EP11IN     ((uint8_t) 0x9C)
#define USBDC_IVECT_HCIN11ERR  ((uint8_t) 0x9E)
#define USBDC_IVECT_EP11OUT    ((uint8_t) 0xA0)
#define USBDC_IVECT_EP11PING   ((uint8_t) 0xA4)
#define USBDC_IVECT_HCOUT12ERR ((uint8_t) 0xA6)
#define USBDC_IVECT_EP12IN     ((uint8_t) 0xA8)
#define USBDC_IVECT_HCIN12ERR  ((uint8_t) 0xAA)
#define USBDC_IVECT_EP12OUT    ((uint8_t) 0xAC)
#define USBDC_IVECT_EP12PING   ((uint8_t) 0xB0)
#define USBDC_IVECT_HCOUT13ERR ((uint8_t) 0xB2)
#define USBDC_IVECT_EP13IN     ((uint8_t) 0xB4)
#define USBDC_IVECT_HCIN13ERR  ((uint8_t) 0xB6)
#define USBDC_IVECT_EP13OUT    ((uint8_t) 0xB8)
#define USBDC_IVECT_EP13PING   ((uint8_t) 0xBC)
#define USBDC_IVECT_HCOUT14ERR ((uint8_t) 0xBE)
#define USBDC_IVECT_EP14IN     ((uint8_t) 0xC0)
#define USBDC_IVECT_HCIN14ERR  ((uint8_t) 0xC2)
#define USBDC_IVECT_EP14OUT    ((uint8_t) 0xC4)
#define USBDC_IVECT_EP14PING   ((uint8_t) 0xC8)
#define USBDC_IVECT_HCOUT15ERR ((uint8_t) 0xCA)
#define USBDC_IVECT_EP15IN     ((uint8_t) 0xCC)
#define USBDC_IVECT_HCIN15ERR  ((uint8_t) 0xCE)
#define USBDC_IVECT_EP15OUT    ((uint8_t) 0xD0)
#define USBDC_IVECT_EP15PING   ((uint8_t) 0xD4)
#define USBDC_IVECT_OTGIRQ     ((uint8_t) 0xD8)
#define USBDC_IVECT_LPMIRQ     ((uint8_t) 0xDC)

// endprst type
typedef union {
  struct {
    uint8_t   EP      : 4;   // endpoint number
    uint8_t   IO      : 1;   // 0 => OUT endpoint
                             // 1 => IN  endpoint
    uint8_t   TOGRST  : 1;   // flag to clear data toggle
    uint8_t   FIFORST : 1;   // flag to reset FIFO buffer
    uint8_t   TOGSETQ : 1;   // read:   current data toggle      for endpoint EP
                             // write:  manually set data toggle for endpoint EP
  };
  uint8_t BYTE;
} USBDC_ENDPRST_t;           // size/align:  1/1 byte

// usb control / status type
typedef union {
  struct {
    uint8_t            : 1;
    uint8_t   LPMNYET  : 1;   // set NYET handshake for LPM transaction
    uint8_t   SLFPWR   : 1;   // indication of self-powered operation.  reported to host in control traffic
    uint8_t   RWAKEN   : 1;   // remote wakeup feature enable
    uint8_t   ENUM     : 1;   // 0 => CPU responsible for enumeration process
                              // 1 => USBDC auto-enumerates
    uint8_t   SIGRSUME : 1;   // set this bit to initiate resume signalling
    uint8_t   DISCON   : 1;   // set this bit to force disconnect
    uint8_t   WAKESRC  : 1;   // wakeup source.  if set then source of wakeup is the USBDC
  };
  uint8_t BYTE;
} USBDC_USBCS_t;              // size/align:  1/1 byte

// frame counter type
typedef union {
  struct {
    uint16_t  MFR :  3;   // microframe number
    uint16_t  FR  : 11;   // frame      number
    uint16_t      :  2;
  };
  uint16_t HWORD;
} USBDC_FRMNR_t;        // size/align:  2/2 byte

// fifoctrl type
typedef union {
  struct {
    uint8_t  EP   : 4;  // endpoint number
    uint8_t  IO   : 1;  // 0 => OUT endpoint
                        // 1 => IN  endpoint
    uint8_t  AUTO : 1;  // set the fifo auto   setting
    uint8_t  CMIT : 1;  // set the fifo commit permission
    uint8_t  ACC  : 1;  // set to 1 to block fifo access permission
  };
  uint8_t BYTE;
} USBDC_FIFOCTRL_t;

// hcportctrl type
typedef union {
  struct {
    uint8_t TESTM   : 5;  // test mode select:
                          //   ????1 => TEST_J
                          //   ???10 => TEST_K
                          //   ??100 => TEST_SE0_NAK
                          //   ?1000 => TEST_PACKET
                          //   10000 => TEST_FORCE_ENABLE
    uint8_t PORTRST : 1;  // host mode:  force line reset
    uint8_t RSTDUR  : 2;  // host mode:  reset duration:
                          //   00 => 10ms
                          //   01 => 55ms
                          //   10 => 1.6ms
  } FIELD;
  uint8_t BYTE;
} USBDC_HCPORTCTRL_t;


// ********************************************************************************************************************
// USB device controller SFR map - device controller portion
// ********************************************************************************************************************

typedef struct {
  // endpoint 0 control
  struct {
    uint8_t       OUTBC;               // number of bytes for received OUT packet.  read only.
    uint8_t       INBC;                // number of bytes for IN packet.  write the number after loading the endpoint
    USBDC_EP0CS_t CS;                  //  buffer to control/status
    uint8_t       _RESERVED_03;        // <reserved>
  } EP0CTRL;                           // offset:  0x00 ... 0x03

  // link power management
  union {
    struct {
      uint16_t                : 4;     // <reserved>
      uint16_t  HIRD          : 4;     // host initiated resume duration from LPM transaction
      uint16_t  BREMOTEWAKEUP : 1;     // bRemoteWakeup from LPM command
      uint16_t                : 6;     // <reserved>
      uint16_t  LPMNYETR      : 1;     // read only:  alias of similar bit in USB control status register
    };
    uint16_t HWORD;
  } LPMCTRL;                           // offset:  0x04 ... 0x05

  uint8_t       _RESERVED_06;          // offset:  0x06     <reserved>

  // ep0 fifo config (no hardware support, do not use)
  struct {
    uint8_t           : 2;
    uint8_t  FIFORST  : 1;
    uint8_t           : 1;
    uint8_t  IO       : 1;
    uint8_t  FIFOAUTO : 1;
    uint8_t  FIFOCMT  : 1;
    uint8_t  FIFOACC  : 1;             // fifo access permission.  must not write 1 to this.
  } EP0FIFO;                           // offset:  0x07

  // endpoint controller mappings.  address mapping is a bit confused.  convenience functions recommended.
  USBDC_EPOUT_t EPOUT1;                // offset:  0x08 ... 0x0B
  USBDC_EPIN_t  EPIN1;                 // offset:  0x0C ... 0x0F
  USBDC_EPOUT_t EPOUT2;                // offset:  0x10 ... 0x13
  USBDC_EPIN_t  EPIN2;                 // offset:  0x14 ... 0x17
  USBDC_EPOUT_t EPOUT3;                // offset:  0x18 ... 0x1B
  USBDC_EPIN_t  EPIN3;                 // offset:  0x1C ... 0x1F
  USBDC_EPOUT_t EPOUT4;                // offset:  0x20 ... 0x23
  USBDC_EPIN_t  EPIN4;                 // offset:  0x24 ... 0x27
  USBDC_EPOUT_t EPOUT5;                // offset:  0x28 ... 0x2B
  USBDC_EPIN_t  EPIN5;                 // offset:  0x2C ... 0x2F
  USBDC_EPOUT_t EPOUT6;                // offset:  0x30 ... 0x33
  USBDC_EPIN_t  EPIN6;                 // offset:  0x34 ... 0x37
  USBDC_EPOUT_t EPOUT7;                // offset:  0x38 ... 0x3B
  USBDC_EPIN_t  EPIN7;                 // offset:  0x3C ... 0x3F
  USBDC_EPOUT_t EPOUT8;                // offset:  0x40 ... 0x43
  USBDC_EPIN_t  EPIN8;                 // offset:  0x44 ... 0x47
  USBDC_EPOUT_t EPOUT9;                // offset:  0x48 ... 0x4B
  USBDC_EPIN_t  EPIN9;                 // offset:  0x4C ... 0x4F
  USBDC_EPOUT_t EPOUT10;               // offset:  0x50 ... 0x53
  USBDC_EPIN_t  EPIN10;                // offset:  0x54 ... 0x57
  USBDC_EPOUT_t EPOUT11;               // offset:  0x58 ... 0x5B
  USBDC_EPIN_t  EPIN11;                // offset:  0x5C ... 0x5F
  USBDC_EPOUT_t EPOUT12;               // offset:  0x60 ... 0x63
  USBDC_EPIN_t  EPIN12;                // offset:  0x64 ... 0x67
  USBDC_EPOUT_t EPOUT13;               // offset:  0x68 ... 0x6B
  USBDC_EPIN_t  EPIN13;                // offset:  0x6C ... 0x6F
  USBDC_EPOUT_t EPOUT14;               // offset:  0x70 ... 0x73
  USBDC_EPIN_t  EPIN14;                // offset:  0x74 ... 0x77
  USBDC_EPOUT_t EPOUT15;               // offset:  0x78 ... 0x7B
  USBDC_EPIN_t  EPIN15;                // offset:  0x7C ... 0x7F

  uint32_t      _RESERVED_80;          // offset:  0x80 ... 0x83 (placeholder for FIFO0DAT)

  // endpoint buffer FIFOs.  read takes data fron OUT FIFO.  write puts data to IN FIFO.  Again, very confused.
  uint32_t      FIFO1DAT;              // offset:  0x84 ... 0x87
  uint32_t      FIFO2DAT;              // offset:  0x88 ... 0x8B
  uint32_t      FIFO3DAT;              // offset:  0x8C ... 0x8F
  uint32_t      FIFO4DAT;              // offset:  0x90 ... 0x93
  uint32_t      FIFO5DAT;              // offset:  0x94 ... 0x97
  uint32_t      FIFO6DAT;              // offset:  0x98 ... 0x9B
  uint32_t      FIFO7DAT;              // offset:  0x9C ... 0x9F
  uint32_t      FIFO8DAT;              // offset:  0xA0 ... 0xA3
  uint32_t      FIFO9DAT;              // offset:  0xA4 ... 0xA7
  uint32_t      FIFO10DAT;             // offset:  0xA8 ... 0xAB
  uint32_t      FIFO11DAT;             // offset:  0xAC ... 0xAF
  uint32_t      FIFO12DAT;             // offset:  0xB0 ... 0xB3
  uint32_t      FIFO13DAT;             // offset:  0xB4 ... 0xB7
  uint32_t      FIFO14DAT;             // offset:  0xB8 ... 0xBB
  uint32_t      FIFO15DAT;             // offset:  0xBC ... 0xBF

  // HCxxx registers.  Not used in device controller mode.  again, very confused mapping.
  USBDC_HCEP0_t   HCEP0;                // offset: 0xC0 ... 0xC3
  USBDC_HCEPOUT_t HCOUT1;               // offset: 0xC4 ... 0xC5
  USBDC_HCEPIN_t  HCIN1;                // offset: 0xC6 ... 0xC7
  USBDC_HCEPOUT_t HCOUT2;               // offset: 0xC8 ... 0xC9
  USBDC_HCEPIN_t  HCIN2;                // offset: 0xCA ... 0xCB
  USBDC_HCEPOUT_t HCOUT3;               // offset: 0xCC ... 0xCD
  USBDC_HCEPIN_t  HCIN3;                // offset: 0xCE ... 0xCF
  USBDC_HCEPOUT_t HCOUT4;               // offset: 0xD0 ... 0xD1
  USBDC_HCEPIN_t  HCIN4;                // offset: 0xD2 ... 0xD3
  USBDC_HCEPOUT_t HCOUT5;               // offset: 0xD4 ... 0xD5
  USBDC_HCEPIN_t  HCIN5;                // offset: 0xD6 ... 0xD7
  USBDC_HCEPOUT_t HCOUT6;               // offset: 0xD8 ... 0xD9
  USBDC_HCEPIN_t  HCIN6;                // offset: 0xDA ... 0xDB
  USBDC_HCEPOUT_t HCOUT7;               // offset: 0xDC ... 0xDD
  USBDC_HCEPIN_t  HCIN7;                // offset: 0xDE ... 0xDF
  USBDC_HCEPOUT_t HCOUT8;               // offset: 0xE0 ... 0xE1
  USBDC_HCEPIN_t  HCIN8;                // offset: 0xE2 ... 0xE3
  USBDC_HCEPOUT_t HCOUT9;               // offset: 0xE4 ... 0xE5
  USBDC_HCEPIN_t  HCIN9;                // offset: 0xE6 ... 0xE7
  USBDC_HCEPOUT_t HCOUT10;              // offset: 0xE8 ... 0xE9
  USBDC_HCEPIN_t  HCIN10;               // offset: 0xEA ... 0xEB
  USBDC_HCEPOUT_t HCOUT11;              // offset: 0xEC ... 0xED
  USBDC_HCEPIN_t  HCIN11;               // offset: 0xEE ... 0xEF
  USBDC_HCEPOUT_t HCOUT12;              // offset: 0xF0 ... 0xF1
  USBDC_HCEPIN_t  HCIN12;               // offset: 0xF2 ... 0xF3
  USBDC_HCEPOUT_t HCOUT13;              // offset: 0xF4 ... 0xF5
  USBDC_HCEPIN_t  HCIN13;               // offset: 0xF6 ... 0xF7
  USBDC_HCEPOUT_t HCOUT14;              // offset: 0xF8 ... 0xF9
  USBDC_HCEPIN_t  HCIN14;               // offset: 0xFA ... 0xFB
  USBDC_HCEPOUT_t HCOUT15;              // offset: 0xFC ... 0xFD
  USBDC_HCEPIN_t  HCIN15;               // offset: 0xFE ... 0xFF

  // EP0 buffer
  uint32_t        EP0INDAT [16];        // offset: 0x100 ... 0x13F
  uint32_t        EP0OUTDAT[16];        // offset: 0x140 ... 0x17F
  uint32_t        SETUPDAT [2];         // offset: 0x180 ... 0x187

  // interrupts
  uint16_t        INIRQ;                // offset: 0x188 ... 0x189
  uint16_t        OUTIRQ;               // offset: 0x18A ... 0x18B
  USBDC_USBIRQ_t  USBIRQ;               // offset: 0x18C
  uint8_t         _RESERVED_18D;        // offset: 0x18D
  uint16_t        OUTPNGIRQ;            // offset: 0x18E ... 0x18F
  uint16_t        INFULLIRQ;            // offset: 0x190 ... 0x191
  uint16_t        OUTEMPTIRQ;           // offset: 0x192 ... 0x193
  uint16_t        INIEN;                // offset: 0x194 ... 0x195
  uint16_t        OUTIEN;               // offset: 0x196 ... 0x197
  USBDC_USBIRQ_t  USBIEN;               // offset: 0x198
  uint8_t         _RESERVED_199;        // offset: 0x199
  uint16_t        OUTPNGIEN;            // offset: 0x19A ... 0x19B
  uint16_t        INFULLIEN;            // offset: 0x19C ... 0x19D
  uint16_t        OUTEMPTIEN;           // offset: 0x19E ... 0x19F
  uint8_t         IVECT;                // offset: 0x1A0
  uint8_t         FIFOIVECT;            // offset: 0x1A1            (hardware FIFO IRQ: no hardware support)

  // utility SFRs
  USBDC_ENDPRST_t    ENDPRST;            // offset: 0x1A2
  USBDC_USBCS_t      USBCS;              // offset: 0x1A3
  USBDC_FRMNR_t      FRMNR;              // offset: 0x1A4 ... 0x1A5
  uint8_t            FNADDR;             // offset: 0x1A6
  uint8_t            _RESERVED_1A7;      // offset: 0x1A7
  USBDC_FIFOCTRL_t   FIFOCTRL;           // offset: 0x1A8
  uint8_t            _RESERVED_1A9;      // offset: 0x1A9
  uint8_t            _RESERVED_1AA;      // offset: 0x1AA
  USBDC_HCPORTCTRL_t HCPORTCTRL;         // offset: 0x1AB
  uint16_t           HCFRMNR;            // offset: 0x1AC ... 0x1AD
  uint16_t           HCFRMREMAIN;        // offset: 0x1AE ... 0x1AF  (host mode:  number of bytes which can be sent
                                         //                                       in remainder of current frame)
  uint32_t           _RESERVED_1B0;      // offset: 0x1B0 ... 0x1B3
  uint16_t           HCINERRIRQ;         // offset: 0x1B4 ... 0x1B5  (host mode:  IN  error IRQ)
  uint16_t           HCOUTERRIRQ;        // offset: 0x1B6 ... 0x1B7  (host mode:  OUT error IRQ)
  uint16_t           HCINERRIEN;         // offset: 0x1B8 ... 0x1B9  (host mode:  IN error  IRQ enable)
  uint16_t           HCOUTERRIEN;        // offset: 0x1BA ... 0x1BB  (host mode:  OUT error IRQ enable)
  uint32_t           _RESERVED_1BC;      // offset: 0x1BC ... 0x1BF
  uint32_t           _RESERVED_1C0;      // offset: 0x1C0 ... 0x1C3
  uint32_t           _RESERVED_1C4;      // offset: 0x1C4 ... 0x1C7
  uint32_t           _RESERVED_1C8;      // offset: 0x1C8 ... 0x1CB
  uint16_t           ISOAUTOARM;         // offset: 0x1CC ... 0x1CD  (set bit to auto-arm ISO IN buffer on SOF)
  uint16_t           _RESERVED_1CE;      // offset: 0x1CE ... 0x1CF
  uint32_t           _RESERVED_1D0;      // offset: 0x1D0 ... 0x1D3
  uint32_t           _RESERVED_1D4;      // offset: 0x1D4 ... 0x1D7
  uint16_t           ISODCTRL;           // offset: 0x1D8 ... 0x1D9  (set bit to auto-compute number of ISO pkts)
  uint16_t           _RESERVED_1DA;      // offset: 0x1DA ... 0x1DB
  uint16_t           ISOAUTODUMP;        // offset: 0x1DC ..  0x1DD  (ISO auto dump enable)
  uint16_t           _RESERVED_1DE;      // offset: 0x1DE ... 0x1DF
  uint8_t            OUT0MAXPCK;         // offset: 0x1E0
  uint8_t            _RESERVED_1E1;      // offset: 0x1E1
  uint16_t           OUT1MAXPCK;         // offset: 0x1E2 ... 0x1E3
  uint16_t           OUT2MAXPCK;         // offset: 0x1E4 ... 0x1E5
  uint16_t           OUT3MAXPCK;         // offset: 0x1E6 ... 0x1E7
  uint16_t           OUT4MAXPCK;         // offset: 0x1E8 ... 0x1E9
  uint16_t           OUT5MAXPCK;         // offset: 0x1EA ... 0x1EB
  uint16_t           OUT6MAXPCK;         // offset: 0x1EC ... 0x1ED
  uint16_t           OUT7MAXPCK;         // offset: 0x1EE ... 0x1EF
  uint16_t           OUT8MAXPCK;         // offset: 0x1F0 ... 0x1F1
  uint16_t           OUT9MAXPCK;         // offset: 0x1F2 ... 0x1F3
  uint16_t           OUT10MAXPCK;        // offset: 0x1F4 ... 0x1F5
  uint16_t           OUT11MAXPCK;        // offset: 0x1F6 ... 0x1F7
  uint16_t           OUT12MAXPCK;        // offset: 0x1F8 ... 0x1F9
  uint16_t           OUT13MAXPCK;        // offset: 0x1FA ... 0x1FB
  uint16_t           OUT14MAXPCK;        // offset: 0x1FC ... 0x1FD
  uint16_t           OUT15MAXPCK;        // offset: 0x1FE ... 0x1FF

  uint32_t           _RESERVED_200[64];  // offset: 0x200 ... 0x2FF (empty space for some reason)

  uint32_t           _RESERVED_300;      // 0x300
  uint16_t           OUT1STADDR;         // 0x304
  uint16_t           _RESERVED_306;      // 0x306
  uint16_t           OUT2STADDR;         // 0x308
  uint16_t           _RESERVED_30A;      // 0x30A
  uint16_t           OUT3STADDR;         // 0x30C
  uint16_t           _RESERVED_30E;      // 0x30E
  uint16_t           OUT4STADDR;         // 0x310
  uint16_t           _RESERVED_312;      // 0x312
  uint16_t           OUT5STADDR;         // 0x314
  uint16_t           _RESERVED_316;      // 0x316
  uint16_t           OUT6STADDR;         // 0x318
  uint16_t           _RESERVED_31A;      // 0x31A
  uint16_t           OUT7STADDR;         // 0x31C
  uint16_t           _RESERVED_31E;      // 0x31E
  uint16_t           OUT8STADDR;         // 0x320
  uint16_t           _RESERVED_322;      // 0x322
  uint16_t           OUT9STADDR;         // 0x324
  uint16_t           _RESERVED_326;      // 0x326
  uint16_t           OUT10STADDR;        // 0x328
  uint16_t           _RESERVED_32A;      // 0x32A
  uint16_t           OUT11STADDR;        // 0x32C
  uint16_t           _RESERVED_32E;      // 0x32E
  uint16_t           OUT12STADDR;        // 0x330
  uint16_t           _RESERVED_332;      // 0x332
  uint16_t           OUT13STADDR;        // 0x334
  uint16_t           _RESERVED_336;      // 0x336
  uint16_t           OUT14STADDR;        // 0x338
  uint16_t           _RESERVED_33A;      // 0x33A
  uint16_t           OUT15STADDR;        // 0x33C
  uint16_t           _RESERVED_33E;      // 0x33E

  uint32_t           _RESERVED_340;      // 0x340
  uint16_t           IN1STADDR;          // 0x344
  uint16_t           _RESERVED_346;      // 0x346
  uint16_t           IN2STADDR;          // 0x348
  uint16_t           _RESERVED_34A;      // 0x34A
  uint16_t           IN3STADDR;          // 0x34C
  uint16_t           _RESERVED_34E;      // 0x34E
  uint16_t           IN4STADDR;          // 0x350
  uint16_t           _RESERVED_352;      // 0x352
  uint16_t           IN5STADDR;          // 0x354
  uint16_t           _RESERVED_356;      // 0x356
  uint16_t           IN6STADDR;          // 0x358
  uint16_t           _RESERVED_35A;      // 0x35A
  uint16_t           IN7STADDR;          // 0x35C
  uint16_t           _RESERVED_35E;      // 0x35E
  uint16_t           IN8STADDR;          // 0x360
  uint16_t           _RESERVED_362;      // 0x362
  uint16_t           IN9STADDR;          // 0x364
  uint16_t           _RESERVED_366;      // 0x366
  uint16_t           IN10STADDR;         // 0x368
  uint16_t           _RESERVED_36A;      // 0x36A
  uint16_t           IN11STADDR;         // 0x36C
  uint16_t           _RESERVED_36E;      // 0x36E
  uint16_t           IN12STADDR;         // 0x370
  uint16_t           _RESERVED_372;      // 0x372
  uint16_t           IN13STADDR;         // 0x374
  uint16_t           _RESERVED_376;      // 0x376
  uint16_t           IN14STADDR;         // 0x378
  uint16_t           _RESERVED_37A;      // 0x37A
  uint16_t           IN15STADDR;         // 0x37C
  uint16_t           _RESERVED_37E;      // 0x37E

  uint32_t           _RESERVED_380[24];  // offset:  0x380 ... 0x3DF

  // IN max packet.
  uint16_t           _RESERVED_3E0;      // 0x3E0
  uint16_t           IN1MAXPCK;          // 0x3E2
  uint16_t           IN2MAXPCK;          // 0x3E4
  uint16_t           IN3MAXPCK;          // 0x3E6
  uint16_t           IN4MAXPCK;          // 0x3E8
  uint16_t           IN5MAXPCK;          // 0x3EA
  uint16_t           IN6MAXPCK;          // 0x3EC
  uint16_t           IN7MAXPCK;          // 0x3EE
  uint16_t           IN8MAXPCK;          // 0x3F0
  uint16_t           IN9MAXPCK;          // 0x3F2
  uint16_t           IN10MAXPCK;         // 0x3F4
  uint16_t           IN11MAXPCK;         // 0x3F6
  uint16_t           IN12MAXPCK;         // 0x3F8
  uint16_t           IN13MAXPCK;         // 0x3FA
  uint16_t           IN14MAXPCK;         // 0x3FC
  uint16_t           IN15MAXPCK;         // 0x3FE .. 0x3FF
} USBDC_SFRS_t;                          // size/align : 0x400 / WORD


// ********************************************************************************************************************
// SFR memory mapping
// ********************************************************************************************************************


#define _USBDCAPP   ((volatile USBDC_SFRS_t *) (ASIC_16B_BASE + 0x1000))
#define _USBDCH2HH  ((volatile USBDC_SFRS_t *) (ASIC_16B_BASE + 0x9000))
#define _USBDCH2HD  ((volatile USBDC_SFRS_t *) (ASIC_16B_BASE + 0xA000))


#ifdef __SFR_MAP_ASOBJ__
  // optional: provide as struct
  #ifdef __ICCARM__
    // IAR style
    volatile __no_init USBDC_SFRS_t USBDCAPP   @ (ASIC_16B_BASE + 0x1000);
    volatile __no_init USBDC_SFRS_t USBDCH2HH  @ (ASIC_16B_BASE + 0x9000);
    volatile __no_init USBDC_SFRS_t USBDCH2HD  @ (ASIC_16B_BASE + 0xA000);
  #else
    // generic style
    #define USBDCAPP  (* _USBDCAPP)
    #define USBDCH2HH (* _USBDCH2HH)
    #define USBDCH2HD (* _USBDCH2HD)
  #endif
#else
  // default : provide as struct pointer
  #define USBDCAPP       _USBDCAPP
  #define USBDCH2HH      _USBDCH2HH
  #define USBDCH2HD      _USBDCH2HD
#endif


// ********************************************************************************************************************
// convenience functions
// ********************************************************************************************************************

// convenience names for endpoint type "enum"
#define USBDC_EPTYPE_ISO ((uint8_t) 0x01)
#define USBDC_EPTYPE_BT  ((uint8_t) 0x02)
#define USBDC_EPTYPE_INT ((uint8_t) 0x03)


// setup memory map.  this provides the recommended configuration.  also sets max packet to the
// maximum allowable by hardware.  if desired, this can be reduced at a later stage.
__STATIC_INLINE void USBDC_configMemoryMap() {
  uint16_t aptr;

  // Application function device: OUT endpoints
  aptr = 0;
  _USBDCAPP->OUT0MAXPCK  = 64;    aptr += 64;       // OUT0: 1 x  64 byte
  _USBDCAPP->OUT1STADDR  = aptr;
  _USBDCAPP->OUT1MAXPCK  = 512;   aptr += (4*512);  // OUT1: 4 x 512 byte
  _USBDCAPP->OUT2STADDR  = aptr;
  _USBDCAPP->OUT2MAXPCK  = 64;    aptr += (4* 64);  // OUT2: 4 x  64 byte

  // Application function device: IN endpoints
  aptr = 0;
                                  aptr += 64;       // IN0: 1 x  64 byte
  _USBDCAPP->IN1STADDR   = aptr;
  _USBDCAPP->IN1MAXPCK   = 512;   aptr += (4*512);  // IN1: 4 x 512 byte
  _USBDCAPP->IN2STADDR   = aptr;
  _USBDCAPP->IN2MAXPCK   = 64;    aptr += 64;       // IN2: 1 x  64 byte
  _USBDCAPP->IN3STADDR   = aptr;
  _USBDCAPP->IN3MAXPCK   = 320;   aptr += (4*320);  // IN3: 4 x 320 byte

  // Hub-facing H2H device: OUT endpoints
  aptr = 0;
  _USBDCH2HH->OUT0MAXPCK = 64;    aptr += 64;       // OUT0: 1 x  64 byte
  _USBDCH2HH->OUT2STADDR = aptr;
  _USBDCH2HH->OUT2MAXPCK = 512;   aptr += (4*512);  // OUT2: 4 x 512 byte
  _USBDCH2HH->OUT4STADDR = aptr;
  _USBDCH2HH->OUT4MAXPCK = 512;   aptr += (4*512);  // OUT4: 4 x 512 byte
  _USBDCH2HH->OUT6STADDR = aptr;
  _USBDCH2HH->OUT6MAXPCK = 64;    aptr += (4* 64);  // OUT6: 4 x  64 byte
  _USBDCH2HH->OUT8STADDR = aptr;
  _USBDCH2HH->OUT8MAXPCK = 320;   aptr += (4* 320); // OUT8: 4 x 320 byte

  // Hub-facing H2H device: IN endpoints
  aptr = 0;
                                  aptr += 64;        // IN0: 1 x  64 byte
  _USBDCH2HH->IN1STADDR  = aptr;
  _USBDCH2HH->IN1MAXPCK  = 512;   aptr += (4*512);   // IN1: 4 x 512 byte
  _USBDCH2HH->IN3STADDR  = aptr;
  _USBDCH2HH->IN3MAXPCK  = 512;   aptr += (4*512);   // IN3: 4 x 512 byte
  _USBDCH2HH->IN7STADDR  = aptr;
  _USBDCH2HH->IN7MAXPCK  = 320;   aptr += (4*320);   // IN7: 4 x 320 byte
  _USBDCH2HH->IN9STADDR  = aptr;
  _USBDCH2HH->IN9MAXPCK  = 64;    aptr += (4* 64);   // IN9: 4 x  64 byte

  // Carplay-facing H2H device: OUT endpoints
  aptr = 0;
  _USBDCH2HD->OUT0MAXPCK = 64;    aptr += 64;        // OUT0: 1 x  64 byte
  _USBDCH2HD->OUT2STADDR = aptr;
  _USBDCH2HD->OUT2MAXPCK = 512;   aptr += (4*512);   // OUT2: 4 x 512 byte
  _USBDCH2HD->OUT4STADDR = aptr;
  _USBDCH2HD->OUT4MAXPCK = 512;   aptr += (4*512);   // OUT4: 4 x 512 byte
  _USBDCH2HD->OUT6STADDR = aptr;
  _USBDCH2HD->OUT6MAXPCK = 320;   aptr += (4*320);   // OUT6: 4 x 320 byte

  // Carplay-facing H2H device: IN endpoints
  aptr = 0;
                                  aptr += 64;        // IN0: 1 x  64 byte
  _USBDCH2HD->IN1STADDR  = aptr;
  _USBDCH2HD->IN1MAXPCK  = 512;   aptr += (4*512);   // IN1: 4 x 512 byte
  _USBDCH2HD->IN3STADDR  = aptr;
  _USBDCH2HD->IN3MAXPCK  = 512;   aptr += (4*512);   // IN3: 4 x 512 byte
  _USBDCH2HD->IN5STADDR  = aptr;
  _USBDCH2HD->IN5MAXPCK  = 64;    aptr += (4* 64);   // IN5: 4 x 512 byte
  _USBDCH2HD->IN7STADDR  = aptr;
  _USBDCH2HD->IN7MAXPCK  = 320;   aptr += (4*320);   // IN7: 4 x 320 byte
};


// configure OUT endpoint.  unfortunately the endpoint config is scattered all over, so this has to write in several
// places.  configures the following SFRs:
//   - EPOUTx
//   - OUTxMAXPCK
__STATIC_INLINE void USBDC_configEPOUT(
  volatile USBDC_SFRS_t *const usbdc,                   // device controller base address
  const uint8_t                epnum,                   // endpoint number to configure
  const uint8_t                valid,                   // is endpoint valid?  boolean.
  const uint8_t                eptype,                  // endpoint type (use USBDC_EPTYPE_xxx defines)
  const uint8_t                nbuf,                    // number of buffers.  provide a value: 1..4
  const uint8_t                maxpkt                   // maximum packet size (zero to leave default)
) {
  register USBDC_OUTCON_t  outcon;
  volatile USBDC_OUTCON_t* wptr;
  volatile uint16_t*       mptr;

  // check the endpoint number is reasonable (since we are going to be doing offsets later)
  if( (epnum < 1) || (epnum > 15) ) dbgTrapErr();

  // use switch / case for the moment.  if the compiler is not good at this and we care, we can change
  // to calculate the offsets directly later.  Evatronix was not particularly helpful with the layout
  // here.
  switch( epnum ) {
    case  1: wptr = &( usbdc->EPOUT1.CON  );  mptr = &( usbdc->OUT1MAXPCK  ); break;
    case  2: wptr = &( usbdc->EPOUT2.CON  );  mptr = &( usbdc->OUT2MAXPCK  ); break;
    case  3: wptr = &( usbdc->EPOUT3.CON  );  mptr = &( usbdc->OUT3MAXPCK  ); break;
    case  4: wptr = &( usbdc->EPOUT4.CON  );  mptr = &( usbdc->OUT4MAXPCK  ); break;
    case  5: wptr = &( usbdc->EPOUT5.CON  );  mptr = &( usbdc->OUT5MAXPCK  ); break;
    case  6: wptr = &( usbdc->EPOUT6.CON  );  mptr = &( usbdc->OUT6MAXPCK  ); break;
    case  7: wptr = &( usbdc->EPOUT7.CON  );  mptr = &( usbdc->OUT7MAXPCK  ); break;
    case  8: wptr = &( usbdc->EPOUT8.CON  );  mptr = &( usbdc->OUT8MAXPCK  ); break;
    case  9: wptr = &( usbdc->EPOUT9.CON  );  mptr = &( usbdc->OUT9MAXPCK  ); break;
    case 10: wptr = &( usbdc->EPOUT10.CON );  mptr = &( usbdc->OUT10MAXPCK ); break;
    case 11: wptr = &( usbdc->EPOUT11.CON );  mptr = &( usbdc->OUT11MAXPCK ); break;
    case 12: wptr = &( usbdc->EPOUT12.CON );  mptr = &( usbdc->OUT12MAXPCK ); break;
    case 13: wptr = &( usbdc->EPOUT13.CON );  mptr = &( usbdc->OUT13MAXPCK ); break;
    case 14: wptr = &( usbdc->EPOUT14.CON );  mptr = &( usbdc->OUT14MAXPCK ); break;
    case 15: wptr = &( usbdc->EPOUT15.CON );  mptr = &( usbdc->OUT15MAXPCK ); break;
    default: dbgTrapErr();                                                    return;
  }

  // write CON register
  outcon.BUF   = (nbuf-1);
  outcon.TYPE  = eptype;
  outcon.STALL = 0;
  outcon.VAL   = valid ? 1 : 0;
  wptr->BYTE = outcon.BYTE;

  // write max packet
  if( maxpkt > 0 ) *mptr = maxpkt;
}


// configure IN endpoint.  unfortunately the endpoint config is scattered all over, so this has to write in several
// places.  configures the following SFRs:
//   - EPINx
//   - INxMAXPCK
__STATIC_INLINE void USBDC_configEPIN(
  volatile USBDC_SFRS_t *const usbdc,                   // device controller base address
  const uint8_t                epnum,                   // endpoint number to configure
  const uint8_t                valid,                   // is endpoint valid?  boolean.
  const uint8_t                eptype,                  // endpoint type (use USBDC_EPTYPE_xxx defines)
  const uint8_t                nbuf,                    // number of buffers.  provide a value: 1..4
  const uint8_t                maxpkt,                  // maximum packet size (zero to leave default)
  const uint8_t                isod                     // ISO packets per uFrame (ignored if not ISO type)
) {
  register USBDC_INCON_t   incon;
  volatile USBDC_INCON_t*  wptr;
  volatile uint16_t*       mptr;

  // check the endpoint number is reasonable (since we are going to be doing offsets later)
  if( (epnum < 1) || (epnum > 15) ) dbgTrapErr();

  // use switch / case for the moment.  if the compiler is not good at this and we care, we can change
  // to calculate the offsets directly later.  Evatronix was not particularly helpful with the layout
  // here.
  switch( epnum ) {
    case  1: wptr = &( usbdc->EPIN1.CON  );  mptr = &( usbdc->IN1MAXPCK  ); break;
    case  2: wptr = &( usbdc->EPIN2.CON  );  mptr = &( usbdc->IN2MAXPCK  ); break;
    case  3: wptr = &( usbdc->EPIN3.CON  );  mptr = &( usbdc->IN3MAXPCK  ); break;
    case  4: wptr = &( usbdc->EPIN4.CON  );  mptr = &( usbdc->IN4MAXPCK  ); break;
    case  5: wptr = &( usbdc->EPIN5.CON  );  mptr = &( usbdc->IN5MAXPCK  ); break;
    case  6: wptr = &( usbdc->EPIN6.CON  );  mptr = &( usbdc->IN6MAXPCK  ); break;
    case  7: wptr = &( usbdc->EPIN7.CON  );  mptr = &( usbdc->IN7MAXPCK  ); break;
    case  8: wptr = &( usbdc->EPIN8.CON  );  mptr = &( usbdc->IN8MAXPCK  ); break;
    case  9: wptr = &( usbdc->EPIN9.CON  );  mptr = &( usbdc->IN9MAXPCK  ); break;
    case 10: wptr = &( usbdc->EPIN10.CON );  mptr = &( usbdc->IN10MAXPCK ); break;
    case 11: wptr = &( usbdc->EPIN11.CON );  mptr = &( usbdc->IN11MAXPCK ); break;
    case 12: wptr = &( usbdc->EPIN12.CON );  mptr = &( usbdc->IN12MAXPCK ); break;
    case 13: wptr = &( usbdc->EPIN13.CON );  mptr = &( usbdc->IN13MAXPCK ); break;
    case 14: wptr = &( usbdc->EPIN14.CON );  mptr = &( usbdc->IN14MAXPCK ); break;
    case 15: wptr = &( usbdc->EPIN15.CON );  mptr = &( usbdc->IN15MAXPCK ); break;
    default: dbgTrapErr();                                                  return;
  }

  // write CON register
  incon.BUF   = (nbuf-1);
  incon.TYPE  = eptype;
  incon.STALL = 0;
  incon.ISOD  = (isod-1);
  incon.VAL   = valid ? 1 : 0;
  wptr->BYTE  = incon.BYTE;

  // write max packet
  if( maxpkt > 0 ) *mptr = maxpkt;
}

#endif // include guard
