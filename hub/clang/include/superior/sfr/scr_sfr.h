/**
 * @copyright 2018 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file scr_sfr.h
 */

#ifndef __SCR_SFR_H__
#define __SCR_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for SCR.
 */
typedef struct {

  union {
    struct {
      uint16_t INVLEV                   :  1;
      uint16_t INVORD                   :  1;
      uint16_t PECH2FIFO                :  1;
      uint16_t                          :  3; /*   (reserved) */
      uint16_t CLKSTOP                  :  1;
      uint16_t CLKSTOPVAL               :  1;
      uint16_t TXEN                     :  1;
      uint16_t RXEN                     :  1;
      uint16_t TS2FIFO                  :  1;
      uint16_t T0T1                     :  1;
      uint16_t ATRSTFLUSH               :  1;
      uint16_t TCKEN                    :  1;
      uint16_t                          :  1; /*   (reserved) */
      uint16_t GINTEN                   :  1;
    };
    uint16_t HWORD;
  } CTRL1; /* +0x000 */

  uint16_t _RESERVED_02;

  union {
    struct {
      uint16_t                          :  2; /*   (reserved) */
      uint16_t WARMRST                  :  1;
      uint16_t ACT                      :  1;
      uint16_t DEACT                    :  1;
      uint16_t VCC18                    :  1;
      uint16_t VCC33                    :  1;
      uint16_t VCC50                    :  1;
      uint16_t ACTFAST                  :  8;
    };
    uint16_t HWORD;
  } CTRL2; /* +0x004 */

  uint16_t _RESERVED_06;

  union {
    struct {
      uint16_t DIRACCPADS               :  1;
      uint16_t DSCIO                    :  1;
      uint16_t DSCCLK                   :  1;
      uint16_t DSCRST                   :  1;
      uint16_t DSCVCC                   :  1;
      uint16_t AUTOADEAVPP              :  1;
      uint16_t DSCVPPEN                 :  1;
      uint16_t DSCVPPPP                 :  1;
      uint16_t DSCFCB                   :  1;
      uint16_t SCPRESENT                :  1;
      uint16_t                          :  6; /*   (reserved) */
    };
    uint16_t HWORD;
  } SCPADS; /* +0x008 */

  uint16_t _RESERVED_0A;

  uint16_t INTEN1;                            /* +0x00C */
  uint16_t _RESERVED_0E;                      /* +0x00E */

  union {
    struct {
      uint16_t TXFIDONE                 :  1;
      uint16_t TXFIEMPTY                :  1;
      uint16_t RXFIFULL                 :  1;
      uint16_t CLKSTOPRUN               :  1;
      uint16_t TXDONE                   :  1;
      uint16_t RXDONE                   :  1;
      uint16_t TXPERR                   :  1;
      uint16_t RXPERR                   :  1;
      uint16_t C2CFULL                  :  1;
      uint16_t RXTHRESHOLD              :  1;
      uint16_t ATRFAIL                  :  1;
      uint16_t ATRDONE                  :  1;
      uint16_t SCREM                    :  1;
      uint16_t SCINS                    :  1;
      uint16_t SCACT                    :  1;
      uint16_t SCDEACT                  :  1;
    };
    uint16_t HWORD;
  } INTSTAT1; /* +0x010 */

  uint16_t _RESERVED_12;

  union {
    struct {
      uint16_t TXFIFOEMPTY              :  1;
      uint16_t TXFIFOFULL               :  1;
      uint16_t TXFIFOFLUSH              :  1;
      uint16_t                          :  5; /*   (reserved) */
      uint16_t RXFIFOEMPTY              :  1;
      uint16_t RXFIFOFULL               :  1;
      uint16_t RXFIFOFLUSH              :  1;
      uint16_t                          :  5; /*   (reserved) */
    };
    uint16_t HWORD;
  } FIFOCTRL; /* +0x014 */

  uint16_t _RESERVED_16;

  union {
    struct {
      uint16_t TXCNT                    :  8;
      uint16_t RXCNT                    :  8;
    };
    uint16_t HWORD;
  } LEGACYFIFOCNT; /* +0x018 */

  uint16_t _RESERVED_1A;

  uint16_t RXFIFOTHR;                         /* +0x01C */
  uint16_t _RESERVED_1E;                      /* +0x01E */

  union {
    struct {
      uint8_t  TX                       :  4;
      uint8_t  RX                       :  4;
      uint8_t                           :  8; /*   (reserved) */
    };
    uint16_t HWORD;
  } REPEAT; /* +0x020 */

  uint16_t _RESERVED_22;

  uint16_t SCCDIV;                            /* +0x024 */
  uint16_t _RESERVED_26;                      /* +0x026 */

  uint16_t BAUDDIV;                           /* +0x028 */
  uint16_t _RESERVED_2A;                      /* +0x02A */

  uint8_t  GUARDTIME;                         /* +0x02C */
  uint8_t  _RESERVED_2D;                      /* +0x02D */
  uint16_t _RESERVED_2E;                      /* +0x02E */

  uint16_t ADEATIME;                          /* +0x030 */
  uint16_t _RESERVED_32;                      /* +0x032 */

  uint16_t LOWRSTTIME;                        /* +0x034 */
  uint16_t _RESERVED_36;                      /* +0x036 */

  uint16_t ATRSTARTLIMIT;                     /* +0x038 */
  uint16_t _RESERVED_3A;                      /* +0x03A */

  uint16_t C2CLIMLO;                          /* +0x03C */
  uint16_t _RESERVED_3E;                      /* +0x03E */

  uint16_t INTEN2;                            /* +0x040 */
  uint16_t _RESERVED_42;                      /* +0x042 */

  union {
    struct {
      uint8_t  TXTHRESHOLD              :  1;
      uint8_t  TCKERR                   :  1;
      uint8_t                           :  6; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
    };
    uint16_t HWORD;
  } INTSTAT2; /* +0x044 */

  uint16_t _RESERVED_46;

  uint16_t TXFIFOTHR;                         /* +0x048 */
  uint16_t _RESERVED_4A;                      /* +0x04A */

  uint16_t TXFIFOCNT;                         /* +0x04C */
  uint16_t _RESERVED_4E;                      /* +0x04E */

  uint16_t RXFIFOCNT;                         /* +0x050 */
  uint16_t _RESERVED_52;                      /* +0x052 */

  uint8_t  BAUDTUNE;                          /* +0x054 */
  uint8_t  _RESERVED_55;                      /* +0x055 */
  uint16_t _RESERVED_56;                      /* +0x056 */

  uint8_t  C2CLIMHI;                          /* +0x058 */
  uint8_t  _RESERVED_59;                      /* +0x059 */
  uint16_t _RESERVED_5A;                      /* +0x05A */

  uint32_t _RESERVED_5B[105];                 /* +0x05B */

  uint8_t  FIFO[512];                         /* +0x200 */

} SCR_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of SCR SFRS.
 */
#define SCR_SFRS ((__IO SCR_SFRS_t *)0x5001e000)

#endif /* end of __SCR_SFR_H__ section */


