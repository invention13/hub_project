/**********************************************************************************************************************
 * @copyright 2017 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file usbhub.h
 *********************************************************************************************************************/

#ifndef __USBHUB_H__
#define __USBHUB_H__

#include "../clough/clough_core.h"
#include "../clough/clough_dbg.h"
#include <stdint.h>


// ********************************************************************************************************************
// compiler specific
// ********************************************************************************************************************

// IAR
#ifdef __ICCARM__
  #include "io_macros.h"
#endif


// ********************************************************************************************************************
// USB hub SFR typedefs
// ********************************************************************************************************************

// AFE disconnect bits typedef
typedef union {
  struct {
    uint8_t      : 5;
    uint8_t APPF : 1;   // set bit to disable the AFE bridge for application  USBDC
    uint8_t H2HH : 1;   // set bit to disable the AFE bridge for host-to-host USBDC (hub facing)
    uint8_t      : 1;
  };
  uint8_t BYTE;
} USBHUB_BRDSCN_t;

// portstate enum
#define USB_PORTSTATE_NOTCFG     ((uint8_t) 0x00)
#define USB_PORTSTATE_POWOFF     ((uint8_t) 0x01)
#define USB_PORTSTATE_DISCON     ((uint8_t) 0x02)
#define USB_PORTSTATE_DISABLED   ((uint8_t) 0x03)
#define USB_PORTSTATE_RESET      ((uint8_t) 0x04)
#define USB_PORTSTATE_ENABLED    ((uint8_t) 0x05)
#define USB_PORTSTATE_TRANSMIT   ((uint8_t) 0x06)
#define USB_PORTSTATE_TRANSMITR  ((uint8_t) 0x07)
#define USB_PORTSTATE_SUSPEND    ((uint8_t) 0x08)
#define USB_PORTSTATE_RESUME     ((uint8_t) 0x09)
#define USB_PORTSTATE_SENDEOR    ((uint8_t) 0x0A)
#define USB_PORTSTATE_RESTARTS   ((uint8_t) 0x0B)
#define USB_PORTSTATE_RESTARTE   ((uint8_t) 0x0C)
#define USB_PORTSTATE_TEST       ((uint8_t) 0x0D)
#define USB_PORTSTATE_WLPM       ((uint8_t) 0x0E)
#define USB_PORTSTATE_L1SUSPEND  ((uint8_t) 0x0F)
#define USB_PORTSTATE_L1RESUME   ((uint8_t) 0x10)
#define USB_PORTSTATE_RESTARTL1S ((uint8_t) 0x11)


// PORT STATE CHANGE Register
typedef union {
  struct {
    uint32_t PORT1      : 1;    // bit set if port state has changed.  clear on write.  note that index N is for hub port N+1:
    uint32_t PORT2      : 1;    // bit set if port state has changed.  clear on write.  note that index N is for hub port N+1:
    uint32_t PORT3      : 1;    // bit set if port state has changed.  clear on write.  note that index N is for hub port N+1:
    uint32_t PORT4      : 1;    // bit set if port state has changed.  clear on write.  note that index N is for hub port N+1:
    uint32_t PORT5      : 1;    // bit set if port state has changed.  clear on write.  note that index N is for hub port N+1:
    uint32_t            : 3;
    uint32_t MASK1      : 4;    // Enables the Port State Change Interrupt for PORT1. Enabled by default.
    uint32_t MASK2      : 4;    // Enables the Port State Change Interrupt for PORT2. Enabled by default.
    uint32_t MASK3      : 4;    // Enables the Port State Change Interrupt for PORT3. Enabled by default.
    uint32_t MASK4      : 4;    // Enables the Port State Change Interrupt for PORT4. Enabled by default.
    uint32_t MASK5      : 4;    // Enables the Port State Change Interrupt for PORT5. Enabled by default.
    uint32_t            : 3;
    uint32_t MODE       : 1;
    };
  uint32_t WORD;
} PORTSTCHG_t;


#define DWN_PORTSTATE_POWERED     ((uint8_t) 0x01)
#define DWN_PORTSTATE_CONNECTED   ((uint8_t) 0x02)
#define DWN_PORTSTATE_ENABLED     ((uint8_t) 0x04)
#define DWN_PORTSTATE_SUSPENDED   ((uint8_t) 0x08)
// PORT STATE CHANGE MONITOR Register
typedef union {
  struct {
    uint32_t PORT1      : 4;
    uint32_t PORT2      : 4;
    uint32_t PORT3      : 4;
    uint32_t PORT4      : 4;
    uint32_t PORT5      : 4;
    uint32_t            : 12;
    };
  uint32_t WORD;
} DWNPORTSTATEMONITOR_t;


// AFE switching typedef
#define USBHUB_PORTSW_DISABLED ((uint8_t) 0x00)
#define USBHUB_PORTSW_HUBPORT  ((uint8_t) 0x01)
#define USBHUB_PORTSW_H2HD     ((uint8_t) 0x02)
typedef union {
  struct {
    // mapping for the connectorized USB ports.  each port receives two bits:
    //   00 => PORT DISABLED
    //   01 => AFE <=> HUB
    //   10 => AFE <=> H2HD
    uint8_t PORT1 : 2;    // port 1
    uint8_t PORT2 : 2;    // port 2
    uint8_t PORT3 : 2;    // port 3
  };
  uint8_t BYTE;
} USBHUB_PORTSW_t;

// power on reset control typedef (now mostly other functions...)
typedef union {
  struct {
    uint8_t         : 5;
    uint8_t PLLRST  : 1;   // if set then PLL is forced into reset
    uint8_t VBUSDIS : 1;   // set to force hub to think upstream VBUS is not present.  hub will detach.
    uint8_t VBUSVAL : 1;   // current value of upstream VBUS detection, as perceived by the hub
  };
  uint8_t BYTE;
} USBHUB_PORCTRL_t;

// host to host device controller settings
typedef union {
  struct {
    uint8_t         : 5;
    uint8_t VBUSVAL : 1;  // control H2H device facing controller's perception of VBUS valid
    uint8_t PD      : 1;  // force DP/DM pull downs on
    uint8_t FASTRST : 1;  // produce a faster than spec-compliant line reset time when set
  };
  uint8_t BYTE;
} USBHUB_H2HDCTRL_t;

// IRQ disambiguation register
typedef union {
  struct {
    uint8_t TIMER : 1;
    uint8_t VBUS  : 1;
    uint8_t       : 6;
  };
  uint8_t BYTE;
} USBHUB_IRQID_t;

// DP/DM direct control registers
typedef union {
  struct {
    uint8_t SRC   : 1;  // if set, then this pull control overrides in this register have effect
    uint8_t DPPDN : 1;  // if SRC set, then this controls DP pulldown resistor enable
    uint8_t DMPUP : 1;  // if SRC set, then this controls DM pullup   resistor enable
    uint8_t DMPDN : 1;  // if SRC set, then this controls DM pulldown resistor enable
    uint8_t       : 4;
  };
  uint8_t BYTE;
} USBHUB_PULLCTRL_t;

// downstream vbus pull up control
typedef union {
  struct {
    uint8_t       : 4;
    uint8_t PORT1 : 1;
    uint8_t PORT2 : 1;
    uint8_t PORT3 : 1;
    uint8_t       : 1;
  };
  uint8_t BYTE;
} USBHUB_DWNVBUSPU_t;

// downstream vbus pull down control
typedef union {
  struct {
    uint16_t PDSEL1 : 1;  // pull down source select (per port):
    uint16_t PDSEL2 : 1;  //   0 => hardware decides
    uint16_t PDSEL3 : 1;  //   1 => direct firmware control
    uint16_t FW1    : 1;  // firmware control value (per port)
    uint16_t FW2    : 1;
    uint16_t FW3    : 1;
    uint16_t POL1   : 1;  // hardware mode:  polarity of signal deciding pull down enable (per port)
    uint16_t POL2   : 1;
    uint16_t POL3   : 1;
    uint16_t        : 7;
  };
  uint16_t HWORD;
} USBHUB_DWNVBUSPD_t;

// over-voltage detect register mapping (various purposes)
typedef union {
  struct {
    uint8_t UP  : 1;  // upstream port
    uint8_t DN1 : 1;  // downstream port 1
    uint8_t DN2 : 1;  // downstream port 2
    uint8_t DN3 : 1;  // downstream port 3
    uint8_t     : 4;
  };
  uint8_t BYTE;
} USBHUB_OVD_t;

// PHY trims
typedef union {
  struct {
    uint8_t SORPFLT : 1;  // implement start-of-receive packet SYNC filtering
    uint8_t WRAPDET : 1;  // implement buffer overrun detection
    uint8_t         : 2;
    uint8_t SYNCEXT : 2;  // SYNC field extension control
    uint8_t EOI     : 2;  // elasticity buffer priming depth control
  };
  uint8_t BYTE;
} USBHUB_PHYTRIM_t;

// upstream state:  RX
#define USBHUB_UPSTATERX_RESET          ((uint8_t) 0x00)
#define USHHUB_UPSTATERX_RECEIVINGJ     ((uint8_t) 0x01)
#define USHBUB_UPSTATERX_SUSPEND        ((uint8_t) 0x02)
#define USHBUB_UPSTATERX_RECEIVINGK     ((uint8_t) 0x03)
#define USBHUB_UPSTATERX_RESUME         ((uint8_t) 0x04)
#define USBHUB_UPSTATERX_RECEIVINGSE0   ((uint8_t) 0x05)
#define USBHUB_UPSTATERX_RECEIVINGIS    ((uint8_t) 0x06)
#define USBHUB_UPSTATERX_RECEIVINGHJK   ((uint8_t) 0x07)
#define USBHUB_UPSTATERX_L1SUSPEND      ((uint8_t) 0x08)
#define USBHUB_UPSTATERX_L1RESUME       ((uint8_t) 0x09)
#define USBHUB_UPSTATERX_L1RECEIVINGSE0 ((uint8_t) 0x0A)

// upstream state: TX
#define USBHUB_UPSTATETX_INACTIVE       ((uint8_t) 0x00)
#define USBHUB_UPSTATETX_ACTIVE         ((uint8_t) 0x01)
#define USBHUB_UPSTATETX_REPEATINGSE0   ((uint8_t) 0x02)
#define USBHUB_UPSTATETX_SENDJ          ((uint8_t) 0x03)
#define USBHUB_UPSTATETX_GEOPTU         ((uint8_t) 0x04)
#define USBHUB_UPSTATETX_SRESUME        ((uint8_t) 0x05)
#define USBHUB_UPSTATETX_L1RESUME       ((uint8_t) 0x06)

// upstream port state
typedef union {
  struct {
    uint8_t RX;  // use USBHUB_UPSTATERX_xxx defines
    uint8_t TX;  // use USBHUB_UPSTATETX_xxx defines
  };
  uint16_t HWORD;
} USBHUB_UPSTATE_t;

// LED indicator from hub controller
typedef union {
  struct {
    uint16_t PORT1 : 2;
    uint16_t PORT2 : 2;
    uint16_t PORT3 : 2;
    uint16_t PORT4 : 2;
    uint16_t PORT5 : 2;
    uint16_t       : 6;
  };
  uint16_t HWORD;
} USBHUB_LED_t;

// descriptor memory.  this memory is mapped strangely (even moreso than Boston).  Although a USB bytestream,
// it *must* populated into the SFR memory in 16 bit chunks.  But the address offsets are 32 bits.  So
// that to write a descriptor with byte contents B[] in memory the mappint is:
// address offset:  0x00   0x01  0x02    0x03     0x04  0x05  0x06     0x07    0x08  ...
//                  B[0]   B[1]  ignored ignored  B[2]  B[3]  ignored  ignored B[4]  ...
// see convenience function in this file for example of how to efficiently write to this "memory"
#define USBHUB_DESCRIPTSFR_NHWORDS 256
typedef struct {
  uint16_t HWORD;                      // these two bytes are actual descriptor contents
  uint16_t IGNORED;                    // these two bytes are ignored by hardware
} USBHUB_DESCRIPTSFT_ELEM16_t;         // one element of the SFR memory array



// ********************************************************************************************************************
// USB hub SFR map
// ********************************************************************************************************************

typedef struct {
  uint8_t             _RESERVED_00;   // offset 0x00
  USBHUB_BRDSCN_t     BRDSCN;         // offset 0x01
  uint16_t            _RESERVED_02;   // offset 0x02 ... 0x03

  uint8_t             PORTSTATE[5];   // offset 0x04 ... 0x08
                                      // downstream port internal state.  note that index N is for hub port N+1:
                                      // coded as per USB_PORTSTATE_xxx defines
  uint8_t             ATTACHN;        // offset:  0x09
                                      // each bit in vector indicates whether the corresponding downstream port
                                      //   has a device attached to it.  Active low.  note that bit index N is for
                                      //   hub port N+1.  Bit positions for which there is no physical port always
                                      //   read 0.
  uint16_t            _RESERVED_0A;   // offset: 0x0A .. 0x0B

  uint32_t            _RESERVED_0C;   // offset: 0x0C ... 0x0F
  uint32_t            _RESERVED_10;   // offset: 0x10 ... 0x13

  PORTSTCHG_t         PORTSTCHG;      // offset: 0x14 ... 0x17

  uint16_t            _RESERVED_18;   // offset: 0x18 ... 0x19
  USBHUB_PORTSW_t     PORTSW;         // offset: 0x1A
  uint8_t             _RESERVED_19;   // offset: 0x1B

  uint8_t             SUSPENDFORCE;   // write value of 1 to force the non-hub logic to think the hub is in suspend.
                                      // this has the following effects:
                                      //   - if the hub is the only thing keeping the PLL on, the PLL will turn off and
                                      //     system clock will revert to RC oscillator.  another register in the clock
                                      //     control SFR can maintain the PLL on even with this bit enabled.
                                      //   - if PLL turns off by above mechanism, then upstream port of hub will
                                      //     revert to FS idle, causing detach from USB network.
                                      // offset: 0x1C
  uint8_t             _RESERVED_1D;   // offset: 0x1D
  uint16_t            _RESERVED_1E;   // offset: 0x1E ... 0x1F


  USBHUB_PORCTRL_t    PORCTRL;        // offset: 0x20
  uint8_t             _RESERVED_21;   // offset: 0x21
  uint16_t            _RESERVED_22;   // offset: 0x22 ... 0x23

  uint32_t            _RESERVED_24;   // offset: 0x24 ... 0x27

  USBHUB_H2HDCTRL_t   H2HDCTRL;       // offset: 0x28
  uint8_t             _RESERVED_29;   // offset: 0x29
  uint16_t            _RESERVED_2A;   // offset: 0x2A ... 0x2B

  USBHUB_IRQID_t      IRQID;          // offset: 0x2C
  uint8_t             _RESERVED_2D;   // offset: 0x2D
  uint16_t            _RESERVED_2E;   // offset: 0x2E ... 0x2F

  USBHUB_PULLCTRL_t   UPPULLCTRL;     // pull up.down override control for upstream port
                                      // offset: 0x30
  USBHUB_PULLCTRL_t   DNPULLCTRL[3];  // pull up/down override control for downstream ports.
                                      // note that index N is for hub port N+1
                                      // offset: 0x31 ... 0x33

  uint16_t            _RESERVED_34;   // offset: 0x34 ... 0x35
  uint8_t             _RESERVED_36;   // offset: 0x36
  USBHUB_DWNVBUSPU_t  DWNVBUSPU;      // offset: 0x37

  USBHUB_DWNVBUSPD_t  DWNVBUSPD;      // offset: 0x38 ... 0x39
  uint16_t            _RESERVED_3A;   // offset: 0x3A ... 0x3B

  uint32_t            _RESERVED_3C;   // offset: 0x3C ... 0x3F

  // over-voltage detection related
  struct {
    USBHUB_OVD_t      STAT;           // offset: 0x40.  current detector reading
    uint8_t           _RESERVED_41;
    uint16_t          _RESERVED_42;

    USBHUB_OVD_t      IRQ;            // offset: 0x44.  irq status
    uint8_t           _RESERVED_45;
    uint16_t          _RESERVED_46;

    USBHUB_OVD_t      IE;             // offset: 0x48.  irq enable
    uint8_t           _RESERVED_49;
    uint16_t          _RESERVED_4A;

    USBHUB_OVD_t      IRQCLR;         // offset: 0x4C.  irq clear.  self-clears.
    uint8_t           _RESERVED_4D;
    uint16_t          _RESERVED_4E;   // offset: 0x4F
  } OVD;

  struct {
    USBHUB_PHYTRIM_t  UP;             // offset: 0x50.  upstream port.
    uint8_t           _RESERVED_51;
    uint16_t          _RESERVED_52;

    USBHUB_PHYTRIM_t  DN1;            // offset: 0x54.  downstream port 1.
    uint8_t           _RESERVED_55;
    uint16_t          _RESERVED_56;

    USBHUB_PHYTRIM_t  DN2;            // offset: 0x58.  downstream port 2.
    uint8_t           _RESERVED_59;
    uint16_t          _RESERVED_5A;

    USBHUB_PHYTRIM_t  DN3;            // offset: 0x5C.  downstream port 3.
    uint8_t           _RESERVED_5D;
    uint16_t          _RESERVED_5E;   // offset: 0x5F
  } PHYTRIM;

  USBHUB_UPSTATE_t    UPSTATE;        // offset: 0x60 ... 0x61.  ustream port state
  uint16_t            _RESERVED_62;   // offset: 0x62 ... 0x63

  uint8_t             WUCONN;         // offset: 0x64.  write 1 to have hub immediately attempt remote wakeup if
                                      // suspended with a connected port.
  uint8_t             _RESERVED_65;   // offset: 0x65
  uint16_t            _RESERVED_66;   // offset: 0x66 ... 0x67

  DWNPORTSTATEMONITOR_t DWNPORTSTATEMONITOR; // offset: 0x68 ... 0x6B

  uint8_t             PORTREMAP;     // offset: 0x6C
  uint8_t             _RESERVED_6D;   // offset: 0x6D
  uint16_t            _RESERVED_6E;   // offset: 0x6E ... 0x6F

  uint32_t            _RESERVED_70[36]; // offset: 0x70 ... 0xFF

  USBHUB_LED_t        LED;            // offset: 0x100 ... 0x101
  uint16_t            _RESERVED_102;  // offset: 0x102 ... 0x103
} USBHUB_SFRS_t;


// USBPORTTEST.PORTTEST.PORT enum
#define USBPORTTEST_NONE    ((uint8_t) 0x00)
#define USBPORTTEST_J       ((uint8_t) 0x01)
#define USBPORTTEST_K       ((uint8_t) 0x02)
#define USBPORTTEST_SE0NAK  ((uint8_t) 0x03)
#define USBPORTTEST_TESTPKT ((uint8_t) 0x04)
#define USBPORTTEST_FRCENA  ((uint8_t) 0x05)
#define USBPORTTEST_EXIT    ((uint8_t) 0x07)
#define USBPORTTEST_TX55    ((uint8_t) 0x08)
#define USBPORTTEST_TXAA    ((uint8_t) 0x09)
#define USBPORTTEST_TX66    ((uint8_t) 0x0A)
#define USBPORTTEST_RPTENA  ((uint8_t) 0x0C)
#define USBPORTTEST_LOOPTX  ((uint8_t) 0x0D)
#define USBPORTTEST_LOOPRX  ((uint8_t) 0x0E)
#define USBPORTTEST_LOOPSLF ((uint8_t) 0x0F)

// typedef for loopback test counter
typedef union {
  uint8_t  PORT[4];
  uint32_t WORD;
} USBPORTEST_CNTOK_t;

// typedef for loopback tester SFRs
typedef struct {
  union {
    uint8_t  PORT[4];
    uint32_t WORD;
  } PORTTEST;                // offset 0x00

  USBPORTEST_CNTOK_t CNTOK;  // offset 0x04

  union {
    struct {
      uint8_t  NPKTS    : 3;    // number of packets to test
      uint8_t           : 1;
      uint8_t  TESTSYNC : 1;    // if set then sync field should be tested
      uint8_t  TESTDRBL : 1;    // if set then dribble length should be tested
      uint8_t           : 1;
      uint8_t  ENA      : 1;    // test enable.  self-clearing.
    };
    uint8_t BYTE;            // offset 0x05
  } CTRL;
} USBPORTEST_SFRS_t;

// BUGFIX_CTRL
typedef union {
  struct {
    uint8_t TT_DISCON_FIX_DISABLE        : 5;
    uint8_t WUCONN_FIX_DISABLE           : 1;
    uint8_t CHANGE_BIT_CLEAR_FIX_DISABLE : 1;
    uint8_t PATCH_REMOTE_WAKEUP_N        : 1;
    uint8_t PATCH_CRAZYK_N               : 1;
    uint32_t                             : 23;
    };
  uint32_t WORD;
} USBHUB_BUGFIX_CTRL_t;

#define _USBHUB                 ((volatile USBHUB_SFRS_t          *) (ASIC_16B_BASE + 0x3000))         /* hub SFRs                           */
#define _USBHUB_DESCRIPT_HS     ((USBHUB_DESCRIPTSFT_ELEM16_t     *) (ASIC_16B_BASE + 0x3400        )) /* hub descriptor base address (HS)   */
#define _USBHUB_DESCRIPT_FS     ((USBHUB_DESCRIPTSFT_ELEM16_t     *) (ASIC_16B_BASE + 0x3400 + 0x200)) /* hub descriptor base address (FS)   */
#define _USBPORTTEST            ((volatile USBPORTEST_SFRS_t      *) (ASIC_16B_BASE + 0x3300))         /* port test SFRs                     */
#define _USBHUB_BUGFIX_CTRL     ((volatile USBHUB_BUGFIX_CTRL_t   *) (ASIC_16B_BASE + 0x3310))         /* bug fix control SFRs               */
#define _USBHUB_TDCNN_TRIM_CTRL ((volatile uint8_t                *) (ASIC_16B_BASE + 0x3314))         /* increasing DFP connect event timer */

#ifdef __SFR_MAP_ASOBJ__
  // optional: provide as struct
  #ifdef __ICCARM__
    // IAR style
    volatile __no_init USBHUB_SFRS_t        USBHUB                 @ (ASIC_16B_BASE + 0x3000);
    volatile __no_init USBPORTEST_SFRS_t    USBPORTTEST            @ (ASIC_16B_BASE + 0x3300);
    volatile __no_init USBHUB_BUGFIX_CTRL_t USBHUB_BUGFIX_CTRL     @ (ASIC_16B_BASE + 0x3310);
    volatile __no_init uint8_t              USBHUB_TDCNN_TRIM_CTRL @ (ASIC_16B_BASE + 0x3314);
  #else
    // generic style
    #define USBHUB                 (* _USBHUB)
    #define USBPORTTEST            (* _USBPORTTEST)
    #define USBHUB_BUGFIX_CTRL     (* _USBHUB_BUGFIX_CTRL)
    #define USBHUB_TDCNN_TRIM_CTRL (* _USBHUB_TDCNN_TRIM_CTRL)
  #endif
#else
  // default : provide as struct pointer
  #define USBHUB                 _USBHUB
  #define USBPORTTEST            _USBPORTTEST
  #define USBHUB_BUGFIX_CTRL     _USBHUB_BUGFIX_CTRL
  #define USBHUB_TDCNN_TRIM_CTRL _USBHUB_TDCNN_TRIM_CTRL
#endif


// release the reset and VBUS bypass so that the hub can start up
__STATIC_INLINE void USBHUB_releaseReset() {
  register USBHUB_PORCTRL_t wdata;
  wdata.BYTE    = 0;
  wdata.PLLRST  = 0;   // PLL not held in reset
  wdata.VBUSDIS = 0;   // VBUS not blocked to hub
  _USBHUB->PORCTRL = wdata;
}

// SDK:  this is a feature for only our own test convenience.  The customer would never need this.
// Moved to verif/util/include/usbhub_tests.h
// enable fast down port reset feature
//__STATIC_INLINE void USBHUB_enablefastReset() {
//  _USBHUB->H2HDCTRL.FASTRST = 1;
//}

// SDK:  disable the disable?  This is a horrible function name.
// - changed name to something less goofy.
// - provided parameterization
// Connect the AFE_BRIDGE
__STATIC_INLINE void setAfeBridgeConnect(
  uint8_t bmCon    // bitmap for which ports to bridge.  USB numbered, so
                   //   bmCon[4] is the application function
                   //   bmCon[5] is the host-to-host hub facing device
) {
  register USBHUB_BRDSCN_t wdata;
  wdata.BYTE = 0;
  wdata.APPF = (bmCon & (1<<4)) ? 0 : 1;  // register controls DISABLE
  wdata.H2HH = (bmCon & (1<<5)) ? 0 : 1;  // register controls DISABLE
  _USBHUB->BRDSCN = wdata;
}

// write descriptor to "ROM".  If the contents are in memory (flash or SRAM) can just use this.  If the contents
// need to be dynamically constructed, use this function as an example.
__STATIC_INLINE void USBHUB_descriptorMemcpy(
  const uint8_t* src,     // pointer to source data.  source data should be a simple memory map, e.g.:
                          // offset:  0x00 0x01 0x02 0x03 0x04 ...
                          //          B[0] B[1] B[2] B[3] B[4] ...
  const uint16_t nbytes,  // number of bytes from source data to populate
  const uint8_t  isFS     // set nonzero if this should be populated into the FS desciptor, otherwise HS
) {
  uint16_t* dptr;  // destination pointer
  const uint8_t*  sptr;  // source      pointer
  const uint8_t*  eptr;  // end         pointer (source referenced)
  union {
    uint8_t  BYTE[2];
    uint16_t HWORD;
  } dbfr;                   // data buffer, for assembly of write data

  // check for data too much data for buffer to handle
  if( nbytes > (USBHUB_DESCRIPTSFR_NHWORDS * 2) ) {
    dbgTrapErr();
  }

  // perform the copy.  if performance of this bit needs improvement, we can require the source pointer to
  // be 16 bit aligned and not bother with dbfr re-alignment.  this version is flexible and slightly slower...
  if( isFS ) {
    dptr = (uint16_t *) _USBHUB_DESCRIPT_FS; // base address of SFR "memory"
  } else {
    dptr = (uint16_t *) _USBHUB_DESCRIPT_HS; // base address of SFR "memory"
  }
  sptr = src;                                // source pointer
  eptr = src + nbytes;                       // end pointer
  while( sptr < eptr ) {                     // while still more data to copy
    dbfr.BYTE[0] = *(sptr++);                // populate LS byte of buffer
    if( sptr < eptr ) {                      // check if more data still
      dbfr.BYTE[1] = *(sptr++);              // populate MS byte of buffer
    } else {
      dbfr.BYTE[1] = 0;                      // fill MS byte with zero
    }
    *dptr = dbfr.HWORD;                      // write the 16 bit chunk
    dptr  += 2;                              // increment pointer as usual, then increment again to skip ignored addresses
  };
}

#endif // include guard
