/**
 * @copyright 2019 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file usbafedfp2_sfr.h
 */

#ifndef __USBAFEDFP2_SFR_H__
#define __USBAFEDFP2_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for USBAFEDFP2.
 */
typedef struct {

  union {
    struct {
      uint8_t  BIASENA                  :  1; /*!< bias_ena */
      uint8_t  AFEENA                   :  1; /*!< afe_ena */
      uint8_t  USBPOL                   :  1; /*!< usb_pol */
      uint8_t  FCETPOL                  :  1; /*!< fcet_port_pol */
      uint8_t  OVDENA                   :  1; /*!< ovd_ena */
      uint8_t  OVDAUTO                  :  1; /*!< ovd_auto */
      uint8_t  SE0AFEDGLTCH             :  1; /*!< se0_afe_dgltch */
      uint8_t  FSLSSE0FLTBYP            :  1; /*!< fsls_se0flt_byp */
      uint8_t  RTERMTRIM                :  4; /*!< rterm_trim */
      uint8_t  SE0GLITCHSEL             :  1; /*!< se0_glitch_sel */
      uint8_t  CAPTRIM                  :  2; /*!< cap_trim */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  HSSQLCHTRIM              :  3; /*!< hs_sqlch_trim */
      uint8_t  HSDSCNTRIM               :  3; /*!< hs_dscn_trim */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  HSTXCSTRIM               :  4; /*!< hs_txcs_trim */
      uint8_t  HSTXRTTRIM               :  4; /*!< hs_txrt_trim */
    };
    uint32_t WORD;
  } CTRL1; /* +0x000 */

  union {
    struct {
      uint8_t  FSLSVILTRIM              :  2; /*!< fsls_vil_trim */
      uint8_t  FSLSVIHTRIM              :  2; /*!< fsls_vih_trim */
      uint8_t  FSLSRPTLAT               :  1; /*!< fsls_rptlat */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  FSLSRPTDLY               :  2; /*!< fsls_rptdly */
      uint8_t  FSLSSE0FLTPWTRIM         :  3; /*!< fsls_se0flt_pw_trim */
      uint8_t  FSEOPRTDLY               :  1; /*!< fs_eop_rtdly */
      uint8_t  FSLSSE0FLTFETRIM         :  4; /*!< fsls_se0flt_fe_trim */
      uint8_t  CDRIDLMONENA             :  1; /*!< cdr_idlmon_ena */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  CDRFLSHTRIM              :  1; /*!< cdr_flsh_trim */
      uint8_t  CDRSYNCTRIM              :  2; /*!< cdr_sync_trim */
      uint8_t  CDRDRBLTRIM              :  1; /*!< cdr_drbl_trim */
      uint8_t  CDRTXMSKBYP              :  1; /*!< cdr_txmsk_byp */
      uint8_t  FCETDCPSHRT              :  1; /*!< fcet_dcp_shrt */
      uint8_t  FCETCDPTXENA             :  1; /*!< fcet_cdp_txena */
      uint8_t  FCETCDPRXENA             :  1; /*!< fcet_cdp_rxena */
      uint8_t  FCETCDPRXD               :  1; /*!< fcet_cdp_rxd */
      uint8_t  FCETAPPLRSELDP           :  2; /*!< fcet_appl_rselDP */
      uint8_t  FCETAPPLRSELDM           :  2; /*!< fcet_appl_rselDM */
    };
    uint32_t WORD;
  } CTRL2; /* +0x004 */

  union {
    struct {
      uint8_t  TERMCTRL                 :  1; /*!< term_ctrl */
      uint8_t  ENACTRL                  :  1; /*!< ena_ctrl */
      uint8_t  DRVCTRL                  :  1; /*!< drv_ctrl */
      uint8_t  SUSPENDCTRL              :  1; /*!< suspend_ctrl */
      uint8_t  DIGLOOPCTRL              :  1; /*!< digloop_ctrl */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
      uint8_t  FSLSRXD                  :  1; /*!< fsls_rxd */
      uint8_t  FSLSRXSE0                :  1; /*!< fsls_rxse0 */
      uint8_t  FSLSRXDM                 :  1; /*!< fsls_rxdm */
      uint8_t  FSLSRXDP                 :  1; /*!< fsls_rxdp */
      uint8_t  HSRXD                    :  1; /*!< hs_rxd */
      uint8_t  HSSQLCH                  :  1; /*!< hs_sqlch */
      uint8_t  HSDSCN                   :  1; /*!< hs_dscn */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } TESTCTRL; /* +0x008 */

  union {
    struct {
      uint8_t  SFRDMRPD                 :  1; /*!< sfr_dm_rpd */
      uint8_t  SFRDPRPD                 :  1; /*!< sfr_dp_rpd */
      uint8_t  SFRDMRPU                 :  2; /*!< sfr_dm_rpu */
      uint8_t  SFRDPRPU                 :  2; /*!< sfr_dp_rpu */
      uint8_t  SFRTERMON                :  1; /*!< sfr_termon */
      uint8_t  PUPSTRDSBL               :  1; /*!< pupstr_dsbl */
      uint8_t  SFRDSCNENA               :  1; /*!< sfr_dscn_ena */
      uint8_t  SFRSQLCHENA              :  1; /*!< sfr_sqlch_ena */
      uint8_t  SFRSERXENA               :  1; /*!< sfr_se_rxena */
      uint8_t  SFRFSLSRXENA             :  1; /*!< sfr_fsls_rxena */
      uint8_t  SFRHSRXENA               :  1; /*!< sfr_hs_rxena */
      uint8_t  SFRHSCDRENA              :  1; /*!< sfr_hs_cdrena */
      uint8_t  SFRSUSPEND               :  1; /*!< sfr_suspend */
      uint8_t  SFRDIGLOOP               :  1; /*!< sfr_digloop */
      uint8_t  SFRFSLSTXOE              :  1; /*!< sfr_fsls_txoe */
      uint8_t  SFRFSLSTXD               :  1; /*!< sfr_fsls_txd */
      uint8_t  SFRFSLSLSMODE            :  1; /*!< sfr_fsls_lsmode */
      uint8_t  SFRFSLSTXSE0             :  1; /*!< sfr_fsls_txse0 */
      uint8_t  SFRHSTXENA               :  1; /*!< sfr_hs_txena */
      uint8_t  SFRHSTXSE0N              :  1; /*!< sfr_hs_txse0_n */
      uint8_t  SFRHSTXD                 :  1; /*!< sfr_hs_txd */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
    };
    uint32_t WORD;
  } OVRCTRL; /* +0x00C */

} USBAFEDFP2_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of USBAFEDFP2 SFRS.
 */
#define USBAFEDFP2_SFRS ((__IO USBAFEDFP2_SFRS_t *)0x50016020)

#endif /* end of __USBAFEDFP2_SFR_H__ section */


