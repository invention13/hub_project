/**
 * @copyright 2019 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file usbafecdr_sfr.h
 */

#ifndef __USBAFECDR_SFR_H__
#define __USBAFECDR_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for USBAFECDR.
 */
typedef struct {

  union {
    struct {
      uint8_t  CURPH                    :  8;
      uint8_t  FRCPH                    :  3;
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  PHFORCE                  :  1;
      uint8_t  PHROT                    :  2;
      uint8_t                           :  1; /*   (reserved) */
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } UFPPHASE; /* +0x000 */

  union {
    struct {
      uint8_t  CLKDLYENA                :  1;
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  CLKFT                    :  2;
      uint8_t  CLKRT                    :  2;
      uint8_t  RXDDLYENA                :  1;
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  RXDFT                    :  2;
      uint8_t  RXDRT                    :  2;
      uint8_t  CLKSEL                   :  8;
      uint8_t  FASTAQ                   :  1;
      uint8_t  SQLCHMSK                 :  1;
      uint8_t                           :  6; /*   (reserved) */
    };
    uint32_t WORD;
  } UFPTRIM; /* +0x004 */

  union {
    struct {
      uint8_t  CURPH                    :  8;
      uint8_t  FRCPH                    :  3;
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  PHFORCE                  :  1;
      uint8_t  PHROT                    :  2;
      uint8_t                           :  1; /*   (reserved) */
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } DFP1PHASE; /* +0x008 */

  union {
    struct {
      uint8_t  CLKDLYENA                :  1;
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  CLKFT                    :  2;
      uint8_t  CLKRT                    :  2;
      uint8_t  RXDDLYENA                :  1;
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  RXDFT                    :  2;
      uint8_t  RXDRT                    :  2;
      uint8_t  CLKSEL                   :  8;
      uint8_t  FASTAQ                   :  1;
      uint8_t  SQLCHMSK                 :  1;
      uint8_t                           :  6; /*   (reserved) */
    };
    uint32_t WORD;
  } DFP1TRIM; /* +0x00C */

  union {
    struct {
      uint8_t  CURPH                    :  8;
      uint8_t  FRCPH                    :  3;
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  PHFORCE                  :  1;
      uint8_t  PHROT                    :  2;
      uint8_t                           :  1; /*   (reserved) */
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } DFP2PHASE; /* +0x010 */

  union {
    struct {
      uint8_t  CLKDLYENA                :  1;
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  CLKFT                    :  2;
      uint8_t  CLKRT                    :  2;
      uint8_t  RXDDLYENA                :  1;
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  RXDFT                    :  2;
      uint8_t  RXDRT                    :  2;
      uint8_t  CLKSEL                   :  8;
      uint8_t  FASTAQ                   :  1;
      uint8_t  SQLCHMSK                 :  1;
      uint8_t                           :  6; /*   (reserved) */
    };
    uint32_t WORD;
  } DFP2TRIM; /* +0x014 */

  union {
    struct {
      uint8_t  CURPH                    :  8;
      uint8_t  FRCPH                    :  3;
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  PHFORCE                  :  1;
      uint8_t  PHROT                    :  2;
      uint8_t                           :  1; /*   (reserved) */
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } DFP3PHASE; /* +0x018 */

  union {
    struct {
      uint8_t  CLKDLYENA                :  1;
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  CLKFT                    :  2;
      uint8_t  CLKRT                    :  2;
      uint8_t  RXDDLYENA                :  1;
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  RXDFT                    :  2;
      uint8_t  RXDRT                    :  2;
      uint8_t  CLKSEL                   :  8;
      uint8_t  FASTAQ                   :  1;
      uint8_t  SQLCHMSK                 :  1;
      uint8_t                           :  6; /*   (reserved) */
    };
    uint32_t WORD;
  } DFP3TRIM; /* +0x01C */

} USBAFECDR_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of USBAFECDR SFRS.
 */
#define USBAFECDR_SFRS ((__IO USBAFECDR_SFRS_t *)0x50016500)

#endif /* end of __USBAFECDR_SFR_H__ section */


