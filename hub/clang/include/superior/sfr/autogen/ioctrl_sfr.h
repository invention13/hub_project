/**
 * @copyright 2019 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file ioctrl_sfr.h
 */

#ifndef __IOCTRL_SFR_H__
#define __IOCTRL_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for IOCTRL.
 */
typedef struct {

  union {
    struct {
      uint8_t  GP1MODE                  :  8; /*!< Mode */
      uint8_t  GP1PD                    :  1; /*!< Pull-Down */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP1PU                    :  1; /*!< Pull-Up */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP1RE                    :  1; /*!< Read Enable */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  TESTMUX_SELECT           :  4; /*!< Testmux Select */
      uint8_t                           :  8; /*   (reserved) */
    };
    uint32_t WORD;
  } GPIO1CTRL; /* +0x000 */

  union {
    struct {
      uint8_t  GP2MODE                  :  8; /*!< Mode */
      uint8_t  GP2PD                    :  1; /*!< Pull-Down */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP2PU                    :  1; /*!< Pull-Up */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP2RE                    :  1; /*!< Read Enable */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  PRTCTL3SEL               :  1; /*!< Source Select */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GP2DINPOLARITY           :  1; /*!< Data In Polarity Control */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  GP2CHSEL                 :  3; /*!< PWM Barium Channel Select */
      uint8_t  GP2POLARITY              :  1; /*!< Polarity Control */
    };
    uint32_t WORD;
  } GPIO2CTRL; /* +0x004 */

  union {
    struct {
      uint8_t  GP3MODE                  :  3; /*!< Mode */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  GP3OE                    :  1; /*!< Output Enable */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP3PD                    :  1; /*!< Pull-Down */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP3PU                    :  1; /*!< Pull-Up */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP3RE                    :  1; /*!< Read Enable */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP3PUPOLARITY            :  1; /*!< Pull Up Polarity Control */
      uint8_t  GP3PDPOLARITY            :  1; /*!< Pull Down Polarity Control */
      uint8_t  GP3OEPOLARITY            :  1; /*!< Output Enable Polarity Control */
      uint8_t  GP3DINPOLARITY           :  1; /*!< Data In Polarity Control */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  GP3CHSEL                 :  3; /*!< PWM Barium Channel Select */
      uint8_t  GP3POLARITY              :  1; /*!< Polarity Control */
    };
    uint32_t WORD;
  } GPIO3CTRL; /* +0x008 */

  union {
    struct {
      uint8_t  GP4MODE                  :  8; /*!< Mode */
      uint8_t                           :  8; /*   (reserved) */
      uint8_t  GP4RE                    :  8; /*!< Read Enable */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  GP4CHSEL                 :  3; /*!< PWM Barium Channel Select */
      uint8_t  GP4POLARITY              :  1; /*!< Polarity Control */
    };
    uint32_t WORD;
  } GPIO4CTRL; /* +0x00C */

  union {
    struct {
      uint8_t  GP5MODE                  :  2; /*!< Mode */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GP5OE                    :  1; /*!< Output Enable */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP5PD                    :  1; /*!< Pull-Down */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP5PU                    :  1; /*!< Pull-Up */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP5RE                    :  1; /*!< Read Enable */
      uint8_t                           :  6; /*   (reserved) */
      uint8_t  GP5DINPOLARITY           :  1; /*!< Data In Polarity Control */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  GP5CHSEL                 :  3; /*!< PWM Barium Channel Select */
      uint8_t  GP5POLARITY              :  1; /*!< Polarity Control */
    };
    uint32_t WORD;
  } GPIO5CTRL; /* +0x010 */

  union {
    struct {
      uint8_t  GP6MODE                  :  2; /*!< Mode */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GP6OE                    :  1; /*!< Output Enable */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP6PD                    :  1; /*!< Pull-Down */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP6PU                    :  1; /*!< Pull-Up */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP6RE                    :  1; /*!< Read Enable */
      uint8_t                           :  6; /*   (reserved) */
      uint8_t  GP6DINPOLARITY           :  1; /*!< Data In Polarity Control */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  GP6CHSEL                 :  3; /*!< PWM Barium Channel Select */
      uint8_t  GP6POLARITY              :  1; /*!< Polarity Control */
    };
    uint32_t WORD;
  } GPIO6CTRL; /* +0x014 */

  union {
    struct {
      uint8_t  GP7MODE                  :  3; /*!< Mode */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  GP7OE                    :  1; /*!< Output Enable */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP7PD                    :  1; /*!< Pull-Down */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP7PU                    :  1; /*!< Pull-Up */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP7RE                    :  8; /*!< Read Enable */
      uint8_t  GP7DINPOLARITY           :  1; /*!< Data In Polarity Control */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP7CHSEL                 :  3; /*!< PWM Barium Channel Select */
      uint8_t  GP7POLARITY              :  1; /*!< Polarity Control */
    };
    uint32_t WORD;
  } GPIO7CTRL; /* +0x018 */

  union {
    struct {
      uint8_t  GP8MODE                  :  3; /*!< Mode */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  GP8OE                    :  1; /*!< Output Enable */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP8PD                    :  1; /*!< Pull-Down */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP8PU                    :  1; /*!< Pull-Up */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP8RE                    :  8; /*!< Read Enable */
      uint8_t  GP8DINPOLARITY           :  1; /*!< Data In Polarity Control */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP8CHSEL                 :  3; /*!< PWM Barium Channel Select */
      uint8_t  GP8POLARITY              :  1; /*!< Polarity Control */
    };
    uint32_t WORD;
  } GPIO8CTRL; /* +0x01C */

  union {
    struct {
      uint8_t  GP9MODE                  :  8; /*!< Mode */
      uint8_t  GP9PD                    :  1; /*!< Pull-Down */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP9PU                    :  1; /*!< Pull-Up */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  GP9SONOF                 :  1; /*!< Scmitt Trigger */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP9CONOF                 :  1; /*!< Non-Scmitt Trigger */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP9OEPOLARITY            :  1; /*!< Output Enable Polarity Control */
      uint8_t  GP9DINPOLARITY           :  1; /*!< Data In Polarity Control */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  GP9POLARITY              :  1; /*!< Data Out Polarity Control */
    };
    uint32_t WORD;
  } GPIO9CTRL; /* +0x020 */

  union {
    struct {
      uint8_t  GP10MODE                 :  2; /*!< Mode */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GP10OE                   :  1; /*!< Output Enable */
      uint8_t  GP10OD                   :  1; /*!< Open Drain Enable */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GP10PD                   :  1; /*!< Pull-Down */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP10PU                   :  1; /*!< Pull-Up */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  GP10SONOF                :  1; /*!< Scmitt Trigger */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP10CONOF                :  1; /*!< Non-Scmitt Trigger */
      uint8_t                           :  6; /*   (reserved) */
      uint8_t  GP10POLARITY             :  1; /*!< Data Out Polarity Control */
    };
    uint32_t WORD;
  } GPIO10CTRL; /* +0x024 */

  union {
    struct {
      uint8_t  GP11MODE                 :  3; /*!< Mode */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  GP11OE                   :  1; /*!< Output Enable */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP11PD                   :  1; /*!< Pull-Down */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP11PU                   :  1; /*!< Pull-Up */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  GP11SONOF                :  1; /*!< Scmitt Trigger */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP11CONOF                :  1; /*!< Non-Scmitt Trigger */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP11OEPOLARITY           :  1; /*!< Output Enable Polarity Control */
      uint8_t  GP11DINPOLARITY          :  1; /*!< Data In Polarity Control */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  GP11POLARITY             :  1; /*!< Data Out Polarity Control */
    };
    uint32_t WORD;
  } GPIO11CTRL; /* +0x028 */

  union {
    struct {
      uint8_t  GP12MODE                 :  2; /*!< Mode */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GP12OE                   :  1; /*!< Output Enable */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP12PD                   :  1; /*!< Pull-Down */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP12PU                   :  1; /*!< Pull-Up */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  GP12SONOF                :  1; /*!< Scmitt Trigger */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP12CONOF                :  1; /*!< Non-Scmitt Trigger */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP12OEPOLARITY           :  1; /*!< Output Enable Polarity Control */
      uint8_t  GP12DINPOLARITY          :  1; /*!< Data In Polarity Control */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  GP12POLARITY             :  1; /*!< Data Out Polarity Control */
    };
    uint32_t WORD;
  } GPIO12CTRL; /* +0x02C */

  union {
    struct {
      uint8_t  GP13MODE                 :  2; /*!< Mode */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GP13OE                   :  1; /*!< Output Enable */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP13PD                   :  1; /*!< Pull-Down */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP13PU                   :  1; /*!< Pull-Up */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  GP13SONOF                :  1; /*!< Scmitt Trigger */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP13CONOF                :  1; /*!< Non-Scmitt Trigger */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP13OEPOLARITY           :  1; /*!< Output Enable Polarity Control */
      uint8_t  GP13DINPOLARITY          :  1; /*!< Data In Polarity Control */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  GP13POLARITY             :  1; /*!< Data Out Polarity Control */
    };
    uint32_t WORD;
  } GPIO13CTRL; /* +0x030 */

  union {
    struct {
      uint8_t  GP14MODE                 :  2; /*!< Mode */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GP14OE                   :  1; /*!< Output Enable */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP14PD                   :  1; /*!< Pull-Down */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP14PU                   :  1; /*!< Pull-Up */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  GP14SONOF                :  1; /*!< Scmitt Trigger */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP14CONOF                :  1; /*!< Non-Scmitt Trigger */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP14OEPOLARITY           :  1; /*!< Output Enable Polarity Control */
      uint8_t  GP14DINPOLARITY          :  1; /*!< Data In Polarity Control */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  GP14POLARITY             :  1; /*!< Data Out Polarity Control */
    };
    uint32_t WORD;
  } GPIO14CTRL; /* +0x034 */

  union {
    struct {
      uint8_t  GP15MODE                 :  3; /*!< Mode */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  GP15OE                   :  1; /*!< Output Enable */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP15PD                   :  1; /*!< Pull-Down */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP15PU                   :  1; /*!< Pull-Up */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP15RE                   :  1; /*!< Read Enable */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP15DINPOLARITY          :  1; /*!< Data In Polarity Control */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  GP15CHSEL                :  3; /*!< PWM Barium Channel Select */
      uint8_t  GP15POLARITY             :  1; /*!< Polarity Control */
    };
    uint32_t WORD;
  } GPIO15CTRL; /* +0x038 */

  union {
    struct {
      uint8_t  GP16MODE                 :  2; /*!< Mode */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GP16OE                   :  1; /*!< Output Enable */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP16PD                   :  1; /*!< Pull-Down */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP16PU                   :  1; /*!< Pull-Up */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP16RE                   :  1; /*!< Read Enable */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP16DINPOLARITY          :  1; /*!< Data In Polarity Control */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  GP16CHSEL                :  3; /*!< PWM Barium Channel Select */
      uint8_t  GP16POLARITY             :  1; /*!< Polarity Control */
    };
    uint32_t WORD;
  } GPIO16CTRL; /* +0x03C */

  union {
    struct {
      uint8_t  GP17MODE                 :  3; /*!< Mode */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  GP17OE                   :  1; /*!< Output Enable */
      uint8_t  GP17OD                   :  1; /*!< Open Drain Enable */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GP17PD                   :  1; /*!< Pull-Down */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP17PU                   :  1; /*!< Pull-Up */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP17RE                   :  1; /*!< Read Enable */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  TESTMUX_SELECT           :  4; /*!< Testmux Select */
      uint8_t  GP17DINPOLARITY          :  1; /*!< Data In Polarity Control */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP17CHSEL                :  3; /*!< PWM Barium Channel Select */
      uint8_t  GP17POLARITY             :  1; /*!< Polarity Control */
    };
    uint32_t WORD;
  } GPIO17CTRL; /* +0x040 */

  union {
    struct {
      uint8_t  GP18MODE                 :  3; /*!< Mode */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  GP18OE                   :  1; /*!< Output Enable */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP18PD                   :  1; /*!< Pull-Down */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP18PU                   :  1; /*!< Pull-Up */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP18RE                   :  1; /*!< Read Enable */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  TESTMUX_SELECT           :  4; /*!< Testmux Select */
      uint8_t  GP18DINPOLARITY          :  1; /*!< Data In Polarity Control */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  GP18CHSEL                :  3; /*!< PWM Barium Channel Select */
      uint8_t  GP18POLARITY             :  1; /*!< Polarity Control */
    };
    uint32_t WORD;
  } GPIO18CTRL; /* +0x044 */

  union {
    struct {
      uint8_t  FAULTN1MODE              :  8; /*!< Mode */
      uint8_t  FAULTN1PD                :  1; /*!< Pull-Down */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  FAULTN1PU                :  1; /*!< Pull-Up */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  FAULTN1RE                :  8; /*!< Read Enable */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  FAULTN1PUPOLARITY        :  1; /*!< Pull Up Polarity Control */
      uint8_t  FAULTN1PDPOLARITY        :  1; /*!< Pull Down Polarity Control */
      uint8_t  FAULTN1OEPOLARITY        :  1; /*!< Output Enable Polarity Control */
      uint8_t  FAULTN1DINPOLARITY       :  1; /*!< Data In Polarity Control */
      uint8_t  FAULTN1POLARITY          :  1; /*!< Polarity Control */
    };
    uint32_t WORD;
  } FAULTN1CTRL; /* +0x048 */

  union {
    struct {
      uint8_t  FAULTN2MODE              :  8; /*!< Mode */
      uint8_t  FAULTN2PD                :  1; /*!< Pull-Down */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  FAULTN2PU                :  1; /*!< Pull-Up */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  FAULTN2RE                :  8; /*!< Read Enable */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  FAULTN2PUPOLARITY        :  1; /*!< Pull Up Polarity Control */
      uint8_t  FAULTN2PDPOLARITY        :  1; /*!< Pull Down Polarity Control */
      uint8_t  FAULTN2OEPOLARITY        :  1; /*!< Output Enable Polarity Control */
      uint8_t  FAULTN2DINPOLARITY       :  1; /*!< Data In Polarity Control */
      uint8_t  FAULTN2POLARITY          :  1; /*!< Polarity Control */
    };
    uint32_t WORD;
  } FAULTN2CTRL; /* +0x04C */

  union {
    struct {
      uint8_t  I2CSCLMODE               :  2; /*!< i2c Mode */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  I2CSCLDATAOUT            :  1; /*!< i2c Data Out */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
      uint8_t  I2CSCLRE                 :  1; /*!< Read Enable */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  TESTMUX_SELECT           :  4; /*!< Testmux Select */
      uint8_t                           :  6; /*   (reserved) */
      uint8_t  I2CSCLDINPOLARITY        :  1; /*!< Data In Polarity Control */
      uint8_t  I2CSCLOEPOLARITY         :  1; /*!< OE Polarity Control */
    };
    uint32_t WORD;
  } I2CSCLCTRL; /* +0x050 */

  union {
    struct {
      uint8_t  I2CSDAMODE               :  2; /*!< i2c Mode */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  I2CSDADATAOUT            :  1; /*!< i2c Data Out */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
      uint8_t  I2CSDARE                 :  1; /*!< Read Enable */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  TESTMUX_SELECT           :  4; /*!< Testmux Select */
      uint8_t                           :  6; /*   (reserved) */
      uint8_t  I2CSDADINPOLARITY        :  1; /*!< Data In Polarity Control */
      uint8_t  I2CSDAOEPOLARITY         :  1; /*!< OE Polarity Control */
    };
    uint32_t WORD;
  } I2CSDACTRL; /* +0x054 */

  union {
    struct {
      uint8_t  PRTCTL1MODE              :  8; /*!< Mode */
      uint8_t  PRTCTL1PD                :  1; /*!< Pull-Down */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  PRTCTL1PU                :  1; /*!< Pull-Up */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  PRTCTL1RE                :  1; /*!< Read Enable */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  PRTCTL1SEL               :  1; /*!< Source Select */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t                           :  7; /*   (reserved) */
      uint8_t  PRTCTL1POLARITY          :  1; /*!< Polarity Control */
    };
    uint32_t WORD;
  } PRTCTL1CTRL; /* +0x058 */

  union {
    struct {
      uint8_t  PRTCTL2MODE              :  8; /*!< Mode */
      uint8_t  PRTCTL2PD                :  1; /*!< Pull-Down */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  PRTCTL2PU                :  1; /*!< Pull-Up */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  PRTCTL2RE                :  1; /*!< Read Enable */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  PRTCTL2SEL               :  1; /*!< Source Select */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t                           :  7; /*   (reserved) */
      uint8_t  PRTCTL2POLARITY          :  1; /*!< Polarity Control */
    };
    uint32_t WORD;
  } PRTCTL2CTRL; /* +0x05C */

  union {
    struct {
      uint8_t  PWM1MODE                 :  8; /*!< Mode */
      uint8_t                           :  8; /*   (reserved) */
      uint8_t  PWM1RE                   :  8; /*!< Read Enable */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  PWM1CHSEL                :  3; /*!< PWM Barium Channel Select */
      uint8_t  PWM1POLARITY             :  1; /*!< PWM Polarity Control */
    };
    uint32_t WORD;
  } PWM1CTRL; /* +0x060 */

  union {
    struct {
      uint8_t  VBUSDETMODE              :  8; /*!< Mode */
      uint8_t                           :  8; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
      uint8_t                           :  7; /*   (reserved) */
      uint8_t  VBUSDETPOLARITY          :  1; /*!< Polarity Control */
    };
    uint32_t WORD;
  } VBUSDETCTRL; /* +0x064 */

  uint32_t _RESERVED_68[38];

  union {
    struct {
      uint8_t  GPA_HW_SYNC0             :  1; /*!< GPIO Port A0 Hardware Synchronization */
      uint8_t  GPA_HW_SYNC1             :  1; /*!< GPIO Port A1 Hardware Synchronization */
      uint8_t  GPA_HW_SYNC2             :  1; /*!< GPIO Port A2 Hardware Synchronization */
      uint8_t  GPA_HW_SYNC3             :  1; /*!< GPIO Port A3 Hardware Synchronization */
      uint8_t  GPA_HW_SYNC4             :  1; /*!< GPIO Port A4 Hardware Synchronization */
      uint8_t  GPA_HW_SYNC5             :  1; /*!< GPIO Port A5 Hardware Synchronization */
      uint8_t  GPA_HW_SYNC6             :  1; /*!< GPIO Port A6 Hardware Synchronization */
      uint8_t  GPA_HW_SYNC7             :  1; /*!< GPIO Port A7 Hardware Synchronization */
      uint8_t  GPB_HW_SYNC0             :  1; /*!< GPIO Port B0 Hardware Synchronization */
      uint8_t  GPB_HW_SYNC1             :  1; /*!< GPIO Port B1 Hardware Synchronization */
      uint8_t  GPB_HW_SYNC2             :  1; /*!< GPIO Port B2 Hardware Synchronization */
      uint8_t  GPB_HW_SYNC3             :  1; /*!< GPIO Port B3 Hardware Synchronization */
      uint8_t  GPB_HW_SYNC4             :  1; /*!< GPIO Port B4 Hardware Synchronization */
      uint8_t  GPB_HW_SYNC5             :  1; /*!< GPIO Port B5 Hardware Synchronization */
      uint8_t  GPB_HW_SYNC6             :  1; /*!< GPIO Port B6 Hardware Synchronization */
      uint8_t  GPB_HW_SYNC7             :  1; /*!< GPIO Port B7 Hardware Synchronization */
      uint8_t  GPC_HW_SYNC0             :  1; /*!< GPIO Port C0 Hardware Synchronization */
      uint8_t  GPC_HW_SYNC1             :  1; /*!< GPIO Port C1 Hardware Synchronization */
      uint8_t  GPC_HW_SYNC2             :  1; /*!< GPIO Port C2 Hardware Synchronization */
      uint8_t  GPC_HW_SYNC3             :  1; /*!< GPIO Port C3 Hardware Synchronization */
      uint8_t  GPC_HW_SYNC4             :  1; /*!< GPIO Port C4 Hardware Synchronization */
      uint8_t  GPC_HW_SYNC5             :  1; /*!< GPIO Port C5 Hardware Synchronization */
      uint8_t  GPC_HW_SYNC6             :  1; /*!< GPIO Port C6 Hardware Synchronization */
      uint8_t  GPC_HW_SYNC7             :  1; /*!< GPIO Port C7 Hardware Synchronization */
      uint8_t  GPD_HW_SYNC0             :  1; /*!< GPIO Port D0 Hardware Synchronization */
      uint8_t  GPD_HW_SYNC1             :  1; /*!< GPIO Port D1 Hardware Synchronization */
      uint8_t  GPD_HW_SYNC2             :  1; /*!< GPIO Port D2 Hardware Synchronization */
      uint8_t  GPD_HW_SYNC3             :  1; /*!< GPIO Port D3 Hardware Synchronization */
      uint8_t  GPD_HW_SYNC4             :  1; /*!< GPIO Port D4 Hardware Synchronization */
      uint8_t  GPD_HW_SYNC5             :  1; /*!< GPIO Port D5 Hardware Synchronization */
      uint8_t  GPD_HW_SYNC6             :  1; /*!< GPIO Port D6 Hardware Synchronization */
      uint8_t  GPD_HW_SYNC7             :  1; /*!< GPIO Port D7 Hardware Synchronization */
    };
    uint32_t WORD;
  } HWSYNCCTRL; /* +0x100 */

  uint32_t _RESERVED_104[63];

  union {
    struct {
      uint8_t  DFP1GPIOMODE             :  1; /*!< Downstream Facing Port 1 GPIO Mode */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  DFP2GPIOMODE             :  1; /*!< Downstream Facing Port 2 GPIO Mode */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  DFP3GPIOMODE             :  8; /*!< Downstream Facing Port 3 GPIO Mode */
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } USBGPIOMODECTRL; /* +0x200 */

  union {
    struct {
      uint8_t  DFP1DPRE                 :  1; /*!< Downstream Facing Port 1 DP Read-enable */
      uint8_t  DFP1DPPD                 :  1; /*!< Downstream Facing Port 1 DP Pull-Down */
      uint8_t  DFP1DPPU                 :  1; /*!< Downstream Facing Port 1 DP Pull-Up */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  DFP1DMRE                 :  1; /*!< Downstream Facing Port 1 DM Read-enable */
      uint8_t  DFP1DMPD                 :  1; /*!< Downstream Facing Port 1 DM Pull-Down */
      uint8_t  DFP1DMPU                 :  1; /*!< Downstream Facing Port 1 DM Pull-Up */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  DFP2DPRE                 :  1; /*!< Downstream Facing Port 2 DP Read-enable */
      uint8_t  DFP2DPPD                 :  1; /*!< Downstream Facing Port 2 DP Pull-Down */
      uint8_t  DFP2DPPU                 :  1; /*!< Downstream Facing Port 2 DP Pull-Up */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  DFP2DMRE                 :  1; /*!< Downstream Facing Port 2 DM Read-enable */
      uint8_t  DFP2DMPD                 :  1; /*!< Downstream Facing Port 2 DM Pull-Down */
      uint8_t  DFP2DMPU                 :  1; /*!< Downstream Facing Port 2 DM Pull-Up */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  DFP3DPRE                 :  1; /*!< Downstream Facing Port 3 DP Read-enable */
      uint8_t  DFP3DPPD                 :  1; /*!< Downstream Facing Port 3 DP Pull-Down */
      uint8_t  DFP3DPPU                 :  1; /*!< Downstream Facing Port 3 DP Pull-Up */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  DFP3DMRE                 :  1; /*!< Downstream Facing Port 3 DM Read-enable */
      uint8_t  DFP3DMPD                 :  1; /*!< Downstream Facing Port 3 DM Pull-Down */
      uint8_t  DFP3DMPU                 :  1; /*!< Downstream Facing Port 3 DM Pull-Up */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
    };
    uint32_t WORD;
  } USBGPIOCTRL; /* +0x204 */

  uint32_t _RESERVED_208[62];

  union {
    struct {
      uint8_t  CC_P1_USBC_SVBUSDET_O    :  1; /*!< SVBUSDET P1 */
      uint8_t  CC_P2_USBC_SVBUSDET_O    :  1; /*!< SVBUSDET P2 */
      uint8_t                           :  6; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } USBCSVBUSDETCTRL; /* +0x300 */

  uint8_t  BLOCK_SEL;                         /* +0x304 */
  uint8_t  _RESERVED_305[3];                  /* +0x305 */

} IOCTRL_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of IOCTRL SFRS.
 */
#define IOCTRL_SFRS ((__IO IOCTRL_SFRS_t *)0x50014c00)

#endif /* end of __IOCTRL_SFR_H__ section */


