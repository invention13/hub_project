/**
 * @copyright 2019 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file typec2_sfr.h
 */

#ifndef __TYPEC2_SFR_H__
#define __TYPEC2_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for TYPEC2.
 */
typedef struct {

  union {
    struct {
      uint8_t  MINREV                   :  3; /*!< Minor Revision */
      uint8_t  MAJREV                   :  3; /*!< Major Revision */
      uint8_t  VENDID                   :  2; /*!< Vendor ID */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } VERSION; /* +0x000 */

  union {
    __packed struct {
      uint8_t  MODES                    :  3;
      uint8_t  CURSRC                   :  2; /*!< Current Source */
      uint8_t  DRPTOGGLE                :  3; /*!< DRP Toggle */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } TYPECCTL1; /* +0x004 */

  union {
    __packed struct {
      uint8_t  UNATTSNK                 :  1; /*!< UNATT_SNK */
      uint8_t  UNATTSRC                 :  1; /*!< UNATT_SRC */
      uint8_t  DISST                    :  1; /*!< DIS_ST */
      uint8_t  ERRREC                   :  1; /*!< ERR_REC */
      uint8_t  ATTSNK                   :  1; /*!< ATT_SNK */
      uint8_t  ATTSRC                   :  1; /*!< ATT_SRC */
      uint8_t  TRYSRC                   :  1; /*!< TRY_SRC */
      uint8_t  UNSUPACC                 :  1; /*!< UNSUP_ACC */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } TYPECCTL2; /* +0x008 */

  union {
    __packed struct {
      uint8_t  DETDIS                   :  1; /*!< DET_DIS */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  RESETPHY                 :  1;
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  VBUSIGNORE               :  1; /*!< VBUS_IGNORE */
      uint8_t                           :  2; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } TYPECCTL3; /* +0x00C */

  union {
    __packed struct {
      uint8_t  PU1                      :  1;
      uint8_t  VCONN1                   :  1;
      uint8_t  TXE1                     :  1;
      uint8_t  PDWN1                    :  1;
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  ADCON                    :  1; /*!< ADC on */
      uint8_t  VBUSOK1                  :  1; /*!< VBUSOK */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } CC1CONTROL; /* +0x010 */

  union {
    __packed struct {
      uint8_t  PU2                      :  1;
      uint8_t  VCONN2                   :  1;
      uint8_t  TXE2                     :  1;
      uint8_t  PDWN2                    :  1;
      uint8_t  S5B                      :  1; /*!< CurrentDetect */
      uint8_t  S6B                      :  1; /*!< RaRdDetect */
      uint8_t  S7B                      :  1; /*!< ADC on */
      uint8_t  VBUSOK                   :  1;
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } CC2CONTROL; /* +0x014 */

  union {
    __packed struct {
      uint8_t  CCSEL                    :  2; /*!< CC_SEL */
      uint8_t  VCONNSWAPON              :  1; /*!< VCONN_SWAP_ON */
      uint8_t  VCONNSWAPOFF             :  1; /*!< VCONN_SWAP_OFF */
      uint8_t                           :  4; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } CCSEL; /* +0x018 */

  union {
    __packed struct {
      uint8_t  TYPECRSLT                :  4; /*!< Type C Result */
      uint8_t  CCORIENT                 :  2; /*!< Connection Orientation */
      uint8_t  TYPECDET                 :  2; /*!< Type C Detection */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } TYPECSTATUS1; /* +0x01C */

  union {
    __packed struct {
      uint8_t  SHORT                    :  1;
      uint8_t  OVRTEMP                  :  1; /*!< Over Temp */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  PDNOTALLOWED             :  1; /*!< PD Not Allowed */
      uint8_t  VBUSREQ                  :  1; /*!< VBUS_REQ */
      uint8_t                           :  2; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } TYPECSTATUS2; /* +0x020 */

  union {
    __packed struct {
      uint8_t  TYPECSTATE               :  5; /*!< Type C FSM state Used for test and debug */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  TYPECACTIVE              :  1; /*!< FSM is active */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } TYPECSTATUS3; /* +0x024 */

  union {
    __packed struct {
      uint8_t  CC1DETRA                 :  1; /*!< DET_RA */
      uint8_t  CC1DETRD                 :  1; /*!< DET_RD */
      uint8_t  CC1DETDEF                :  1; /*!< DET_DEF */
      uint8_t  CC1DET1P5A               :  1; /*!< DET_1P5A */
      uint8_t  CC1DET3A                 :  1; /*!< DET_3A */
      uint8_t                           :  3; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } CC1CMP; /* +0x028 */

  union {
    __packed struct {
      uint8_t  CC2DETRA                 :  1; /*!< DET_RA */
      uint8_t  CC2DETRD                 :  1; /*!< DET_RD */
      uint8_t  CC2DETDEF                :  1; /*!< DET_DEF */
      uint8_t  CC2DET1P5A               :  1; /*!< DET_1P5A */
      uint8_t  CC2DET3A                 :  1; /*!< DET_3A */
      uint8_t                           :  3; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } CC2CMP; /* +0x02C */

  union {
    __packed struct {
      uint8_t  SNKRP1                   :  1; /*!< SNK_RP1 */
      uint8_t  PWRDEFSNK1               :  1; /*!< PWRDEF_SNK1 */
      uint8_t  PWR1P5ASNK1              :  1; /*!< PWR1P5A_SNK1 */
      uint8_t  PWR3ASNK1                :  1; /*!< PWR3A_SNK1 */
      uint8_t  SRCRP1                   :  1; /*!< SRC_RP1 */
      uint8_t  SRCRX1                   :  2; /*!< SRC_RX1 */
      uint8_t                           :  1; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } CC1STATUS; /* +0x030 */

  union {
    __packed struct {
      uint8_t  SNKRP2                   :  1; /*!< SNK_RP2 */
      uint8_t  PWRDEFSNK2               :  1; /*!< PWRDEF_SNK2 */
      uint8_t  PWR1P5ASNK2              :  1; /*!< PWR1P5A_SNK2 */
      uint8_t  PWR3ASNK2                :  1; /*!< PWR3A_SNK2 */
      uint8_t  SRCRP2                   :  1; /*!< SRC_RP2 */
      uint8_t  SRCRX2                   :  2; /*!< SRC_RX2 */
      uint8_t                           :  1; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } CC2STATUS; /* +0x034 */

  union {
    __packed struct {
      uint8_t  ADC1                     :  6; /*!< Voltage measurement ADC input */
      uint8_t  ADCSTART1                :  1; /*!< CC1 ADC conversion start */
      uint8_t                           :  1; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } ADC1; /* +0x038 */

  union {
    __packed struct {
      uint8_t  DAC                      :  6; /*!< Scaled VBUS threshold value */
      uint8_t  COMP                     :  1; /*!< VBUS higher than DAC set threshold */
      uint8_t  VBUSMONEN                :  1; /*!< Enable VBUS monitor comparator */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } VBUSMON; /* +0x03C */

  union {
    __packed struct {
      uint8_t  TRIMRD1                  :  4; /*!< TRIM_RD1 */
      uint8_t  TRIMRD2                  :  4; /*!< TRIM_RD2 */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } AFETRIM1; /* +0x040 */

   //uint8_t RESERVED_16[3];
   
   union {
      struct {
         uint8_t TRIMCCDRV :  2; /*!< TRIM_CCDRV */
         uint8_t TRIMCCHI  :  3; /*!< TRIM_CCHI */
         uint8_t           :  2; /*    (reserved) */
      };
      uint8_t BYTE;
   } AFETRIM2;
   
   uint8_t RESERVED_17[3];
  
   union {
     struct {
        uint8_t TRIMHCUR:  4;
        uint8_t         :  4; /*    (reserved) */
      };
     uint8_t BYTE;
   } AFETRIM3;
   
   uint8_t RESERVED_18[3];

   union {
     struct {
         uint8_t TST:  4;
         uint8_t    :  4; /*    (reserved) */
      };
     uint8_t BYTE;
   } ATEST;
  
   uint8_t RESERVED_19[3];

   union {
     struct {
       uint8_t PWR:  4;
       uint8_t    :  4; /*    (reserved) */
      };
     uint8_t BYTE;
   } POWER;   
   
   uint8_t RESERVED_20[3];

   union {
     struct {
       uint8_t SLICE:  3;
       uint8_t      :  5; /*    (reserved) */
      };
     uint8_t BYTE;
   } SLICE; 

   uint8_t RESERVED_21[3];

   union {
      struct {
         uint8_t ISHORT    :  1; /*!< I_SHORT */
         uint8_t IOVRTEMP  :  1; /*!< I_OVRTEMP */
         uint8_t IADCDONE1 :  1; /*!< I_ADC_DONE1 */
         uint8_t           :  5; /*    (reserved) */
      };
      uint8_t BYTE;
   } INTERRUPT1;
   
   uint8_t RESERVED_22[3];

   union {
      struct {
         uint8_t          :  1; /*    (reserved) */
         uint8_t IFASTSWAP :  1; /*!< I_FAST_SWAP */
         uint8_t IPDTXFAIL :  1; /*!< I_PD_TX_FAIL */
         uint8_t IPDTXOK   :  1; /*!< I_PD_TX_OK */
         uint8_t IPDCRRX   :  1; /*!< I_PD_CR_RX */
         uint8_t IPDHRRX   :  1; /*!< I_PD_HR_RX */
         uint8_t IPDRX     :  1; /*!< I_PD_RX */
         uint8_t ICCCHANGE :  1; /*!< I_CC_CHANGE */
      };
      uint8_t BYTE;
   } INTERRUPT2;

  uint8_t RESERVED_23[3];
   
   union {
      struct {
         uint8_t MSHORT    :  1; /*!< M_SHORT */
         uint8_t MOVRTEMP  :  1; /*!< M_OVRTEMP */
         uint8_t MADCDONE1 :  1; /*!< M_ADC_DONE1 */
         uint8_t           :  5; /*    (reserved) */
      };
      uint8_t BYTE;
   } MASK1;

   uint8_t RESERVED_24[3];
   
   union {
      struct {
         uint8_t             :  1; /*    (reserved) */
         uint8_t MFASTSWAP    :  1; /*!< M_FAST_SWAP */
         uint8_t MPDTXFAIL    :  1; /*!< I_PD_TX_FAIL */
         uint8_t MPDTXSUCCESS :  1; /*!< I_PD_TX_SUCCESS */
         uint8_t MPDCRRX      :  1; /*!< I_PD_CR_RX */
         uint8_t MPDHRRX      :  1; /*!< I_PD_HR_RX */
         uint8_t MPDRX        :  1; /*!< I_PD_RX */
         uint8_t MCCCHANGE    :  1; /*!< I_CC_CHANGE */
      };
      uint8_t BYTE;
   } MASK2;

   uint8_t RESERVED_25[3];
   
   union {
      struct {
         uint8_t             :  5; /*    (reserved) */
         uint8_t GUIDETRYSNK  :  1; /*!< GUIDE_TRY_SNK */
         uint8_t VBUSHIGHVOLT :  1; /*!< VBUS_HIGH_VOLT */
         uint8_t IDINSERT     :  1; /*!< Insert */
      };
      uint8_t BYTE;
   } PDCFG1;

   uint8_t RESERVED_26[3];   
   
   union {
      struct {
         uint8_t SOPRCV      :  5; /*!< Which SOP to receive */
         uint8_t CDRSELECT   :  1; /*!< Which SOP to receive */
         uint8_t FASTSWAPSRC :  1; /*!< Which SOP to receive */
         uint8_t FASTSWAPSNK :  1; /*!< Which SOP to receive */
      };
      uint8_t BYTE;
   } PDCFG2;

   uint8_t RESERVED_27[3];
   
   
   union {
      struct {
         uint8_t PPWRROLE  :  1; /*!< Current Port Power Role */
         uint8_t PDATAROLE :  1; /*!< Current Port Data Role */
         uint8_t SRSOP0    :  2; /*!< SOP spec rev header bits */
         uint8_t SRSOP1    :  2; /*!< SOP' spec rev header bits */
         uint8_t SRSOP2    :  2; /*!< SOP'' spec rev header bits */
      };
      uint8_t BYTE;
   } PDCFG3;

   uint8_t RESERVED_28[3];   
 
   union {
      struct {
         uint8_t SHORTTIME  :  7; /*!< Tx Result */
         uint8_t SHORTRESET :  1; /*!< Rx Result */
      };
      uint8_t BYTE;
   } SHORTPROTECT;

   uint8_t RESERVED_29[3];   
   
   union {
      struct {
         uint8_t TXRESULT :  3; /*!< Tx Result */
         uint8_t RXRESULT :  3; /*!< Rx Result */
         uint8_t         :  1; /*    (reserved) */
         uint8_t FASTSWAP :  1; /*!< FAST_SWAP */
      };
      uint8_t BYTE;
   } PDSTATUS;

   uint8_t RESERVED_30[3];   
   
   union {
      struct {
         uint8_t RXCLEAR   :  1; /*!< RX_CLEAR */
         uint8_t          :  5; /*    (reserved) */
         uint8_t RXOVERRUN :  1; /*!< RX_OVERRUN */
         uint8_t RXDATA    :  1; /*!< RX_DATA */
      };
      uint8_t BYTE;
   } RXSTATUS;

   uint8_t RESERVED_31[3];   
   
   union {
      struct {
         uint8_t RXSOP   :  3; /*!< RX_SOP */
         uint8_t RXBYTES :  5; /*!< RX_BYTES */
      };
      uint8_t BYTE;
   } RXINFO;

   uint8_t RESERVED_32[3];      
   
   union {
      struct {
         uint8_t TXBUFREADY :  1; /*!< Transmission Status */
         uint8_t TXSTART    :  1; /*!< Transmission Start */
         uint8_t TXWAITRP   :  1; /*!< Transmission Wait RP */
         uint8_t           :  2; /*    (reserved) */
         uint8_t TXCMD      :  3; /*!< Transmission Command */
      };
      uint8_t BYTE;
   } TXCOMMAND;

   uint8_t RESERVED_33[3];   
   
   union {
      struct {
         uint8_t TXSOP     :  3; /*!< Transmission Status */
         uint8_t TXRETRIES :  3; /*!< Transmission Start */
         uint8_t           :  2; /*    (reserved) */
      };
      uint8_t BYTE;
   } TXINFO;

   //uint8_t RESERVED_33[3];   
   
   uint8_t RESERVED34[107];

   uint8_t RXDATA1:  8;
   uint8_t RESERVED_35[3];

   uint8_t RXDATA2:  8;
   uint8_t RESERVED_36[3];

   uint8_t RXDATA3:  8;
   uint8_t RESERVED_37[3];

   uint8_t RXDATA4:  8;
   uint8_t RESERVED_38[3];

   uint8_t RXDATA5:  8;
   uint8_t RESERVED_39[3];

   uint8_t RXDATA6:  8;
   uint8_t RESERVED_40[3];

   uint8_t RXDATA7:  8;
   uint8_t RESERVED_41[3];

   uint8_t RXDATA8:  8;
   uint8_t RESERVED_42[3];

   uint8_t RXDATA9:  8;
   uint8_t RESERVED_43[3];

   uint8_t RXDATA10:  8;
   uint8_t RESERVED_44[3];

   uint8_t RXDATA11:  8;
   uint8_t RESERVED_45[3];

   uint8_t RXDATA12:  8;
   uint8_t RESERVED_46[3];

   uint8_t RXDATA13:  8;
   uint8_t RESERVED_47[3];

   uint8_t RXDATA14:  8;
   uint8_t RESERVED_48[3];

   uint8_t RXDATA15:  8;
   uint8_t RESERVED_49[3];

   uint8_t RXDATA16:  8;
   uint8_t RESERVED_50[3];

   uint8_t RXDATA17:  8;
   uint8_t RESERVED_51[3];

   uint8_t RXDATA18:  8;
   uint8_t RESERVED_52[3];

   uint8_t RXDATA19:  8;
   uint8_t RESERVED_53[3];

   uint8_t RXDATA20:  8;
   uint8_t RESERVED_54[3];

   uint8_t RXDATA21:  8;
   uint8_t RESERVED_55[3];

   uint8_t RXDATA22:  8;
   uint8_t RESERVED_56[3];

   uint8_t RXDATA23:  8;
   uint8_t RESERVED_57[3];

   uint8_t RXDATA24:  8;
   uint8_t RESERVED_58[3];

   uint8_t RXDATA25:  8;
   uint8_t RESERVED_59[3];

   uint8_t RXDATA26:  8;
   uint8_t RESERVED_60[3];

   uint8_t RXDATA27:  8;
   uint8_t RESERVED_61[3];

   uint8_t RXDATA28:  8;
   uint8_t RESERVED_62[3];

   uint8_t RXDATA29:  8;
   uint8_t RESERVED_63[3];

   uint8_t RXDATA30:  8;
   uint8_t RESERVED_64[3];

   uint8_t TXDATA1:  8;
   uint8_t RESERVED_65[3];

   uint8_t TXDATA2:  8;
   uint8_t RESERVED_66[3];

   uint8_t TXDATA3:  8;
   uint8_t RESERVED_67[3];

   uint8_t TXDATA4:  8;
   uint8_t RESERVED_68[3];

   uint8_t TXDATA5:  8;
   uint8_t RESERVED_69[3];

   uint8_t TXDATA6:  8;
   uint8_t RESERVED_70[3];

   uint8_t TXDATA7:  8;
   uint8_t RESERVED_71[3];

   uint8_t TXDATA8:  8;
   uint8_t RESERVED_72[3];

   uint8_t TXDATA9:  8;
   uint8_t RESERVED_73[3];

   uint8_t TXDATA10:  8;
   uint8_t RESERVED_74[3];

   uint8_t TXDATA11:  8;
   uint8_t RESERVED_75[3];

   uint8_t TXDATA12:  8;
   uint8_t RESERVED_76[3];

   uint8_t TXDATA13:  8;
   uint8_t RESERVED_77[3];

   uint8_t TXDATA14:  8;
   uint8_t RESERVED_78[3];

   uint8_t TXDATA15:  8;
   uint8_t RESERVED_79[3];

   uint8_t TXDATA16:  8;
   uint8_t RESERVED_80[3];

   uint8_t TXDATA17:  8;
   uint8_t RESERVED_81[3];

   uint8_t TXDATA18:  8;
   uint8_t RESERVED_82[3];

   uint8_t TXDATA19:  8;
   uint8_t RESERVED_83[3];

   uint8_t TXDATA20:  8;
   uint8_t RESERVED_84[3];

   uint8_t TXDATA21:  8;
   uint8_t RESERVED_85[3];

   uint8_t TXDATA22:  8;
   uint8_t RESERVED_86[3];

   uint8_t TXDATA23:  8;
   uint8_t RESERVED_87[3];

   uint8_t TXDATA24:  8;
   uint8_t RESERVED_88[3];

   uint8_t TXDATA25:  8;
   uint8_t RESERVED_89[3];

   uint8_t TXDATA26:  8;
   uint8_t RESERVED_90[3];

   uint8_t TXDATA27:  8;
   uint8_t RESERVED_91[3];

   uint8_t TXDATA28:  8;
   uint8_t RESERVED_92[3];

   uint8_t TXDATA29:  8;
   uint8_t RESERVED_93[3];

   uint8_t TXDATA30:  8;
   uint8_t RESERVED_94[3];

} TYPEC2_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of TYPEC2 SFRS.
 */
#define TYPEC2_SFRS ((__IO TYPEC2_SFRS_t *)0x50017400)

#endif /* end of __TYPEC2_SFR_H__ section */

