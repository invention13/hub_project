/**
 * @copyright 2019 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file i2saudin_sfr.h
 */

#ifndef __I2SAUDIN_SFR_H__
#define __I2SAUDIN_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for I2SAUDIN.
 */
typedef struct {

  union {
    struct {
      uint8_t  FIFOWAITTEST             :  1; /*!< FIFO Wait Test Control */
      uint8_t  AUDINZLPDMP              :  1;
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  ISOSYNC                  :  1;
      uint8_t  I2SZLP                   :  1;
      uint8_t  AUDEPENA                 :  1;
      uint8_t  I2SENA                   :  1;
      uint8_t  I2SNBADC                 :  5;
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  AEPSAMPSZ                :  1;
      uint8_t  I2SNBSAMP                :  8;
      uint8_t  I2SSLAVE                 :  1;
      uint8_t  I2SBCLKE                 :  1;
      uint8_t  I2SCAPDLY                :  2;
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  FIFOINFULL               :  1;
    };
    uint32_t WORD;
  } I2SCFG; /* +0x000 */

  union {
    struct {
      uint8_t  BCKMD                    :  5;
      uint8_t  BCKPD                    :  1;
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  BCLK2XENA                :  1;
      uint8_t  MCKMD                    :  5;
      uint8_t  MCKPD                    :  1;
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  MCLKENA                  :  1;
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } I2SBCLKRT; /* +0x004 */

} I2SAUDIN_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of I2SAUDIN SFRS.
 */
#define I2SAUDIN_SFRS ((__IO I2SAUDIN_SFRS_t *)0x5001b100)

#endif /* end of __I2SAUDIN_SFR_H__ section */


