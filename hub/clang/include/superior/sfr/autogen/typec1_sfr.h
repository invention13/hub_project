/**
 * @copyright 2019 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file typec1_sfr.h
 */

#ifndef __TYPEC1_SFR_H__
#define __TYPEC1_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for TYPEC1.
 */
typedef  struct {

  union {
    struct {
      uint8_t  MINREV                   :  3; /*!< Minor Revision */
      uint8_t  MAJREV                   :  3; /*!< Major Revision */
      uint8_t  VENDID                   :  2; /*!< Vendor ID */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } VERSION; /* +0x000 */

  union {
    __packed struct {
      uint8_t  MODES                    :  3;
      uint8_t  CURSRC                   :  2; /*!< Current Source */
      uint8_t  DRPTOGGLE                :  3; /*!< DRP Toggle */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } TYPECCTL1; /* +0x004 */

  union {
    __packed struct {
      uint8_t  UNATTSNK                 :  1; /*!< UNATT_SNK */
      uint8_t  UNATTSRC                 :  1; /*!< UNATT_SRC */
      uint8_t  DISST                    :  1; /*!< DIS_ST */
      uint8_t  ERRREC                   :  1; /*!< ERR_REC */
      uint8_t  ATTSNK                   :  1; /*!< ATT_SNK */
      uint8_t  ATTSRC                   :  1; /*!< ATT_SRC */
      uint8_t  TRYSRC                   :  1; /*!< TRY_SRC */
      uint8_t  UNSUPACC                 :  1; /*!< UNSUP_ACC */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } TYPECCTL2; /* +0x008 */

  union {
    __packed struct {
      uint8_t  DETDIS                   :  1; /*!< DET_DIS */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  RESETPHY                 :  1;
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  VBUSIGNORE               :  1; /*!< VBUS_IGNORE */
      uint8_t                           :  2; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } TYPECCTL3; /* +0x00C */

  union {
    __packed struct {
      uint8_t  PU1                      :  1;
      uint8_t  VCONN1                   :  1;
      uint8_t  TXE1                     :  1;
      uint8_t  PDWN1                    :  1;
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  ADCON                    :  1; /*!< ADC on */
      uint8_t  VBUSOK1                  :  1; /*!< VBUSOK */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } CC1CONTROL; /* +0x010 */

  union {
    __packed struct {
      uint8_t  PU2                      :  1;
      uint8_t  VCONN2                   :  1;
      uint8_t  TXE2                     :  1;
      uint8_t  PDWN2                    :  1;
      uint8_t  S5B                      :  1; /*!< CurrentDetect */
      uint8_t  S6B                      :  1; /*!< RaRdDetect */
      uint8_t  S7B                      :  1; /*!< ADC on */
      uint8_t  VBUSOK                   :  1;
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } CC2CONTROL; /* +0x014 */

  union {
    __packed struct {
      uint8_t  CCSEL                    :  2; /*!< CC_SEL */
      uint8_t  VCONNSWAPON              :  1; /*!< VCONN_SWAP_ON */
      uint8_t  VCONNSWAPOFF             :  1; /*!< VCONN_SWAP_OFF */
      uint8_t                           :  4; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } CCSEL; /* +0x018 */

  union {
    __packed struct {
      uint8_t  TYPECRSLT                :  4; /*!< Type C Result */
      uint8_t  CCORIENT                 :  2; /*!< Connection Orientation */
      uint8_t  TYPECDET                 :  2; /*!< Type C Detection */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } TYPECSTATUS1; /* +0x01C */

  union {
    __packed struct {
      uint8_t  SHORT                    :  1;
      uint8_t  OVRTEMP                  :  1; /*!< Over Temp */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  PDNOTALLOWED             :  1; /*!< PD Not Allowed */
      uint8_t  VBUSREQ                  :  1; /*!< VBUS_REQ */
      uint8_t                           :  2; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } TYPECSTATUS2; /* +0x020 */

  union {
    __packed struct {
      uint8_t  TYPECSTATE               :  5; /*!< Type C FSM state Used for test and debug */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  TYPECACTIVE              :  1; /*!< FSM is active */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } TYPECSTATUS3; /* +0x024 */

  union {
    __packed struct {
      uint8_t  CC1DETRA                 :  1; /*!< DET_RA */
      uint8_t  CC1DETRD                 :  1; /*!< DET_RD */
      uint8_t  CC1DETDEF                :  1; /*!< DET_DEF */
      uint8_t  CC1DET1P5A               :  1; /*!< DET_1P5A */
      uint8_t  CC1DET3A                 :  1; /*!< DET_3A */
      uint8_t                           :  3; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } CC1CMP; /* +0x028 */

  union {
    __packed struct {
      uint8_t  CC2DETRA                 :  1; /*!< DET_RA */
      uint8_t  CC2DETRD                 :  1; /*!< DET_RD */
      uint8_t  CC2DETDEF                :  1; /*!< DET_DEF */
      uint8_t  CC2DET1P5A               :  1; /*!< DET_1P5A */
      uint8_t  CC2DET3A                 :  1; /*!< DET_3A */
      uint8_t                           :  3; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } CC2CMP; /* +0x02C */

  union {
    __packed struct {
      uint8_t  SNKRP1                   :  1; /*!< SNK_RP1 */
      uint8_t  PWRDEFSNK1               :  1; /*!< PWRDEF_SNK1 */
      uint8_t  PWR1P5ASNK1              :  1; /*!< PWR1P5A_SNK1 */
      uint8_t  PWR3ASNK1                :  1; /*!< PWR3A_SNK1 */
      uint8_t  SRCRP1                   :  1; /*!< SRC_RP1 */
      uint8_t  SRCRX1                   :  2; /*!< SRC_RX1 */
      uint8_t                           :  1; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } CC1STATUS; /* +0x030 */

  union {
    __packed struct {
      uint8_t  SNKRP2                   :  1; /*!< SNK_RP2 */
      uint8_t  PWRDEFSNK2               :  1; /*!< PWRDEF_SNK2 */
      uint8_t  PWR1P5ASNK2              :  1; /*!< PWR1P5A_SNK2 */
      uint8_t  PWR3ASNK2                :  1; /*!< PWR3A_SNK2 */
      uint8_t  SRCRP2                   :  1; /*!< SRC_RP2 */
      uint8_t  SRCRX2                   :  2; /*!< SRC_RX2 */
      uint8_t                           :  1; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } CC2STATUS; /* +0x034 */

  union {
    __packed struct {
      uint8_t  ADC1                     :  6; /*!< Voltage measurement ADC input */
      uint8_t  ADCSTART1                :  1; /*!< CC1 ADC conversion start */
      uint8_t                           :  1; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } ADC1; /* +0x038 */

  union {
    __packed struct {
      uint8_t  DAC                      :  6; /*!< Scaled VBUS threshold value */
      uint8_t  COMP                     :  1; /*!< VBUS higher than DAC set threshold */
      uint8_t  VBUSMONEN                :  1; /*!< Enable VBUS monitor comparator */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } VBUSMON; /* +0x03C */

  union {
    __packed struct {
      uint8_t  TRIMRD1                  :  4; /*!< TRIM_RD1 */
      uint8_t  TRIMRD2                  :  4; /*!< TRIM_RD2 */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } AFETRIM1; /* +0x040 */

  union {
    __packed struct {
      uint8_t  TRIMCCDRV                :  2; /*!< TRIM_CCDRV */
      uint8_t  TRIMGP                   :  3; /*!< TRIM_GP */
      uint8_t                           :  3; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } AFETRIM2; /* +0x044 */

  union {
    __packed struct {
      uint8_t  TRIMHCUR                 :  4; /*!< TRIM_HCUR */
      uint8_t  TRIMSLICE                :  4; /*!< TRIM_SLICE */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } AFETRIM3; /* +0x048 */

  uint8_t  TST;                               /*<! Control the test modes of the AFE +0x04C */
  uint8_t  _RESERVED_4D[3];                   /* +0x04D */

  union {
    __packed struct {
      uint8_t  PWR                      :  4; /*!< Power enables */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  VCONNCTRLEXT             :  1; /*!< Enable external VCONN generator */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } POWER; /* +0x050 */

  uint8_t  SLICE;                             /*<! Output from slicer +0x054 */
  uint8_t  _RESERVED_55[3];                   /* +0x055 */

  union {
    __packed struct {
      uint8_t  ISHORT                   :  1; /*!< I_SHORT */
      uint8_t  IOVRTEMP                 :  1; /*!< I_OVRTEMP */
      uint8_t  IADCDONE1                :  1; /*!< I_ADC_DONE1 */
      uint8_t  IVBUSDROP                :  1; /*!< I_VBUS_CROP */
      uint8_t                           :  4; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } INTERRUPT1; /* +0x058 */

  union {
    __packed struct {
      uint8_t  ITXDISCARD               :  1; /*!< I_TX_DISCARD */
      uint8_t  IFASTSWAP                :  1; /*!< I_FAST_SWAP */
      uint8_t  IPDTXFAIL                :  1; /*!< I_PD_TX_FAIL */
      uint8_t  IPDTXOK                  :  1; /*!< I_PD_TX_OK */
      uint8_t  IPDCRRX                  :  1; /*!< I_PD_CR_RX */
      uint8_t  IPDHRRX                  :  1; /*!< I_PD_HR_RX */
      uint8_t  IPDRX                    :  1; /*!< I_PD_RX */
      uint8_t  ICCCHANGE                :  1; /*!< I_CC_CHANGE */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } INTERRUPT2; /* +0x05C */

  union {
    __packed struct {
      uint8_t  MSHORT                   :  1; /*!< M_SHORT */
      uint8_t  MOVRTEMP                 :  1; /*!< M_OVRTEMP */
      uint8_t  MADCDONE1                :  1; /*!< M_ADC_DONE1 */
      uint8_t  MVBUSDROP                :  1; /*!< M_VBUS_CROP */
      uint8_t                           :  4; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } MASK1; /* +0x060 */

  union {
    __packed struct {
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  MFASTSWAP                :  1; /*!< M_FAST_SWAP */
      uint8_t  MPDTXFAIL                :  1; /*!< I_PD_TX_FAIL */
      uint8_t  MPDTXSUCCESS             :  1; /*!< I_PD_TX_SUCCESS */
      uint8_t  MPDCRRX                  :  1; /*!< I_PD_CR_RX */
      uint8_t  MPDHRRX                  :  1; /*!< I_PD_HR_RX */
      uint8_t  MPDRX                    :  1; /*!< I_PD_RX */
      uint8_t  MCCCHANGE                :  1; /*!< I_CC_CHANGE */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } MASK2; /* +0x064 */

  union {
    __packed struct {
      uint8_t                           :  5; /*   (reserved) */
      uint8_t  GUIDETRYSNK              :  1; /*!< GUIDE_TRY_SNK */
      uint8_t  VBUSHIGHVOLT             :  1; /*!< VBUS_HIGH_VOLT */
      uint8_t  IDINSERT                 :  1; /*!< Insert */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } PDCFG1; /* +0x068 */

  union {
    __packed struct {
      uint8_t  SOPRCV                   :  5; /*!< Which SOP to receive */
      uint8_t  CDRSELECT                :  1; /*!< Which SOP to receive */
      uint8_t  FASTSWAPSRC              :  1; /*!< Which SOP to receive */
      uint8_t  FASTSWAPSNK              :  1; /*!< Which SOP to receive */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } PDCFG2; /* +0x06C */

  union {
    __packed struct {
      uint8_t  PPWRROLE                 :  1; /*!< SOP  Current Port Power Role */
      uint8_t  PDATAROLE                :  1; /*!< SOP  Current Port Data Role */
      uint8_t  PPWRROLEPR               :  1; /*!< SOP' Current Port Power Role */
      uint8_t  PDATAROLEPR              :  1; /*!< SOP' Current Port Data Role */
      uint8_t  PPWRROLEDP               :  1; /*!< SOP" Current Port Power Role */
      uint8_t  PDATAROLEDP              :  1; /*!< SOP" Current Port Data Role */
      uint8_t                           :  2; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } PDCFG3; /* +0x070 */

  union {
    __packed struct {
      uint8_t  SHORTTIME                :  7;
      uint8_t  SHORTRESET               :  1;
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } SHORTPROTECT; /* +0x074 */

  union {
    __packed struct {
      uint8_t  TXRESULT                 :  3; /*!< Tx Result */
      uint8_t  RXRESULT                 :  3; /*!< Rx Result */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  FASTSWAP                 :  1; /*!< Fast Role Swap has happened */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } PDSTATUS; /* +0x078 */

  union {
    __packed struct {
      uint8_t  RXCLEAR                  :  1; /*!< RX_CLEAR */
      uint8_t                           :  5; /*   (reserved) */
      uint8_t  RXOVERRUN                :  1; /*!< RX_OVERRUN */
      uint8_t  RXDATA                   :  1; /*!< RX_DATA */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } RXSTATUS; /* +0x07C */

  union {
    __packed struct {
      uint8_t  RXSOP                    :  3; /*!< RX_SOP */
      uint8_t  RXBYTES                  :  5; /*!< RX_BYTES */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } RXINFO; /* +0x080 */

  union {
    __packed struct {
      uint8_t  TXBUFREADY               :  1; /*!< Transmission Status */
      uint8_t  TXSTART                  :  1; /*!< Transmission Start */
      uint8_t  TXWAITRP                 :  1; /*!< Transmission Wait RP */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  TXCMD                    :  3; /*!< Transmission Command */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } TXCOMMAND; /* +0x084 */

  union {
    __packed struct {
      uint8_t  TXSOP                    :  3; /*!< Transmission Status */
      uint8_t  TXRETRIES                :  3; /*!< Transmission Start */
      uint8_t                           :  2; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } TXINFO; /* +0x088 */

  uint32_t _RESERVED_8C[26];

  uint8_t  RXDATA1;                           /*<! RX DATA BYTE +0x0F4 */
  uint8_t  _RESERVED_F5[3];                   /* +0x0F5 */

  uint8_t  RXDATA2;                           /*<! RX DATA BYTE +0x0F8 */
  uint8_t  _RESERVED_F9[3];                   /* +0x0F9 */

  uint8_t  RXDATA3;                           /*<! RX DATA BYTE +0x0FC */
  uint8_t  _RESERVED_FD[3];                   /* +0x0FD */

  uint8_t  RXDATA4;                           /*<! RX DATA BYTE +0x100 */
  uint8_t  _RESERVED_101[3];                  /* +0x101 */

  uint8_t  RXDATA5;                           /*<! RX DATA BYTE +0x104 */
  uint8_t  _RESERVED_105[3];                  /* +0x105 */

  uint8_t  RXDATA6;                           /*<! RX DATA BYTE +0x108 */
  uint8_t  _RESERVED_109[3];                  /* +0x109 */

  uint8_t  RXDATA7;                           /*<! RX DATA BYTE +0x10C */
  uint8_t  _RESERVED_10D[3];                  /* +0x10D */

  uint8_t  RXDATA8;                           /*<! RX DATA BYTE +0x110 */
  uint8_t  _RESERVED_111[3];                  /* +0x111 */

  uint8_t  RXDATA9;                           /*<! RX DATA BYTE +0x114 */
  uint8_t  _RESERVED_115[3];                  /* +0x115 */

  uint8_t  RXDATA10;                          /*<! RX DATA BYTE +0x118 */
  uint8_t  _RESERVED_119[3];                  /* +0x119 */

  uint8_t  RXDATA11;                          /*<! RX DATA BYTE +0x11C */
  uint8_t  _RESERVED_11D[3];                  /* +0x11D */

  uint8_t  RXDATA12;                          /*<! RX DATA BYTE +0x120 */
  uint8_t  _RESERVED_121[3];                  /* +0x121 */

  uint8_t  RXDATA13;                          /*<! RX DATA BYTE +0x124 */
  uint8_t  _RESERVED_125[3];                  /* +0x125 */

  uint8_t  RXDATA14;                          /*<! RX DATA BYTE +0x128 */
  uint8_t  _RESERVED_129[3];                  /* +0x129 */

  uint8_t  RXDATA15;                          /*<! RX DATA BYTE +0x12C */
  uint8_t  _RESERVED_12D[3];                  /* +0x12D */

  uint8_t  RXDATA16;                          /*<! RX DATA BYTE +0x130 */
  uint8_t  _RESERVED_131[3];                  /* +0x131 */

  uint8_t  RXDATA17;                          /*<! RX DATA BYTE +0x134 */
  uint8_t  _RESERVED_135[3];                  /* +0x135 */

  uint8_t  RXDATA18;                          /*<! RX DATA BYTE +0x138 */
  uint8_t  _RESERVED_139[3];                  /* +0x139 */

  uint8_t  RXDATA19;                          /*<! RX DATA BYTE +0x13C */
  uint8_t  _RESERVED_13D[3];                  /* +0x13D */

  uint8_t  RXDATA20;                          /*<! RX DATA BYTE +0x140 */
  uint8_t  _RESERVED_141[3];                  /* +0x141 */

  uint8_t  RXDATA21;                          /*<! RX DATA BYTE +0x144 */
  uint8_t  _RESERVED_145[3];                  /* +0x145 */

  uint8_t  RXDATA22;                          /*<! RX DATA BYTE +0x148 */
  uint8_t  _RESERVED_149[3];                  /* +0x149 */

  uint8_t  RXDATA23;                          /*<! RX DATA BYTE +0x14C */
  uint8_t  _RESERVED_14D[3];                  /* +0x14D */

  uint8_t  RXDATA24;                          /*<! RX DATA BYTE +0x150 */
  uint8_t  _RESERVED_151[3];                  /* +0x151 */

  uint8_t  RXDATA25;                          /*<! RX DATA BYTE +0x154 */
  uint8_t  _RESERVED_155[3];                  /* +0x155 */

  uint8_t  RXDATA26;                          /*<! RX DATA BYTE +0x158 */
  uint8_t  _RESERVED_159[3];                  /* +0x159 */

  uint8_t  RXDATA27;                          /*<! RX DATA BYTE +0x15C */
  uint8_t  _RESERVED_15D[3];                  /* +0x15D */

  uint8_t  RXDATA28;                          /*<! RX DATA BYTE +0x160 */
  uint8_t  _RESERVED_161[3];                  /* +0x161 */

  uint8_t  RXDATA29;                          /*<! RX DATA BYTE +0x164 */
  uint8_t  _RESERVED_165[3];                  /* +0x165 */

  uint8_t  RXDATA30;                          /*<! RX DATA BYTE +0x168 */
  uint8_t  _RESERVED_169[3];                  /* +0x169 */

  uint8_t  TXDATA1;                           /*<! TX DATA BYTE +0x16C */
  uint8_t  _RESERVED_16D[3];                  /* +0x16D */

  uint8_t  TXDATA2;                           /*<! TX DATA BYTE +0x170 */
  uint8_t  _RESERVED_171[3];                  /* +0x171 */

  uint8_t  TXDATA3;                           /*<! TX DATA BYTE +0x174 */
  uint8_t  _RESERVED_175[3];                  /* +0x175 */

  uint8_t  TXDATA4;                           /*<! TX DATA BYTE +0x178 */
  uint8_t  _RESERVED_179[3];                  /* +0x179 */

  uint8_t  TXDATA5;                           /*<! TX DATA BYTE +0x17C */
  uint8_t  _RESERVED_17D[3];                  /* +0x17D */

  uint8_t  TXDATA6;                           /*<! TX DATA BYTE +0x180 */
  uint8_t  _RESERVED_181[3];                  /* +0x181 */

  uint8_t  TXDATA7;                           /*<! TX DATA BYTE +0x184 */
  uint8_t  _RESERVED_185[3];                  /* +0x185 */

  uint8_t  TXDATA8;                           /*<! TX DATA BYTE +0x188 */
  uint8_t  _RESERVED_189[3];                  /* +0x189 */

  uint8_t  TXDATA9;                           /*<! TX DATA BYTE +0x18C */
  uint8_t  _RESERVED_18D[3];                  /* +0x18D */

  uint8_t  TXDATA10;                          /*<! TX DATA BYTE +0x190 */
  uint8_t  _RESERVED_191[3];                  /* +0x191 */

  uint8_t  TXDATA11;                          /*<! TX DATA BYTE +0x194 */
  uint8_t  _RESERVED_195[3];                  /* +0x195 */

  uint8_t  TXDATA12;                          /*<! TX DATA BYTE +0x198 */
  uint8_t  _RESERVED_199[3];                  /* +0x199 */

  uint8_t  TXDATA13;                          /*<! TX DATA BYTE +0x19C */
  uint8_t  _RESERVED_19D[3];                  /* +0x19D */

  uint8_t  TXDATA14;                          /*<! TX DATA BYTE +0x1A0 */
  uint8_t  _RESERVED_1A1[3];                  /* +0x1A1 */

  uint8_t  TXDATA15;                          /*<! TX DATA BYTE +0x1A4 */
  uint8_t  _RESERVED_1A5[3];                  /* +0x1A5 */

  uint8_t  TXDATA16;                          /*<! TX DATA BYTE +0x1A8 */
  uint8_t  _RESERVED_1A9[3];                  /* +0x1A9 */

  uint8_t  TXDATA17;                          /*<! TX DATA BYTE +0x1AC */
  uint8_t  _RESERVED_1AD[3];                  /* +0x1AD */

  uint8_t  TXDATA18;                          /*<! TX DATA BYTE +0x1B0 */
  uint8_t  _RESERVED_1B1[3];                  /* +0x1B1 */

  uint8_t  TXDATA19;                          /*<! TX DATA BYTE +0x1B4 */
  uint8_t  _RESERVED_1B5[3];                  /* +0x1B5 */

  uint8_t  TXDATA20;                          /*<! TX DATA BYTE +0x1B8 */
  uint8_t  _RESERVED_1B9[3];                  /* +0x1B9 */

  uint8_t  TXDATA21;                          /*<! TX DATA BYTE +0x1BC */
  uint8_t  _RESERVED_1BD[3];                  /* +0x1BD */

  uint8_t  TXDATA22;                          /*<! TX DATA BYTE +0x1C0 */
  uint8_t  _RESERVED_1C1[3];                  /* +0x1C1 */

  uint8_t  TXDATA23;                          /*<! TX DATA BYTE +0x1C4 */
  uint8_t  _RESERVED_1C5[3];                  /* +0x1C5 */

  uint8_t  TXDATA24;                          /*<! TX DATA BYTE +0x1C8 */
  uint8_t  _RESERVED_1C9[3];                  /* +0x1C9 */

  uint8_t  TXDATA25;                          /*<! TX DATA BYTE +0x1CC */
  uint8_t  _RESERVED_1CD[3];                  /* +0x1CD */

  uint8_t  TXDATA26;                          /*<! TX DATA BYTE +0x1D0 */
  uint8_t  _RESERVED_1D1[3];                  /* +0x1D1 */

  uint8_t  TXDATA27;                          /*<! TX DATA BYTE +0x1D4 */
  uint8_t  _RESERVED_1D5[3];                  /* +0x1D5 */

  uint8_t  TXDATA28;                          /*<! TX DATA BYTE +0x1D8 */
  uint8_t  _RESERVED_1D9[3];                  /* +0x1D9 */

  uint8_t  TXDATA29;                          /*<! TX DATA BYTE +0x1DC */
  uint8_t  _RESERVED_1DD[3];                  /* +0x1DD */

  uint8_t  TXDATA30;                          /*<! TX DATA BYTE +0x1E0 */
  uint8_t  _RESERVED_1E1[3];                  /* +0x1E1 */

} TYPEC1_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of TYPEC1 SFRS.
 */
#define TYPEC1_SFRS ((__IO TYPEC1_SFRS_t *)0x50017000)

#endif /* end of __TYPEC1_SFR_H__ section */

