/**
 * @copyright 2019 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file trimmemory_sfr.h
 */

#ifndef __TRIMMEMORY_SFR_H__
#define __TRIMMEMORY_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for TRIMMEMORY.
 */
typedef struct {

  union {
    struct {
      uint8_t  Q0BIT0                   :  1; /*!< Q0_BIT0 SD GPIO drive select */
      uint8_t  Q0BIT1                   :  1; /*!< Q0_BIT1 SD GPIO drive select */
      uint8_t  TMENBUFF3V               :  1; /*!< TM_ENBUFF_3V */
      uint8_t  TMDIR3V                  :  1; /*!< TM_DIR_3V */
      uint8_t  TMBYP3V                  :  1; /*!< TM_BYP_3V */
      uint8_t  TMSWSH3V                 :  1; /*!< TM_SWSH_3V */
      uint8_t  OGNDNFW3V                :  1; /*!< O_GND_N_FW_3V */
      uint8_t  OGNDNFWCTRL3V            :  1; /*!< O_GND_N_FW_CTRL_3V */
      uint8_t  O3P31P8NFW               :  1; /*!< O_3P3_1P8N_FW */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  OCP3P3N1P8FW             :  1; /*!< O_CP_3P3N_1P8_FW */
      uint8_t  OCP3P3NFW                :  1; /*!< O_CP_3P3N_FW */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  CARDPOWERLIM             :  2; /*!< CARD_POWER_LIM */
      uint32_t CARDPOWERTHRESHOLD       :  2; /*!< CARD_POWER_THRESHOLD !!misaligned!! */
      uint8_t  CARDPOWEREN              :  1; /*!< CARD_POWER_EN */
      uint8_t  CARDPOWERFWCTL           :  1; /*!< CARD_POWER_FW_CTL */
      uint8_t  CARDPOWERFWVAL           :  1; /*!< CARD_POWER_FW_VAL */
      uint8_t  CARDPOWERFILTEN          :  1; /*!< CARD_POWER_FILT_EN */
      uint32_t MCUREG                   :  4; /*!< MCU_REG !!misaligned!! */
      uint8_t  V2ITRIM                  :  7; /*!< V2I_TRIM */
    };
    uint32_t WORD;
  } TRIMDATA0; /* +0x000 */

  union {
    struct {
      uint8_t  DIGREGR3V                :  4; /*!< DIG_REG_R_3V */
      uint8_t  V2ITRIMENCALB3V          :  1; /*!< V2I_TRIM_ENCAL_B_3V */
      uint32_t BGTRIM3V                 :  4; /*!< BG_TRIM_3V !!misaligned!! */
      uint8_t  ICOSCTRIM3V              :  6; /*!< ICOSC_TRIM_3V */
      uint32_t VREGFLSTRIM3V            :  3; /*!< VREG_FLS_TRIM_3V !!misaligned!! */
      uint8_t  ATB3V                    :  6; /*!< ATB_3V */
      uint8_t  DINPRTCTL33V             :  1; /*!< DIN_PRTCTL_3_3V */
      uint8_t  DINPRTCTL23V             :  1; /*!< DIN_PRTCTL_2_3V */
      uint8_t  DINPRTCTL13V             :  1; /*!< DIN_PRTCTL_1_3V */
      uint8_t  DINBUCKENA3V             :  1; /*!< DIN_BUCKENA_3V */
      uint8_t  OEPRTCTL33V              :  1; /*!< OE_PRTCTL_3_3V */
      uint8_t  OEPRTCTL23V              :  1; /*!< OE_PRTCTL_2_3V */
      uint8_t  OEPRTCTL13V              :  1; /*!< OE_PRTCTL_1_3V */
      uint8_t  OEBUCKENA3V              :  1; /*!< OE_BUCKENA_3V */
    };
    uint32_t WORD;
  } TRIMDATA1; /* +0x004 */

} TRIMMEMORY_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of TRIMMEMORY SFRS.
 */
#define TRIMMEMORY_SFRS ((__IO TRIMMEMORY_SFRS_t *)0x50018000)

#endif /* end of __TRIMMEMORY_SFR_H__ section */


