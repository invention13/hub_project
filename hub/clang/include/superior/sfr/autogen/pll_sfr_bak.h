/**
 * @copyright 2017 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file pll_sfr.h
 */

#ifndef __PLL_SFR_H__
#define __PLL_SFR_H__

#include <stdint.h>

/**
 * @brief A structure to represent Special Function Registers for PLL.
 */
typedef struct {

  union {
    struct {
      uint8_t  PLLTIPFD                 :  4; /*!< PFD Test Inputs */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  PLLTILPF                 :  2; /*!< LPF Test Inputs */
      uint32_t PLLTIVCO                 :  9; /*!< VCO Test Inputs !!misaligned!! */
      uint8_t                           :  7; /*   (reserved) */
      uint8_t  PLLTO                    :  2; /*!< PLL Test Output */
      uint8_t  GMT                      :  2;
      uint8_t  OST                      :  4;
    };
    uint32_t WORD;
  } PLLTEST; /* +0x000 */

  union {
    struct {
      uint8_t  DATA                     :  4;
      uint8_t  CMD                      :  4;
      uint8_t  ZT                       :  4;
      uint8_t  IRT                      :  4;
      uint8_t  CPT                      :  3;
      uint8_t  AUTOCAL                  :  1;
      uint8_t  FT                       :  4;
      uint8_t  LK                       :  1;
      uint8_t  XTALEN                   :  1; /*!< Xtal En */
      uint8_t  LOCK                     :  1;
      uint8_t  CLKUSABLE                :  1;
      uint8_t  DT                       :  2;
      uint8_t  GODIV                    :  1;
      uint8_t  GOVCO                    :  1;
    };
    uint32_t WORD;
  } PLLCMDLFT; /* +0x004 */

} PLL_SFRS_t;

/**
 * @brief The starting address of PLL SFRS.
 */
#define PLL_SFRS ((__IO PLL_SFRS_t *)0x50000050)

#endif /* end of __PLL_SFR_H__ section */


