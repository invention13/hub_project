/**
 * @copyright 2019 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file gpio_sfr.h
 */

#ifndef __GPIO_SFR_H__
#define __GPIO_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for GPIO.
 */
typedef struct {

  uint8_t  GPADATA[1024];                     /*<! Port A data +0x000 */

  uint8_t  GPBDATA[1024];                     /*<! Port B data +0x400 */

  uint8_t  GPCDATA[1024];                     /*<! Port C data +0x800 */

  uint8_t  GPDDATA[1024];                     /*<! Port D data +0xC00 */

  union {
    struct {
      uint8_t  GPADIR0                  :  1; /*!< Pin 0 output enable */
      uint8_t  GPAIE0                   :  1; /*!< Pin 0 interrupt mask */
      uint8_t  GPARE0                   :  1; /*!< Pin 0 rising edge enable */
      uint8_t  GPAFE0                   :  1; /*!< Pin 0 falling edge enable */
      uint8_t  GPACLR0                  :  1; /*!< Pin 0 interrupt clear */
      uint8_t  GPAACTDET0               :  1; /*!< Pin 0 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GPADIR1                  :  1; /*!< Pin 1 output enable */
      uint8_t  GPAIE1                   :  1; /*!< Pin 1 interrupt mask */
      uint8_t  GPARE1                   :  1; /*!< Pin 1 rising edge enable */
      uint8_t  GPAFE1                   :  1; /*!< Pin 1 falling edge enable */
      uint8_t  GPACLR1                  :  1; /*!< Pin 1 interrupt clear */
      uint8_t  GPAACTDET1               :  1; /*!< Pin 1 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GPADIR2                  :  1; /*!< Pin 2 output enable */
      uint8_t  GPAIE2                   :  1; /*!< Pin 2 interrupt mask */
      uint8_t  GPARE2                   :  1; /*!< Pin 2 rising edge enable */
      uint8_t  GPAFE2                   :  1; /*!< Pin 2 falling edge enable */
      uint8_t  GPACLR2                  :  1; /*!< Pin 2 interrupt clear */
      uint8_t  GPAACTDET2               :  1; /*!< Pin 2 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GPADIR3                  :  1; /*!< Pin 3 output enable */
      uint8_t  GPAIE3                   :  1; /*!< Pin 3 interrupt mask */
      uint8_t  GPARE3                   :  1; /*!< Pin 3 rising edge enable */
      uint8_t  GPAFE3                   :  1; /*!< Pin 3 falling edge enable */
      uint8_t  GPACLR3                  :  1; /*!< Pin 3 interrupt clear */
      uint8_t  GPAACTDET3               :  1; /*!< Pin 3 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
    };
    uint32_t WORD;
  } GPAP03; /* +0x1000 */

  union {
    struct {
      uint8_t  GPADIR4                  :  1; /*!< Pin 4 output enable */
      uint8_t  GPAIE4                   :  1; /*!< Pin 4 interrupt mask */
      uint8_t  GPARE4                   :  1; /*!< Pin 4 rising edge enable */
      uint8_t  GPAFE4                   :  1; /*!< Pin 4 falling edge enable */
      uint8_t  GPACLR4                  :  1; /*!< Pin 4 interrupt clear */
      uint8_t  GPAACTDET4               :  1; /*!< Pin 4 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GPADIR5                  :  1; /*!< Pin 5 output enable */
      uint8_t  GPAIE5                   :  1; /*!< Pin 5 interrupt mask */
      uint8_t  GPARE5                   :  1; /*!< Pin 5 rising edge enable */
      uint8_t  GPAFE5                   :  1; /*!< Pin 5 falling edge enable */
      uint8_t  GPACLR5                  :  1; /*!< Pin 5 interrupt clear */
      uint8_t  GPAACTDET5               :  1; /*!< Pin 5 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GPADIR6                  :  1; /*!< Pin 6 output enable */
      uint8_t  GPAIE6                   :  1; /*!< Pin 6 interrupt mask */
      uint8_t  GPARE6                   :  1; /*!< Pin 6 rising edge enable */
      uint8_t  GPAFE6                   :  1; /*!< Pin 6 falling edge enable */
      uint8_t  GPACLR6                  :  1; /*!< Pin 6 interrupt clear */
      uint8_t  GPAACTDET6               :  1; /*!< Pin 6 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GPADIR7                  :  1; /*!< Pin 7 output enable */
      uint8_t  GPAIE7                   :  1; /*!< Pin 7 interrupt mask */
      uint8_t  GPARE7                   :  1; /*!< Pin 7 rising edge enable */
      uint8_t  GPAFE7                   :  1; /*!< Pin 7 falling edge enable */
      uint8_t  GPACLR7                  :  1; /*!< Pin 7 interrupt clear */
      uint8_t  GPAACTDET7               :  1; /*!< Pin 7 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
    };
    uint32_t WORD;
  } GPAP47; /* +0x1004 */

  union {
    struct {
      uint8_t  GPBDIR0                  :  1; /*!< Pin 0 output enable */
      uint8_t  GPBIE0                   :  1; /*!< Pin 0 interrupt mask */
      uint8_t  GPBRE0                   :  1; /*!< Pin 0 rising edge enable */
      uint8_t  GPBFE0                   :  1; /*!< Pin 0 falling edge enable */
      uint8_t  GPBCLR0                  :  1; /*!< Pin 0 interrupt clear */
      uint8_t  GPBACTDET0               :  1; /*!< Pin 0 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GPBDIR1                  :  1; /*!< Pin 1 output enable */
      uint8_t  GPBIE1                   :  1; /*!< Pin 1 interrupt mask */
      uint8_t  GPBRE1                   :  1; /*!< Pin 1 rising edge enable */
      uint8_t  GPBFE1                   :  1; /*!< Pin 1 falling edge enable */
      uint8_t  GPBCLR1                  :  1; /*!< Pin 1 interrupt clear */
      uint8_t  GPBACTDET1               :  1; /*!< Pin 1 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GPBDIR2                  :  1; /*!< Pin 2 output enable */
      uint8_t  GPBIE2                   :  1; /*!< Pin 2 interrupt mask */
      uint8_t  GPBRE2                   :  1; /*!< Pin 2 rising edge enable */
      uint8_t  GPBFE2                   :  1; /*!< Pin 2 falling edge enable */
      uint8_t  GPBCLR2                  :  1; /*!< Pin 2 interrupt clear */
      uint8_t  GPBACTDET2               :  1; /*!< Pin 2 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GPBDIR3                  :  1; /*!< Pin 3 output enable */
      uint8_t  GPBIE3                   :  1; /*!< Pin 3 interrupt mask */
      uint8_t  GPBRE3                   :  1; /*!< Pin 3 rising edge enable */
      uint8_t  GPBFE3                   :  1; /*!< Pin 3 falling edge enable */
      uint8_t  GPBCLR3                  :  1; /*!< Pin 3 interrupt clear */
      uint8_t  GPBACTDET3               :  1; /*!< Pin 3 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
    };
    uint32_t WORD;
  } GPBP03; /* +0x1008 */

  union {
    struct {
      uint8_t  GPBDIR4                  :  1; /*!< Pin 4 output enable */
      uint8_t  GPBIE4                   :  1; /*!< Pin 4 interrupt mask */
      uint8_t  GPBRE4                   :  1; /*!< Pin 4 rising edge enable */
      uint8_t  GPBFE4                   :  1; /*!< Pin 4 falling edge enable */
      uint8_t  GPBCLR4                  :  1; /*!< Pin 4 interrupt clear */
      uint8_t  GPBACTDET4               :  1; /*!< Pin 4 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GPBDIR5                  :  1; /*!< Pin 5 output enable */
      uint8_t  GPBIE5                   :  1; /*!< Pin 5 interrupt mask */
      uint8_t  GPBRE5                   :  1; /*!< Pin 5 rising edge enable */
      uint8_t  GPBFE5                   :  1; /*!< Pin 5 falling edge enable */
      uint8_t  GPBCLR5                  :  1; /*!< Pin 5 interrupt clear */
      uint8_t  GPBACTDET5               :  1; /*!< Pin 5 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GPBDIR6                  :  1; /*!< Pin 6 output enable */
      uint8_t  GPBIE6                   :  1; /*!< Pin 6 interrupt mask */
      uint8_t  GPBRE6                   :  1; /*!< Pin 6 rising edge enable */
      uint8_t  GPBFE6                   :  1; /*!< Pin 6 falling edge enable */
      uint8_t  GPBCLR6                  :  1; /*!< Pin 6 interrupt clear */
      uint8_t  GPBACTDET6               :  1; /*!< Pin 6 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GPBDIR7                  :  1; /*!< Pin 7 output enable */
      uint8_t  GPBIE7                   :  1; /*!< Pin 7 interrupt mask */
      uint8_t  GPBRE7                   :  1; /*!< Pin 7 rising edge enable */
      uint8_t  GPBFE7                   :  1; /*!< Pin 7 falling edge enable */
      uint8_t  GPBCLR7                  :  1; /*!< Pin 7 interrupt clear */
      uint8_t  GPBACTDET7               :  1; /*!< Pin 7 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
    };
    uint32_t WORD;
  } GPBP47; /* +0x100C */

  union {
    struct {
      uint8_t  GPCDIR0                  :  1; /*!< Pin 0 output enable */
      uint8_t  GPCIE0                   :  1; /*!< Pin 0 interrupt mask */
      uint8_t  GPCRE0                   :  1; /*!< Pin 0 rising edge enable */
      uint8_t  GPCFE0                   :  1; /*!< Pin 0 falling edge enable */
      uint8_t  GPCCLR0                  :  1; /*!< Pin 0 interrupt clear */
      uint8_t  GPCACTDET0               :  1; /*!< Pin 0 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GPCDIR1                  :  1; /*!< Pin 1 output enable */
      uint8_t  GPCIE1                   :  1; /*!< Pin 1 interrupt mask */
      uint8_t  GPCRE1                   :  1; /*!< Pin 1 rising edge enable */
      uint8_t  GPCFE1                   :  1; /*!< Pin 1 falling edge enable */
      uint8_t  GPCCLR1                  :  1; /*!< Pin 1 interrupt clear */
      uint8_t  GPCACTDET1               :  1; /*!< Pin 1 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GPCDIR2                  :  1; /*!< Pin 2 output enable */
      uint8_t  GPCIE2                   :  1; /*!< Pin 2 interrupt mask */
      uint8_t  GPCRE2                   :  1; /*!< Pin 2 rising edge enable */
      uint8_t  GPCFE2                   :  1; /*!< Pin 2 falling edge enable */
      uint8_t  GPCCLR2                  :  1; /*!< Pin 2 interrupt clear */
      uint8_t  GPCACTDET2               :  1; /*!< Pin 2 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GPCDIR3                  :  1; /*!< Pin 3 output enable */
      uint8_t  GPCIE3                   :  1; /*!< Pin 3 interrupt mask */
      uint8_t  GPCRE3                   :  1; /*!< Pin 3 rising edge enable */
      uint8_t  GPCFE3                   :  1; /*!< Pin 3 falling edge enable */
      uint8_t  GPCCLR3                  :  1; /*!< Pin 3 interrupt clear */
      uint8_t  GPCACTDET3               :  1; /*!< Pin 3 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
    };
    uint32_t WORD;
  } GPCP03; /* +0x1010 */

  union {
    struct {
      uint8_t  GPCDIR4                  :  1; /*!< Pin 4 output enable */
      uint8_t  GPCIE4                   :  1; /*!< Pin 4 interrupt mask */
      uint8_t  GPCRE4                   :  1; /*!< Pin 4 rising edge enable */
      uint8_t  GPCFE4                   :  1; /*!< Pin 4 falling edge enable */
      uint8_t  GPCCLR4                  :  1; /*!< Pin 4 interrupt clear */
      uint8_t  GPCACTDET4               :  1; /*!< Pin 4 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GPCDIR5                  :  1; /*!< Pin 5 output enable */
      uint8_t  GPCIE5                   :  1; /*!< Pin 5 interrupt mask */
      uint8_t  GPCRE5                   :  1; /*!< Pin 5 rising edge enable */
      uint8_t  GPCFE5                   :  1; /*!< Pin 5 falling edge enable */
      uint8_t  GPCCLR5                  :  1; /*!< Pin 5 interrupt clear */
      uint8_t  GPCACTDET5               :  1; /*!< Pin 5 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GPCDIR6                  :  1; /*!< Pin 6 output enable */
      uint8_t  GPCIE6                   :  1; /*!< Pin 6 interrupt mask */
      uint8_t  GPCRE6                   :  1; /*!< Pin 6 rising edge enable */
      uint8_t  GPCFE6                   :  1; /*!< Pin 6 falling edge enable */
      uint8_t  GPCCLR6                  :  1; /*!< Pin 6 interrupt clear */
      uint8_t  GPCACTDET6               :  1; /*!< Pin 6 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GPCDIR7                  :  1; /*!< Pin 7 output enable */
      uint8_t  GPCIE7                   :  1; /*!< Pin 7 interrupt mask */
      uint8_t  GPCRE7                   :  1; /*!< Pin 7 rising edge enable */
      uint8_t  GPCFE7                   :  1; /*!< Pin 7 falling edge enable */
      uint8_t  GPCCLR7                  :  1; /*!< Pin 7 interrupt clear */
      uint8_t  GPCACTDET7               :  1; /*!< Pin 7 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
    };
    uint32_t WORD;
  } GPCP47; /* +0x1014 */

  union {
    struct {
      uint8_t  GPDDIR0                  :  1; /*!< Pin 0 output enable */
      uint8_t  GPDIE0                   :  1; /*!< Pin 0 interrupt mask */
      uint8_t  GPDRE0                   :  1; /*!< Pin 0 rising edge enable */
      uint8_t  GPDFE0                   :  1; /*!< Pin 0 falling edge enable */
      uint8_t  GPDCLR0                  :  1; /*!< Pin 0 interrupt clear */
      uint8_t  GPDACTDET0               :  1; /*!< Pin 0 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GPDDIR1                  :  1; /*!< Pin 1 output enable */
      uint8_t  GPDIE1                   :  1; /*!< Pin 1 interrupt mask */
      uint8_t  GPDRE1                   :  1; /*!< Pin 1 rising edge enable */
      uint8_t  GPDFE1                   :  1; /*!< Pin 1 falling edge enable */
      uint8_t  GPDCLR1                  :  1; /*!< Pin 1 interrupt clear */
      uint8_t  GPDACTDET1               :  1; /*!< Pin 1 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GPDDIR2                  :  1; /*!< Pin 2 output enable */
      uint8_t  GPDIE2                   :  1; /*!< Pin 2 interrupt mask */
      uint8_t  GPDRE2                   :  1; /*!< Pin 2 rising edge enable */
      uint8_t  GPDFE2                   :  1; /*!< Pin 2 falling edge enable */
      uint8_t  GPDCLR2                  :  1; /*!< Pin 2 interrupt clear */
      uint8_t  GPDACTDET2               :  1; /*!< Pin 2 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GPDDIR3                  :  1; /*!< Pin 3 output enable */
      uint8_t  GPDIE3                   :  1; /*!< Pin 3 interrupt mask */
      uint8_t  GPDRE3                   :  1; /*!< Pin 3 rising edge enable */
      uint8_t  GPDFE3                   :  1; /*!< Pin 3 falling edge enable */
      uint8_t  GPDCLR3                  :  1; /*!< Pin 3 interrupt clear */
      uint8_t  GPDACTDET3               :  1; /*!< Pin 3 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
    };
    uint32_t WORD;
  } GPDP03; /* +0x1018 */

  union {
    struct {
      uint8_t  GPDDIR4                  :  1; /*!< Pin 4 output enable */
      uint8_t  GPDIE4                   :  1; /*!< Pin 4 interrupt mask */
      uint8_t  GPDRE4                   :  1; /*!< Pin 4 rising edge enable */
      uint8_t  GPDFE4                   :  1; /*!< Pin 4 falling edge enable */
      uint8_t  GPDCLR4                  :  1; /*!< Pin 4 interrupt clear */
      uint8_t  GPDACTDET4               :  1; /*!< Pin 4 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GPDDIR5                  :  1; /*!< Pin 5 output enable */
      uint8_t  GPDIE5                   :  1; /*!< Pin 5 interrupt mask */
      uint8_t  GPDRE5                   :  1; /*!< Pin 5 rising edge enable */
      uint8_t  GPDFE5                   :  1; /*!< Pin 5 falling edge enable */
      uint8_t  GPDCLR5                  :  1; /*!< Pin 5 interrupt clear */
      uint8_t  GPDACTDET5               :  1; /*!< Pin 5 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GPDDIR6                  :  1; /*!< Pin 6 output enable */
      uint8_t  GPDIE6                   :  1; /*!< Pin 6 interrupt mask */
      uint8_t  GPDRE6                   :  1; /*!< Pin 6 rising edge enable */
      uint8_t  GPDFE6                   :  1; /*!< Pin 6 falling edge enable */
      uint8_t  GPDCLR6                  :  1; /*!< Pin 6 interrupt clear */
      uint8_t  GPDACTDET6               :  1; /*!< Pin 6 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GPDDIR7                  :  1; /*!< Pin 7 output enable */
      uint8_t  GPDIE7                   :  1; /*!< Pin 7 interrupt mask */
      uint8_t  GPDRE7                   :  1; /*!< Pin 7 rising edge enable */
      uint8_t  GPDFE7                   :  1; /*!< Pin 7 falling edge enable */
      uint8_t  GPDCLR7                  :  1; /*!< Pin 7 interrupt clear */
      uint8_t  GPDACTDET7               :  1; /*!< Pin 7 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
    };
    uint32_t WORD;
  } GPDP47; /* +0x101C */

  uint32_t _RESERVED_1020[258];

  union {
    struct {
      uint8_t  GPAENA                   :  1; /*!< Port A Data Clock Gate Enable */
      uint8_t  GPBENA                   :  1; /*!< Port B Data Clock Gate Enable */
      uint8_t  GPCENA                   :  1; /*!< Port C Data Clock Gate Enable */
      uint8_t  GPDENA                   :  1; /*!< Port D Data Clock Gate Enable */
      uint8_t                           :  4; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } GPENA; /* +0x1428 */

} GPIO_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of GPIO SFRS.
 */
#define GPIO_SFRS ((__IO GPIO_SFRS_t *)0x5001c000)

#endif /* end of __GPIO_SFR_H__ section */


