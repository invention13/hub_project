/**
 * @copyright 2019 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file pwm4_sfr.h
 */

#ifndef __PWM4_SFR_H__
#define __PWM4_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for PWM4.
 */
typedef struct {

  union {
    struct {
      uint8_t  ENABLE                   :  1;
      uint8_t  INVERT                   :  1;
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  PRESCALESEL              :  3; /*!< Prescaler select */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  UPDATE                   :  8;
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } CTRL; /* +0x000 */

  union {
    struct {
      uint16_t PERIOD                   : 16;
      uint16_t PWIDTH                   : 16; /*!< Pulse Width */
    };
    uint32_t WORD;
  } SETUP; /* +0x004 */

} PWM4_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of PWM4 SFRS.
 */
#define PWM4_SFRS ((__IO PWM4_SFRS_t *)0x50014680)

#endif /* end of __PWM4_SFR_H__ section */


