/**
 * @copyright 2019 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file ppc_sfr.h
 */

#ifndef __PPC_SFR_H__
#define __PPC_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for PPC.
 */
typedef struct {

  union {
    struct {
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  DFP1ENA                  :  1; /*!< Overcurrent IRQ Enable DFP1 */
      uint8_t  DFP2ENA                  :  1; /*!< Overcurrent IRQ Enable DFP2 */
      uint8_t  DFP3ENA                  :  1; /*!< Overcurrent IRQ Enable DFP3 */
      uint8_t                           :  4; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } OCIRQCTRL; /* +0x000 */

  union {
    struct {
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  DFP1                     :  1; /*!< Downstream Facing Port 1 */
      uint8_t  DFP2                     :  1; /*!< Downstream Facing Port 2 */
      uint8_t  DFP3                     :  1; /*!< Downstream Facing Port 3 */
      uint8_t                           :  4; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } OCIRQFLAG; /* +0x001 */

  union {
    struct {
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  FWDFP1                   :  1; /*!< DRVVBUS from Firmware State Machine */
      uint8_t  FWDFP2                   :  1; /*!< DRVVBUS from Firmware State Machine */
      uint8_t  FWDFP3                   :  1; /*!< DRVVBUS from Firmware State Machine */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  HUBDFP1                  :  1; /*!< DRVVBUS from USB HUB */
      uint8_t  HUBDFP2                  :  1; /*!< DRVVBUS from USB HUB */
      uint8_t  HUBDFP3                  :  1; /*!< DRVVBUS from USB HUB */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } DRVVBUSSTATUS; /* +0x002 */

  union {
    struct {
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  STATUS                   :  1; /*!< VBUS Detect Status */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  FLAG                     :  1; /*!< VBUS Detect Flag */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } VBUSDET; /* +0x003 */

  union {
    struct {
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  DFP1                     :  1; /*!< Overcurrent Status DFP1 */
      uint8_t  DFP2                     :  1; /*!< Overcurrent Status DFP2 */
      uint8_t  DFP3                     :  1; /*!< Overcurrent Status DFP3 */
      uint8_t                           :  4; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } OC_STATUS; /* +0x004 */

  union {
    struct {
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  FAULTNPU1                :  1; /*!< FAULT_N Pull UP Control */
      uint8_t  FAULTNPU2                :  1; /*!< FAULT_N Pull UP Control */
      uint8_t  FAULTNPU3                :  1; /*!< FAULT_N Pull UP Control */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  FAULTNPD1                :  1; /*!< FAULT_N Pull DOWN Control */
      uint8_t  FAULTNPD2                :  1; /*!< FAULT_N Pull DOWN Control */
      uint8_t  FAULTNPD3                :  1; /*!< FAULT_N Pull DOWN Control */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } FAULTNPUPDCTRL; /* +0x005 */

  union {
    struct {
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  FAULTNOE1                :  1; /*!< FAULT_N Output Enable Control */
      uint8_t  FAULTNOE2                :  1; /*!< FAULT_N Output Enable Control */
      uint8_t  FAULTNOE3                :  1; /*!< FAULT_N Output Enable Control */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  FAULTNOUT1               :  1; /*!< FAULT_N Data Out Control */
      uint8_t  FAULTNOUT2               :  1; /*!< FAULT_N Data Out Control */
      uint8_t  FAULTNOUT3               :  1; /*!< FAULT_N Data Out Control */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } FAULTNOUTPUTCTRL; /* +0x006 */

  union {
    struct {
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  FAULTNIN1                :  1; /*!< FAULT_N Input Data */
      uint8_t  FAULTNIN2                :  1; /*!< FAULT_N Input Data */
      uint8_t  FAULTNIN3                :  1; /*!< FAULT_N Input Data */
      uint8_t                           :  4; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } FAULTNINPUT; /* +0x007 */

  union {
    struct {
      uint8_t  MODE                     :  2; /*!< Drive VBUS Mode Select */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  VBUSFWREQ                :  1; /*!< Firmware VBUS Request */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  STATUS                   :  1; /*!< DRVVBUS Status */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } DFP1DRVVBUSCTRL; /* +0x008 */

  union {
    struct {
      uint8_t  MODE                     :  3; /*!< Overcurrent Multiplexer Mode Select */
      uint8_t  CLRDBNC                  :  1; /*!< Clear Overcurrent Debounce */
      uint8_t  OCFWSFR                  :  1; /*!< Firmware Control */
      uint8_t  DBNCDELAY                :  2; /*!< Overcurrent Debouncer Delay Control */
      uint8_t  POL                      :  1; /*!< Overcurrent Debouncer Polarity Control */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } DFP1OCCTRL; /* +0x009 */

  union {
    struct {
      uint8_t  MODE                     :  4; /*!< Drive VBUS Multiplexer Mode Select */
      uint8_t  VBUSFWREQ                :  1; /*!< Firmware VBUS Request */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  STATUS                   :  1; /*!< DRVVBUS Status */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } DFP2DRVVBUSCTRL; /* +0x00A */

  union {
    struct {
      uint8_t  MODE                     :  3; /*!< Overcurrent Multiplexer Mode Select */
      uint8_t  CLRDBNC                  :  1; /*!< Clear Overcurrent Debounce */
      uint8_t  OCFWSFR                  :  1; /*!< Firmware Control */
      uint8_t  DBNCDELAY                :  2; /*!< Overcurrent Debouncer Delay Control */
      uint8_t  POL                      :  1; /*!< Overcurrent Debouncer Polarity Control */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } DFP2OCCTRL; /* +0x00B */

  union {
    struct {
      uint8_t  MODE                     :  4; /*!< Drive VBUS Multiplexer Mode Select */
      uint8_t  VBUSFWREQ                :  1; /*!< Firmware VBUS Request */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  STATUS                   :  1; /*!< DRVVBUS Status */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } DFP3DRVVBUSCTRL; /* +0x00C */

  union {
    struct {
      uint8_t  MODE                     :  3; /*!< Overcurrent Multiplexer Mode Select */
      uint8_t  CLRDBNC                  :  1; /*!< Clear Overcurrent Debounce */
      uint8_t  OCFWSFR                  :  1; /*!< Firmware Control */
      uint8_t  DBNCDELAY                :  2; /*!< Overcurrent Debouncer Delay Control */
      uint8_t  POL                      :  1; /*!< Overcurrent Debouncer Polarity Control */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } DFP3OCCTRL; /* +0x00D */

  union {
    struct {
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  DFP1                     :  1; /*!< Force Overcurrent Event into HUB */
      uint8_t  DFP2                     :  1; /*!< Force Overcurrent Event into HUB */
      uint8_t  DFP3                     :  1; /*!< Force Overcurrent Event into HUB */
      uint8_t  DFP4                     :  1; /*!< Force Overcurrent Event into HUB */
      uint8_t  DFP5                     :  1; /*!< Force Overcurrent Event into HUB */
      uint8_t                           :  2; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } HUBOCFORCE; /* +0x00E */

} PPC_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of PPC SFRS.
 */
#define PPC_SFRS ((__IO PPC_SFRS_t *)0x50000030)

#endif /* end of __PPC_SFR_H__ section */


