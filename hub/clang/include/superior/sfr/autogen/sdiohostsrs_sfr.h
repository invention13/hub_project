/**
 * @copyright 2019 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file sdiohostsrs_sfr.h
 */

#ifndef __SDIOHOSTSRS_SFR_H__
#define __SDIOHOSTSRS_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for SDIOHOSTSRS.
 */
typedef struct {

  uint32_t SDMASAARG2;                        /*<! Sdma Sa Arg 2 +0x000 */

  union {
    struct {
      uint16_t TBS                      : 12;
      uint8_t  HDMABB                   :  3;
      uint8_t                           :  1; /*   (reserved) */
      uint16_t BCCT                     : 16;
    };
    uint32_t WORD;
  } SRS01; /* +0x004 */

  uint32_t ARG;                               /* +0x008 */

  union {
    struct {
      uint8_t  DMAE                     :  1;
      uint8_t  BCE                      :  1;
      uint8_t  ACE                      :  2;
      uint8_t  DTDS                     :  1;
      uint8_t  MSBS                     :  1;
      uint8_t                           :  2; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
      uint8_t  RTS                      :  2;
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  CRCCE                    :  1;
      uint8_t  CICE                     :  1;
      uint8_t  DPS                      :  1;
      uint8_t  CT                       :  2;
      uint8_t  CI                       :  8;
    };
    uint32_t WORD;
  } SRS03; /* +0x00C */

  uint32_t RESP0;                             /* +0x010 */

  uint32_t RESP1;                             /* +0x014 */

  uint32_t RESP2;                             /* +0x018 */

  uint32_t RESP3;                             /* +0x01C */

  uint32_t BDP;                               /* +0x020 */

  union {
    struct {
      uint8_t  CICMD                    :  1;
      uint8_t  CIDAT                    :  1;
      uint8_t  DATLA                    :  1;
      uint8_t  RTNGR                    :  1;
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  WTA                      :  1;
      uint8_t  RTA                      :  1;
      uint8_t  BWE                      :  1;
      uint8_t  BRE                      :  1;
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  CI                       :  1;
      uint8_t  CSS                      :  1;
      uint8_t  CDL                      :  1;
      uint8_t  WPSL                     :  1;
      uint8_t  DATSL                    :  4;
      uint8_t  CMDSL                    :  8;
    };
    uint32_t WORD;
  } SRS09; /* +0x024 */

  union {
    struct {
      uint8_t  LEDC                     :  1;
      uint8_t  DTW                      :  1;
      uint8_t  HSE                      :  1;
      uint8_t  DMASEL                   :  2;
      uint8_t  EDTW                     :  1;
      uint8_t  CDTL                     :  1;
      uint8_t  CDSS                     :  1;
      uint8_t  BP                       :  1;
      uint8_t  BVS                      :  3;
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  SBGR                     :  1;
      uint8_t  CR                       :  1;
      uint8_t  RWC                      :  1;
      uint8_t  IBG                      :  1;
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  WCINT                    :  1;
      uint8_t  WCI                      :  1;
      uint8_t  WCR                      :  1;
      uint8_t                           :  5; /*   (reserved) */
    };
    uint32_t WORD;
  } SRS10; /* +0x028 */

  union {
    struct {
      uint8_t  ICE                      :  1;
      uint8_t  ICS                      :  1;
      uint8_t  SDCE                     :  1;
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  CGS                      :  1;
      uint8_t  USDCLKFS                 :  2;
      uint8_t  SDCLKFS                  :  8;
      uint8_t  DTCV                     :  8;
      uint8_t  SRFA                     :  1;
      uint8_t  SRCMD                    :  1;
      uint8_t  SRDAT                    :  1;
      uint8_t                           :  5; /*   (reserved) */
    };
    uint32_t WORD;
  } SRS11; /* +0x02C */

  union {
    struct {
      uint8_t  CC                       :  1;
      uint8_t  TC                       :  1;
      uint8_t  BGE                      :  1;
      uint8_t  DMAINT                   :  1;
      uint8_t  BWR                      :  1;
      uint8_t  BRR0                     :  1;
      uint8_t  CIN                      :  1;
      uint8_t  CR                       :  1;
      uint8_t  CINT                     :  1;
      uint8_t  INTA                     :  1; /*!< Int A */
      uint8_t  INTB                     :  1; /*!< Int B */
      uint8_t  INTC                     :  1; /*!< Int C */
      uint8_t  RTNGE                    :  1;
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  EINT                     :  1;
      uint8_t  ECT                      :  1;
      uint8_t  ECCRC                    :  1;
      uint8_t  ECEB                     :  1;
      uint8_t  ECI                      :  1;
      uint8_t  EDT                      :  1;
      uint8_t  EDCRC                    :  1;
      uint8_t  EDEB                     :  1;
      uint8_t  ECL                      :  1;
      uint8_t  EAC                      :  1;
      uint8_t  EADMA                    :  1;
      uint8_t  ETNG                     :  1;
      uint8_t                           :  5; /*   (reserved) */
    };
    uint32_t WORD;
  } SRS12; /* +0x030 */

  union {
    struct {
      uint8_t  CCSE                     :  1; /*!< Cc Se */
      uint8_t  TCSE                     :  1; /*!< Tc Se */
      uint8_t  BGESE                    :  1; /*!< Bge Se */
      uint8_t  DMAINTSE                 :  1; /*!< Dmaint Se */
      uint8_t  BWRSE                    :  1; /*!< Bwr Se */
      uint8_t  BBRSE                    :  1; /*!< Bbr Se */
      uint8_t  CINSE                    :  1; /*!< Cin Se */
      uint8_t  CRSE                     :  1; /*!< Cr Se */
      uint8_t  CINTSE                   :  1; /*!< Cint Se */
      uint8_t  INTASE                   :  1; /*!< Int A Se */
      uint8_t  INTBSE                   :  1; /*!< Int B Se */
      uint8_t  INTCSE                   :  1; /*!< Int C Se */
      uint8_t  RTNGESE                  :  1; /*!< Rtnge Se */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  ECTSE                    :  1; /*!< Ect Se */
      uint8_t  ECCRCSE                  :  1; /*!< Eccrc Se */
      uint8_t  ECEBSE                   :  1; /*!< Eceb Se */
      uint8_t  ECISE                    :  1; /*!< Eci Se */
      uint8_t  EDTSE                    :  1; /*!< Edt Se */
      uint8_t  EDCRCSE                  :  1; /*!< Edcrc Se */
      uint8_t  EDEBSE                   :  1; /*!< Edeb Se */
      uint8_t  ECLSE                    :  1; /*!< Ecl Se */
      uint8_t  EAC12SE                  :  1; /*!< Eac12 Se */
      uint8_t  EADMASE                  :  1; /*!< Eadma Se */
      uint8_t  ETNGSE                   :  1; /*!< Etng Se */
      uint8_t                           :  5; /*   (reserved) */
    };
    uint32_t WORD;
  } SRS13; /* +0x034 */

  union {
    struct {
      uint8_t  CCIE                     :  1; /*!< Cc Ie */
      uint8_t  TCIE                     :  1; /*!< Tc Ie */
      uint8_t  BGEIE                    :  1; /*!< Bge Ie */
      uint8_t  DMAINTIE                 :  1; /*!< Dmaint Ie */
      uint8_t  BWRIE                    :  1; /*!< Bwr Ie */
      uint8_t  BBRIE                    :  1; /*!< Bbr Ie */
      uint8_t  CINIE                    :  1; /*!< Cin Ie */
      uint8_t  CRIE                     :  1; /*!< Cr Ie */
      uint8_t  CINTIE                   :  1; /*!< Cint Ie */
      uint8_t  INTAIE                   :  1; /*!< Int A Ie */
      uint8_t  INTBIE                   :  1; /*!< Int B Ie */
      uint8_t  INTCIE                   :  1; /*!< Int C Ie */
      uint8_t  RTNGEIE                  :  1; /*!< Rtnge Ie */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  ECTIE                    :  1; /*!< Ect Ie */
      uint8_t  ECCRCIE                  :  1; /*!< Eccrc Ie */
      uint8_t  ECEBIE                   :  1; /*!< Eceb Ie */
      uint8_t  ECIIE                    :  1; /*!< Eci Ie */
      uint8_t  EDTIE                    :  1; /*!< Edt Ie */
      uint8_t  EDCRCIE                  :  1; /*!< Edcrc Ie */
      uint8_t  EDEBIE                   :  1; /*!< Edeb Ie */
      uint8_t  ECLIE                    :  1; /*!< Ecl Ie */
      uint8_t  EAC12IE                  :  1; /*!< Eac12 Ie */
      uint8_t  EADMAIE                  :  1; /*!< Eadma Ie */
      uint8_t  ETNGIE                   :  1; /*!< Etng Ie */
      uint8_t                           :  5; /*   (reserved) */
    };
    uint32_t WORD;
  } SRS14; /* +0x038 */

  union {
    struct {
      uint8_t  ACNE                     :  1;
      uint8_t  ACTE                     :  1;
      uint8_t  ACCE                     :  1;
      uint8_t  ACEBE                    :  1;
      uint8_t  ACIE                     :  1;
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  CNIACE                   :  1;
      uint8_t                           :  8; /*   (reserved) */
      uint8_t  UHSMS                    :  3;
      uint8_t  SRS151V8SE               :  1; /*!< 1v8se */
      uint8_t  DSS                      :  2;
      uint8_t  EXTNG                    :  1;
      uint8_t  SCS                      :  1;
      uint8_t                           :  6; /*   (reserved) */
      uint8_t  AIE                      :  1;
      uint8_t  PVE                      :  1;
    };
    uint32_t WORD;
  } SRS15; /* +0x03C */

  union {
    struct {
      uint8_t  TCF                      :  6;
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  TCU                      :  1;
      uint8_t  BCSDCLK                  :  8;
      uint8_t  MBL                      :  2;
      uint8_t  SRS168EDS                :  1; /*!< 8eds */
      uint8_t  ADMA2S                   :  1;
      uint8_t  ADMA1S                   :  1;
      uint8_t  HSS                      :  1;
      uint8_t  DMAS                     :  1;
      uint8_t  SRS                      :  1;
      uint8_t  VS33                     :  1;
      uint8_t  VS30                     :  1;
      uint8_t  VS18                     :  1;
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  SRS1664BS                :  1; /*!< 64bs */
      uint8_t  AIS                      :  1;
      uint8_t  SLT                      :  2;
    };
    uint32_t WORD;
  } SRS16; /* +0x040 */

  union {
    struct {
      uint8_t  SDR50                    :  1;
      uint8_t  SDR104                   :  1;
      uint8_t  DDR50                    :  1;
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  DRVA                     :  1;
      uint8_t  DRVC                     :  1;
      uint8_t  DRVD                     :  1;
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  RTNGCNT                  :  4;
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  UTSM50                   :  1;
      uint8_t  RTNGM                    :  2;
      uint8_t  CLKMPR                   :  8;
      uint8_t                           :  8; /*   (reserved) */
    };
    uint32_t WORD;
  } SRS17; /* +0x044 */

  union {
    struct {
      uint8_t  MC33                     :  8;
      uint8_t  MC30                     :  8;
      uint8_t  MC18                     :  8;
      uint8_t                           :  8; /*   (reserved) */
    };
    uint32_t WORD;
  } SRS18; /* +0x048 */

  uint32_t _RESERVED_4C;

  union {
    struct {
      uint8_t  ACNEFE                   :  1; /*!< Acne Fe */
      uint8_t  ACTEFE                   :  1; /*!< Acte Fe */
      uint8_t  ACCEFE                   :  1; /*!< Acce Fe */
      uint8_t  ACEBEFE                  :  1; /*!< Acebe Fe */
      uint8_t  ACIEFE                   :  1; /*!< Acie Fe */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  CNIACEFE                 :  1; /*!< Cniace Fe */
      uint8_t                           :  8; /*   (reserved) */
      uint8_t  ECTFE                    :  1; /*!< Ect Fe */
      uint8_t  ECCRCFE                  :  1; /*!< Eccrc Fe */
      uint8_t  ECEBFE                   :  1; /*!< Eceb Fe */
      uint8_t  ECIFE                    :  1; /*!< Eci Fe */
      uint8_t  EDTFE                    :  1; /*!< Edt Fe */
      uint8_t  EDCRCFE                  :  1; /*!< Edcrc Fe */
      uint8_t  EDEBFE                   :  1; /*!< Edeb Fe */
      uint8_t  ECLFE                    :  1; /*!< Ecl Fe */
      uint8_t  EAC12FE                  :  1; /*!< Eac12 Fe */
      uint8_t  EADMAFE                  :  1; /*!< Eadma Fe */
      uint8_t                           :  6; /*   (reserved) */
    };
    uint32_t WORD;
  } SRS20; /* +0x050 */

  union {
    struct {
      uint8_t  ADMAES                   :  2;
      uint8_t  ADMAL                    :  1;
      uint8_t                           :  5; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } SRS21; /* +0x054 */

  uint32_t ADMASA;                            /*<! Adma Sa +0x058 */

  uint32_t _RESERVED_5C;

  union {
    struct {
      uint16_t SDCLKFSVID               : 10; /*!< Sdclkfsv Id */
      uint8_t  CGSVID                   :  1; /*!< Cgsv Id */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  DRVSSVID                 :  2; /*!< Drv Ssv Id */
      uint16_t SDCLKFSVDS               : 10; /*!< Sdclkfsv Ds */
      uint8_t  CGSVDS                   :  1; /*!< Cgsv Ds */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  DRVSSVDS                 :  2; /*!< Drv Ssv Ds */
    };
    uint32_t WORD;
  } SRS24; /* +0x060 */

  union {
    struct {
      uint16_t SDCLKFSVHS               : 10; /*!< Sdclkfsv Hs */
      uint8_t  CGSVHS                   :  1; /*!< Cgsv Hs */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  DRVSSVHS                 :  2; /*!< Drv Ssv Hs */
      uint16_t SDCLKFSVSDR12            : 10; /*!< Sdclkfsv Sdr12 */
      uint8_t  CGSVSDR12                :  1; /*!< Cgsv Sdr12 */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  DRVSSVSDR12              :  2; /*!< Drv Ssv Sdr12 */
    };
    uint32_t WORD;
  } SRS25; /* +0x064 */

  union {
    struct {
      uint16_t SDCLKFSVSDR25            : 10; /*!< Sdclkfsv Sdr25 */
      uint8_t  CGSVSDR25                :  1; /*!< Cgsv Sdr25 */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  DRVSSVSDR25              :  2; /*!< Drv Ssv Sdr25 */
      uint16_t SDCLKFSVSDR50            : 10; /*!< Sdclkfsv Sdr50 */
      uint8_t  CGSVSDR50                :  1; /*!< Cgsv Sdr50 */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  DRVSSVSDR50              :  2; /*!< Drv Ssv Sdr50 */
    };
    uint32_t WORD;
  } SRS26; /* +0x068 */

  union {
    struct {
      uint16_t SDCLKFSVSDR104           : 10; /*!< Sdclkfsv Sdr104 */
      uint8_t  CGSVSDR104               :  1; /*!< Cgsv Sdr104 */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  DRVSSVSDR104             :  2; /*!< Drv Ssv Sdr104 */
      uint16_t SDCLKFSVDDR50            : 10; /*!< Sdclkfsv Ddr50 */
      uint8_t  CGSVDDR50                :  1; /*!< Cgsv Ddr50 */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  DRVSSVDDR50              :  2; /*!< Drv Ssv Ddr50 */
    };
    uint32_t WORD;
  } SRS27; /* +0x06C */

  uint32_t _RESERVED_70[28];

  union {
    struct {
      uint8_t  NCP                      :  3;
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  NIIP                     :  2;
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  BWP                      :  8;
      uint8_t  CPS                      :  3;
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  IPS                      :  3;
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  BEPC                     :  8;
    };
    uint32_t WORD;
  } SRS56; /* +0x0E0 */

  uint32_t _RESERVED_E4[3];

  union {
    struct {
      uint8_t  ISES                     :  8; /*!< Interrupt Signal for Each Slot */
      uint8_t                           :  8; /*   (reserved) */
      uint8_t  SVN                      :  8; /*!< Specification Version Number */
      uint8_t  VVN                      :  8; /*!< Vendor Version Number */
    };
    uint32_t WORD;
  } CRS63; /* +0x0F0 */

} SDIOHOSTSRS_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of SDIOHOSTSRS SFRS.
 */
#define SDIOHOSTSRS_SFRS ((__IO SDIOHOSTSRS_SFRS_t *)0x50010100)

#endif /* end of __SDIOHOSTSRS_SFR_H__ section */


