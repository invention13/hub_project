/**
 * @copyright 2019 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file crg_sfr.h
 */

#ifndef __CRG_SFR_H__
#define __CRG_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for CRG.
 */
typedef struct {

  union {
    struct {
      uint8_t  PLL_ENV1P5REG            :  1;
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  PLL_LDO_1P5_ON           :  1;
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  PLL_VREG_TRIM            :  8;
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } PLL; /* +0x000 */

  uint16_t SPARE0;                            /* +0x004 */
  uint8_t  _RESERVED_06[2];                   /* +0x006 */

  union {
    struct {
      uint8_t  CCP1RSTENA               :  1;
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  CCP2RSTENA               :  1;
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  CCP1RSTPULSE             :  1;
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  CCP2RSTPULSE             :  1;
      uint8_t                           :  3; /*   (reserved) */
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } USBTYPEC; /* +0x008 */

  uint32_t _RESERVED_0C[60];

  uint8_t  BIST_SW_TMODE_ENA;                 /* +0x0FC */
  uint8_t  _RESERVED_FD[3];                   /* +0x0FD */

  union {
    struct {
      uint8_t  H2HD_IN                  :  1;
      uint8_t  H2HD_OUT                 :  1;
      uint8_t  H2HH_IN                  :  1;
      uint8_t  H2HH_OUT                 :  1;
      uint8_t  DEV_IN                   :  1;
      uint8_t  DEV_OUT                  :  1;
      uint8_t  TT_BUF                   :  1;
      uint8_t  HUB_CFGROM               :  1;
      uint8_t  AUDIO_MEM_HD             :  1;
      uint8_t  AUDIO_MEM_DH             :  1;
      uint8_t  SD_MEM                   :  1;
      uint8_t  HKADC_MEM                :  1;
      uint8_t  SCR_RX_MEM               :  1;
      uint8_t  SCR_TX_MEM               :  1;
      uint8_t                           :  2; /*   (reserved) */
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } BIST_ENA; /* +0x100 */

  union {
    struct {
      uint8_t  H2HD_IN                  :  1;
      uint8_t  H2HD_OUT                 :  1;
      uint8_t  H2HH_IN                  :  1;
      uint8_t  H2HH_OUT                 :  1;
      uint8_t  DEV_IN                   :  1;
      uint8_t  DEV_OUT                  :  1;
      uint8_t  TT_BUF                   :  1;
      uint8_t  HUB_CFGROM               :  1;
      uint8_t  AUDIO_MEM_HD             :  1;
      uint8_t  AUDIO_MEM_DH             :  1;
      uint8_t  SD_MEM                   :  1;
      uint8_t  HKADC_MEM                :  1;
      uint8_t  SCR_RX_MEM               :  1;
      uint8_t  SCR_TX_MEM               :  1;
      uint8_t                           :  2; /*   (reserved) */
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } BIST_DONE; /* +0x104 */

  uint32_t BIST_H2HD_INBUF_CRC;               /* +0x108 */

  uint32_t BIST_H2HD_OUTBUF_CRC;              /* +0x10C */

  uint32_t BIST_H2HH_INBUF_CRC;               /* +0x110 */

  uint32_t BIST_H2HH_OUTBUF_CRC;              /* +0x114 */

  uint32_t BIST_DEV_INBUF_CRC;                /* +0x118 */

  uint32_t BIST_DEV_OUTBUF_CRC;               /* +0x11C */

  uint32_t BIST_TT_BUF_CRC;                   /* +0x120 */

  uint16_t BIST_HUB_CFGROM_CRC;               /* +0x124 */
  uint8_t  _RESERVED_126[2];                  /* +0x126 */

  uint32_t BIST_AUDIO_MEM_HD_CRC1;            /* +0x128 */

  uint16_t BIST_AUDIO_MEM_HD_CRC2;            /* +0x12C */
  uint8_t  _RESERVED_12E[2];                  /* +0x12E */

  uint32_t BIST_AUDIO_MEM_DH_CRC1;            /* +0x130 */

  uint16_t BIST_AUDIO_MEM_DH_CRC2;            /* +0x134 */
  uint8_t  _RESERVED_136[2];                  /* +0x136 */

  uint32_t BIST_SD_MEM_CRC;                   /* +0x138 */

  uint32_t BIST_HKADC_MEM_CRC;                /* +0x13C */

  uint32_t BIST_SCR_RX_MEM_CRC;               /* +0x140 */

  uint32_t BIST_SCR_TX_MEM_CRC;               /* +0x144 */

} CRG_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of CRG SFRS.
 */
#define CRG_SFRS ((__IO CRG_SFRS_t *)0x50015800)

#endif /* end of __CRG_SFR_H__ section */


