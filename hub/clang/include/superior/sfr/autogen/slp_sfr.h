/**
 * @copyright 2019 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file slp_sfr.h
 */

#ifndef __SLP_SFR_H__
#define __SLP_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for SLP.
 */
typedef struct {

  union {
    struct {
      uint16_t AUXCLKCALIBCNT           : 16; /*!< Aux Clk Calib Cnt */
      uint8_t  AUXCLKCALIBCTL           :  8; /*!< Aux Clk Calib Ctl */
      uint8_t                           :  8; /*   (reserved) */
    };
    uint32_t WORD;
  } AUXCLKCALIBCNT; /* +0x000 */

  uint32_t AUXCLK1MINCNT;                     /*<! Aux Clk 1min Cnt +0x004 */

  union {
    struct {
      uint8_t  SLPTIMERTIMEOUT          :  8; /*!< Slp Timer Timeout */
      uint8_t  SLPTIMERLEFTOVER         :  8; /*!< Slp Timer Leftover */
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } SLPTIMERTIMEOUT; /* +0x008 */

} SLP_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of SLP SFRS.
 */
#define SLP_SFRS ((__IO SLP_SFRS_t *)0x50000020)

#endif /* end of __SLP_SFR_H__ section */


