/**
 * @copyright 2019 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file cmadc_sfr.h
 */

#ifndef __CMADC_SFR_H__
#define __CMADC_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for CMADC.
 */
typedef struct {

  union {
    struct {
      uint8_t  ENB                      :  1; /*!< CM Enb */
      uint8_t  STAY_ON                  :  1; /*!< Stay On */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  SMP_CYC                  :  3; /*!< Cmadc Smp Cyc */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  CLR_STATE_VAR            :  8; /*!< Clr State Var */
      uint8_t  IRQ_EN                   :  1; /*!< Cm Irq En */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  CHANNEL                  :  3; /*!< Cm Adc Chan */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
    };
    uint32_t WORD;
  } CTRL; /* +0x000 */

  union {
    struct {
      uint8_t  FLAG                     :  8; /*!< Cm Irq Flag */
      uint8_t  CLR                      :  8; /*!< Cm Irq Clear */
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } IRQFLAG; /* +0x004 */

  union {
    struct {
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  PORT_ENA                 :  3; /*!< Cm Port Ena */
      uint8_t                           :  4; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } PORTENA; /* +0x008 */

  uint16_t DATA_P1;                           /*<! Cmadc Data P1 +0x00C */
  uint8_t  _RESERVED_0E[2];                   /* +0x00E */

  uint16_t DATA_P2;                           /*<! Cmadc Data P2 +0x010 */
  uint8_t  _RESERVED_12[2];                   /* +0x012 */

  uint16_t DATA_P3;                           /*<! Cmadc Data P3 +0x014 */
  uint8_t  _RESERVED_16[2];                   /* +0x016 */

  union {
    struct {
      uint16_t DATA_P1                  : 16;
      uint16_t DATA_P1_FLIP             : 16;
    };
    uint32_t WORD;
  } DBGDATA1; /* +0x018 */

  union {
    struct {
      uint16_t DATA_P2                  : 16;
      uint16_t DATA_P2_FLIP             : 16;
    };
    uint32_t WORD;
  } DBGDATA2; /* +0x01C */

  union {
    struct {
      uint16_t DATA_P3                  : 16;
      uint16_t DATA_P3_FLIP             : 16;
    };
    uint32_t WORD;
  } DBGDATA3; /* +0x020 */

  uint16_t DATA;                              /*<! ADC DATA VALID +0x024 */
  uint8_t  _RESERVED_26[2];                   /* +0x026 */

  union {
    struct {
      uint8_t  OPEN                     :  3; /*!< QMEAS_OPEN */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  SHRTIN                   :  3; /*!< QMEAS_SHRTIN */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  SWR                      :  3; /*!< QMEAS_SWR */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  TRIM                     :  3; /*!< QMEAS_TRIM */
      uint8_t                           :  1; /*   (reserved) */
      uint16_t SWC                      : 12; /*!< QMEAS_SWC */
      uint8_t  ADC_VALID                :  1; /*!< QMEAS_ADC_VALID */
      uint8_t                           :  3; /*   (reserved) */
    };
    uint32_t WORD;
  } CONFIG; /* +0x028 */

} CMADC_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of CMADC SFRS.
 */
#define CMADC_SFRS ((__IO CMADC_SFRS_t *)0x50015000)

#endif /* end of __CMADC_SFR_H__ section */


