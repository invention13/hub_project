/* this header brings in the individual files that define the
 * struct typdefs for all the special function registers of ICEBLUE
 */

#ifndef __SUPERIOR_SFR_H__
#define __SUPERIOR_SFR_H__

#include "bte_sfr.h"
#include "cmadc_sfr.h"
#include "crg_sfr.h"
#include "gpio_sfr.h"
#include "hkadcdge_sfr.h"
#include "i2c_sfr.h"
#include "i2saudin_sfr.h"
#include "ioctrl_sfr.h"
#include "linm_sfr.h"
#include "lins_sfr.h"
//#include "pll_sfr.h"
//#include "pmu_sfr.h"   /* SDK:  PMU has a hand-coded version */
//#include "ppc_sfr.h"   /* DRM:  PPC has a hand-coded version */
#include "pwm1_sfr.h"
#include "pwm2_sfr.h"
#include "pwm3_sfr.h"
#include "pwm4_sfr.h"
#include "pwm5_sfr.h"
#include "pwm6_sfr.h"
#include "sdiohosthrs_sfr.h"
#include "sdiohostsrs_sfr.h"
#include "slp_sfr.h"
#include "spi_sfr.h"
#include "sysctrla_sfr.h"
#include "timer_sfr.h"
#include "trimmemory_sfr.h"
#include "typec1_sfr.h"
#include "typec2_sfr.h"
//#include "uart_sfr.h"  /* DRM: UART has a hand-coded version */
#include "usbsdiobrg_sfr.h"
#include "wdterbium_sfr.h"
#include "usbafecdr_sfr.h"
#include "usbafefcet_sfr.h"
#include "usbhsappfunc_sfr.h"
#include "usbhsh2hdev_sfr.h"
#include "usbhsh2hhub_sfr.h"

#endif
