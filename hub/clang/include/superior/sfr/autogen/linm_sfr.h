/**
 * @copyright 2019 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file linm_sfr.h
 */

#ifndef __LINM_SFR_H__
#define __LINM_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for LINM.
 */
typedef struct {

  uint8_t  DATABUF1;                          /*<! Data Buffer 1 +0x000 */
  uint8_t  _RESERVED_01[3];                   /* +0x001 */

  uint8_t  DATABUF2;                          /*<! Data Buffer 2 +0x004 */
  uint8_t  _RESERVED_05[3];                   /* +0x005 */

  uint8_t  DATABUF3;                          /*<! Data Buffer 3 +0x008 */
  uint8_t  _RESERVED_09[3];                   /* +0x009 */

  uint8_t  DATABUF4;                          /*<! Data Buffer 4 +0x00C */
  uint8_t  _RESERVED_0D[3];                   /* +0x00D */

  uint8_t  DATABUF5;                          /*<! Data Buffer 5 +0x010 */
  uint8_t  _RESERVED_11[3];                   /* +0x011 */

  uint8_t  DATABUF6;                          /*<! Data Buffer 6 +0x014 */
  uint8_t  _RESERVED_15[3];                   /* +0x015 */

  uint8_t  DATABUF7;                          /*<! Data Buffer 7 +0x018 */
  uint8_t  _RESERVED_19[3];                   /* +0x019 */

  uint8_t  DATABUF8;                          /*<! Data Buffer 8 +0x01C */
  uint8_t  _RESERVED_1D[3];                   /* +0x01D */

  union {
    struct {
      uint8_t  STARTREQ                 :  1; /*!< START Request */
      uint8_t  WAKEUPREQ                :  1; /*!< WakeUp Request */
      uint8_t  RSTERR                   :  1; /*!< Reset Error */
      uint8_t  RSTINT                   :  1; /*!< Reset interrupt */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  TRANSMIT                 :  1; /*!< Transmit Operation */
      uint8_t  SLEEP                    :  1; /*!< Sleep Request */
      uint8_t                           :  1; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } CTRL; /* +0x020 */

  union {
    struct {
      uint8_t  COMPLETE                 :  1;
      uint8_t  WAKEUP                   :  1;
      uint8_t  ERROR                    :  1; /*!< Lin Error */
      uint8_t  INTR                     :  1; /*!< Interupt Request */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  ACTIVE                   :  1; /*!< Lin Bus Active */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } STATUS; /* +0x024 */

  union {
    struct {
      uint8_t  BITMON                   :  1; /*!< Bit Error */
      uint8_t  CHK                      :  1; /*!< Checksum Error */
      uint8_t  TIMEOUT                  :  1; /*!< Timeout Error */
      uint8_t                           :  5; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } ERROR; /* +0x028 */

  union {
    struct {
      uint8_t  LENGTH                   :  4; /*!< Data Length */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  ENHCHK                   :  1; /*!< Enhancement Check */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } DL; /* +0x02C */

  uint8_t  BTDIV07;                           /*<! Bt Div LSBs +0x030 */
  uint8_t  _RESERVED_31[3];                   /* +0x031 */

  union {
    struct {
      uint8_t  BTDIV8                   :  1; /*!< Bt Div Most Significant bit */
      uint8_t  BTMULT                   :  5; /*!< Bt Mult */
      uint8_t  PRESCL                   :  2; /*!< Prescaler Register */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } BITTIME; /* +0x034 */

  uint8_t  ID;                                /* +0x038 */
  uint8_t  _RESERVED_39[3];                   /* +0x039 */

} LINM_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of LINM SFRS.
 */
#define LINM_SFRS ((__IO LINM_SFRS_t *)0x50014400)

#endif /* end of __LINM_SFR_H__ section */


