/**
 * @copyright 2019 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file usbsdiobrg_sfr.h
 */

#ifndef __USBSDIOBRG_SFR_H__
#define __USBSDIOBRG_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for USBSDIOBRG.
 */
typedef struct {

  union {
    struct {
      uint16_t DMADATLEN                : 16;
      uint8_t  DMAOUTEPSEL              :  2; /*!< Dma Outep Sel */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  DMAINEPSEL               :  2; /*!< Dma Inep Sel */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  DMAINT                   :  8;
    };
    uint32_t WORD;
  } DMADATLEN; /* +0x000 */

} USBSDIOBRG_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of USBSDIOBRG SFRS.
 */
#define USBSDIOBRG_SFRS ((__IO USBSDIOBRG_SFRS_t *)0x50012000)

#endif /* end of __USBSDIOBRG_SFR_H__ section */


