/**
 * @copyright 2019 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file h2h_sfr.h
 */

#ifndef __H2H_SFR_H__
#define __H2H_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for H2H.
 */
typedef struct {

  union {
    struct {
      uint8_t  D2H1_ENA                 :  1;
      uint8_t  D4H3_ENA                 :  1;
      uint8_t  H2D1_ENA                 :  1;
      uint8_t  H4D3_ENA                 :  1;
      uint8_t  H6D5_ENA                 :  1;
      uint8_t  AUD_DH_RAD_ENA           :  1;
      uint8_t  AUD_DH_RX_ENA            :  1;
      uint8_t  AUD_DH_TX_ENA            :  1;
      uint8_t  AUD_HD_RAD_ENA           :  1;
      uint8_t  AUD_HD_RX_ENA            :  1;
      uint8_t  AUD_HD_TX_ENA            :  1;
      uint8_t                           :  5; /*   (reserved) */
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } PIPENA; /* +0x000 */

  union {
    struct {
      uint8_t  BT_PKTD                  :  2;
      uint8_t  INT_PKTD                 :  2;
      uint8_t  AUD_DH_PKTD              :  2;
      uint8_t  AUD_HD_PKTD              :  2;
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } PKTD; /* +0x004 */

  union {
    struct {
      uint8_t  AUD_DH_BUFC              :  8;
      uint8_t  AUD_HD_BUFC              :  8;
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } AUDBUFC; /* +0x008 */

  union {
    struct {
      uint8_t  AUD_DH_TXZ               :  1;
      uint8_t  AUD_DH_TXF               :  1;
      uint8_t  RAD_DH_SLWTYP            :  1;
      uint8_t  AUD_HD_TXZ               :  1;
      uint8_t  AUD_HD_TXF               :  1;
      uint8_t  RAD_HD_SLWTYP            :  1;
      uint8_t  EPNMAP                   :  1;
      uint8_t  AUD_DH_SMPSZ             :  1;
      uint8_t  AUD_HD_SMPSZ             :  8;
      uint8_t  DHZLPDMP                 :  1;
      uint8_t  HDZLPDMP                 :  1;
      uint8_t                           :  6; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
    };
    uint32_t WORD;
  } AUDCFG; /* +0x00C */

  union {
    struct {
      uint8_t  RAD_DH_INTG              :  4;
      uint8_t  RAD_ACW                  :  2;
      uint8_t  AUD_DH_CLR_STRB          :  1;
      uint8_t  RAD_DH_TSRC              :  1;
      uint8_t  RAD_DH_HGTHR             :  8;
      uint8_t  RAD_DH_DEP               :  8;
      uint8_t  AUD_DH_CLR_ST            :  8;
    };
    uint32_t WORD;
  } RADDHBW; /* +0x010 */

  union {
    struct {
      uint8_t  RAD_HD_INTG              :  4;
      uint8_t  RAD_HD_ACW               :  2;
      uint8_t  AUD_HD_CLR_STRB          :  1;
      uint8_t  RAD_HD_TSRC              :  1;
      uint8_t  RAD_HD_HGTHR             :  8;
      uint8_t  RAD_HD_DEP               :  8;
      uint8_t  AUD_HD_CLR_ST            :  8;
    };
    uint32_t WORD;
  } RADHDBW; /* +0x014 */

  union {
    struct {
      uint8_t  D2H1_IRQENA              :  1;
      uint8_t  D4H3_IRQENA              :  1;
      uint8_t  H2D1_IRQENA              :  1;
      uint8_t  H4D3_IRQENA              :  1;
      uint8_t  H6D5_IRQENA              :  1;
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  AUD_DH_B_IRQENA          :  1;
      uint8_t  AUD_DH_D_IRQENA          :  1;
      uint8_t  AUD_HD_B_IRQENA          :  1;
      uint8_t  AUD_HD_H_IRQENA          :  1;
      uint8_t  AUD_H_IN7EMPTY_IRQENA    :  1;
      uint8_t  AUD_D_IN7EMPTY_IRQENA    :  1;
      uint8_t                           :  4; /*   (reserved) */
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } H2HIRQENA; /* +0x018 */

  union {
    struct {
      uint8_t  D2H1_IRQF                :  1;
      uint8_t  D4H3_IRQF                :  1;
      uint8_t  H2D1_IRQF                :  1;
      uint8_t  H4D3_IRQF                :  1;
      uint8_t  H6D5_IRQF                :  1;
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  AUD_DH_B_IRQF            :  1;
      uint8_t  AUD_DH_D_IRQF            :  1;
      uint8_t  AUD_HD_B_IRQF            :  1;
      uint8_t  AUD_HD_H_IRQF            :  1;
      uint8_t  AUD_H_IN7EMPTY_IRQF      :  1;
      uint8_t  AUD_D_IN7EMPTY_IRQF      :  1;
      uint8_t                           :  4; /*   (reserved) */
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } H2HIRQF; /* +0x01C */

  union {
    struct {
      uint8_t  DBG_WAIT                 :  2;
      uint8_t  WAIT_VIOL                :  1;
      uint8_t                           :  5; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } DBG; /* +0x020 */

} H2H_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of H2H SFRS.
 */
#define H2H_SFRS ((__IO H2H_SFRS_t *)0x5001b000)

#endif /* end of __H2H_SFR_H__ section */


