/**
 * @copyright 2019 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file spi_sfr.h
 */

#ifndef __SPI_SFR_H__
#define __SPI_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for SPI.
 */
typedef struct {

  union {
    struct {
      uint8_t  SCKSTD                   :  2;
      uint8_t  CPHA                     :  1;
      uint8_t  CPOL                     :  1;
      uint8_t  MSTR                     :  1;
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  SINTE                    :  1;
      uint8_t  SRXFE                    :  1;
      uint8_t  SRXFF                    :  1;
      uint8_t  STXFE                    :  1;
      uint8_t  STXFF                    :  1;
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  SWCOL                    :  1;
      uint8_t  SINTF                    :  1;
      uint8_t  SPDR                     :  8;
      uint8_t  SCKEXT                   :  2;
      uint8_t  SPE                      :  1;
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  SICNT                    :  2;
    };
    uint32_t WORD;
  } SPCR; /* +0x000 */

} SPI_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of SPI SFRS.
 */
#define SPI_SFRS ((__IO SPI_SFRS_t *)0x5000001c)

#endif /* end of __SPI_SFR_H__ section */


