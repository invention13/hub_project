/**
 * @copyright 2019 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file i2c_sfr.h
 */

#ifndef __I2C_SFR_H__
#define __I2C_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for I2C.
 */
typedef struct {

  union {
    struct {
      uint8_t  BUFOVFL                  :  1; /*!< Buf Ovfl */
      uint8_t  BUFINVLD                 :  1; /*!< Buf Invld */
      uint8_t  BUFFULL                  :  1; /*!< Buf Full */
      uint8_t  RWBUSY                   :  1; /*!< Rw Busy */
      uint8_t  PRCVD                    :  1; /*!< P Rcvd */
      uint8_t  SRCVD                    :  1; /*!< S Rcvd */
      uint8_t  ADRCVD                   :  1; /*!< Ad Rcvd */
      uint8_t  ARCVD                    :  1; /*!< A Rcvd */
      uint8_t  SENBCLKSTENB             :  1; /*!< S Enb Clk St Enb */
      uint8_t  PENBAD10BITS             :  1; /*!< P Enb Ad 10bits */
      uint8_t  ACKENB                   :  1; /*!< Ack Enb */
      uint8_t  ACKN                     :  1; /*!< Ack N */
      uint8_t  MRCVENB                  :  1; /*!< M Rcv Enb */
      uint8_t  GCENB                    :  1; /*!< Gc Enb */
      uint8_t  CLKST                    :  1; /*!< Clk St */
      uint8_t  RSENB                    :  1; /*!< Rs Enb */
      uint8_t  M1S0                     :  1;
      uint8_t  FLTENB                   :  1; /*!< Flt Enb */
      uint8_t  I2CENB                   :  1; /*!< I2c Enb */
      uint8_t  AMSK                     :  5; /*!< A Msk */
      uint8_t  I2CDATA                  :  8; /*!< I2c Data */
    };
    uint32_t WORD;
  } I2CSTATUS; /* +0x000 */

  uint16_t I2CADDR;                           /*<! I2c Addr +0x004 */
  uint8_t  _RESERVED_06[2];                   /* +0x006 */

} I2C_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of I2C SFRS.
 */
#define I2C_SFRS ((__IO I2C_SFRS_t *)0x50000008)

#endif /* end of __I2C_SFR_H__ section */


