/**
 * @copyright 2019 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file usbhubsfrs_sfr.h
 */

#ifndef __USBHUBSFRS_SFR_H__
#define __USBHUBSFRS_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for USBHUBSFRS.
 */
typedef struct {

  union {
    struct {
      uint8_t                           :  8; /*   (reserved) */
      uint8_t                           :  5; /*   (reserved) */
      uint8_t  DISCON                   :  2;
      uint8_t                           :  1; /*   (reserved) */
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } AFE_BRIDGE_CTRL; /* +0x000 */

  union {
    struct {
      uint8_t  PORT1                    :  8;
      uint8_t  PORT2                    :  8;
      uint8_t  PORT3                    :  8;
      uint8_t  PORT4                    :  8;
    };
    uint32_t WORD;
  } DWN_PORT_STATE_1; /* +0x004 */

  union {
    struct {
      uint8_t  PORT5                    :  8;
      uint8_t  DISCON                   :  8;
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } DWN_PORT_STATE_2; /* +0x008 */

  uint32_t _RESERVED_0C[2];

  union {
    struct {
      uint8_t  PORT1                    :  1;
      uint8_t  PORT2                    :  1;
      uint8_t  PORT3                    :  1;
      uint8_t  PORT4                    :  1;
      uint8_t  PORT5                    :  1;
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  MASK1                    :  4;
      uint8_t  MASK2                    :  4;
      uint8_t  MASK3                    :  4;
      uint8_t  MASK4                    :  4;
      uint8_t  MASK5                    :  4;
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  MODE                     :  1;
    };
    uint32_t WORD;
  } PORT_STATE_CHANGE; /* +0x014 */

  union {
    struct {
      uint8_t                           :  8; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
      uint8_t  DWN1PORTSW               :  2;
      uint8_t  DWN2PORTSW               :  2;
      uint8_t  DWN3PORTSW               :  2;
      uint8_t                           :  2; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
    };
    uint32_t WORD;
  } DWN_PORT_CTRL; /* +0x018 */

  uint8_t  SUSPEND;                           /* +0x01C */
  uint8_t  _RESERVED_1D[3];                   /* +0x01D */

  union {
    struct {
      uint8_t                           :  5; /*   (reserved) */
      uint8_t  PLL_POR                  :  1;
      uint8_t  UPVBUS_DET_DISABLE       :  1;
      uint8_t  UP_VBUSVALID_INT         :  1;
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } POR_CTRL; /* +0x020 */

  uint32_t _RESERVED_24;

  union {
    struct {
      uint8_t                           :  5; /*   (reserved) */
      uint8_t  H2H_D_UTMIVBUSVALID      :  1;
      uint8_t  H2H_D_DFP_PD             :  1;
      uint8_t  FAST_PRTRST              :  1;
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } H2H_PORTCTRL; /* +0x028 */

  union {
    struct {
      uint8_t  TIMER_IRQ                :  1;
      uint8_t  VBUS_IRQ                 :  1;
      uint8_t                           :  6; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } INT_ID; /* +0x02C */

  union {
    struct {
      uint8_t  UFP_SOURCE_CTL           :  1;
      uint8_t  UFP_DPPULLDOWN_SFR       :  1;
      uint8_t  UFP_DMPULLUP_SFR         :  1;
      uint8_t  UFP_DMPULLDOWN_SFR       :  1;
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  DFP1_SOURCE_CTL          :  1;
      uint8_t  DFP1_DPPULLDOWN_SFR      :  1;
      uint8_t  DFP1_DMPULLUP_SFR        :  1;
      uint8_t  DFP1_DMPULLDOWN_SFR      :  1;
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  DFP2_SOURCE_CTL          :  1;
      uint8_t  DFP2_DPPULLDOWN_SFR      :  1;
      uint8_t  DFP2_DMPULLUP_SFR        :  1;
      uint8_t  DFP2_DMPULLDOWN_SFR      :  1;
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  DFP3_SOURCE_CTL          :  1;
      uint8_t  DFP3_DPPULLDOWN_SFR      :  1;
      uint8_t  DFP3_DMPULLUP_SFR        :  1;
      uint8_t  DFP3_DMPULLDOWN_SFR      :  1;
      uint8_t                           :  4; /*   (reserved) */
    };
    uint32_t WORD;
  } DPDM_PAD_CTRL; /* +0x030 */

  union {
    struct {
      uint8_t                           :  8; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
      uint8_t  SELECT                   :  3;
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  ENA                      :  3;
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } VBUS_PU_CTRL; /* +0x034 */

  union {
    struct {
      uint8_t  VBUS_PD_SEL1             :  1;
      uint8_t  VBUS_PD_SEL2             :  1;
      uint8_t  VBUS_PD_SEL3             :  1;
      uint8_t  VBUS_PD_FW1              :  1;
      uint8_t  VBUS_PD_FW2              :  1;
      uint8_t  VBUS_PD_FW3              :  1;
      uint8_t  VBUS_PD_POL1             :  1;
      uint8_t  VBUS_PD_POL2             :  1;
      uint8_t  VBUS_PD_POL3             :  8;
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } VBUS_PD_CTRL; /* +0x038 */

  uint32_t _RESERVED_3C;

  union {
    struct {
      uint8_t  UFP                      :  1;
      uint8_t  DFP1                     :  1;
      uint8_t  DFP2                     :  1;
      uint8_t  DFP3                     :  1;
      uint8_t                           :  4; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } OVD_DET_STAT; /* +0x040 */

  union {
    struct {
      uint8_t  UFP                      :  1;
      uint8_t  DFP1                     :  1;
      uint8_t  DFP2                     :  1;
      uint8_t  DFP3                     :  1;
      uint8_t                           :  4; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } OVD_DET_IRQ; /* +0x044 */

  union {
    struct {
      uint8_t  UFP                      :  1;
      uint8_t  DFP1                     :  1;
      uint8_t  DFP2                     :  1;
      uint8_t  DFP3                     :  1;
      uint8_t                           :  4; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } OVD_DET_IRQ_ENA; /* +0x048 */

  union {
    struct {
      uint8_t  UFP                      :  1;
      uint8_t  DFP1                     :  1;
      uint8_t  DFP2                     :  1;
      uint8_t  DFP3                     :  1;
      uint8_t                           :  4; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } OVD_DET_IRQ_CLR; /* +0x04C */

  union {
    struct {
      uint8_t  SORPFLT                  :  1;
      uint8_t  WRAPDET                  :  1;
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  SYNCEXT                  :  2;
      uint8_t  EOI                      :  2;
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } UPPHYTRIM; /* +0x050 */

  union {
    struct {
      uint8_t  SORPFLT                  :  1;
      uint8_t  WRAPDET                  :  1;
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  SYNCEXT                  :  2;
      uint8_t  EOI                      :  2;
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } DN1PHYTRIM; /* +0x054 */

  union {
    struct {
      uint8_t  SORPFLT                  :  1;
      uint8_t  WRAPDET                  :  1;
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  SYNCEXT                  :  2;
      uint8_t  EOI                      :  2;
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } DN2PHYTRIM; /* +0x058 */

  union {
    struct {
      uint8_t  SORPFLT                  :  1;
      uint8_t  WRAPDET                  :  1;
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  SYNCEXT                  :  2;
      uint8_t  EOI                      :  2;
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } DN3PHYTRIM; /* +0x05C */

  union {
    struct {
      uint8_t  RX                       :  8;
      uint8_t  TX                       :  8;
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } UP_PORT_STATE; /* +0x060 */

  uint8_t  WUCONNTRIM;                        /* +0x064 */
  uint8_t  _RESERVED_65[3];                   /* +0x065 */

  union {
    struct {
      uint8_t  PORT1                    :  4;
      uint8_t  PORT2                    :  4;
      uint8_t  PORT3                    :  4;
      uint8_t  PORT4                    :  4;
      uint8_t  PORT5                    :  8;
      uint8_t                           :  8; /*   (reserved) */
    };
    uint32_t WORD;
  } DWNPORTSTATEMONITOR; /* +0x068 */

  uint8_t  PORTREMAP;                         /* +0x06C */
  uint8_t  _RESERVED_6D[3];                   /* +0x06D */

  uint32_t _RESERVED_70[36];

  union {
    struct {
      uint8_t  LED1                     :  2;
      uint8_t  LED2                     :  2;
      uint8_t  LED3                     :  2;
      uint8_t  LED4                     :  2;
      uint8_t  LED5                     :  8;
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } LED; /* +0x100 */

  uint32_t _RESERVED_104[127];

  union {
    struct {
      uint8_t  UFP                      :  8;
      uint8_t  DFP1                     :  8;
      uint8_t  DFP2                     :  8;
      uint8_t  DFP3                     :  8;
    };
    uint32_t WORD;
  } HS_LOOPBACK_PORTTEST; /* +0x300 */

  union {
    struct {
      uint8_t  CNTOK0                   :  8;
      uint8_t  CNTOK1                   :  8;
      uint8_t  CNTOK2                   :  8;
      uint8_t  CNTOK3                   :  8;
    };
    uint32_t WORD;
  } HS_LOOPBACK_CNTOK; /* +0x304 */

  union {
    struct {
      uint8_t  NPKTS                    :  3;
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  TESTSYNC                 :  1;
      uint8_t  TESTDRBL                 :  1;
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  ENA                      :  1;
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } HS_LOOPBACK_CTRL; /* +0x308 */

  uint32_t _RESERVED_30C;

  union {
    struct {
      uint8_t  TT_DISCON_FIX_DISABLE    :  5;
      uint8_t  WUCONN_FIX_DISABLE       :  1;
      uint8_t  CHANGE_BIT_CLEAR_FIX_DISABLE :  1;
      uint8_t  PATCH_REMOTE_WAKEUP_N    :  1;
      uint8_t  PATCH_CRAZYK_N           :  1;
      uint8_t  PATCH_RPTR_GLITCH_N      :  1;
      uint8_t  PATCH_TTEOF2_N           :  1;
      uint8_t  PATCH_MULTICHIRP_N       :  1;
      uint8_t  PATCH_LSTURN_N           :  1;
      uint8_t  RESERVED                 :  3;
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } SUP_C0_BUGFIX_CTRL; /* +0x310 */

  uint8_t  TDCNN_TRIM;                        /* +0x314 */
  uint8_t  _RESERVED_315[3];                  /* +0x315 */

  uint32_t _RESERVED_318[58];

  uint16_t DATA;                              /* +0x400 */
  uint8_t  _RESERVED_402[2];                  /* +0x402 */

} USBHUBSFRS_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of USBHUBSFRS SFRS.
 */
#define USBHUBSFRS_SFRS ((__IO USBHUBSFRS_SFRS_t *)0x50013000)

#endif /* end of __USBHUBSFRS_SFR_H__ section */


