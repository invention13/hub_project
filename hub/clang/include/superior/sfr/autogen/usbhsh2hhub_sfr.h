/**
 * @copyright 2019 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file usbhsh2hhub_sfr.h
 */

#ifndef __USBHSH2HHUB_SFR_H__
#define __USBHSH2HHUB_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for USBHSH2HHUB.
 */
typedef struct {

  union {
    struct {
      uint8_t  OUT0BC                   :  8;
      uint8_t  IN0BC                    :  8;
      uint8_t  STALL                    :  1;
      uint8_t  HSNAK                    :  1;
      uint8_t  IN_BSY                   :  1; /*!< In0 bsy */
      uint8_t  OUT_BSY                  :  1; /*!< Outbsy */
      uint8_t  DSTALL                   :  1;
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  CHGSET                   :  1;
      uint8_t                           :  8; /*   (reserved) */
    };
    uint32_t WORD;
  } EP0_CTRL; /* +0x000 */

  uint32_t _RESERVED_04;

  union {
    struct {
      uint16_t OUT1BC                   : 16;
      uint8_t  BUF_SIZE                 :  2; /*!< OUT1 Buffer */
      uint8_t  EP_TYPE                  :  2; /*!< Type */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  STALL                    :  1;
      uint8_t  VAL                      :  1;
      uint8_t  ERR                      :  1;
      uint8_t  BUSY                     :  1;
      uint8_t  NPAK                     :  2;
      uint8_t  AUTOOUT                  :  1;
      uint8_t                           :  3; /*   (reserved) */
    };
    uint32_t WORD;
  } OUT1_CTRL; /* +0x008 */

  union {
    struct {
      uint16_t IN1BC                    : 16;
      uint8_t  BUF_SIZE                 :  2; /*!< IN1 Buffer */
      uint8_t  EP_TYPE                  :  2; /*!< Type */
      uint8_t  ISOD                     :  2;
      uint8_t  STALL                    :  1;
      uint8_t  VAL                      :  1;
      uint8_t  ERR                      :  1;
      uint8_t  BUSY                     :  1;
      uint8_t  NPAK                     :  2;
      uint8_t  AUTOIN                   :  1;
      uint8_t                           :  3; /*   (reserved) */
    };
    uint32_t WORD;
  } IN1_CTRL; /* +0x00C */

  union {
    struct {
      uint16_t OUT2BC                   : 16;
      uint8_t  BUF_SIZE                 :  2; /*!< OUT2 Buffer */
      uint8_t  EP_TYPE                  :  2; /*!< Type */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  STALL                    :  1;
      uint8_t  VAL                      :  1;
      uint8_t  ERR                      :  1;
      uint8_t  BUSY                     :  1;
      uint8_t  NPAK                     :  2;
      uint8_t  AUTOOUT                  :  1;
      uint8_t                           :  3; /*   (reserved) */
    };
    uint32_t WORD;
  } OUT2_CTRL; /* +0x010 */

  union {
    struct {
      uint16_t IN2BC                    : 16;
      uint8_t  BUF_SIZE                 :  2; /*!< IN2 Buffer */
      uint8_t  EP_TYPE                  :  2; /*!< Type */
      uint8_t  ISOD                     :  2;
      uint8_t  STALL                    :  1;
      uint8_t  VAL                      :  1;
      uint8_t  ERR                      :  1;
      uint8_t  BUSY                     :  1;
      uint8_t  NPAK                     :  2;
      uint8_t  AUTOIN                   :  1;
      uint8_t                           :  3; /*   (reserved) */
    };
    uint32_t WORD;
  } IN2_CTRL; /* +0x014 */

  uint32_t _RESERVED_18[27];

  uint32_t FIFO1DAT;                          /* +0x084 */

  uint32_t FIFO2DAT;                          /* +0x088 */

  uint32_t _RESERVED_8C[63];

  union {
    struct {
      uint8_t  IN0IRQ                   :  1;
      uint8_t  IN1IRQ                   :  1;
      uint8_t  IN2IRQ                   :  1;
      uint8_t                           :  5; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
      uint8_t  OUT0IRQ                  :  1;
      uint8_t  OUT1IRQ                  :  1;
      uint8_t  OUT2IRQ                  :  1;
      uint8_t                           :  5; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
    };
    uint32_t WORD;
  } IN07IRQ; /* +0x188 */

  union {
    struct {
      uint8_t  SUDAVIR                  :  1;
      uint8_t  SOFIR                    :  1;
      uint8_t  SUTOKIR                  :  1;
      uint8_t  SUSPIR                   :  1;
      uint8_t  URESIR                   :  1;
      uint8_t  HSPEEDIR                 :  1;
      uint8_t                           :  2; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
      uint8_t  OUT0PNG                  :  1;
      uint8_t  OUT1PNG                  :  1;
      uint8_t  OUT2PNG                  :  1;
      uint8_t                           :  5; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
    };
    uint32_t WORD;
  } USBIRQ; /* +0x18C */

  union {
    struct {
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  IN1FULL                  :  1;
      uint8_t  IN2FULL                  :  1;
      uint8_t                           :  5; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  OUT1EMPT                 :  1;
      uint8_t  OUT2EMPT                 :  1;
      uint8_t                           :  5; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
    };
    uint32_t WORD;
  } IN07FULLIRQ; /* +0x190 */

  union {
    struct {
      uint8_t  IN0IEN                   :  1;
      uint8_t  IN1IEN                   :  1;
      uint8_t  IN2IEN                   :  1;
      uint8_t                           :  5; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
      uint8_t  OUT0IEN                  :  1;
      uint8_t  OUT1IEN                  :  1;
      uint8_t  OUT2IEN                  :  1;
      uint8_t                           :  5; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
    };
    uint32_t WORD;
  } IN07IEN; /* +0x194 */

  union {
    struct {
      uint8_t  SUDAVIE                  :  1;
      uint8_t  SOFIE                    :  1;
      uint8_t  SUTOKIE                  :  1;
      uint8_t  SUSPIE                   :  1;
      uint8_t  URESIE                   :  1;
      uint8_t  HSPEEDIE                 :  1;
      uint8_t                           :  2; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
      uint8_t  OUT0PNGIEN               :  1;
      uint8_t  OUT1PNGIEN               :  1;
      uint8_t  OUT2PNGIEN               :  1;
      uint8_t                           :  5; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
    };
    uint32_t WORD;
  } USBIEN; /* +0x198 */

  union {
    struct {
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  IN1FUIE                  :  1;
      uint8_t  IN2FUIE                  :  1;
      uint8_t                           :  5; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  OUT1EMIE                 :  1;
      uint8_t  OUT2EMIE                 :  1;
      uint8_t                           :  5; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
    };
    uint32_t WORD;
  } IN07FULLIEN; /* +0x19C */

  union {
    struct {
      uint8_t  IVECT                    :  8;
      uint8_t  FIFOIVECT                :  8;
      uint8_t  EP                       :  4;
      uint8_t  IO                       :  1;
      uint8_t  TOGRST                   :  1;
      uint8_t  FIFORST                  :  1;
      uint8_t  TOGSETQ                  :  1;
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  SLFPWR                   :  1;
      uint8_t  RWAKEN                   :  1;
      uint8_t  ENUMERATION              :  1;
      uint8_t  SIGRSUME                 :  1;
      uint8_t  DISCON                   :  1;
      uint8_t  WAKESRC                  :  1;
    };
    uint32_t WORD;
  } IVECT; /* +0x1A0 */

  union {
    struct {
      uint8_t  MFR                      :  3;
      uint16_t FR                       : 11;
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  FA                       :  8;
      uint8_t                           :  8; /*   (reserved) */
    };
    uint32_t WORD;
  } FRMNR; /* +0x1A4 */

  union {
    struct {
      uint8_t  FCEP                     :  4;
      uint8_t  FCIO                     :  1;
      uint8_t  FIFOAUTO                 :  1;
      uint8_t  FIFOCMIT                 :  1;
      uint8_t  FIFOACC                  :  1;
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } FIFOCTRL; /* +0x1A8 */

  uint32_t _RESERVED_1AC[13];

  union {
    struct {
      uint8_t  OUT0MAXPCK               :  8;
      uint8_t                           :  8; /*   (reserved) */
      uint16_t OUT1MAXPCK               : 16;
    };
    uint32_t WORD;
  } OUT0MAXPCK; /* +0x1E0 */

  uint16_t OUT2MAXPCK;                        /* +0x1E4 */
  uint8_t  _RESERVED_1E6[2];                  /* +0x1E6 */

  uint32_t _RESERVED_1E8[71];

  union {
    struct {
      uint8_t                           :  2; /*   (reserved) */
      uint16_t OUT1STADDR               : 14;
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } OUT1STADDR; /* +0x304 */

  union {
    struct {
      uint8_t                           :  2; /*   (reserved) */
      uint16_t OUT2STADDR               : 14;
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } OUT2STADDR; /* +0x308 */

  uint32_t _RESERVED_30C[14];

  union {
    struct {
      uint8_t                           :  2; /*   (reserved) */
      uint16_t IN1STADDR                : 14;
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } IN1STADDR; /* +0x344 */

  union {
    struct {
      uint8_t                           :  2; /*   (reserved) */
      uint16_t IN2STADDR                : 14;
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } IN2STADDR; /* +0x348 */

  uint32_t _RESERVED_34C[37];

  uint8_t  _RESERVED_3E0;                     /* +0x3E0 */
  uint8_t  _RESERVED_3E1;                     /* +0x3E1 */
  uint16_t IN1MAXPCK;                         /* +0x3E2 */

  uint16_t IN2MAXPCK;                         /* +0x3E4 */
  uint8_t  _RESERVED_3E6[2];                  /* +0x3E6 */

} USBHSH2HHUB_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of USBHSH2HHUB SFRS.
 */
#define USBHSH2HHUB_SFRS ((__IO USBHSH2HHUB_SFRS_t *)0x50019000)

#endif /* end of __USBHSH2HHUB_SFR_H__ section */


