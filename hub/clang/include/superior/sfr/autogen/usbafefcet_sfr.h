/**
 * @copyright 2019 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file usbafefcet_sfr.h
 */

#ifndef __USBAFEFCET_SFR_H__
#define __USBAFEFCET_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for USBAFEFCET.
 */
typedef struct {

  union {
    struct {
      uint8_t  UFP_ISNK                 :  1; /*!< Upstream Facing Port Current Sink */
      uint8_t  DFP1_ISNK                :  1; /*!< Downstream Facing Port 1 Current Sink */
      uint8_t  DFP2_ISNK                :  1; /*!< Downstream Facing Port 2 Current Sink */
      uint8_t  DFP3_ISNK                :  1; /*!< Downstream Facing Port 3 Current Sink */
      uint8_t                           :  4; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } ISNK; /* +0x000 */

  union {
    struct {
      union {
        struct {
          uint8_t  DFP1_RE_FLG          :  1; /*!< DFP1_RE_IRQ_FLAG */
          uint8_t  DFP1_FE_FLG          :  1; /*!< DFP1_FE_IRQ_FLAG */
          uint8_t  DFP2_RE_FLG          :  1; /*!< DFP2_RE_IRQ_FLAG */
          uint8_t  DFP2_FE_FLG          :  1; /*!< DFP2_FE_IRQ_FLAG */
          uint8_t  DFP3_RE_FLG          :  1; /*!< DFP3_RE_IRQ_FLAG */
          uint8_t  DFP3_FE_FLG          :  1; /*!< DFP3_FE_IRQ_FLAG */
          uint8_t                       :  2; /*   (reserved) */
        };
        uint8_t BYTE;
      } OCCURED;
      union {
        struct {
          uint8_t  DFP1_RE_ENA          :  1; /*!< DFP1_RE_IRQ_ENA */
          uint8_t  DFP1_FE_ENA          :  1; /*!< DFP1_FE_IRQ_ENA */
          uint8_t  DFP2_RE_ENA          :  1; /*!< DFP2_RE_IRQ_ENA */
          uint8_t  DFP2_FE_ENA          :  1; /*!< DFP2_FE_IRQ_ENA */
          uint8_t  DFP3_RE_ENA          :  1; /*!< DFP3_RE_IRQ_ENA */
          uint8_t  DFP3_FE_ENA          :  1; /*!< DFP3_FE_IRQ_ENA */
          uint8_t                       :  2; /*   (reserved) */
        };
        uint8_t BYTE;
      } IRQ_ENABLE;
      union {
        struct {
          uint8_t  DFP1_RE_FLGM         :  1; /*!< DFP1_RE_IRQ_FLAG_MASKED */
          uint8_t  DFP1_FE_FLGM         :  1; /*!< DFP1_FE_IRQ_FLAG_MASKED */
          uint8_t  DFP2_RE_FLGM         :  1; /*!< DFP2_RE_IRQ_FLAG_MASKED */
          uint8_t  DFP2_FE_FLGM         :  1; /*!< DFP2_FE_IRQ_FLAG_MASKED */
          uint8_t  DFP3_RE_FLGM         :  1; /*!< DFP3_RE_IRQ_FLAG_MASKED */
          uint8_t  DFP3_FE_FLGM         :  1; /*!< DFP3_FE_IRQ_FLAG_MASKED */
          uint8_t                       :  2; /*   (reserved) */
        };
        uint8_t BYTE;
      } OCCURED_AND_ENABLED;
      union {
        struct {
          uint8_t  DFP1_RE_CLR          :  1; /*!< DFP1_IRQ_CLR */
          uint8_t  DFP1_FE_CLR          :  1; /*!< DFP1_IRQ_CLR */
          uint8_t  DFP2_RE_CLR          :  1; /*!< DFP2_IRQ_CLR */
          uint8_t  DFP2_FE_CLR          :  1; /*!< DFP2_IRQ_CLR */
          uint8_t  DFP3_RE_CLR          :  1; /*!< DFP3_IRQ_CLR */
          uint8_t  DFP3_FE_CLR          :  1; /*!< DFP3_IRQ_CLR */
          uint8_t                       :  2; /*   (reserved) */
        };
        uint8_t BYTE;
      } CLEAR;
    };
    uint32_t WORD;
  } FCETEVENTS; /* +0x004 */

  union {
    struct {
      uint8_t  DFP1_CDP_RXD             :  1;
      uint8_t  DFP2_CDP_RXD             :  1;
      uint8_t  DFP3_CDP_RXD             :  1;
      uint8_t                           :  5; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } FCETCDPRXD; /* +0x008 */

} USBAFEFCET_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of USBAFEFCET SFRS.
 */
#define USBAFEFCET_SFRS ((__IO USBAFEFCET_SFRS_t *)0x50016600)

#endif /* end of __USBAFEFCET_SFR_H__ section */


