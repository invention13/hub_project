/**
 * @copyright 2019 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file timer_sfr.h
 */

#ifndef __TIMER_SFR_H__
#define __TIMER_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for TIMER.
 */
typedef struct {

  uint32_t TIMER0VAL;                         /*<! Timer0 Val +0x000 */

  uint8_t  TIMER0ENB;                         /*<! Timer0 Enb +0x004 */
  uint8_t  _RESERVED_05[3];                   /* +0x005 */

  uint32_t TIMER1VAL;                         /*<! Timer1 Val +0x008 */

  uint8_t  TIMER1ENB;                         /*<! Timer1 Enb +0x00C */
  uint8_t  _RESERVED_0D[3];                   /* +0x00D */

  uint32_t TIMER2VAL;                         /*<! Timer2 Val +0x010 */

  uint8_t  TIMER2ENB;                         /*<! Timer2 Enb +0x014 */
  uint8_t  _RESERVED_15[3];                   /* +0x015 */

  union {
    struct {
      uint8_t  WATCHDOGENB              :  1; /*!< Watchdog Enb */
      uint8_t  TRGRESET                 :  1; /*!< Trg Reset */
      uint8_t  RESETBYWATCHDOG          :  1; /*!< Reset By Watchdog */
      uint8_t  OVFLVALSEL               :  2; /*!< Ovfl Val Sel */
      uint8_t                           :  3; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } WATCHDOGCFG; /* +0x018 */

  uint32_t WATCHDOGCLR;                       /*<! Watchdog Clr +0x01C */

} TIMER_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of TIMER SFRS.
 */
#define TIMER_SFRS ((__IO TIMER_SFRS_t *)0x50020000)

#endif /* end of __TIMER_SFR_H__ section */


