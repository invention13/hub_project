/**
 * @copyright 2019 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file sysctrla_sfr.h
 */

#ifndef __SYSCTRLA_SFR_H__
#define __SYSCTRLA_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for SYSCTRLA.
 */
typedef struct {

  uint32_t NAME;                              /*<! ASIC name +0x000 */

  uint16_t REV;                               /*<! Silicon Revision +0x004 */
  uint8_t  _RESERVED_06[2];                   /* +0x006 */

  uint8_t  RETAIN0;                           /*<! Firmware scratch register 0 +0x008 */
  uint8_t  _RESERVED_09[3];                   /* +0x009 */

  uint8_t  RETAIN1;                           /*<! Firmware scratch register 1 +0x00C */
  uint8_t  _RESERVED_0D[3];                   /* +0x00D */

  uint32_t DEBUG_ACCESS_KEY;                  /* +0x010 */

  uint8_t  DEBUG_ACCESS_ENABLED;              /* +0x014 */
  uint8_t  _RESERVED_15[3];                   /* +0x015 */

  union {
    struct {
      uint8_t  TBBUFENA                 :  1; /*!< Test Buffer Enable */
      uint8_t  TBDASEL                  :  1; /*!< Data Select */
      uint8_t                           :  6; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } AFE_TEST; /* +0x018 */

  union {
    struct {
      uint8_t  LINMODE                  :  1;
      uint8_t  LINCLKEN                 :  1; /*!< LIN Clock Enable */
      uint8_t                           :  6; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } LIN; /* +0x01C */

  union {
    struct {
      uint8_t  B_LOW_P1                 :  8; /*!< B Low  from BMC_CDR Port1 */
      uint8_t  B_HI_P1                  :  8; /*!< B High from BMC_CDR Port1 */
      uint8_t  B_LOW_P2                 :  8; /*!< B Low  from BMC_CDR Port2 */
      uint8_t  B_HI_P2                  :  8; /*!< B High from BMC_CDR Port2 */
    };
    uint32_t WORD;
  } TYPEC_CDR_DEBUG; /* +0x020 */

} SYSCTRLA_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of SYSCTRLA SFRS.
 */
#define SYSCTRLA_SFRS ((__IO SYSCTRLA_SFRS_t *)0x5001f000)

#endif /* end of __SYSCTRLA_SFR_H__ section */


