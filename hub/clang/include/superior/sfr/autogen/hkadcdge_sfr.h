/**
 * @copyright 2019 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file hkadcdge_sfr.h
 */

#ifndef __HKADCDGE_SFR_H__
#define __HKADCDGE_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for HKADCDGE.
 */
typedef struct {

  union {
    struct {
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  IRQI2OARMED              :  1; /*!< Irqi2o Armed */
      uint8_t  IRQO2IARMED              :  1; /*!< Irqo2i Armed */
      uint8_t  DBCIRQ                   :  4;
      uint8_t  ADCMUX                   :  8;
      uint16_t DLYA2N                   : 12;
      uint8_t  DLYM2A                   :  4;
    };
    uint32_t WORD;
  } NODE00CTRLA; /* +0x000 */

  union {
    struct {
      uint16_t THRLOW                   : 16; /*!< Thr Low */
      uint16_t THRHIGH                  : 12; /*!< Thr High */
      uint8_t  ADCSMPCYC                :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE00CTRLB; /* +0x004 */

  union {
    struct {
      uint16_t ADCWORD                  : 12;
      uint8_t  THRLOWEXCD               :  1; /*!< Thr Low Excd */
      uint8_t  THRHIGHEXCD              :  1; /*!< Thr High Excd */
      uint8_t                           :  2; /*   (reserved) */
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE00CTRLC; /* +0x008 */

  uint32_t _RESERVED_0C[125];

  uint32_t ADCIRQFLAG;                        /* +0x200 */

  uint32_t NODEACTIVE;                        /*<! Node Active +0x204 */

  uint32_t NODEONESHOT;                       /*<! Node Oneshot +0x208 */

  uint8_t  HKADCENB;                          /* +0x20C */
  uint8_t  _RESERVED_20D[3];                  /* +0x20D */

  uint8_t  HKADCENAPTAT;                      /*<! Hkadc Ena Ptat +0x210 */
  uint8_t  _RESERVED_211[3];                  /* +0x211 */

} HKADCDGE_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of HKADCDGE SFRS.
 */
#define HKADCDGE_SFRS ((__IO HKADCDGE_SFRS_t *)0x50014000)

#endif /* end of __HKADCDGE_SFR_H__ section */


