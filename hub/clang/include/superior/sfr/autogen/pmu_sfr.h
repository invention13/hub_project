/**
 * @copyright 2019 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file pmu_sfr.h
 */

#ifndef __PMU_SFR_H__
#define __PMU_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for PMU.
 */
typedef struct {

  union {
    struct {
      uint8_t  CKSEL                    :  1;
      uint8_t  PLLEN                    :  1; /*!< Pll En */
      uint8_t  SUSPEND                  :  1;
      uint8_t  CLKUSABLE                :  1;
      uint8_t  XTALENEXTCLOCK           :  1; /*!< Xtal Enextclock */
      uint8_t  PLLLOCK                  :  1; /*!< Pll Lock */
      uint8_t  CLKDIV                   :  2; /*!< Clk Div */
      uint8_t  PMURESETFLAG             :  1; /*!< Pmu Reset Flag */
      uint8_t  AWDTFLAG                 :  1; /*!< Awdt Flag */
      uint8_t  MCUBORFLAG               :  1; /*!< Mcu Bor Flag */
      uint8_t  BORFLAG                  :  1; /*!< Bor Flag */
      uint8_t  PORFLAG                  :  1; /*!< Por Flag */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  PMURESET                 :  1; /*!< Pmu Reset */
      uint8_t  PMUSLEEP                 :  1; /*!< Pmu Sleep */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  VBUSTRP                  :  1; /*!< Vbus Trp */
      uint8_t  SLEEPTIMERTRP            :  1; /*!< Sleep Timer Trp */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
    };
    uint32_t WORD;
  } PMUCLK; /* +0x000 */

} PMU_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of PMU SFRS.
 */
#define PMU_SFRS ((__IO PMU_SFRS_t *)0x50000000)

#endif /* end of __PMU_SFR_H__ section */


