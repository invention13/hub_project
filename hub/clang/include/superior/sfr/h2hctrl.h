/**********************************************************************************************************************
 * @copyright 2017 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file h2h.h
 *********************************************************************************************************************/

#ifndef __H2H_H__
#define __H2H_H__

#include "../clough/clough_core.h"
#include "../clough/clough_dbg.h"
#include <stdint.h>


// ********************************************************************************************************************
// compiler specific
// ********************************************************************************************************************

// IAR
#ifdef __ICCARM__
  #include "io_macros.h"
#endif


// ********************************************************************************************************************
// H2H SFR typedefs
// ********************************************************************************************************************

// enabling register
typedef union {
  struct {
    uint16_t D2H1     : 1;  // enable D.OUT2 => H.IN1 pipe
    uint16_t D4H3     : 1;  // enable D.OUT4 => H.IN3 pipe
    uint16_t H2D1     : 1;  // enable H.OUT2 => D.IN1 pipe
    uint16_t H4D3     : 1;  // enable H.OUT4 => D.IN3 pipe
    uint16_t H6D5     : 1;  // enable H.OUT6 => D.IN5 pipe
    uint16_t DHRAD    : 1;  // enable D=>H audio rate adapter
    uint16_t DHAUDRX  : 1;  // enable D=>H audio receive
    uint16_t DHAUDTX  : 1;  // enable D=>H audio transmit
    uint16_t HDRAD    : 1;  // enable H=>D audio rate adapter
    uint16_t HDAUDRX  : 1;  // enable D=>H audio receive
    uint16_t HDAUDTX  : 1;  // enable D=>H audio transmit
    uint16_t          : 5;
  };
  uint16_t HWORD;
} H2HCTRL_ENA_t;

// packet depth
typedef union {
  struct {
    uint8_t BT_PKTD     : 2;
    uint8_t INT_PKTD    : 2;
    uint8_t AUD_DH_PKTD : 2;
    uint8_t AUD_HD_PKTD : 2;
  };
  uint8_t BYTE;
} H2HCTRL_PKTD_t;

// current audio buffer count
typedef union {
  struct {
    uint8_t DH;
    uint8_t HD;
  };
  uint16_t HWORD;
} H2HCTRL_AUDBUFC_t;

// audio config
#define H2HCTRL_SMPSZ_16 ((uint16_t) 0)
#define H2HCTRL_SMPSZ_24 ((uint16_t) 1)
typedef union {
  struct {
    uint16_t DHTXZ    : 1;   // D=>H:  if bit set then transmit zeros instead of buffer data (mute)
    uint16_t DHTXF    : 1;   // D=>H:  if RX packet missing, infill with zeros
    uint16_t DHSLWTYP : 1;   // D=>H:  set rate adapter slewing algorithm
    uint16_t HDTXZ    : 1;   // H=>D:  if bit set then transmit zeros instead of buffer data (mute)
    uint16_t HDTXF    : 1;   // H=>D:  if RX packet missing, infill with zeros
    uint16_t HDSLWTYP : 1;   // H=>D:  set rate adapter slewing algorithm
    uint16_t DHEPNMAP : 1;   // D=>H:  remap endpoint D.OUT6 to D.OUT8
    uint16_t DHSMPSZ  : 1;   // D=>H:  sample size:  0=>16bit 1=>24bit
    uint16_t HDSMPSZ  : 1;   // H=>D:  sample size:  0=>16bit 1=>24bit
    uint16_t          : 7;
  };
  uint16_t HWORD;
} H2HCTRL_AUDCFG_t;

typedef union {
  struct {
    uint8_t DHZLPDMP  : 1;   // D=>H:  Whether to dump ISO IN packet if a ZLP has been delivered due to no data present.
    uint8_t HDZLPDMP  : 1;   // H=>D:  Whether to dump ISO IN packet if a ZLP has been delivered due to no data present.
    uint8_t           : 6;
  };
  uint8_t BYTE;
} H2HCTRL_AUDZLPDMP_t;


// rate adapter config:  bandwidth settings
typedef union {
  struct {
    uint8_t INTG  : 4;  // integrator gain
    uint8_t ACW   : 2;  // accumulator width
    uint8_t CLR   : 1;  // set to 1 to clear/preset.  self-clearing bit.
    uint8_t TSRC  : 1;  // timing source to use.  0=>SOF 1=>OUT
  };
  uint8_t BYTE;
} H2HCTRL_RADCFG_BW_t;

// rate adapter config
typedef struct {
  H2HCTRL_RADCFG_BW_t BW;     // bandwidth config
  uint8_t             HGTHR;  // threshold for gain adjust
  uint8_t             DEP;    // target FIFO depth
  uint8_t             BUFCLR; // write 1 to clear audio buffer.  self clearing.
} H2HCTRL_RADCFG_t;

// interrupt config bitmap
typedef union {
  struct {
    uint16_t D2H1     : 1;  // D.OUT2 => H.IN1 pipe
    uint16_t D4H3     : 1;  // D.OUT4 => H.IN3 pipe
    uint16_t H2D1     : 1;  // H.OUT2 => D.IN1 pipe
    uint16_t H4D3     : 1;  // H.OUT4 => D.IN3 pipe
    uint16_t H6D5     : 1;  // H.OUT6 => D.IN5 pipe
    uint16_t          : 1;
    uint16_t DHAUDB   : 1;  // D=>H audio buffer over/under flow
    uint16_t DHAUDI   : 1;  // D=>H audio ISO error (miss expected packet)
    uint16_t HDAUDB   : 1;  // H=>D audio buffer over/under flow
    uint16_t HDAUDI   : 1;  // H=>D audio ISO error (miss expected packet)
    uint16_t HIN7EMP  : 1;  // Host IN7 endpoint empty
    uint16_t DIN7EMP  : 1;  // Device IN7 endpoiny empty
    uint16_t          : 4;
  };
  uint16_t HWORD;
} H2HCTRL_IRQV_t;

// DFT
typedef union {
  struct {
    uint8_t WAIT  : 2;
    uint8_t VIOL  : 1;
    uint8_t       : 5;
  };
  uint8_t BYTE;
} H2HCTRL_DBG_t;


// ********************************************************************************************************************
// H2H register map
// ********************************************************************************************************************

typedef struct {
  H2HCTRL_ENA_t       ENA;           // offset: 0x00 ... 0x01
  uint16_t            _NOASSIGN_02;  // offset: 0x02 ... 0x03

  H2HCTRL_PKTD_t      PKTD;          // offset: 0x04
  uint8_t             _NOASSIGN_05;  // offset: 0x05
  uint16_t            _NOASSIGN_06;  // offset: 0x06 ... 0x07

  H2HCTRL_AUDBUFC_t   AUDBUFC;       // offset: 0x08 ... 0x09
  uint16_t            _NOASSIGN_0A;  // offset: 0x0A ... 0x0B

  H2HCTRL_AUDCFG_t    AUDCFG;        // offset: 0x0C ... 0x0D
  H2HCTRL_AUDZLPDMP_t AUDZLPDMP;     // offset: 0x0E
  uint8_t             _NOASSIGN_0F;  // offset: 0x0F

  H2HCTRL_RADCFG_t    DHRADCFG;      // offset: 0x10 ... 0x13.  D=>H rate adapter config
  H2HCTRL_RADCFG_t    HDRADCFG;      // offset: 0x14 ... 0x17.  H=>D rate adapter config

  H2HCTRL_IRQV_t      IRQENA;        // offset: 0x18 ... 0x19.  interrupt source enables
  uint16_t            _NOASSIGN_1A;  // offset: 0x1A ... 0x1B

  H2HCTRL_IRQV_t      IRQF;          // offset: 0x1C ... 0x1D.  interrupt flags
  uint16_t            _NOASSIGN_1E;  // offset: 0x1E ... 0x1F

  H2HCTRL_DBG_t       DBG;           // offset: 0x20
  uint8_t             _NOASSIGN_21;  // offset: 0x21
  uint16_t            _NOASSIGN_22;  // offset: 0x22 ... 0x23
} H2HCTRL_SFRS_t;


// ********************************************************************************************************************
// SFR memory mapping
// ********************************************************************************************************************


#define _H2HCTRL        ((volatile H2HCTRL_SFRS_t   *) (ASIC_16B_BASE + 0xB000))


#ifdef __SFR_MAP_ASOBJ__
  // optional: provide as struct
  #ifdef __ICCARM__
    // IAR style
    volatile __no_init H2HCTRL_SFRS_t H2HCTRL @ (ASIC_16B_BASE + 0xB000);
  #else
    // generic style
    #define H2HCTRL  (* _H2HCTRL)
  #endif
#else
  // default : provide as struct pointer
  #define H2HCTRL       _H2HCTRL
#endif

#endif // include guard
