/**
 * @copyright 2016 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file uart_sfr.h
 */

#ifndef __UART_SFR_H__
#define __UART_SFR_H__


typedef union {
      struct {
         uint8_t FRAMEERROR                 :  1; /*!< Framing Error */
         uint8_t PARITYERROR                :  1; /*!< Parity Error */
         uint8_t BREAKERROR                 :  1; /*!< Break Error */
         uint8_t                            :  5; /*    (reserved) */
      };
      uint8_t BYTE;
   } UART_DATARECEIVESTATUS_t;

typedef union {
      struct {
         uint32_t ENABLE                    :  1;
         uint32_t ENABLE_STS                :  1; /*!< Enable status */
         uint32_t UFIFOSOFTRESET            :  1; /*!< FIFO SOFT RESET */
         uint32_t                           : 13; /*    (reserved) */
         uint32_t SIZE                      :  2; /*!< Transmission word size */
         uint32_t STOP                      :  1; /*!< Stop bit control */
         uint32_t PARENA                    :  1; /*!< Parity enable */
         uint32_t PARODD                    :  1; /*!< Odd parity */
         uint32_t STICKENA                  :  1; /*!< Sticky partiy enable */
         uint32_t BREAKENA                  :  1; /*!< Break enable */
         uint32_t LOOPENA                   :  1; /*!< Loopback enable */
         uint32_t                           :  8; /*    (reserved) */
      };
      uint32_t WORD;
   } UART_MSGCTRL_t;

typedef union {
      struct {
         uint8_t DTRDY                      :  1; /*!< Rx Data ready */
         uint8_t OVRUNERR                   :  1; /*!< Overrun error */
         uint8_t FRMERR                     :  1; /*!< Framing error */
         uint8_t PRTYERR                    :  1; /*!< Parity Error */
         uint8_t BREAKIRQ                   :  1; /*!< Break IRQ */
         uint8_t TXDONE                     :  1; /*!< Transmission is done */
         uint8_t RXMULTIPLEXFERDONE         :  1; /*!< Multiple Receive Transactions Done */
         uint8_t TXMULTIPLEXFERDONE         :  1; /*!< Multiple Transmit Transactions Done */
      };
      uint8_t BYTE;
   } UART_STATUS_t;

typedef union {
      struct {
         uint8_t INTRXDATAREADY             :  1; /*!< Rx Data ready Interrupt */
         uint8_t INTRXOVERRUNERROR          :  1; /*!< Overrun error Interrupt */
         uint8_t INTRXFRAMINGERROR          :  1; /*!< Framing error Interrupt */
         uint8_t INTRXPARITYERROR           :  1; /*!< Parity Error Interrupt */
         uint8_t INTRXBREAKERROR            :  1; /*!< Break Error Interrupt */
         uint8_t INTTXDONE                  :  1; /*!< Transmission done Interrupt */
         uint8_t INTRXMULTIPLEXFERDONE      :  1; /*!< Multiple Receive Transactions Done Interrupt */
         uint8_t INTTXMULTIPLEXFERDONE      :  1; /*!< Multiple Transmit Transactions Done Interrupt */
      };
      uint8_t BYTE;
   } UART_INTSTATUS_t;

typedef union {
      struct {
         uint8_t INTRXDATAREADYENA          :  1; /*!< Rx Data ready Interrupt Enable */
         uint8_t INTRXOVERRUNERRORENA       :  1; /*!< Overrun error Interrupt Enable */
         uint8_t INTRXFRAMINGERRORENA       :  1; /*!< Framing error Interrupt Enable */
         uint8_t INTRXPARITYERRORENA        :  1; /*!< Parity Error Interrupt Enable */
         uint8_t INTRXBREAKERRORENA         :  1; /*!< Break Error Interrupt Enable */
         uint8_t INTTXDONEENA               :  1; /*!< Transmission done Interrupt Enable */
         uint8_t INTRXMULTIPLEXFERDONEENA   :  1; /*!< Multiple Receive Transactions Done Interrupt Enable */
         uint8_t INTTXMULTIPLEXFERDONEENA   :  1; /*!< Multiple Transmit Transactions Done Interrupt Enable */
      };
      uint8_t BYTE;
   } UART_INTENABLE_t;

typedef union {
      struct {
         uint8_t INTRXDATAREADYCLR          :  1; /*!< Rx Data ready Interrupt Clear */
         uint8_t INTRXOVERRUNERRORCLR       :  1; /*!< Overrun error Interrupt Clear */
         uint8_t INTRXFRAMINGERRORCLR       :  1; /*!< Framing error Interrupt Clear */
         uint8_t INTRXPARITYERRORCLR        :  1; /*!< Parity Error Interrupt Clear */
         uint8_t INTRXBREAKERRORCLR         :  1; /*!< Break Error Interrupt  Clear */
         uint8_t INTTXDONECLR               :  1; /*!< Transmission done Interrupt Clear */
         uint8_t INTRXMULTIPLEXFERDONECLR   :  1; /*!< Multiple Receive Transactions Done Interrupt Clear */
         uint8_t INTTXMULTIPLEXFERDONECLR   :  1; /*!< Multiple Transmit Transactions Done Interrupt Clear */
      };
      uint8_t BYTE;
   } UART_INTCLEAR_t;

typedef union {
      struct {
         uint16_t BAUDDIV                   : 16; /*!< Baud rate divider */
         uint8_t  OSR                       :  5; /*!< Over-sampling ratio */
         uint8_t                            :  3; /*   (reserved) */
         uint8_t  UADVANCE                  :  1; /*!< Advance Register */
         uint8_t                            :  3; /*   (reserved) */
         uint8_t  URETARD                   :  1; /*!< Retard Register */
         uint8_t                            :  3; /*   (reserved) */
    };
    uint32_t WORD;
   } UART_BAUD_t;

typedef union {
      struct {
         uint8_t RXUF                       :  1; /*!< Reception FIFO underflow */
         uint8_t RXEMPTY                    :  1; /*!< Reception FIFO empty */
         uint8_t RXFULL                     :  1; /*!< Reception FIFO empty */
         uint8_t RXCOUNT                    :  5; /*!< Reception FIFO current count */
      };
      uint8_t BYTE;
   } UART_RXFIFOSTATUS_t;

typedef union {
      struct {
         uint8_t TXOF                       :  1; /*!< Transmit FIFO overflow */
         uint8_t TXEMPTY                    :  1; /*!< Transmit FIFO empty */
         uint8_t TXFULL                     :  1; /*!< Transmit FIFO full */
         uint8_t TXCOUNT                    :  5; /*!< Transmit FIFO current count */
      };
      uint8_t BYTE;
   } UART_TXFIFOSTATUS_t;

typedef union {
      struct {
         uint16_t RXMULTIPLEXFERDONECNT     :  5; /*!< Receive Data Count Interrupt */
         uint16_t                           :  3; /*   (reserved) */
         uint16_t TXMULTIPLEXFERDONECNT     :  5; /*!< Transmit Data Count Interrupt */
         uint16_t                           :  3; /*   (reserved) */
      };
      uint16_t HWORD;
   } UART_FIFOLEVELCTL_t;

typedef union {
      struct {
         uint16_t TXDMAENA                  :  1; /*!< Transmit FIFO DMA Enable.  When set to 1, DMA requests are enabled for the transmit FIFO. */
         uint16_t RXDMAENA                  :  1; /*!< Receive FIFO DMA Enable.  When set to 1, DMA requests are enabled for the receive FIFO. */
         uint16_t TXFIFODMAREQLVL           :  5; /*!< Transmit FIFO Watermark */
         uint16_t RXFIFODMAREQLVL           :  5; /*!< Receive FIFO Watermark */
         uint16_t                           :  4; /*    (reserved) */
      };
      uint16_t HWORD;
   } UART_DMACTL_t;



/**
 * @brief A structure to represent Special Function Registers for UART.
 */
typedef struct {

  uint8_t                       DATA;                       /*  offset 0x00            Data     */
  uint8_t                       _RESERVED_01;               /*  offset 0x01                     */
  uint16_t                      _RESERVED_02;               /*  offset 0x02 ... 0x03            */

  UART_DATARECEIVESTATUS_t      UARTDATARECEIVESTATUS;      /*  offset 0x04                     */
  uint8_t                       _RESERVED_05;               /*  offset 0x05                     */
  uint16_t                      _RESERVED_06;               /*  offset 0x06 ... 0x07            */

  UART_MSGCTRL_t                MSGCTRL;                    /*  offset 0x08 ... 0x0B            */

  UART_STATUS_t                 UARTSTATUS;                 /*  offset 0x0C                     */
  uint8_t                       _RESERVED_0D;               /*  offset 0x0D                     */
  uint16_t                      _RESERVED_0E;               /*  offset 0x0E ... 0x0F            */

  UART_INTSTATUS_t              UARTINTSTATUS;              /*  offset 0x10                     */
  uint8_t                       _RESERVED_11;               /*  offset 0x11                     */
  uint16_t                      _RESERVED_12;               /*  offset 0x12 ... 0x13            */

  UART_INTENABLE_t              UARTINTENABLE;              /*  offset 0x14                     */
  uint8_t                       _RESERVED_15;               /*  offset 0x15                     */
  uint16_t                      _RESERVED_16;               /*  offset 0x16 ... 0x17            */

  UART_INTCLEAR_t               UARTINTCLEAR;               /*  offset 0x18                     */
  uint8_t                       _RESERVED_19;               /*  offset 0x19                     */
  uint16_t                      _RESERVED_1A;               /*  offset 0x1A ... 0x1B            */

  UART_BAUD_t                   BAUD;                       /*  offset 0x1C ... 0x1F            */

  UART_RXFIFOSTATUS_t           UARTRXFIFOSTATUS;           /*  offset 0x20                     */
  UART_TXFIFOSTATUS_t           UARTTXFIFOSTATUS;           /*  offset 0x21                     */
  uint16_t                      _RESERVED_22;               /*  offset 0x22 ... 0x23            */

  UART_FIFOLEVELCTL_t           UARTFIFOLEVELCTL;           /*  offset 0x24 ... 0x25            */

} UART_SFRS_t;

/**
 * @brief The starting address of UART SFRS.
 */
#define UART_SFRS ((__IO UART_SFRS_t *)0x50014800)

#endif /* end of __UART_SFR_H__ section */

