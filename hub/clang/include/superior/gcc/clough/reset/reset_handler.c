// ********************************************************************************************************************
//
// COPYRIGHT 2017 AyDeeKay LLC - All Rights Reserved
// No part of this document can be used, copied, transmitted, or modified by
// any existing or to be invented means without express approval of AyDeeKay
// LLC.
//
// ********************************************************************************************************************
//
// File:
//   reset_handler.c
//
// Simple bare-metal reset procedure.  Extablishes stack, but no heap (so don't use malloc with this one!)
//
// ********************************************************************************************************************

#include <stdint.h>
#include "superior.h"


// ********************************************************************************************************************
// Values passed back to us from the linker for use in initialization routines.  These are not real variables to
// store data in, but we can take their address to find out where certain locations in memory are.  DON'T WRITE
// TO THESE "VARIABLES".  Use the linker script provided.
// ********************************************************************************************************************

extern uint32_t _etext;       // first address higher than the code memory
extern uint32_t _sidata;      // first address of the FLASH containing the initialized SRAM data source
extern uint32_t _sdata;       // first address of the initialized SRAM data
extern uint32_t _edata;       // first address higher than the end of the initialized SRAM data
extern uint32_t _sbss;        // first address of the unitialized SRAM data
extern uint32_t _ebss;        // first address higher than the end of the uninitialized SRAM data
extern uint32_t _HEAP_START;  // first address of the heap


// ********************************************************************************************************************
// System initialization and call to main()
// ********************************************************************************************************************

// Initialize the data in SRAM
void inline __init_sram( void ) {
  uint32_t* fptr;
  uint32_t* mptr;

  // Initialize data section of SRAM.  Linker WORD aligned this to for faster initialization.
  fptr = &_sidata;
  mptr = &_sdata;
  while( mptr < &_edata ) *(mptr++) = *(fptr++);

  // Clear bss section of SRAM.  Linker WORD aligned this to for faster initialization.
  mptr = &_sbss;
  while( mptr < &_ebss ) *(mptr++) = 0;
}

// Code when main() exits.  Default implementation produces a software reset.
extern void _sys_exit( int return_code );
#pragma weak _sys_exit = __sys_exit
void __sys_exit( int return_code ) {
  NVIC_SystemReset();
}

// code to execute before sram initialization.  keep in mind that no SRAM has been populated by the time this fucntion
// executes, referencing non-local variables or local static variables is ill advised...
extern void _pre_init( void );
#pragma weak _pre_init = __pre_init
void __pre_init( void ) {
  FLASH_CTRL->FLSCTRL = FLSCTRL_16TO32MHZ;  // speed up the flash access
}

// reset handler
extern int main( void );
void Reset_Handler( void ) {
  int ret_val;
  _pre_init();         // run pre-initialization steps
  __init_sram();       // Initialize the data space in SRAM
  ret_val = main();    // user defined main()
  _sys_exit(ret_val);  // if main exits
}
