// ****************************************************************************
//
// COPYRIGHT 2016 AyDeeKay LLC - All Rights Reserved
// No part of this document can be used, copied, transmitted, or modified by
// any existing or to be invented means without express approval of AyDeeKay
// LLC.
//
// ****************************************************************************
//
// File:
//   Superior.h
//
// Description:
//   Definitions and function declarations for Superior IC
//
//   Note:  this file should also work for HURON, using:
//          #define CHIPVARIANT_HURON
//
// ****************************************************************************

#ifndef __SUPERIOR_H__
#define __SUPERIOR_H__


// ********************************************************************************************************************
// IRQ enumeration - must be defined before core_cm0.h loaded
// ********************************************************************************************************************

// Interrupt table:
typedef enum IRQn
{
  //******  Cortex-M0 Processor Exceptions Numbers ****************************
  NonMaskableInt_IRQn   = -14,      // Non Maskable Interrupt
  HardFault_IRQn        = -13,      // Hard Fault Interrupt
  SVCall_IRQn           = -5,       // SV Call Interrupt
  PendSV_IRQn           = -2,       // Pend SV Interrupt
  SysTick_IRQn          = -1,       // System Tick Interrupt

  //*****  CM0IKMCU Cortex-M0 specific Interrupt Numbers **********************
  H2HH_IRQn             =  0,       // SUPERIOR: USB Host to Host host-side
  H2HD_IRQn             =  1,       // SUPERIOR: USB Host to Host device-side
  H2H_IRQn              =  2,       // SUPERIOR: USB Host to Host bridge
  UHKADC_IRQn           =  3,       // SUPERIOR: House Keeping ADC. Reports to MCU when one of the preprogrammed
                                    //           HKADC comparison thresholds has been exceed
  PortChng_IRQn         =  4,       // SUPERIOR: Port State Change. Reports to MCU when an interesting downstream port
                                    //           state change event has occurred.
  USB_IRQn              =  5,       // SUPERIOR: USB Interrupt
  Suspend_IRQn          =  6,       // SUPERIOR: Reports to MCU when the USB Hub has been placed into suspend mode
  WUSB_IRQn             =  7,       // SUPERIOR: USB Wakeup Interrupt
  OvrCrnt_OvrDrv_IRQn   =  8,       // SUPERIOR: Over Current and Over Drive Interrupts
  VBus_IRQn             =  9,       // SUPERIOR: Change in state of the VBUS_DET pin
  SD_IRQn               = 10,       // SUPERIOR: SD Card Host Interrupt Request
  FIFO_IRQn             = 11,       // SUPERIOR: USB Device Endpoint FIFO Interrupt Request
  UART_LIN_IRQn         = 12,       // SUPERIOR: UART and LIN interrupts
  GPIO_WDT_IRQn         = 13,       // SUPERIOR: GPIO and ASIC Watchdog Timer interrupt
  SPI_I2C_IRQn          = 14,       // SUPERIOR: SPI and I2C interrupts
  CHGMEAS_USBC_IRQn     = 15,       // SUPERIOR: Charge Measurement and USB Type C.
                                    //             CM irq when a Charge Measurement DBE thresholds has triggered
                                    //             See USB Type C specification for USBC interrupt information
  Timer0_IRQn           = 16,       // CLOUGH:   Timer 0 overflow
  Timer1_IRQn           = 17,       // CLOUGH:   Timer 1 overflow
  Timer2_IRQn           = 18,       // CLOUGH:   Timer 2 overflow
  Watchdog_IRQn         = 19,       // CLOUGH:   Watchdog timer overflow
  BTE_IRQn              = 20,       // CLOUGH:   Block transfer complete
  SDIO_IRQn             = 21,       // CLOUGH:   Serial link error
} IRQn_Type;


#include <stdint.h>
#include "./clough/clough_core.h"


// ****************************************************************************
// interrupt vector prototypes
// ****************************************************************************

// Exception
void Reset_Handler(void);
void HardFault_Handler(void);
void SysTick_Handler(void);
void PendSV_Handler(void);                 // no hardware for this
void SVC_Handler(void);                    // no hardware for this
void NMI_Handler(void);                    // no hardware for this

// IRQ
void USBDCH2HH_Handler(void);              // (IRQ  0)
void USBDCH2HD_Handler(void);              // (IRQ  1)
void H2H_Handler(void);                    // (IRQ  2)
void UHKADC_Handler(void);                 // (IRQ  3)
void PortChng_Handler(void);               // (IRQ  4)
void USBDCAPP_Handler(void);               // (IRQ  5)
void Suspend_Handler(void);                // (IRQ  6)
void WUSB_Handler(void);                   // (IRQ  7)
void OvrCrnt_OvrDrv_Handler(void);         // (IRQ  8)
void VBus_SlpTimer_Handler(void);          // (IRQ  9)
void SD_Handler(void);                     // (IRQ 10)
void USBDCAPP_FIFO_Handler(void);          // (IRQ 11)
void UART_LIN_Handler(void);               // (IRQ 12)
void GPIO_WDT_Handler(void);               // (IRQ 13)
void SPI_I2C_Handler(void);                // (IRQ 14)
void CHGMEAS_USBC_Handler(void);           // (IRQ 15)
void Timer0_Handler(void);                 // (IRQ 16)
void Timer1_Handler(void);                 // (IRQ 17)
void Timer2_Handler(void);                 // (IRQ 18)
void WDOG_Handler(void);                   // (IRQ 19)
void BTE_Handler(void);                    // (IRQ 20)
void SDIO_Handler(void);                   // (IRQ 21)


// ****************************************************************************
// REGISTER MAP STARTS HERE
// ****************************************************************************

// SFR definitions and convenience functions
#include "./sfr/usbdc.h"
#include "./sfr/usbhub.h"
#include "./sfr/usbafe.h"
#include "./sfr/h2hctrl.h"
#include "./sfr/pmu.h"
#include "./sfr/ppc_sfr.h"
#include "./sfr/uart_sfr.h"
#include "./sfr/i2c_sfr.h"
#include "./sfr/scr_sfr.h"
#include "./sfr/pll.h"


// Autogenerated SFR definitions
#include "./sfr/autogen/autogen_sfr.h"

// Convenience functions
#include "./sfr/utility_func.h"

#endif   // include guard
