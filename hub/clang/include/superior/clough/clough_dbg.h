// ********************************************************************************************************************
//
// COPYRIGHT 2017 AyDeeKay LLC - All Rights Reserved
// No part of this document can be used, copied, transmitted, or modified by
// any existing or to be invented means without express approval of AyDeeKay
// LLC.
//
// ********************************************************************************************************************
//
// File:
//   clough_dbg.h
//
// Description:
//   Prototypes for debug functions useful in Clough-based designs
//
// ********************************************************************************************************************

#ifndef __CLOUGH_DBG_H__
#define __CLOUGH_DBG_H__

// debug trap.  useful for assertion traps during code development.  C code provided is implemented as WEAK so that
// firmware can provide an alternate as desired.  ideally the code should preserve the stack so that return address
// can be used to determine source of error.
void dbgTrapErr( void );

#endif // include guard
