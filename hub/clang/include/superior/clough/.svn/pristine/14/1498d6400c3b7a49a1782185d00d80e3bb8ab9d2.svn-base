// ****************************************************************************
//
// COPYRIGHT 2012 AyDeeKay LLC - All Rights Reserved
// No part of this document can be used, copied, transmitted, or modified by
// any existing or to be invented means without express approval of AyDeeKay
// LLC.
//
// ****************************************************************************
//
// File:
//   clough_core.h
//
// Description:  
//   Definitions and declarations for functions incorporated in the Clough
//   Cortex-M0 microcontroller core
//
// ****************************************************************************
//
// REVISION HISTORY
//
// Version   Date:        By:          Description:
// V0.01.00  04/06/2012   Scott Kee    Unify various versions into one file
//
// **************************************************************************** 


#ifndef __CLOUGH_MCU_H__
#define __CLOUGH_MCU_H__

// ********************************************************************************************************************
// Number of IRQ priority level bits - must be defined before loading core_cm0.h
// ********************************************************************************************************************

#define __NVIC_PRIO_BITS 2


// ********************************************************************************************************************
// Includes
// ********************************************************************************************************************

#include "core_cm0.h"
#include <stdint.h>

// ********************************************************************************************************************
// Base addresses
// ********************************************************************************************************************

#define FLASH_BASE       ((uint32_t) 0x00000000)
#define SRAM_BASE        ((uint32_t) 0x20000000)
#define SRAM_TOP         ((uint32_t) 0x20001FFF)
#define ASIC_7B_BASE     ((uint32_t) 0x50000000)
#define ASIC_16B_BASE    ((uint32_t) 0x50010000)

// ********************************************************************************************************************
// Peripheral addresses -- Flash Controller
// ********************************************************************************************************************

typedef struct {
  __O  uint32_t  FLADDR;
  __O  uint8_t   FLWRDT;
  __IO uint8_t   FLFLAG;
  __I  uint16_t  empty1;
  __O  uint32_t  UNLBWR;
  __O  uint32_t  BWRSTRT;
  __O  uint32_t  UNLSER;
  __O  uint32_t  SERSTRT;
  __O  uint32_t  UNLBLKER;
  __O  uint32_t  BKERSTRT;
  __IO uint8_t   FLSCTRL;
} FlashCtrlReg_Type;

// Flash controller structure.  User FLASH_CTRL->(whatever) to access
#define FLASH_CTRL ((FlashCtrlReg_Type *) 0x50020020)

// Special AyDeeKay flash sector 0x27F00 to 0x27FFF
#define FLASH_ADK_SECTOR 0x27F00

// Flash unlock  keys
#define FLASH_UNLOCK_BYTE_WRITE_KEY      ((uint32_t) 0x55555555)
#define FLASH_BYTE_WRITE_KEY             ((uint32_t) 0xAAAAAAAA)
#define FLASH_UNLOCK_BLOCK_ERASE_KEY     ((uint32_t) 0x77777777)
#define FLASH_BLOCK_ERASE_KEY            ((uint32_t) 0x88888888)
#define FLASH_UNLOCK_SECTOR_ERASE_KEY    ((uint32_t) 0x66666666)
#define FLASH_SECTOR_ERASE_KEY           ((uint32_t) 0x99999999)

// Bit indexing helpers
#define FLFLAG_WRITE          ((uint8_t) 0x01)
#define FLFLAG_SECTOR_ERASE   ((uint8_t) 0x02)
#define FLFLAG_BLOCK_ERASE    ((uint8_t) 0x04)
#define FLSCTRL_BELOW16MHZ    ((uint8_t) ((0x00 << 0) | (0x00 << 3)) )
#define FLSCTRL_16TO32MHZ     ((uint8_t) ((0x01 << 0) | (0x01 << 3)) )    
#define FLSCTRL_ABOVE32MHZ    ((uint8_t) ((0x02 << 0) | (0x02 << 3)) )

// chip serial number
typedef struct
{
  __I uint32_t TIMESTAMP;  // { LBNUM[3:0] , TIMESTAMP[27:0] }
  __I uint16_t LOTNUM;     // { LOTNUM[15:0] }
  __I uint16_t CHIPID;     // { CHIPCODE[12:0] , CHIPREV[2:0] }
} ChipSerNum_Type;

#define CHIPSERNUM ((ChipSerNum_Type *) (FLASH_ADK_SECTOR + 0x00))


// ********************************************************************************************************************
// Peripheral addresses -- Code protection SerialWire disable
// ********************************************************************************************************************

// Code protection SerialWire disable
#define FLSCP  ((volatile uint32_t *) 0x50020044)

// Code protection key values
#define CODE_PROTECT_ON  ((uint32_t) 0xF2E11047)
#define CODE_PROTECT_OFF ((uint32_t) 0x00000000)


// ********************************************************************************************************************
// Peripheral addresses -- Block Transfer Engine
// ********************************************************************************************************************

typedef struct {
  __IO uint16_t  BXADD;
  __IO uint8_t   BXNUM;
  __IO uint8_t   BXCONF;
  __IO uint16_t  BXSRAMADDR;
} BTEReg_Type;

// BTE controller structure.  User BTE_CTRL->(whatever) to access
#define BTE_CTRL ((BTEReg_Type *) 0x50020080)

// bit addressing helpers
#define BXCONF_START     ((uint8_t) 0x01) << 3)
#define BXCONF_BLOCKING  ((uint8_t) 0x01) << 2)
#define BXCONF_NBLOCKING ((uint8_t) 0x00) << 2)
#define BXCONF_WRITE     ((uint8_t) 0x01) << 1)
#define BXCONF_READ      ((uint8_t) 0x00) << 1)
#define BXCONF_INCR      ((uint8_t) 0x01) << 0)
#define BXCONF_NOINCR    ((uint8_t) 0x00) << 0)


// ********************************************************************************************************************
// Peripheral addresses -- Timers & Watchdog
// ********************************************************************************************************************

typedef struct {
  __IO int32_t  COUNT;
  __IO uint8_t  CFG;
} Timer_Type;

typedef struct {
  __IO uint8_t  CFG;
  __I  uint8_t  empty1;
  __I  uint16_t empty2;
  __O  uint32_t KEY;
} Watchdog_Type;

// General purpose timers - User TIMERx->(whatever) to access
#define TIMER0  ((Timer_Type *) 0x50020000)
#define TIMER1  ((Timer_Type *) 0x50020008)
#define TIMER2  ((Timer_Type *) 0x50020010)

// General purpose timers - bitmask helpers
#define TIMER_CFG_ENA ((uint8_t) 0x01)  

// Watchdog timer - User WDT->(whatever) to access
#define WDT ((Watchdog_Type *) (0x50020018))

// Watchdog keys
#define WDT_KEY_KEY0     ((uint32_t) 0x3C570001)
#define WDT_KEY_KEY1     ((uint32_t) 0x007F4AD6)
#define WDT_KEY_CLEAR    ((uint32_t) 0x00000000)

// bit addressing helpers for WDT->CFG
// {RESERVED[31:5], WDTPRES[1:0], RSTFLAG, RESETEN, WDTEN}
#define WDT_CFG_ENA   ((uint8_t) (0x01 << 0))
#define	WDT_CFG_RST		((uint8_t) (0x01 << 1))
#define	WDT_CFG_INT		((uint8_t) (0x00 << 1))
#define WDT_CFG_FLAG  ((uint8_t) (0x01 << 2))
#define	WDT_CFG_13		((uint8_t) (0x00 << 3))
#define	WDT_CFG_19		((uint8_t) (0x01 << 3))
#define	WDT_CFG_22		((uint8_t) (0x02 << 3))
#define	WDT_CFG_32		((uint8_t) (0x03 << 3))


// @brief Clears WDT.
//
// @param none
// @return none
//
// Clears (pets) the WDT. 
//
static __INLINE void WDT_Clear( void )
{
  WDT->KEY = WDT_KEY_CLEAR;
  WDT->KEY = WDT_KEY_KEY0;
  WDT->KEY = WDT_KEY_KEY1;
}


// @brief Reads WDT flag returning it.
//
// @param none
// @return Flag (RSTFLAG)
//
static __INLINE uint32_t WDT_GetFLag( void )
{
  return (WDT->CFG & WDT_CFG_FLAG);
}


#endif  // __CLOUGH_MCU_H__
