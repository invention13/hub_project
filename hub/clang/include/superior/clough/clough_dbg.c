// ********************************************************************************************************************
//
// COPYRIGHT 2017 AyDeeKay LLC - All Rights Reserved
// No part of this document can be used, copied, transmitted, or modified by
// any existing or to be invented means without express approval of AyDeeKay
// LLC.
//
// ********************************************************************************************************************
//
// File:
//   clough_dbg.c
//
// Description:
//   Default code for Clough debug functions
//
// ********************************************************************************************************************

#include "clough_dbg.h"
#include <stdint.h>


// debug trap for assertions during code development.  this version simply hardfaults so that the stack can
// be inspected by a debugger.  declared as weak, allowing alternate code to be provided.
#pragma weak dbgTrapErr
void dbgTrapErr( void ) {
  *((uint32_t *) 0x00 ) = 0;  // generate hard fault by writing to address 0
}
