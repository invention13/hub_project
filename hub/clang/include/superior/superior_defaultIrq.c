// ********************************************************************************************************************
//
// COPYRIGHT 2017 AyDeeKay LLC - All Rights Reserved
// No part of this document can be used, copied, transmitted, or modified by
// any existing or to be invented means without express approval of AyDeeKay
// LLC.
//
// ********************************************************************************************************************
//
// File:
//   superior_defaultIrq.c
//
// Default IRQ vectors for Superior chip.  If the default function is acceptable then use this.  Otherwise
// treat as an example.
//
// ********************************************************************************************************************


#include "superior.h"
#include "./clough/clough_dbg.h"

void Default_IRQHandler( void ) {
  dbgTrapErr();  // call debug trapper function
}

// weakly propagate this default handler to all named interrupts and exceptions
#pragma weak HardFault_Handler              = Default_IRQHandler
#pragma weak SysTick_Handler                = Default_IRQHandler
#pragma weak PendSV_Handler                 = Default_IRQHandler
#pragma weak SVC_Handler                    = Default_IRQHandler
#pragma weak NMI_Handler                    = Default_IRQHandler
#pragma weak USBDCH2HH_Handler              = Default_IRQHandler
#pragma weak USBDCH2HD_Handler              = Default_IRQHandler
#pragma weak H2H_Handler                    = Default_IRQHandler
#pragma weak UHKADC_Handler                 = Default_IRQHandler
#pragma weak PortChng_Handler               = Default_IRQHandler
#pragma weak USBDCAPP_Handler               = Default_IRQHandler
#pragma weak Suspend_Handler                = Default_IRQHandler
#pragma weak WUSB_Handler                   = Default_IRQHandler
#pragma weak OvrCrnt_OvrDrv_Handler         = Default_IRQHandler
#pragma weak VBus_SlpTimer_Handler          = Default_IRQHandler
#pragma weak SD_Handler                     = Default_IRQHandler
#pragma weak USBDCAPP_FIFO_Handler          = Default_IRQHandler
#pragma weak UART_LIN_Handler               = Default_IRQHandler
#pragma weak GPIO_WDT_Handler               = Default_IRQHandler
#pragma weak SPI_I2C_Handler                = Default_IRQHandler
#pragma weak CHGMEAS_USBC_Handler           = Default_IRQHandler
#pragma weak Timer0_Handler                 = Default_IRQHandler
#pragma weak Timer1_Handler                 = Default_IRQHandler
#pragma weak Timer2_Handler                 = Default_IRQHandler
#pragma weak WDOG_Handler                   = Default_IRQHandler
#pragma weak BTE_Handler                    = Default_IRQHandler
#pragma weak SDIO_Handler                   = Default_IRQHandler
