// ********************************************************************************************************************
//
// COPYRIGHT 2017 AyDeeKay LLC - All Rights Reserved
// No part of this document can be used, copied, transmitted, or modified by
// any existing or to be invented means without express approval of AyDeeKay
// LLC.
//
// ********************************************************************************************************************
//
// File:
//   superior_boot.c
//
// ********************************************************************************************************************

#include "superior.h"
#include "./clough/clough_dbg.h"
#include <stdint.h>


// ********************************************************************************************************************
// vector table
// ********************************************************************************************************************

#if defined( __ICCARM__ )
  #pragma language=extended
  #pragma segment="CSTACK"
  extern void __iar_program_start( void );
  typedef void( *intfunc )( void );
  typedef union { intfunc __fun; void * __ptr; } intvec_elem;
  #pragma location = ".intvec"
  const intvec_elem __vector_table[] = {
#elif defined( __GNUC__ )
  typedef void( *const vectElem_t ) (void);
  vectElem_t __vectorTable[] __attribute__ ((section(".irq_vectors"))) = {
#else
  #error Unsupported Compiler
#endif
    
  //{ .__ptr = __sfe("CSTACK") },    // [-16]    stack to populated by linker
 
#if defined( __ICCARM__ )
  { .__ptr = __sfe("CSTACK") },  // [-16]    stack to populated by linker
#elif defined( __GNUC__ )
  (vectElem_t) SRAM_TOP,         // [-16]    Top of Stack
#endif    

#if defined( __ICCARM__ )  
  __iar_program_start,             // [-15]    Reset Handler
#elif defined( __GNUC__ )
  Reset_Handler,
#endif
  NMI_Handler,                     // [-14]    NMI Handler
  HardFault_Handler,               // [-13]    Hard Fault Handler
  0,                               // [-12]    Reserved
  0,                               // [-11]    Reserved
  0,                               // [-10]    Reserved
  0,                               // [ -9]    Reserved
  0,                               // [ -8]    Reserved
  0,                               // [ -7]    Reserved
  0,                               // [ -6]    Reserved
  SVC_Handler,                     // [ -5]    SVCall Handler
  0,                               // [ -4]    Reserved
  0,                               // [ -3]    Reserved
  PendSV_Handler,                  // [ -2]    PendSV Handler
  SysTick_Handler,                 // [ -1]    SysTick Handler
  USBDCH2HH_Handler,               // [  0]    H2HH_USB Interrupt
  USBDCH2HD_Handler,               // [  1]    H2HD_USB Interrupt
  H2H_Handler,                     // [  2]    H2H Interrupt
  UHKADC_Handler,                  // [  3]    HKADC interrupt
  PortChng_Handler,                // [  4]    Downstream port state change
  USBDCAPP_Handler,                // [  5]    USB Interrupt
  Suspend_Handler,                 // [  6]    Hub suspend
  WUSB_Handler,                    // [  7]    USB Wakeup Interrupt
  OvrCrnt_OvrDrv_Handler,          // [  8]    port power control switch overcurrent and Over Drive
  VBus_SlpTimer_Handler,           // [  9]    change in state of the VBUS_DET pin & Sleep Timer
  SD_Handler,                      // [ 10]    SD Card Host Interrupt Request
  USBDCAPP_FIFO_Handler,           // [ 11]    USB Device Endpoint FIFO Interrupt Request
  UART_LIN_Handler,                // [ 12]    UART and LIN interrupt
  GPIO_WDT_Handler,                // [ 13]    GPIO Barium and Watchdog Timer Interrupt
  SPI_I2C_Handler,                 // [ 14]    SPI/I2C Muxed Interrupt
  CHGMEAS_USBC_Handler,            // [ 15]    Charge Measurement DBE threshold exceeded & USB Type C
  Timer0_Handler,                  // [ 16]    Timer0 overflow
  Timer1_Handler,                  // [ 17]    Timer1 overflow
  Timer2_Handler,                  // [ 18]    Timer2 overflow
  WDOG_Handler,                    // [ 19]    Watchdog overflow
  BTE_Handler,                     // [ 20]    Block xfer done
  SDIO_Handler,                    // [ 21]    Serial link error
};
