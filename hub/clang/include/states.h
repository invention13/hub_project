#ifndef _STATES_H
#define _STATES_H

#include "FreeRTOS.h"
#include "debug_gpio.h"

#include "events.h"
#include "PD_Port.h"

// Policy Engine States
/*
//Source Port
PE_SRC_Startup
PE_SRC_Discovery
PE_SRC_Send_Capabilities
PE_SRC_Negotiate_Capability
PE_SRC_Transition_Supply
PE_SRC_Ready
PE_SRC_Disabled
PE_SRC_Capability_Response
PE_SRC_Hard_Reset
PE_SRC_Hard_Reset_Received
PE_SRC_Transition_to_default
PE_SRC_Get_Sink_Cap
PE_SRC_Wait_New_Capabilities

//Sink Port
PE_SNK_Startup
PE_SNK_Discovery
PE_SNK_Wait_for_Capabilities
PE_SNK_Evaluate_Capability
PE_SNK_Select_Capability
PE_SNK_Transition_Sink
PE_SNK_Ready
PE_SNK_Hard_Reset
PE_SNK_Transition_to_default
PE_SNK_Give_Sink_Cap
PE_SNK_Get_Source_Cap

//Source Port Soft Reset
PE_SRC_Send_Soft_Reset
PE_SRC_Soft_Reset

//Sink Port Soft Reset
PE_SNK_Send_Soft_Reset
PE_SNK_Soft_Reset

//Source Port Ping
PE_SRC_Ping

//Type-A/B Dual-Role (initially Source Port) Ping
PE_PRS_SRC_SNK_Ping

//Type-A/B Dual-Role (initially Sink Port) Ping
PE_PRS_SNK_SRC_Ping

//Type-A/B Hard Reset of P/C in Sink Role
PE_PC_SNK_Hard_Reset
PE_PC_SNK_Swap_Recovery

Type-A/B Hard Reset of C/P in Source Role
PE_CP_SRC_Hard_Reset
PE_CP_SRC_Transition_to_off

//Type-A/B C/P Dead Battery/Power Loss
PE_DB_CP_Check_for_VBUS
PE_DB_CP_Power_VBUS_DB
PE_DB_CP_Wait_For_Bit_Stream
PE_DB_CP_Power_VBUS_5V
PE_DB_CP_Wait_Bit_Stream_Stop
PE_DB_CP_Unpower_VBUS
PE_DB_CP_PS_Discharge

//Type-A/B P/C Dead Battery/Power Loss
PE_DB_PC_Unpowered
PE_DB_PC_Check_Power
PE_DB_PC_Send_Bit_Stream
PE_DB_PC_Wait_to_Detect
PE_DB_PC_Wait_to_Start

//Type-C DFP to UFP Data Role Swap
PE_DRS_DFP_UFP_Evaluate_DR_Swap
PE_DRS_DFP_UFP_Accept_DR_Swap
PE_DRS_DFP_UFP_Change_to_UFP
PE_DRS_DFP_UFP_Send_DR_Swap
PE_DRS_DFP_UFP_Reject_DR_Swap

//Type-C UFP to DFP Data Role Swap
PE_DRS_UFP_DFP_Evaluate_DR_Swap
PE_DRS_UFP_DFP_Accept_DR_Swap
PE_DRS_UFP_DFP_Change_to_DFP
PE_DRS_UFP_DFP_Send_DR_Swap
PE_DRS_UFP_DFP_Reject_DR_Swap

//Source to Sink Power Role Swap
PE_PRS_SRC_SNK_Evaluate_PR_Swap
PE_PRS_SRC_SNK_Accept_PR_Swap
PE_PRS_SRC_SNK_Transition_to_off
PE_PRS_SRC_SNK_Assert_Rd
PE_PRS_SRC_SNK_Wait_Source_on
PE_PRS_SRC_SNK_Send_PR_Swap
PE_PRS_SRC_SNK_Reject_PR_Swap

//Sink to Source Power Role Swap
PE_PRS_SNK_SRC_Evaluate_PR_Swap
PE_PRS_SNK_SRC_Accept_PR_Swap
PE_PRS_SNK_SRC_Transition_to_off
PE_PRS_SNK_SRC_Assert_Rp
PE_PRS_SNK_SRC_Source_on
PE_PRS_SNK_SRC_Send_PR_Swap
PE_PRS_SNK_SRC_Reject_PR_Swap

//Dual-Role Source Port Get Source Capabilities
PE_DR_SRC_Get_Source_Cap

//Dual-Role Source Port Give Sink Capabilities
PE_DR_SRC_Give_Sink_Cap

//Dual-Role Sink Port Get Sink Capabilities
PE_DR_SNK_Get_Sink_Cap

//Dual-Role Sink Port Give Source Capabilities
PE_DR_SNK_Give_Source_Cap

//Type-C VCONN Swap
PE_VCS_Send_Swap
PE_VCS_Evaluate_Swap
PE_VCS_Accept_Swap
PE_VCS_Reject_Swap
PE_VCS_Wait_For_VCONN
PE_VCS_Turn_Off_VCONN
PE_VCS_Turn_On_VCONN
PE_VCS_Send_Ps_Rdy

//UFP Structured VDM
//UFP Structured VDM Discovery Identity
PE_UFP_VDM_Get_Identity
PE_UFP_VDM_Send_Identity
PE_UFP_VDM_Get_Identity_NAK

//UFP Structured VDM Discovery SVIDs
PE_UFP_VDM_Get_SVIDs
PE_UFP_VDM_Send_SVIDs
PE_UFP_VDM_Get_SVIDs_NAK

//UFP Structured VDM Discovery Modes
PE_UFP_VDM_Get_Modes
PE_UFP_VDM_Send_Modes
PE_UFP_VDM_Get_Modes_NAK

//UFP Structured VDM Enter Mode
PE_UFP_VDM_Evaluate_Mode_Entry
PE_UFP_VDM_Mode_Entry_ACK
PE_UFP_VDM_Mode_Entry_NAK

//UFP Structured VDM Exit Mode
PE_UFP_VDM_Mode_Exit
PE_UFP_VDM_Mode_Exit_ACK
PE_UFP_VDM_Mode_Exit_NAK

//UFP Structured VDM Attention
PE_UFP_VDM_Attention_Request

//DFP Structured VDM
DFP to UFP Structured VDM Discover Identity
PE_DFP_UFP_VDM_Identity_Request
PE_DFP_UFP_VDM_Identity_ACKed
PE_DFP_UFP_VDM_Identity_NAKed

//DFP to Cable Plug Structured VDM Discover Identity
PE_DFP_CBL_VDM_Identity_Request
PE_DFP_CBL_VDM_Identity_ACKed
PE_DFP_CBL_VDM_Identity_NAKed

//DFP Structured VDM Discover SVIDs
PE_DFP_VDM_SVIDs_Request
PE_DFP_VDM_SVIDs_ACKed
PE_DFP_VDM_SVIDs_NAKed

//DFP Structured VDM Discover Modes
PE_DFP_VDM_Modes_Request
PE_DFP_VDM_Modes_ACKed
PE_DFP_VDM_Modes_NAKed

//DFP Structured VDM Mode Entry
PE_DFP_VDM_Mode_Entry_Request
PE_DFP_VDM_Mode_Entry_ACKed
PE_DFP_VDM_Mode_Entry_NAKed

//DFP Structured VDM Mode Exit
PE_DFP_VDM_Mode_Exit_Request
PE_DFP_VDM_Mode_Exit_ACKed

//DFP Structured VDM Attention
PE_DFP_VDM_Attention_Request

//Cable Plug Related
//Cable Ready
PE_CBL_Ready

//Discover Identity
PE_CBL_Get_Identity
PE_CBL_Send_Identity
PE_CBL_Get_Identity_NAK

//Discover SVIDs
PE_CBL_Get_SVIDs
PE_CBL_Send_SVIDs
PE_CBL_Get_SVIDs_NAK

//Discover Modes
PE_CBL_Get_Modes
PE_CBL_Send_Modes
PE_CBL_Get_Modes_NAK

//Mode Entry
PE_CBL_Evaluate_Mode_Entry
PE_CBL_Mode_Entry_ACK
PE_CBL_Mode_Entry_NAK

//Mode Exit
PE_CBL_Mode_Exit
PE_CBL_Mode_Exit_ACK
PE_CBL_Mode_Exit_NAK

//Cable Soft Reset
PE_CBL_Soft_Reset

//Cable Hard Reset
PE_CBL_Hard_Reset

//DFP Soft Reset or Cable Reset
PE_DFP_CBL_Send_Soft_Reset
PE_DFP_CBL_Send_Cable_Reset

//UFP Source Soft Reset
PE_UFP_CBL_Send_Soft_Reset

//Source Startup Structured VDM Discover Identity
PE_SRC_VDM_Identity_Request
PE_SRC_VDM_Identity_ACKed
PE_SRC_VDM_Identity_NAKed

//BIST Receive Mode
PE_BIST_Receive_Mode
PE_BIST_Frame_Received

//BIST Transmit Mode
PE_BIST_Transmit_Mode
PE_BIST_Send_Frame

//BIST Carrier Mode and Eye Pattern
PE_BIST_Eye_Pattern_Mode
PE_BIST_Carrier_Mode_0
PE_BIST_Carrier_Mode_1
PE_BIST_Carrier_Mode_2
PE_BIST_Carrier_Mode_3

//Type-C referenced states
ErrorRecovery

*/

// Functions to initialize and update the state machine
void v_Update_State( PD_Port_t *p_Context, Event_t *event );
void v_Initialize_SM( PD_Port_t *p_Context );

// Port states
void *state_1( PD_Port_t *, Event_t *);
void *state_2( PD_Port_t *, Event_t *);
void *Initial_State( PD_Port_t *p_Context, Event_t *event );

#endif