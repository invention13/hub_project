// ********************************************************************************************************************
//
// COPYRIGHT 2017 AyDeeKay LLC - All Rights Reserved
// No part of this document can be used, copied, transmitted, or modified by
// any existing or to be invented means without express approval of AyDeeKay
// LLC.
//
// ********************************************************************************************************************
//
// File:
//   usbhub_descript.c
//
// Description:
//   Header file to support writing hub descriptors in verification environment
//
// ********************************************************************************************************************


#ifndef __USBHUB_DESCRIPT_H__
#define __USBHUB_DESCRIPT_H__

#include "superior.h"
#include "clough_dbg.h"
#include <stdint.h>

// hub descriptor record
typedef struct {
  uint16_t        HSLEN;    // length of HS record
  uint16_t        FSLEN;    // length of FS record
  const uint8_t*  HSDSCR;   // pointer to HS descriptor record
  const uint8_t*  FSDSCR;   // pointer to FS descriptor record
} USBHUB_DSCRREC_t;


// basic descriptor.  most tests should use this one.
extern const USBHUB_DSCRREC_t _USBHUB_DSCRREC_SHORT;

// long descriptor with some strings to interrogate.
extern const USBHUB_DSCRREC_t _USBHUB_DSCRREC_LONG;


// populate the referenced descriptor to memory
__STATIC_INLINE void USBHUB_DESCRIPT_writeDescriptor(
  const uint8_t whichOne ,   // which descriptor set to populate
                             //   0 => typical descriptor w/o strings
                             //   1 => typical descriptor w/  strings
  const uint8_t doFS         // whether to also populate FS descriptor
) {
  const USBHUB_DSCRREC_t *rec;

  // index the descriptor required.  add case for each descriptor used
  switch( whichOne ) {
    case 0:   rec = &(_USBHUB_DSCRREC_SHORT);  break;   // short form descriptor
    case 1:   rec = &(_USBHUB_DSCRREC_LONG );  break;   // long  form descriptor
    default:  dbgTrapErr();                    return;  // bad thing has happened!
  }

  // write the desciptor to ROM
  if( 1    ) USBHUB_descriptorMemcpy( rec->HSDSCR , rec->HSLEN , 0 );   // HS (currently always)
  if( doFS ) USBHUB_descriptorMemcpy( rec->FSDSCR , rec->FSLEN , 1 );   // FS (optional)
}

#endif // __USBHUB_DESCRIPT_H__
