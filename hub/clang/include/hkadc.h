#ifndef _HKADC_H
#define _HKADC_H

#define MAX_ADC_CHANNELS 32

typedef struct HKADC_Channel_Init_tag
{
  uint8_t u8_Dly_Mux_to_Sample;          
  uint16_t u16_Dly_From_Previous_Sample; 
  uint8_t u8_Adc_Mux;                    
  uint8_t u8_IRQ_Debounce_Interval;      
  uint8_t b_Irq2A_to_Inside_Armed;       
  uint8_t b_Irq_2_to_Outside_Armed;      
  uint8_t u8_ADC_ADC_Sample_Wait_Cycles;  
  uint16_t u16_High_Threshold;            
  uint16_t u16_Low_Threshold;     
  uint8_t b_Active;
} HKADC_Channel_Init_t;

enum
{
  ADC_CHAN_OPEN = 0,
  ADC_CHAN_UFP_DP,
  ADC_CHAN_UFP_DM,
  ADC_CHAN_DFP1_DP,
  ADC_CHAN_DFP1_DM,
  ADC_CHAN_DFP2_DP,
  ADC_CHAN_DFP2_DM,
  ADC_CHAN_DFP3_DP,
  ADC_CHAN_DFP3_DM,
  ADC_CHAN_VDDA5P0,
  ADC_CHAN_VPTAT,
  ADC_CHAN_TESTMUXP,
  ADC_CHAN_TESTMUXN,
  ADC_CHAN_CARD_POWER,
  ADC_CHAN_VDDDIG1P5,
  ADC_CHAN_VDDMCU1P8_EXT,
  ADC_CHAN_VDDFLS2P5_EXT,
  ADC_CHAN_VDDD3P3_DIG,
  ADC_CHAN_SWITCHED_BUS,
  ADC_CHAN_VBUS1_DIV_2,
  ADC_CHAN_VBUS2_DIV_2,
  ADC_CHAN_VBUS3_DIV_2,
  ADC_CHAN_GPIO1,
  ADC_CHAN_PRTCTL_1,
  ADC_CHAN_FAULT_N1,
  ADC_CHAN_FAULT_N2,
  ADC_CHAN_PRTCTL_2,
  ADC_CHAN_FAULT_N3,
  ADC_CHAN_PRTCTL_3,
  ADC_CHAN_PWM1,
  ADC_CHAN_UNUSED1,
  ADC_CHAN_UNUSED2
};
  

void v_HKADC_Init( void );
uint8_t b_HKADC_Channel_Setup( uint8_t u8_Channel, HKADC_Channel_Init_t *init );
void v_Enable_HKADC( uint8_t b_Enable );

#endif