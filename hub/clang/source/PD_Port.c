#include "superior.h"
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "queue.h"
#include <string.h>
#include "events.h"
#include "event_groups.h"
#include "PD_Port.h"
#include "device_manager.h"
#include "debug_gpio.h"

static void Timer_Callback( TimerHandle_t xTimer );
void v_Port_Policy_Engine( void *args );
const uint16_t u16_Port_Policy_Engine_Stack_Size = 128;


void v_Update_State( PD_Port_t *p_Context, Event_t *event );
void v_Initialize_SM( PD_Port_t *p_Context );
void *Initial_State( PD_Port_t *p_Context, Event_t *event );

static PD_Port_t *p_Port_List;
static uint8_t u8_Number_Ports;

static void v_Add_Port( PD_Port_t *p_Port );

void v_Device_Policy_Manager_Task( void * );

void v_Init_PD_Port( PD_Port_Init_t *p_Init, PD_Port_t *p_Port )
{
PD_Port_t *p_Current_Port;

    // Clear structure
    memset( p_Port, 0, sizeof(PD_Port_t));

   // Event group used for synchronization
   p_Port->Startup_Ready = xEventGroupCreate();
   
   if( p_Port->Startup_Ready == NULL )
   {
     // Error creating TODO: need error handler
   }
  
    p_Port->u8_Name = p_Init->u8_Name;
  
    // TODO: should make a HAL Init structure with API and pass struct to port init.
    // TODO: v_Init_HAL( void *p_HAL_Init ); pass typec base pointer in init struct
   
    p_Port->TypeC_Base = p_Init->TypeC_Base;
  
    /*                      Port Policy Engine                    */
    v_Init_Port_Policy_Engine( p_Port, p_Init );
    v_Init_Device_Manager( p_Port, p_Init );
    v_Init_Protocol( p_Port, p_Init );
    
     // Create timers - timer below is just an example
     p_Port->timer =  xTimerCreate
                    ( "test timer",
                       1000,  // blocking time
                       0,    // Don't reload - one shot
                       p_Port,  // reference back to port that owns it
                       Timer_Callback );
    
    // Add to list of ports to scan in tick interrupt
     v_Add_Port( p_Port );
     
 }

static void v_Add_Port( PD_Port_t *p_Port )
{
  PD_Port_t *p_Current_Port;
  
  taskDISABLE_INTERRUPTS();
    
    if( p_Port_List == (PD_Port_t *)NULL )
    {
        p_Port_List = p_Port;
        p_Port->p_Next_Port = (PD_Port_t *)NULL;
    }
    else
    {
        for( p_Current_Port = p_Port_List; 
             p_Current_Port->p_Next_Port != (PD_Port_t *)NULL; 
             p_Current_Port = p_Current_Port->p_Next_Port );

        p_Current_Port->p_Next_Port = p_Port;
        p_Port->p_Next_Port = (PD_Port_t *)NULL;
    }
    ++u8_Number_Ports;
    
    v_Initialize_SM( p_Port );
    
    taskENABLE_INTERRUPTS();
}
 
void Timer_Callback( TimerHandle_t xTimer )
{
  Event_t Event;
  Event.Event_ID = TIMEOUT_EVENT;
  Event.Event_Data.uval = 5;
  
   PD_Port_t *p_Calling_Port = (PD_Port_t *)pvTimerGetTimerID( xTimer );
   xQueueSend( p_Calling_Port->Port_Policy_Engine.Event_Queue, &Event, ( TickType_t ) 0 );
}

void v_Start_Timer( PD_Port_t *p_Port, TickType_t Timeout )
{
  xTimerChangePeriod( p_Port->timer, Timeout, 0 );
}

void v_Announce_Ready( PD_Port_t *p_Port, uint8_t u8_Bit )
{
   xEventGroupSetBits( p_Port->Startup_Ready, u8_Bit );                          
}

void v_Wait_Until_Ready( PD_Port_t *p_Port, uint8_t u8_Bits )
{
    xEventGroupWaitBits(
                       p_Port->Startup_Ready,
                       u8_Bits,
                       pdTRUE,
                       pdTRUE,
                       portMAX_DELAY );
}

 

// Called in interrupt context by systick handler
void v_Poll_Port( PD_Port_t *p_Port );
void v_Poll_Ports( void )
{
PD_Port_t *p_Port;

  // Go through list of currently active ports, get status and generate events
  for( p_Port = p_Port_List; p_Port != (PD_Port_t *)NULL; p_Port = p_Port->p_Next_Port )
  {
    v_Poll_Port( p_Port );
  }
}

void v_Poll_Port( PD_Port_t *p_Port )
{
  // NB - to sample these registers using 32 bit words takes ~22.5 us 
  // To sample as bytes takes ~20 us.


  
  TYPEC1_SFRS_t *TypeC_Reg = (TYPEC1_SFRS_t *)p_Port->TypeC_Base;
  
#if 0
  uint32_t u32_Status1 = TypeC_Reg->TYPECSTATUS1.WORD;
  uint32_t u32_Status2 = TypeC_Reg->TYPECSTATUS2.WORD;
  uint32_t u32_Status3 = TypeC_Reg->TYPECSTATUS3.WORD;
  uint32_t u32_Interrupt1 = TypeC_Reg->INTERRUPT1.WORD;
  uint32_t u32_Interrupt2 = TypeC_Reg->INTERRUPT2.WORD;
#endif

  // TODO: is it better to poll or to use interrupts?
  uint8_t u8_Status1 = *(volatile uint8_t *)&(TypeC_Reg->TYPECSTATUS1.WORD);
  uint8_t u8_Status2 = *(volatile uint8_t *)&(TypeC_Reg->TYPECSTATUS2.WORD);
  uint8_t u8_Status3 = *(volatile uint8_t *)&(TypeC_Reg->TYPECSTATUS3.WORD);
  uint8_t u8_Interrupt1 = *(volatile uint8_t *)&(TypeC_Reg->INTERRUPT1.WORD);
  uint8_t u8_Interrupt2 = *(volatile uint8_t *)&(TypeC_Reg->INTERRUPT2.WORD);

  // To clear interrupts
  *(volatile uint8_t *)&(TypeC_Reg->INTERRUPT1.WORD) = 0xff;
  *(volatile uint8_t *)&(TypeC_Reg->INTERRUPT2.WORD) = 0xff;
  
}