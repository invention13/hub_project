#include "superior.h"
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "queue.h"
#include <string.h>
#include "events.h"
#include "device_manager.h"
#include "PD_Port.h"
#include "debug_gpio.h"
#include "pdo.h"

void v_Update_DPM_State( void *p_Context, Event_t *event );

void v_Init_Device_Manager( void *p_Owning_Port, void *p_Init_Args )
{
    PD_Port_t *p_Context = (PD_Port_t *)p_Owning_Port;
    PD_Port_Init_t *p_Init = (PD_Port_Init_t *)p_Init_Args;
    
    p_Context->Device_Manager.Event_Queue = xQueueCreate( PD_EVENT_QUEUE_LENGTH, sizeof(Event_t) );
    
    // Abort if can't create event queue
    if( p_Context->Device_Manager.Event_Queue == NULL )
    {
    }
    
    xTaskCreate( v_Device_Policy_Manager_Task,
                "Device Policy Manager",
                DEVICE_POLICY_MANAGER_STACK_SIZE,
                p_Context,   // reference back to port that owns task
                tskIDLE_PRIORITY,
                &p_Context->DPM_Task );
}

// Multiple copies of this thread may exist - must be re-entrant
void v_Device_Policy_Manager_Task( void *args )
{
#pragma diag_suppress = Pe177
    PD_Port_t *self = (PD_Port_t *)args;
    Event_t Event;
    uint32_t u32_Msg_Cnt = 0;
    
    v_Announce_Ready( self, DEVICE_MANAGER_READY );
    v_Wait_Until_Ready( self, ALL_READY );

     while(1)
     {
       // Block indefinitely while waiting for events
       if( xQueueReceive( self->Device_Manager.Event_Queue, &Event, portMAX_DELAY ) )
       {
           // Check for DPM requests and other events that are not state specific
           v_Update_DPM_State( self, &Event );
           ++u32_Msg_Cnt;
       }
     }
 }

void v_Post_DPM_Event( void *p_Owning_Port, Event_t *p_Event )
{
    PD_Port_t *p_Port = (PD_Port_t *)p_Owning_Port;
    xQueueSend( p_Port->Device_Manager.Event_Queue, ( void * )p_Event, ( TickType_t ) 0 );
}

