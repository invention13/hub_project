#include "superior.h"
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "queue.h"
#include <string.h>
#include "events.h"
#include "PD_Port.h"
#include "debug_gpio.h"
#include "states.h"

void v_Update_PE_State( void *arg, Event_t *event )
{
        PD_Port_t *p_Context = (PD_Port_t *)arg;
	State_Handler_t Current_State = (State_Handler_t)p_Context->Port_Policy_Engine.p_PE_State;
	State_Handler_t Next_State;

	Next_State = (State_Handler_t)Current_State( p_Context, event );

	if( Next_State != Current_State )
	{
		Event_t Exit_Event;
		Exit_Event.Event_ID = EXIT;
		Exit_Event.Event_Data.pv_val = (void *)0;
		Current_State( p_Context, &Exit_Event );

		Event_t Entrance_Event;
		Entrance_Event.Event_ID = ENTRANCE;
		Entrance_Event.Event_Data.pv_val = (void *)0;

		Next_State( p_Context, &Entrance_Event );

		p_Context->Port_Policy_Engine.p_PE_State = (void*)Next_State;
	}

}

void v_Update_DPM_State( void *p_Owning_Port, Event_t *event )
{
        PD_Port_t *p_Context = (PD_Port_t *)p_Owning_Port;
	State_Handler_t Current_State = (State_Handler_t)(p_Context->Device_Manager.p_DPM_State);
	State_Handler_t Next_State;

	Next_State = (State_Handler_t)Current_State( p_Context, event );

	if( Next_State != Current_State )
	{
		Event_t Exit_Event;
		Exit_Event.Event_ID = EXIT;
		Exit_Event.Event_Data.pv_val = (void *)0;
		Current_State( p_Context, &Exit_Event );

		Event_t Entrance_Event;
		Entrance_Event.Event_ID = ENTRANCE;
		Entrance_Event.Event_Data.pv_val = (void *)0;

		Next_State( p_Context, &Entrance_Event );

		p_Context->Device_Manager.p_DPM_State = (void *)Next_State;
	}

}

void v_Update_Protocol_State( PD_Port_t *p_Context, Event_t *event )
{

	State_Handler_t Current_State = (State_Handler_t)(p_Context->Protocol.p_Protocol_State);
	State_Handler_t Next_State;

	Next_State = (State_Handler_t)Current_State( p_Context, event );

	if( Next_State != Current_State )
	{
		Event_t Exit_Event;
		Exit_Event.Event_ID = EXIT;
		Exit_Event.Event_Data.pv_val = (void *)0;
		Current_State( p_Context, &Exit_Event );

		Event_t Entrance_Event;
		Entrance_Event.Event_ID = ENTRANCE;
		Entrance_Event.Event_Data.pv_val = (void *)0;

		Next_State( p_Context, &Entrance_Event );

		p_Context->Protocol.p_Protocol_State = (void*)Next_State;
	}

}

void v_Initialize_SM( PD_Port_t *p_Context )
{
	Event_t Initial_Event;

	Initial_Event.Event_ID = ENTRANCE;
	Initial_Event.Event_Data.pv_val = (void *)0;
        
        // Port policy engine
        p_Context->Port_Policy_Engine.p_PE_State = (void *)Initial_State;
	State_Handler_t PE_State = (State_Handler_t)(p_Context->Port_Policy_Engine.p_PE_State);
	PE_State( p_Context, &Initial_Event );
        
        // Device policy manager
        p_Context->Device_Manager.p_DPM_State = (void *)Initial_State;
	State_Handler_t DPM_State = (State_Handler_t)(p_Context->Device_Manager.p_DPM_State);
	DPM_State( p_Context, &Initial_Event );
        
        // Protocol
        p_Context->Protocol.p_Protocol_State = (void*)Initial_State;
	State_Handler_t Protocol_State = (State_Handler_t)(p_Context->Protocol.p_Protocol_State);
	Protocol_State( p_Context, &Initial_Event );
}





