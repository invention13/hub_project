#include "superior.h"
#include "FreeRTOS.h"
#include "PD_Port.h"
#include "events.h"
#include "debug_gpio.h"

void v_Start_Timer( PD_Port_t *p_Port, TickType_t Timeout );

void v_Test_Task(void *args )
{
  static uint32_t test_cnt;
  
#pragma diag_suppress = Pe177
  PD_Port_t *p_Port = (PD_Port_t *)args;
  
  while(1)
  {
    vTaskDelay( 1000 );  
    //v_Start_Timer( p_Port, 100 );
    test_cnt++;
  }
}