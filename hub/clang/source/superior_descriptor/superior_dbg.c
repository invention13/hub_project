// ********************************************************************************************************************
// General testbench support:
//
// - console redirect
// - printf
// - __sys_exit override implementation
// ********************************************************************************************************************

#include <stdio.h>
#include <stdint.h>
#include <sys/stat.h>
#include <stdarg.h>
#include <string.h>
#include "superior.h"
#include "superior_dbg.h"

#define PAD_RIGHT   1<<0
#define PAD_ZERO    1<<1
//#define PRNT_CAPS   1<<2
#define DATA_NEG    1<<3
//#define DATA_32     1<<4
//#define DATA_8      1<<5
#define PRNT_HEX    1<<6
#define BUFLEN      32


// Routine to write a char - specific to Cortex-M0 Integration Kit
inline void clough_char_write( uint8_t ch )
{
  *(CLOUGH_PRINTF_ADDR) = ch;  // write a character to the virtual port
}


// write a character to the "console"
void dbg_putch(uint8_t ch)
{
  clough_char_write( ch );
}


// Check Value.  Returns 1 if values match.
int check_value(int value, int expected_value)
{
  if (value != expected_value) {
    return 0;
  }
  else {
    return 1;
  }
}


// PRINT FUNCTION  dbg_printf
void dbg_printf( const char *format, ... )
{

  static const char dig_char[16] = { '0' , '1' , '2' , '3' , '4' , '5' , '6' , '7' , '8' , '9' , 'A' , 'B' , 'C' , 'D' , 'E' , 'F' };
  static const uint32_t mant[10] = { 1 , 10 , 100 , 1000 , 10000 , 100000 , 1000000 , 10000000 , 100000000 , 1000000000 };

  va_list vargs;

  va_start( vargs , format );

  int8_t  wid;   // width declared requested in format string
  uint8_t flg;   // tracks various things in the format string parsing

  uint32_t reg_u;  // working register for unsigned math
  int32_t  reg_d;  // working register for signed math
  uint8_t  dig;

  uint8_t temps[BUFLEN];
  uint8_t *sptr;
  uint8_t scnt;

  while( *format != '\0' )
  {

    if( *format == '%' )
    {

      format++;
      wid = 0;
      flg = 0;

      if( *format == '\0' ) return;

      if( *format == '%' )
      {
        clough_char_write( *format );
        format++;
        continue;
      }

      if( *format == '-' )
      {
        flg = PAD_RIGHT;
        format++;
        if( *format == '\0' ) return;
      }

      while( *format == '0' )
      {
        flg |= PAD_ZERO;
        format++;
        if( *format == '\0' ) return;
      }

      while( (*format >= '0') && (*format <= '9') )
      {
        wid *= 10;
        wid += *format - '0';
        format++;
        if( *format == '\0' ) return;
      }

      if( *format == 'c' )
      {
        clough_char_write( (char) va_arg( vargs , uint32_t ) );
        format++;
        continue;
      }

      if( *format == 'l' )
      {
        format++;
        if( *format == '\0' ) return;
      }
      else if( *format == 'h' )
      {
        format++;
        if( *format == '\0' ) return;
      }

      // everything is integers from here on...

      if( (*format == 'd') || (*format == 'i') )
      {
        reg_d = va_arg( vargs , int32_t );
        format++;

        if( reg_d < 0 )
        {
          reg_d = -reg_d;
          wid--;

          if( flg & PAD_ZERO )
            clough_char_write( '-' );
          else
            flg |= DATA_NEG;
        }
        reg_u = reg_d;
      }
      else if( (*format == 'x') || (*format == 'X') )
      {
        flg |= PRNT_HEX;
        reg_u = va_arg( vargs , uint32_t );
        format++;
      }
      else if( *format == 'u' )
      {
        reg_u = va_arg( vargs , uint32_t );
        format++;
      }

      // convert unsigned number into string
      sptr = temps;
      scnt = 0;
      if( reg_u == 0 )
      {
        *sptr = '0';
        sptr++;
        scnt++;
      }
      else
      {
        if( flg & PRNT_HEX )
        {
          while( reg_u != 0 )
          {
            dig = reg_u & 0xF;
            reg_u = reg_u >> 4;
            *sptr = dig_char[dig];
            sptr++;
            scnt++;
          }
        }
        else
        {
          int8_t   loopa;
          uint32_t tempw1,
                   tempw2;

          if( reg_u == 0 )
          {
            *sptr = '0';
            scnt = 1;
          }
          else
          {
            // figure out how many digits:
            for( loopa=9; reg_u < mant[loopa]; loopa-- ) {}

            scnt = loopa;
            sptr = temps + scnt;

            // 1e9 position
            if( loopa == 9 )
            {
              *sptr = 0;
              while( reg_u >= mant[loopa] )
              {
                reg_u = reg_u - mant[loopa];
                (*sptr)++;
              }
              sptr[loopa] = dig_char[ sptr[loopa] ];
              loopa--;
              sptr--;
            }

            // 1e8 .. 1 positions
            while( loopa >= 0 )
            {

              tempw2 = mant[loopa] << 3;
              if( reg_u >= tempw2 )
              {
                tempw1 = tempw2;
                *sptr  = 8;
              }
              else
              {
                tempw1 = 0;
                *sptr  = 0;
              }

              tempw2 = tempw1 + (mant[loopa] << 2);
              if( reg_u >= tempw2 )
              {
                tempw1 =  tempw2;
                *sptr  += 4;
              }

              tempw2 = tempw1 + (mant[loopa] << 1);
              if( reg_u >= tempw2 )
              {
                tempw1 =  tempw2;
                *sptr  += 2;
              }


              tempw2 = tempw1 + mant[loopa];
              if( reg_u >= tempw2 )
              {
                tempw1 = tempw2;
                (*sptr)++;
              }

              reg_u -= tempw1;
              *sptr = dig_char[ *sptr ];

              loopa--;
              sptr--;
            }

            scnt++;
            sptr = temps + scnt;
          }
        }
      }

      // print pre-pad
      wid -= scnt;
      if( !(flg & PAD_RIGHT) )
      {

        if( flg & PAD_ZERO )
        {
          while( wid > 0 )
          {
            clough_char_write( '0' );
            wid--;
          }
        }
        else
        {
          while( wid > 0 )
          {
            clough_char_write( ' ' );
            wid--;
          }

          if( flg & DATA_NEG )
          {
            clough_char_write( '-' );
          }
        }
      }

      // print number
      while( scnt > 0 )
      {
        sptr--;
        clough_char_write( *sptr );
        scnt--;
      }

      // print post-pad
      if( flg & PAD_RIGHT )
      {
        while( wid > 0 )
        {
          clough_char_write( ' ' );
          wid--;
        }
      }
    }  // if( *format == '%' )
    else
    {
      clough_char_write( *format );
      format++;
      continue;
    }
  } // while( *format != '\0' )

  return; // nchar;
}


// ********************************************************************************************************************
// _sys_exit overide implementation.  this runs after main exits.
// ********************************************************************************************************************

void _sys_exit( int return_code ) {
  dbg_printf( "***** _sys_exit() *****\n" );
  if( return_code ) sim_fail(); else sim_pass();
  while(1) {}
}
