// ********************************************************************************************************************
//
// COPYRIGHT 2017 AyDeeKay LLC - All Rights Reserved
// No part of this document can be used, copied, transmitted, or modified by
// any existing or to be invented means without express approval of AyDeeKay
// LLC.
//
// ********************************************************************************************************************
//
// File:
//   usb_tests.h
//
// Description:
//   Header file to declare variables and functions used in USB related tests
//
// ********************************************************************************************************************

#ifndef __USB_TESTS_H__
#define __USB_TESTS_H__

#include <stdint.h>
#include "superior.h"
#include "clough_dbg.h"


// set enable for all AFEs (including virtual AFEs)
static __INLINE void setUsbAfeEnables(
  uint8_t bmEnable,  // bitmap for AFEs to enable/disable:
                     //   [0]    ==> upstream
                     //   [1..3] ==> downstream
                     //   [4..5] ==> virtual AFEs (internal bridges)
  uint8_t bmSwap     // bitmap for AFEs to swap pins.  same mapping as bmEnable
) {
  // physical AFEs
  for( uint8_t loopi=0;  loopi<=3;  loopi++ ) {
    if( bmEnable & 0x01 ) {
      USBAFE_enableAFE(  loopi , (bmSwap & 0x01) );
    } else {
      USBAFE_disableAFE( loopi );
    }
    bmEnable = bmEnable >> 1;
    bmSwap   = bmSwap   >> 1;
  }

  // virtual AFEs (swap is irrelevant)
  USBHUB_BRDSCN_t tempd;
  tempd.BYTE = 0;
  tempd.APPF = (bmEnable & 0x01) ? 0 : 1;
  bmEnable = bmEnable >> 1;
  tempd.H2HH = (bmEnable & 0x01) ? 0 : 1;
  USBHUB->BRDSCN.BYTE = tempd.BYTE;
}

// set AFEs to loopback test config
static __INLINE void setUsbAfeLoopback(
  uint8_t bmEnable   // bitmap for AFEs to enable/disable:
                     //   [0]    ==> upstream
                     //   [1..3] ==> downstream
) {
  USBAFE_UP->CDRTRIM.CDRTXMSKBYP  = (bmEnable & (1<<0)) ? 1 : 0;
  USBAFE_DN1->CDRTRIM.CDRTXMSKBYP = (bmEnable & (1<<1)) ? 1 : 0;
  USBAFE_DN2->CDRTRIM.CDRTXMSKBYP = (bmEnable & (1<<2)) ? 1 : 0;
  USBAFE_DN3->CDRTRIM.CDRTXMSKBYP = (bmEnable & (1<<3)) ? 1 : 0;
}

// a generalized function to setup the PPC OC Mode for the downstream ports
static __INLINE void PPC_cfgDfpOcModeCtrl(
  uint8_t portNum,    
  uint8_t OcMode,
  uint8_t OcFwSfr      
) {

  OCCTRL_t tempd;
  tempd.BYTE = 0;
  tempd.MODE = OcMode;
  tempd.OCFWSFR = OcFwSfr;

  // DFP1
  if( portNum == 0x1 ) {
      PPC_SFRS->DFP1OCCTRL.BYTE = tempd.BYTE;
  }

  // DFP2
  if( portNum == 0x2 ) {
      PPC_SFRS->DFP2OCCTRL.BYTE = tempd.BYTE;
  }

  // DFP3
  if( portNum == 0x3 ) {
      PPC_SFRS->DFP3OCCTRL.BYTE = tempd.BYTE;
  }
}

// a generalized function to setup the PPC OC Mode for the downstream ports
static __INLINE void PPC_cfgDfpDrvVBUSModeCtrl(
  uint8_t portNum,    
  uint8_t DrvVBUSMode,
  uint8_t VBUSReq      
) {

  DRVVBUSCTRL_t tempd;
  tempd.BYTE = 0;
  tempd.MODE = DrvVBUSMode;
  tempd.VBUSFWREQ = VBUSReq;

  // DFP1
  if( portNum == 0x1 ) {
      PPC_SFRS->DFP1DRVVBUSCTRL.BYTE = tempd.BYTE;
  }

  // DFP2
  if( portNum == 0x2 ) {
      PPC_SFRS->DFP2DRVVBUSCTRL.BYTE = tempd.BYTE;
  }

  // DFP3
  if( portNum == 0x3 ) {
      PPC_SFRS->DFP3DRVVBUSCTRL.BYTE = tempd.BYTE;
  }
}



// a somewhat generalized function to setup the PRTCTL pins for the downstream ports to a particular
// mode, assuming the starting point is power up defaults.
static __INLINE void USBHUB_cfgDfpPrtCtlPins(
  uint8_t bmEna,     // bitmap for enables.  USB numbering used, so bit [1] is for DFP1, etc.
  uint8_t bmRd,   // bitmap for read enables.  USB numbering used, so bit [1] is for DFP1, etc.
  uint8_t bmSel      // sel bitmap.
) {

  // note that if the SFR arrangement had been in an array, this could be a loop.

  // DFP1
  if( bmEna & (1<<1) ) {
    // DFP prtctl mode
    IOCTRL_SFRS->PRTCTL1CTRL.PRTCTL1MODE = 1;  // prtctl mode
    if( bmRd & (1<<1) ) IOCTRL_SFRS->PRTCTL1CTRL.PRTCTL1RE = 1;  // read enable
    if( bmSel & (1<<1) ) IOCTRL_SFRS->PRTCTL1CTRL.PRTCTL1SEL = 1; // select
  } else {
    IOCTRL_SFRS->PRTCTL1CTRL.PRTCTL1MODE = 0;  // gpio mode
  }

  // DFP2
  if( bmEna & (1<<2) ) {
    // DFP prtctl mode
    IOCTRL_SFRS->PRTCTL2CTRL.PRTCTL2MODE = 1; // prtctl mode
    if( bmRd & (1<<2) ) IOCTRL_SFRS->PRTCTL2CTRL.PRTCTL2RE = 1;  // read enable
    if( bmSel & (1<<2) ) IOCTRL_SFRS->PRTCTL2CTRL.PRTCTL2SEL = 1; // select
  } else {
    IOCTRL_SFRS->PRTCTL1CTRL.PRTCTL1MODE = 0;  // gpio mode
  }

  // DFP3
  if( bmEna & (1<<3) ) {
    // DFP prtctl mode
    IOCTRL_SFRS->GPIO2CTRL.GP2MODE = 1; // prtctl mode
    if( bmRd & (1<<3) ) IOCTRL_SFRS->GPIO2CTRL.GP2RE = 1; // read enable
    if( bmSel & (1<<3) ) IOCTRL_SFRS->GPIO2CTRL.PRTCTL3SEL = 1; // select
  } else {
    IOCTRL_SFRS->GPIO2CTRL.GP2MODE = 0; // gpio mode
  }
}

// a somewhat generalized function to setup the FAULT pins for the downstream ports to a particular
// mode, assuming the starting point is power up defaults.
// TODO:  is this code of sufficiently high quality to move to customer code?
// TODO:  whould we supply a single piece of code to configure both this and ALSO the ppc together?
static __INLINE void USBHUB_cfgDfpFaultPins(
  uint8_t bmEna,     // bitmap for enables.  USB numbering used, so bit [1] is for DFP1, etc.
  uint8_t bmRd,      // bitmap for read enables.  USB numbering used, so bit [1] is for DFP1, etc.
  uint8_t bmPol      // polarity bitmap.  each nonzero bit uses alternate signaling polarity.
) {

  // note that if the SFR arrangement had been in an array, this could be a loop.

  // DFP1
  if( bmEna & (1<<1) ) {
    // DFP fault mode
    IOCTRL_SFRS->FAULTN1CTRL.FAULTN1MODE = 1;  // fault mode
    if( bmRd & (1<<1) ) IOCTRL_SFRS->FAULTN1CTRL.FAULTN1RE   = 1;  // read enable
    if( bmPol & (1<<1) ) IOCTRL_SFRS->FAULTN1CTRL.FAULTN1DINPOLARITY = 1;
  } else {
    IOCTRL_SFRS->FAULTN1CTRL.FAULTN1MODE = 0;  // gpio mode
    if( bmRd & (1<<1) ) IOCTRL_SFRS->FAULTN1CTRL.FAULTN1RE   = 1;  // read enable
  }

  // DFP2
  if( bmEna & (1<<2) ) {
    // DFP fault mode
    IOCTRL_SFRS->FAULTN2CTRL.FAULTN2MODE = 1;  // note the change in the field name as well.  grr.
    if( bmRd & (1<<2) ) IOCTRL_SFRS->FAULTN2CTRL.FAULTN2RE   = 1;
    if( bmPol & (1<<2) ) IOCTRL_SFRS->FAULTN2CTRL.FAULTN2DINPOLARITY = 1;  // if there had been a typedef, could have
                                                                           //   made this just a write
  } else {
    IOCTRL_SFRS->FAULTN2CTRL.FAULTN2MODE = 0;  // gpio mode
    if( bmRd & (1<<2) ) IOCTRL_SFRS->FAULTN2CTRL.FAULTN2RE   = 1;
    // GPIO mode (not possible?)
  }

  // DFP3
  if( bmEna & (1<<3) ) {
    // DFP fault mode
    IOCTRL_SFRS->GPIO3CTRL.GP3MODE = 1;  // fault mode.  we seem to have no human readable enum.
    if( bmRd & (1<<3) ) IOCTRL_SFRS->GPIO3CTRL.GP3RE   = 1;  // if this had been a typedef, could have been single write.
                                         // furthermore, one could argue that setting fault mode would imply read enable...
    if( bmPol & (1<<3) ) IOCTRL_SFRS->GPIO3CTRL.GP3DINPOLARITY = 1;  // TODO:  is this correct?  why do we have this at all since teh hub also provides this signal.
  } else {
    // GPIO mode (GPIO 3)
    IOCTRL_SFRS->GPIO3CTRL.GP3MODE = 0;  // if this had been a typedef, could have been single write
    if( bmRd & (1<<3) ) IOCTRL_SFRS->GPIO3CTRL.GP3RE   = 1;  // if this had been a typedef, could have been single write.
  }
}

// enable fast resetting, a test mode only feature
static __INLINE void USBHUB_enablefastReset() {
  USBHUB->H2HDCTRL.FASTRST = 1;
}


// set all AFE/PHY ports to default trimming
static  __INLINE void setDefaultAfeTrims() {
  USBAFE_setCDRTrims(0);
  USBAFE_setCDRTrims(1);
  USBAFE_setCDRTrims(2);
  USBAFE_setCDRTrims(3);
}

#endif // include guard
