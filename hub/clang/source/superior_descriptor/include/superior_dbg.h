// ********************************************************************************************************************
// header file for general testbench support
//
// this file is *NOT* a place to put your test-specific crap in.  put code in here which is generally useful for
// testbenches.
// ********************************************************************************************************************

#ifndef __SUPERIOR_DBG_H__
#define __SUPERIOR_DBG_H__

#include "superior.h"
#include <stdint.h>

// address to send console data to
#define CLOUGH_PRINTF_ADDR  ((volatile uint8_t  *) (ASIC_16B_BASE + 0xFFFF))

// address to send testbench control data to
#define TBENCH_CTRL_ADDR ((volatile uint32_t  *) (ASIC_16B_BASE + 0xFFF8))
#define SIM_PASS   0x50415353
#define SIM_FAIL   0x4641494C
#define PRINT_TIME 0x54494D45
#define PLL_EN     0x2719004B

// for debugging purpose
#define DBG_WORD ((volatile uint32_t  *) (ASIC_16B_BASE + 0xFFF0))

// "console" printing functions
void dbg_putch( uint8_t ch );
void dbg_printf( const char *format, ... );

//void check_hex_value(const char *name, int value, int expected_value, const char *comment);
//void check_dec_value(const char *name, int value, int expected_value, const char *comment);

//void check_hex_value_noprintf(const char *name, int value, int expected_value, const char *comment);
//void check_dec_value_noprintf(const char *name, int value, int expected_value, const char *comment);

int check_value(int value, int expected_value);

// macros to access the testbench control functions
inline static void print_sim_time( void )
{
  *TBENCH_CTRL_ADDR = PRINT_TIME;
}

inline static void sim_pass( void )
{
  *TBENCH_CTRL_ADDR = SIM_PASS;
}

inline static void sim_fail( void )
{
  *TBENCH_CTRL_ADDR = SIM_FAIL;
}

inline static void sim_wrbus( uint32_t *addr, uint32_t data )
{
  *addr = data;
}

#endif // __SUPERIOR_DBG_H__
