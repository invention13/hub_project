// ********************************************************************************************************************
//
// COPYRIGHT 2017 AyDeeKay LLC - All Rights Reserved
// No part of this document can be used, copied, transmitted, or modified by
// any existing or to be invented means without express approval of AyDeeKay
// LLC.
//
// ********************************************************************************************************************
//
// File:
//   usbhub_descript.c
//
// Description:
//   Descriptor consts for verification environment.
//
// ********************************************************************************************************************

#include "usbhub_descript.h"

// ****************************************************************************
// USB defined convenience defines
// ****************************************************************************

// bDescriptorType
#define USB_DESCRTYPE_DEVICE    ((uint8_t) 0x01)
#define USB_DESCRTYPE_CONFIG    ((uint8_t) 0x02)
#define USB_DESCRTYPE_STRING    ((uint8_t) 0x03)
#define USB_DESCRTYPE_INTFX     ((uint8_t) 0x04)
#define USB_DESCRTYPE_ENDPT     ((uint8_t) 0x05)
#define USB_DESCRTYPE_DEVQLF    ((uint8_t) 0x06)
#define USB_DESCRTYPE_OSCFG     ((uint8_t) 0x07)
#define USB_DESCRTYPE_BOS       ((uint8_t) 0x0F)
#define USB_DESCRTYPE_DEVCAP    ((uint8_t) 0x10)
#define USB_DESCRTYPE_HUB       ((uint8_t) 0x29)

// device class:  bDeviceClass, bInterfaceClass
#define USB_CLASSCODE_HUB       ((uint8_t) 0x09)

// bDeviceProtocol
#define USB_DEVPROT_HUB_FSLS    ((uint8_t) 0x00)
#define USB_DEVPROT_HUB_HW1TT   ((uint8_t) 0x00)   /* only single TT possible                                        */
#define USB_DEVPROT_HUB_SNGLTT  ((uint8_t) 0x01)   /* single TT.  in interface descriptor, implies multi-TT possible */
#define USB_DEVPROT_HUB_MULTTT  ((uint8_t) 0x02)   /* multi TT                                                       */

// hub properties
#define USB_HUBCFG_ATTR_BKGRND  ((uint8_t) (1<<7))   /* field background   */
#define USB_HUBCFG_ATTR_SELFPOW ((uint8_t) (1<<6))   /* self-powered flag  */
#define USB_HUBCFG_ATTR_RWAKE   ((uint8_t) (1<<5))   /* remote wakeup flag */

// device capability type
#define USB_DEVCAPTYPE_USB2EXT  ((uint8_t) 0x02)

// endpoint helpers
#define USB_EPADDR_IN           ((uint8_t) (1<<7))
#define USB_EPADDR_OUT          ((uint8_t) (0<<7))
#define USB_EPADDR_IN1          (USB_EPADDR_IN | ((uint8_t) 0x01))
#define USB_EPTYPE_INT          ((uint8_t) 0x03)

// hub characteristics
#define USB_HUBCHAR_PWR_GANGED  ((uint8_t) (0x00<<0))
#define USB_HUBCHAR_PWR_INDIV   ((uint8_t) (0x01<<0))
#define USB_HUBCHAR_PWR_NONE    ((uint8_t) (0x02<<0))
#define USB_HUBCHAR_COMPOUND    ((uint8_t) (1<<2))
#define USB_HUBCHAR_OC_GLOBAL   ((uint8_t) (0x00<<3))
#define USB_HUBCHAR_OC_INDIV    ((uint8_t) (0x01<<3))
#define USB_HUBCHAR_OC_NONE     ((uint8_t) (0x02<<3))
#define USB_HUBCHAR_TTD_8       ((uint8_t) (0x00<<5))
#define USB_HUBCHAR_TTD_16      ((uint8_t) (0x01<<5))
#define USB_HUBCHAR_TTD_24      ((uint8_t) (0x02<<5))
#define USB_HUBCHAR_TTD_32      ((uint8_t) (0x03<<5))
#define USB_HUBCHAR_PORTIND     ((uint8_t) (1<<7))


// ****************************************************************************
// Our device properties convenience defines
// ****************************************************************************

// bcdUSB to advertise
#define BCDUSB   ((uint16_t) 0x0201)

// fake idVendor, idProduct, bcdDevice
#define IDVENDOR ((uint16_t) 0x34E2)
#define IDPROD   ((uint16_t) 0x6560)
#define BCDDEV   ((uint16_t) 0x0100)

// hub config bmAttributes
#define HUB_CFG_ATTR ((uint8_t) USB_HUBCFG_ATTR_BKGRND | USB_HUBCFG_ATTR_SELFPOW | USB_HUBCFG_ATTR_RWAKE)

// hub max power (in 2mA steps)
#define HUB_CFG_IMAX   ((uint8_t) (100/2))   /* 100mA */

// hub controller current, in mA
#define HUB_CHAR_ICTRL ((uint8_t) 2)         /* 2 mA */

// hub characteristics
#define HUB_CHAR (USB_HUBCHAR_PWR_INDIV | USB_HUBCHAR_COMPOUND | USB_HUBCHAR_OC_INDIV)

// hub power on delay (in 2ms steps)
#define HUB_VBUS_WAIT ((uint8_t) (100/2))  /* 100ms */

// (non) removeable hub ports (port 0 is hub controller and must not be set)
#define HUB_INTL_PORTS ((uint8_t) ((1<<4) | (1<<5)))

// string indices
#define IMANUFACTURER ((uint8_t) 1)
#define IPRODUCT      ((uint8_t) 2)
#define ISERIALNUMBER ((uint8_t) 3)


// ####################################################################################################################
// ## Short form descriptor (no strings)
// ####################################################################################################################

// high speed
static const uint8_t DESCRIPT_SHORT_HS[] = {

  // ******************************************************************************************************************
  // Device Descriptor
  // ******************************************************************************************************************

  18                    , // bLength            = 18 bytes
  USB_DESCRTYPE_DEVICE  , // bDescrptorType     = DEVICE
  (BCDUSB & 0x00FF)     , // bcdUSB
  (BCDUSB >> 8)         , // ...
  USB_CLASSCODE_HUB     , // bDeviceClass       = HUB
  0                     , // bDeviceSubClass    = 0
  USB_DEVPROT_HUB_SNGLTT, // bDeviceProtocol    = SINGLE_TT
  64                    , // bMaxPacketSize0    = 64
  (IDVENDOR   & 0x00FF) , // idVendor
  (IDVENDOR   >> 8)     , // ...
  (IDPROD     & 0x00FF) , // idProduct
  (IDPROD     >> 8)     , // ...
  (BCDDEV     & 0x00FF) , // bcdDevice
  (BCDDEV     >> 8)     , // ...
  0                     , // iManufacturer      = <NO STRING>
  0                     , // iProduct           = <NO STRING>
  0                     , // iserialNumber      = <NO STRING>
  1                     , // bNumConfigurations = 1

  // ******************************************************************************************************************
  // Device Qualifier
  // ******************************************************************************************************************

  10                    , // bLength            = 10 bytes
  USB_DESCRTYPE_DEVQLF  , // bDescriptorType    = QUALIFIER
  (BCDUSB & 0x00FF)     , // bcdUSB
  (BCDUSB >> 8)         , // ...
  USB_CLASSCODE_HUB     , // bDeviceClass       = HUB
  0                     , // bDeviceSubClass    = 0
  USB_DEVPROT_HUB_FSLS  , // bDeviceProtocol    = FS/LS
  64                    , // bMaxPacketSize0    = 64
  1                     , // bNumConfigurations = 1
  0x00                  , // bReserved          = 0

  // ******************************************************************************************************************
  // Configuration Descriptor
  // ******************************************************************************************************************

  9                     , // bLength             = 9 bytes
  USB_DESCRTYPE_CONFIG  , // bDescriptorType     = CONFIGURATION
  25 , 0                , // wTotalLength        = 25 bytes
  1                     , // bNumInterfaces      = 1
  1                     , // bConfigurationValue = 1
  0                     , // iConfiguration      = <NO STRING>
  HUB_CFG_ATTR          , // bmAttributes
  HUB_CFG_IMAX          , // bMaxPower

  // **************************************************************************
  // Interface 0
  // **************************************************************************

  9                     , // bLength             = 9 bytes
  USB_DESCRTYPE_INTFX   , // bDescriptorType     = INTERFACE
  0                     , // bInterfaceNumber    = 0
  0                     , // bAlternateSetting   = 0
  1                     , // bNumEndpoints       = 1
  USB_CLASSCODE_HUB     , // bInterfaceClass     = HUB
  0                     , // bInterfaceSubClass  = 0
  USB_DEVPROT_HUB_HW1TT , // bInterfaceProtocol  = HARDWARE ONLY HAS 1 TT
  0                     , // iInterface          = <NO STRING>

  // ******************************************************
  // Endpoint IN1
  // ******************************************************

  7                     , // bLength             = 7 bytes
  USB_DESCRTYPE_ENDPT   , // bDescriptorType     = ENDPOINT
  USB_EPADDR_IN1        , // bEndpointAddress    = IN.1
  USB_EPTYPE_INT        , // bmAttributes        = { INTERRUPT }
  1, 0                  , // wMaxPacketSize      = 1 byte
  0xFF                  , // bInterval           = "max interval" (USB spec seems to forbid this value in 9.6.6, but
                          //                                       require it in 11.23.1?)

  // ******************************************************************************************************************
  // Other Speed Descriptor
  // ******************************************************************************************************************

  9                     , // bLength             = 9 bytes
  USB_DESCRTYPE_OSCFG   , // bDescriptorType     = OSCONFIGURATION
  25 , 0                , // wTotalLength        = 25 bytes
  1                     , // bNumInterfaces      = 1
  1                     , // bConfigurationValue = 1
  0                     , // iConfiguration      = <NO STRING>
  HUB_CFG_ATTR          , // bmAttributes
  HUB_CFG_IMAX          , // bMaxPower

  // **************************************************************************
  // Interface 0
  // **************************************************************************

  9                     , // bLength             = 9 bytes
  USB_DESCRTYPE_INTFX   , // bDescriptorType     = INTERFACE
  0                     , // bInterfaceNumber    = 0
  0                     , // bAlternateSetting   = 0
  1                     , // bNumEndpoints       = 1
  USB_CLASSCODE_HUB     , // bInterfaceClass     = HUB
  0                     , // bInterfaceSubClass  = 0
  USB_DEVPROT_HUB_FSLS  , // bInterfaceProtocol  = FS/LS
  0                     , // iInterface          = <NO STRING>

  // ******************************************************
  // Endpoint IN1
  // ******************************************************

  7                     , // bLength             = 7 bytes
  USB_DESCRTYPE_ENDPT   , // bDescriptorType     = ENDPOINT
  USB_EPADDR_IN1        , // bEndpointAddress    = IN.1
  USB_EPTYPE_INT        , // bmAttributes        = { INTERRUPT }
  1 , 0                 , // wMaxPacketSize      = 1
  0x5                   , // bInterval           = 12 ms

  // ******************************************************************************************************************
  // Binary Object Store descriptor
  // ******************************************************************************************************************

  5                     , // bLength             = 5 bytes
  USB_DESCRTYPE_BOS     , // bDescriptorType     = BOS
  12 , 0                , // wTotalLength        = 12 bytes
  1                     , // bNumDeviceCaps      = 1

  // **************************************************************************
  // Device capability descriptor
  // **************************************************************************

  7                     , // bLength             = 7 bytes
  USB_DESCRTYPE_DEVCAP  , // bDescriptorType     = DEVICE_CAPABILITY
  USB_DEVCAPTYPE_USB2EXT, // bDevCapabilityType  = USB 2p0 Extension
  0x00 , 0 , 0 , 0      , // bmAttributes        = {LPM}
  
  
  // ******************************************************************************************************************
  // Hub descriptor
  // ******************************************************************************************************************

  9                     , // bLength             = 9 bytes
  USB_DESCRTYPE_HUB     , // bDescriptorType     = HUB_DESCRIPTOR
  5                     , // bNbrPorts           = 5
  HUB_CHAR , 0          , // wHubCharacteristics
  HUB_VBUS_WAIT         , // bPwrOn2PwrGood
  HUB_CHAR_ICTRL        , // bHubContrCurrent
  HUB_INTL_PORTS        , // DeviceRemovable
  0xFF                    // PortPwrCtrlMask
    

  // ******************************************************************************************************************
  // String Descriptor
  // ******************************************************************************************************************

  // Not implemented in order to reduce the lines of firmware execution

};

// full speed
static const uint8_t DESCRIPT_SHORT_FS[] = {

  // ******************************************************************************************************************
  // Device Descriptor
  // ******************************************************************************************************************

  18                    , // bLength             = 18 bytes
  USB_DESCRTYPE_DEVICE  , // bDescriptorType     = DEVICE
  (BCDUSB & 0x00FF)     , // bcdUSB
  (BCDUSB >> 8)         , // ...
  USB_CLASSCODE_HUB     , // bDeviceClass        = HUB
  0                     , // bDeviceSubClass     = 0
  USB_DEVPROT_HUB_FSLS  , // bDeviceProtocol     = FS/LS
  64                    , // bMaxPacketSize0     = 64
  (IDVENDOR   & 0x00FF) , // idVendor
  (IDVENDOR   >> 8)     , // ...
  (IDPROD     & 0x00FF) , // idProduct
  (IDPROD     >> 8)     , // ...
  (BCDDEV     & 0x00FF) , // bcdDevice
  (BCDDEV     >> 8)     , // ...
  0                     , // iManufacturer      = <NO STRING>
  0                     , // iProduct           = <NO STRING>
  0                     , // iserialNumber      = <NO STRING>
  1                     , // bNumConfigurations = 1

  // ******************************************************************************************************************
  // Device Qualifier
  // ******************************************************************************************************************

  10                    , // bLength            = 10 bytes
  USB_DESCRTYPE_DEVQLF  , // bDescriptorType    = QUALIFIER
  (BCDUSB & 0x00FF)     , // bcdUSB
  (BCDUSB >> 8)         , // ...
  USB_CLASSCODE_HUB     , // bDeviceClass       = HUB
  0                     , // bDeviceSubClass    = 0
  USB_DEVPROT_HUB_SNGLTT, // bDeviceProtocol    = SINGLE_TT
  64                    , // bMaxPacketSize0    = 64
  1                     , // bNumConfigurations = 1
  0                     , // bReserved          = 0

  // ******************************************************************************************************************
  // Configuration Descriptor
  // ******************************************************************************************************************

  9                     , // bLength             = 9 bytes
  USB_DESCRTYPE_CONFIG  , // bDescriptorType     = CONFIGURATION
  25 , 0                , // wTotalLength        = 25 bytes
  1                     , // bNumInterfaces      = 1
  1                     , // bConfigurationValue = 1
  0                     , // iConfiguration      = <NO STRING>
  HUB_CFG_ATTR          , // bmAttributes
  HUB_CFG_IMAX          , // bMaxPower

  // **************************************************************************
  // Interface 0
  // **************************************************************************

  9                     , // bLength             = 9 bytes
  USB_DESCRTYPE_INTFX   , // bDescriptorType     = INTERFACE
  0                     , // bInterfaceNumber    = 0
  0                     , // bAlternateSetting   = 0
  1                     , // bNumEndpoints       = 1
  USB_CLASSCODE_HUB     , // bInterfaceClass     = HUB
  0                     , // bInterfaceSubClass  = 0
  USB_DEVPROT_HUB_FSLS  , // bInterfaceProtocol  = FS/LS
  0                     , // iInterface          = <NO STRING>

  // ******************************************************
  // Endpoint IN1
  // ******************************************************

  7                     , // bLength             = 7 bytes
  USB_DESCRTYPE_ENDPT   , // bDescriptorType     = ENDPOINT
  USB_EPADDR_IN1        , // bEndpointAddress    = IN.1
  USB_EPTYPE_INT        , // bmAttributes        = { INTERRUPT }
  1, 0                  , // wMaxPacketSize      = 1
  0x5                   , // bInterval           = 12 ms

  // ******************************************************************************************************************
  // Other Speed Descriptor
  // ******************************************************************************************************************

  9                     , // bLength             = 9 bytes
  USB_DESCRTYPE_OSCFG   , // bDescriptorType     = OSCONFIGURATION
  25 , 0                , // wTotalLength        = 25 bytes
  1                     , // bNumInterfaces      = 1
  1                     , // bConfigurationValue = 1
  0                     , // iConfiguration      = <NO STRING>
  HUB_CFG_ATTR          , // bmAttributes
  HUB_CFG_IMAX          , // bMaxPower

  // **************************************************************************
  // Interface 0
  // **************************************************************************

  9                     , // bLength             = 9 bytes
  USB_DESCRTYPE_INTFX   , // bDescriptorType     = INTERFACE
  0                     , // bInterfaceNumber    = 0
  0                     , // bAlternateSetting   = 0
  1                     , // bNumEndpoints       = 1
  USB_CLASSCODE_HUB     , // bInterfaceClass     = HUB
  0                     , // bInterfaceSubClass  = 0
  USB_DEVPROT_HUB_HW1TT , // bInterfaceProtocol  = HW has only 1 TT
  0                     , // iInterface          = <NO STRING>

  // ******************************************************
  // Endpoint IN1
  // ******************************************************

  7                     , // bLength             = 7 bytes
  USB_DESCRTYPE_ENDPT   , // bDescriptorType     = ENDPOINT
  USB_EPADDR_IN1        , // bEndpointAddress    = IN.1
  USB_EPTYPE_INT        , // bmAttributes        = { INTERRUPT }
  1, 0                  , // wMaxPacketSize      = 1
  0x5                   , // bInterval           = 2^15 uFrames

  // ******************************************************************************************************************
  // Binary Object Store descriptor
  // ******************************************************************************************************************

  5                     , // bLength             = 5 bytes
  USB_DESCRTYPE_BOS     , // bDescriptorType     = BOS
  12, 0                 , // wTotalLength        = 12
  1                     , // bNumDeviceCaps      = 1

  // ******************************************************
  // Device capability descriptor
  // ******************************************************

  7                     , // bLength             = 7 bytes
  USB_DESCRTYPE_DEVCAP  , // bDescriptorType     = DEVICE_CAPABILITY
  USB_DEVCAPTYPE_USB2EXT, // bDevCapabilityType  = USB2p0Extension
  0x00, 0, 0, 0         , // bmAttributes        = {LPM}

  // ******************************************************************************************************************
  // Hub descriptor
  // ******************************************************************************************************************

  9                     , // bLength             = 9 bytes
  USB_DESCRTYPE_HUB     , // bDescriptorType     = HUB_DESCRIPTOR
  5                     , // bNbrPorts           = 5
  HUB_CHAR , 0          , // wHubCharacteristics
  HUB_VBUS_WAIT         , // bPwrOn2PwrGood
  HUB_CHAR_ICTRL        , // bHubContrCurrent
  HUB_INTL_PORTS        , // DeviceRemovable
  0xFF                    // PortPwrCtrlMask
};


// descriptor record
const USBHUB_DSCRREC_t _USBHUB_DSCRREC_SHORT = {
  .HSLEN  = sizeof( DESCRIPT_SHORT_HS ) ,
  .FSLEN  = sizeof( DESCRIPT_SHORT_FS ) ,
  .HSDSCR = DESCRIPT_SHORT_HS           ,
  .FSDSCR = DESCRIPT_SHORT_FS
};


// ####################################################################################################################
// ## Long form descriptor (with strings)
// ####################################################################################################################

// high speed
static const uint8_t DESCRIPT_LONG_HS[] = {

  // ******************************************************************************************************************
  // Device Descriptor
  // ******************************************************************************************************************

  18                    , // bLength            = 18 bytes
  USB_DESCRTYPE_DEVICE  , // bDescrptorType     = DEVICE
  (BCDUSB & 0x00FF)     , // bcdUSB
  (BCDUSB >> 8)         , // ...
  USB_CLASSCODE_HUB     , // bDeviceClass       = HUB
  0                     , // bDeviceSubClass    = 0
  USB_DEVPROT_HUB_SNGLTT, // bDeviceProtocol    = SIGLE_TT
  64                    , // bMaxPacketSize0    = 64
  (IDVENDOR   & 0x00FF) , // idVendor
  (IDVENDOR   >> 8)     , // ...
  (IDPROD     & 0x00FF) , // idProduct
  (IDPROD     >> 8)     , // ...
  (BCDDEV     & 0x00FF) , // bcdDevice
  (BCDDEV     >> 8)     , // ...
  IMANUFACTURER         , // iManufacturer      = <NO STRING>
  IPRODUCT              , // iProduct           = <NO STRING>
  ISERIALNUMBER         , // iserialNumber      = <NO STRING>
  1                     , // bNumConfigurations = 1

  // ******************************************************************************************************************
  // Device Qualifier
  // ******************************************************************************************************************

  10                    , // bLength            = 10 bytes
  USB_DESCRTYPE_DEVQLF  , // bDescriptorType    = QUALIFIER
  (BCDUSB & 0x00FF)     , // bcdUSB
  (BCDUSB >> 8)         , // ...
  USB_CLASSCODE_HUB     , // bDeviceClass       = HUB
  0                     , // bDeviceSubClass    = 0
  USB_DEVPROT_HUB_FSLS  , // bDeviceProtocol    = FS/LS
  64                    , // bMaxPacketSize0    = 64
  1                     , // bNumConfigurations = 1
  0x00                  , // bReserved          = 0

  // ******************************************************************************************************************
  // Configuration Descriptor
  // ******************************************************************************************************************

  9                     , // bLength             = 9 bytes
  USB_DESCRTYPE_CONFIG  , // bDescriptorType     = CONFIGURATION
  25 , 0                , // wTotalLength        = 25 bytes
  1                     , // bNumInterfaces      = 1
  1                     , // bConfigurationValue = 1
  0                     , // iConfiguration      = <NO STRING>
  HUB_CFG_ATTR          , // bmAttributes
  HUB_CFG_IMAX          , // bMaxPower

  // **************************************************************************
  // Interface 0
  // **************************************************************************

  9                     , // bLength             = 9 bytes
  USB_DESCRTYPE_INTFX   , // bDescriptorType     = INTERFACE
  0                     , // bInterfaceNumber    = 0
  0                     , // bAlternateSetting   = 0
  1                     , // bNumEndpoints       = 1
  USB_CLASSCODE_HUB     , // bInterfaceClass     = HUB
  0                     , // bInterfaceSubClass  = 0
  USB_DEVPROT_HUB_HW1TT , // bInterfaceProtocol  = HARDWARE ONLY HAS 1 TT
  0                     , // iInterface          = <NO STRING>

  // ******************************************************
  // Endpoint IN1
  // ******************************************************

  7                     , // bLength             = 7 bytes
  USB_DESCRTYPE_ENDPT   , // bDescriptorType     = ENDPOINT
  USB_EPADDR_IN1        , // bEndpointAddress    = IN.1
  USB_EPTYPE_INT        , // bmAttributes        = { INTERRUPT }
  1, 0                  , // wMaxPacketSize      = 1 byte
  0x05                  , // bInterval           = 256ms

  // ******************************************************************************************************************
  // Other Speed Descriptor
  // ******************************************************************************************************************

  9                     , // bLength             = 9 bytes
  USB_DESCRTYPE_OSCFG   , // bDescriptorType     = OSCONFIGURATION
  25 , 0                , // wTotalLength        = 25 bytes
  1                     , // bNumInterfaces      = 1
  1                     , // bConfigurationValue = 1
  0                     , // iConfiguration      = <NO STRING>
  HUB_CFG_ATTR          , // bmAttributes
  HUB_CFG_IMAX          , // bMaxPower

  // **************************************************************************
  // Interface 0
  // **************************************************************************

  9                     , // bLength             = 9 bytes
  USB_DESCRTYPE_INTFX   , // bDescriptorType     = INTERFACE
  0                     , // bInterfaceNumber    = 0
  0                     , // bAlternateSetting   = 0
  1                     , // bNumEndpoints       = 1
  USB_CLASSCODE_HUB     , // bInterfaceClass     = HUB
  0                     , // bInterfaceSubClass  = 0
  USB_DEVPROT_HUB_FSLS  , // bInterfaceProtocol  = FS/LS
  0                     , // iInterface          = <NO STRING>

  // ******************************************************
  // Endpoint IN1
  // ******************************************************

  7                     , // bLength             = 7 bytes
  USB_DESCRTYPE_ENDPT   , // bDescriptorType     = ENDPOINT
  USB_EPADDR_IN1        , // bEndpointAddress    = IN.1
  USB_EPTYPE_INT        , // bmAttributes        = { INTERRUPT }
  1 , 0                 , // wMaxPacketSize      = 1
  0xFF                  , // bInterval           = max allowed

  // ******************************************************************************************************************
  // Binary Object Store descriptor
  // ******************************************************************************************************************

  5                     , // bLength             = 5 bytes
  USB_DESCRTYPE_BOS     , // bDescriptorType     = BOS
  12 , 0                , // wTotalLength        = 12 bytes
  1                     , // bNumDeviceCaps      = 1

  // **************************************************************************
  // Device capability descriptor
  // **************************************************************************

  7                     , // bLength             = 7 bytes
  USB_DESCRTYPE_DEVCAP  , // bDescriptorType     = DEVICE_CAPABILITY
  USB_DEVCAPTYPE_USB2EXT, // bDevCapabilityType  = USB 2p0 Extension
  0x00 , 0 , 0 , 0      , // bmAttributes        = {LPM}

  // ******************************************************************************************************************
  // Hub descriptor
  // ******************************************************************************************************************

  9                     , // bLength             = 9 bytes
  USB_DESCRTYPE_HUB     , // bDescriptorType     = HUB_DESCRIPTOR
  5                     , // bNbrPorts           = 5
  HUB_CHAR , 0          , // wHubCharacteristics
  HUB_VBUS_WAIT         , // bPwrOn2PwrGood
  HUB_CHAR_ICTRL        , // bHubContrCurrent
  HUB_INTL_PORTS        , // DeviceRemovable
  0xFF                  , // PortPwrCtrlMask

  // ******************************************************************************************************************
  // Strings
  // ******************************************************************************************************************

  // **************************************************************************
  // String 0 (language code index)
  // **************************************************************************

  4                     , // bLength             = 4 bytes
  USB_DESCRTYPE_STRING  , // bDescriptorType     = STRING
  0x09 , 0x04           , // wLangId             = USA English

  // **************************************************************************
  // String 1 (manufacturer)
  // **************************************************************************

  18                    , // bLength             = 18 bytes
  USB_DESCRTYPE_STRING  , // bDescriptorType     = STRING
  'i' , 0x00            , // 'i'
  'n' , 0x00            , // 'n'
  'd' , 0x00            , // 'i'
  'i' , 0x00            , // 'e'
  'S' , 0x00            , // 'S'
  'e' , 0x00            , // 'e'
  'm' , 0x00            , // 'm'
  'i' , 0x00            , // 'i'

  // **************************************************************************
  // String 1 (product)
  // **************************************************************************

  18                    , // bLength             = 18 bytes
  USB_DESCRTYPE_STRING  , // bDescriptorType     = STRING
  'S' , 0x00            , // 'i'
  'u' , 0x00            , // 'n'
  'p' , 0x00            , // 'i'
  'e' , 0x00            , // 'e'
  'r' , 0x00            , // 'S'
  'i' , 0x00            , // 'e'
  'o' , 0x00            , // 'm'
  'r' , 0x00            , // 'i'

  // **************************************************************************
  // String 1 (serial number)
  // **************************************************************************

  10                    , // bLength             = 10 bytes
  USB_DESCRTYPE_STRING  , // bDescriptorType     = STRING
  'B' , 0x00            , // 'B'
  '0' , 0x00            , // '0'
  '5' , 0x00            , // '5'
  'T' , 0x00              // 'T'
};


// full speed
static const uint8_t DESCRIPT_LONG_FS[] = {

  // ******************************************************************************************************************
  // Device Descriptor
  // ******************************************************************************************************************

  18                    , // bLength             = 18 bytes
  USB_DESCRTYPE_DEVICE  , // bDescriptorType     = DEVICE
  (BCDUSB & 0x00FF)     , // bcdUSB
  (BCDUSB >> 8)         , // ...
  USB_CLASSCODE_HUB     , // bDeviceClass        = HUB
  0                     , // bDeviceSubClass     = 0
  USB_DEVPROT_HUB_FSLS  , // bDeviceProtocol     = FS/LS
  64                    , // bMaxPacketSize0     = 64
  (IDVENDOR   & 0x00FF) , // idVendor
  (IDVENDOR   >> 8)     , // ...
  (IDPROD     & 0x00FF) , // idProduct
  (IDPROD     >> 8)     , // ...
  (BCDDEV     & 0x00FF) , // bcdDevice
  (BCDDEV     >> 8)     , // ...
  IMANUFACTURER         , // iManufacturer      = <NO STRING>
  IPRODUCT              , // iProduct           = <NO STRING>
  ISERIALNUMBER         , // iserialNumber      = <NO STRING>
  1                     , // bNumConfigurations = 1

  // ******************************************************************************************************************
  // Device Qualifier
  // ******************************************************************************************************************

  10                    , // bLength            = 10 bytes
  USB_DESCRTYPE_DEVQLF  , // bDescriptorType    = QUALIFIER
  (BCDUSB & 0x00FF)     , // bcdUSB
  (BCDUSB >> 8)         , // ...
  USB_CLASSCODE_HUB     , // bDeviceClass       = HUB
  0                     , // bDeviceSubClass    = 0
  USB_DEVPROT_HUB_SNGLTT, // bDeviceProtocol    = SINGLE_TT
  64                    , // bMaxPacketSize0    = 64
  1                     , // bNumConfigurations = 1
  0                     , // bReserved          = 0

  // ******************************************************************************************************************
  // Configuration Descriptor
  // ******************************************************************************************************************

  9                     , // bLength             = 9 bytes
  USB_DESCRTYPE_CONFIG  , // bDescriptorType     = CONFIGURATION
  25 , 0                , // wTotalLength        = 25 bytes
  1                     , // bNumInterfaces      = 1
  1                     , // bConfigurationValue = 1
  0                     , // iConfiguration      = <NO STRING>
  HUB_CFG_ATTR          , // bmAttributes
  HUB_CFG_IMAX          , // bMaxPower

  // **************************************************************************
  // Interface 0
  // **************************************************************************

  9                     , // bLength             = 9 bytes
  USB_DESCRTYPE_INTFX   , // bDescriptorType     = INTERFACE
  0                     , // bInterfaceNumber    = 0
  0                     , // bAlternateSetting   = 0
  1                     , // bNumEndpoints       = 1
  USB_CLASSCODE_HUB     , // bInterfaceClass     = HUB
  0                     , // bInterfaceSubClass  = 0
  USB_DEVPROT_HUB_FSLS  , // bInterfaceProtocol  = FS/LS
  0                     , // iInterface          = <NO STRING>

  // ******************************************************
  // Endpoint IN1
  // ******************************************************

  7                     , // bLength             = 7 bytes
  USB_DESCRTYPE_ENDPT   , // bDescriptorType     = ENDPOINT
  USB_EPADDR_IN1        , // bEndpointAddress    = IN.1
  USB_EPTYPE_INT        , // bmAttributes        = { INTERRUPT }
  1, 0                  , // wMaxPacketSize      = 1
  0xFF                  , // bInterval           = max allowed

  // ******************************************************************************************************************
  // Other Speed Descriptor
  // ******************************************************************************************************************

  9                     , // bLength             = 9 bytes
  USB_DESCRTYPE_OSCFG   , // bDescriptorType     = OSCONFIGURATION
  25 , 0                , // wTotalLength        = 25 bytes
  1                     , // bNumInterfaces      = 1
  1                     , // bConfigurationValue = 1
  0                     , // iConfiguration      = <NO STRING>
  HUB_CFG_ATTR          , // bmAttributes
  HUB_CFG_IMAX          , // bMaxPower

  // **************************************************************************
  // Interface 0
  // **************************************************************************

  9                     , // bLength             = 9 bytes
  USB_DESCRTYPE_INTFX   , // bDescriptorType     = INTERFACE
  0                     , // bInterfaceNumber    = 0
  0                     , // bAlternateSetting   = 0
  1                     , // bNumEndpoints       = 1
  USB_CLASSCODE_HUB     , // bInterfaceClass     = HUB
  0                     , // bInterfaceSubClass  = 0
  USB_DEVPROT_HUB_HW1TT , // bInterfaceProtocol  = HW has only 1 TT
  0                     , // iInterface          = <NO STRING>

  // ******************************************************
  // Endpoint IN1
  // ******************************************************

  7                     , // bLength             = 7 bytes
  USB_DESCRTYPE_ENDPT   , // bDescriptorType     = ENDPOINT
  USB_EPADDR_IN1        , // bEndpointAddress    = IN.1
  USB_EPTYPE_INT        , // bmAttributes        = { INTERRUPT }
  1, 0                  , // wMaxPacketSize      = 1
  0x5                   , // bInterval           = 256ms

  // ******************************************************************************************************************
  // Binary Object Store descriptor
  // ******************************************************************************************************************

  5                     , // bLength             = 5 bytes
  USB_DESCRTYPE_BOS     , // bDescriptorType     = BOS
  12, 0                 , // wTotalLength        = 12
  1                     , // bNumDeviceCaps      = 1

  // ******************************************************
  // Device capability descriptor
  // ******************************************************

  7                     , // bLength             = 7 bytes
  USB_DESCRTYPE_DEVCAP  , // bDescriptorType     = DEVICE_CAPABILITY
  USB_DEVCAPTYPE_USB2EXT, // bDevCapabilityType  = USB2p0Extension
  0x00, 0, 0, 0         , // bmAttributes        = {LPM}

  // ******************************************************************************************************************
  // Hub descriptor
  // ******************************************************************************************************************

  9                     , // bLength             = 9 bytes
  USB_DESCRTYPE_HUB     , // bDescriptorType     = HUB_DESCRIPTOR
  5                     , // bNbrPorts           = 5
  HUB_CHAR , 0          , // wHubCharacteristics
  HUB_VBUS_WAIT         , // bPwrOn2PwrGood
  HUB_CHAR_ICTRL        , // bHubContrCurrent
  HUB_INTL_PORTS        , // DeviceRemovable
  0xFF                  , // PortPwrCtrlMask

  // ******************************************************************************************************************
  // Strings
  // ******************************************************************************************************************

  // **************************************************************************
  // String 0 (language code index)
  // **************************************************************************

  4                     , // bLength             = 4 bytes
  USB_DESCRTYPE_STRING  , // bDescriptorType     = STRING
  0x09 , 0x04           , // wLangId             = USA English

  // **************************************************************************
  // String 1 (manufacturer)
  // **************************************************************************

  18                    , // bLength             = 18 bytes
  USB_DESCRTYPE_STRING  , // bDescriptorType     = STRING
  'i' , 0x00            , // 'i'
  'n' , 0x00            , // 'n'
  'd' , 0x00            , // 'i'
  'i' , 0x00            , // 'e'
  'S' , 0x00            , // 'S'
  'e' , 0x00            , // 'e'
  'm' , 0x00            , // 'm'
  'i' , 0x00            , // 'i'

  // **************************************************************************
  // String 1 (product)
  // **************************************************************************

  18                    , // bLength             = 18 bytes
  USB_DESCRTYPE_STRING  , // bDescriptorType     = STRING
  'S' , 0x00            , // 'i'
  'u' , 0x00            , // 'n'
  'p' , 0x00            , // 'i'
  'e' , 0x00            , // 'e'
  'r' , 0x00            , // 'S'
  'i' , 0x00            , // 'e'
  'o' , 0x00            , // 'm'
  'r' , 0x00            , // 'i'

  // **************************************************************************
  // String 1 (serial number)
  // **************************************************************************

  10                    , // bLength             = 10 bytes
  USB_DESCRTYPE_STRING  , // bDescriptorType     = STRING
  'B' , 0x00            , // 'B'
  '0' , 0x00            , // '0'
  '5' , 0x00            , // '5'
  'T' , 0x00              // 'T'
};

// descriptor record
const USBHUB_DSCRREC_t _USBHUB_DSCRREC_LONG = {
  .HSLEN  = sizeof( DESCRIPT_LONG_HS ) ,
  .FSLEN  = sizeof( DESCRIPT_LONG_FS ) ,
  .HSDSCR = DESCRIPT_LONG_HS           ,
  .FSDSCR = DESCRIPT_LONG_FS
};
