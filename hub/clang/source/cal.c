/** @file cal.c
 *  @brief Board calibration
 *
 *  This transfers calibration data from non-volatile memory to
 *  trim memory.
 *
 *  @author Ian Board
 *  @bug No known bugs.
 */

#include "superior.h"
#include "nvmtrim.h"
#include "hw.h"
#include "trim_defaults.h"
#include "cal.h"

extern uint32_t SystemCoreClock;


/** @brief retrieves trim memory so it can be examined in a debugger.
 *        
 *
 *  For debug so the contents of trim memory can be examined with a debugger.
 *
 *  @param None
 *  @return Void.
 */
void v_Dump_Trim_Memory(void)
{
#pragma diag_suppress = Pe550  // suppress warning that values aren't used - just for debug
  static uint32_t u32_Value;
  static uint32_t u32_Flash_Value;
  
  // Basic trims
  u32_Value = TRIMMEMORY_SFRS->TRIMDATA1.BGTRIM3V;
  u32_Value = TRIMMEMORY_SFRS->TRIMDATA0.V2ITRIM;
  u32_Value = TRIMMEMORY_SFRS->TRIMDATA1.ICOSCTRIM3V;
  
  // Termination resistance trims
  u32_Value = USBAFE_UP->RCTRIM.RTERMTRIM;
  u32_Value = USBAFE_DN1->RCTRIM.RTERMTRIM;
  u32_Value = USBAFE_DN2->RCTRIM.RTERMTRIM;
  u32_Value = USBAFE_DN3->RCTRIM.RTERMTRIM;
  
  // QM Gain and offset
  u32_Value = HKADCDGE_SFRS->NODE00CTRLC.ADCWORD;
  
  // AFE Trims
  u32_Value = TYPEC1_SFRS->AFETRIM1.TRIMRD1;
  u32_Value = TYPEC2_SFRS->AFETRIM1.TRIMRD1;
  u32_Value = TYPEC1_SFRS->AFETRIM1.TRIMRD2;
  u32_Value = TYPEC2_SFRS->AFETRIM1.TRIMRD2;
  u32_Value = TYPEC1_SFRS->AFETRIM3.TRIMHCUR;
  u32_Value = TYPEC2_SFRS->AFETRIM3.TRIMHCUR;
  
  // 1.5V digital supply trim
  u32_Value = TRIMMEMORY_SFRS->TRIMDATA1.DIGREGR3V = V_DIG_SUPPLY_TRIM.VALUE; 

  // Squelch Detector
  u32_Value = USBAFE_UP->HSTRIM.HSSQLCHTRIM;
  u32_Value = USBAFE_DN1->HSTRIM.HSSQLCHTRIM;
  u32_Value = USBAFE_DN2->HSTRIM.HSSQLCHTRIM;
  u32_Value = USBAFE_DN3->HSTRIM.HSSQLCHTRIM;
  
  // Disconnect detector
  u32_Value = USBAFE_UP->HSTRIM.HSDSCNTRIM;
  u32_Value = USBAFE_DN1->HSTRIM.HSDSCNTRIM;
  u32_Value = USBAFE_DN2->HSTRIM.HSDSCNTRIM;
  u32_Value = USBAFE_DN3->HSTRIM.HSDSCNTRIM;

  // Tx Voltage
  u32_Value = USBAFE_DN1->FSLSTRIM.FSLSSE0FLTPWTRIM;
  u32_Value = USBAFE_DN2->FSLSTRIM.FSLSSE0FLTPWTRIM;
  u32_Value = USBAFE_DN3->FSLSTRIM.FSLSSE0FLTPWTRIM;
  
  // CDR Sync Trim
  u32_Value = USBAFE_UP->CDRTRIM.CDRSYNCTRIM;
  u32_Value = USBAFE_DN1->CDRTRIM.CDRSYNCTRIM;
  u32_Value = USBAFE_DN2->CDRTRIM.CDRSYNCTRIM;
  u32_Value = USBAFE_DN3->CDRTRIM.CDRSYNCTRIM;
  
  // FSLS Falling edge trim
  u32_Value = USBAFE_UP->FSLSTRIM.FSLSSE0FLTFETRIM;
  u32_Value = USBAFE_DN1->FSLSTRIM.FSLSSE0FLTFETRIM;
  u32_Value = USBAFE_DN2->FSLSTRIM.FSLSSE0FLTFETRIM;
  u32_Value = USBAFE_DN3->FSLSTRIM.FSLSSE0FLTFETRIM;
  u32_Flash_Value = FSTS_FE_UFP_TRIM.VALUE;
 
  // Fast Acquisition
  u32_Value = USBAFECDR_SFRS->UFPTRIM.FASTAQ;
  u32_Value = USBAFECDR_SFRS->DFP1TRIM.FASTAQ;
  u32_Value = USBAFECDR_SFRS->DFP2TRIM.FASTAQ;
  u32_Value = USBAFECDR_SFRS->DFP3TRIM.FASTAQ;
  
  // Extended Sync
  u32_Value = USBHUB->PHYTRIM.UP.SYNCEXT;
  u32_Value = USBHUB->PHYTRIM.DN1.SYNCEXT;
  u32_Value = USBHUB->PHYTRIM.DN2.SYNCEXT;
  u32_Value = USBHUB->PHYTRIM.DN3.SYNCEXT;
}

/** @brief Basic Trims
 *        
 *
 *  Trims for bandgap reference, V2I, the low speed oscillator and 1.5V supply.
 *  These get applied regardless of the cal version.
 *
 *  @param None
 *  @return Void.
 */
void v_Basic_Trims_From_Flash(void)
{
    TRIMMEMORY_SFRS->TRIMDATA1.BGTRIM3V = NVM_BG_TRIM.VALUE; // Bandgap reference
    TRIMMEMORY_SFRS->TRIMDATA0.V2ITRIM = NVM_V2I_TRIM.VALUE; // V2I
    TRIMMEMORY_SFRS->TRIMDATA1.ICOSCTRIM3V = NVM_ICOSC_TRIM.VALUE; // ICOSC

    
    // Termination resistance
    USBAFE_UP->RCTRIM.RTERMTRIM = NVM_USB45_TRIM.VALUE; 
    USBAFE_DN1->RCTRIM.RTERMTRIM = NVM_USB45_TRIM.VALUE;
    USBAFE_DN2->RCTRIM.RTERMTRIM = NVM_USB45_TRIM.VALUE;
    USBAFE_DN3->RCTRIM.RTERMTRIM = NVM_USB45_TRIM.VALUE;
    
    // PTAT
    HKADCDGE_SFRS->NODE00CTRLC.ADCWORD = NVM_VPTAT_TRIM.VALUE;
    
}



/** @brief Transfer trims from non-volatile memory to trim memory.
 *        
 *
 *  Transfers the trims established at ATE to trim memory.
 *
 *  @param None
 *  @return Void.
 */
void v_Apply_Trims_From_Flash(void)
{
volatile uint32_t u32_Dummy;
Chip_Info_t Chip_Info;

    v_Get_Chip_Info( &Chip_Info );
 
    // Current measuring adc trims
    CMADC_SFRS->CONFIG.TRIM = NVM_CMBIAS_TRIM.VALUE;   //QM Current
    //CMADC_SFRS->CONFIG.SWC  QM Gain

      
#ifdef USE_PD
#pragma message(" *** PD in use *** ")
    
    // Power up and reset hardware
    CRG_SFRS->USBTYPEC.CCP1RSTENA = 1;
    CRG_SFRS->USBTYPEC.CCP2RSTENA = 1;
    
    CRG_SFRS->USBTYPEC.CCP1RSTPULSE = 1; 
    CRG_SFRS->USBTYPEC.CCP2RSTPULSE = 1; 
  
    // wait for power up and reset to complete
    for( u32_Dummy = 0; u32_Dummy < 1000; u32_Dummy++ );
  
    // Turn on AFE to apply trims
    TYPEC1_SFRS->POWER.PWR = 1;
    TYPEC2_SFRS->POWER.PWR = 1;

    // Wait to allow AFE to come up
    for( u32_Dummy = 0; u32_Dummy < 1000; u32_Dummy++ );
  
    // Bring out of reset
    TYPEC1_SFRS->TYPECCTL3.RESETPHY = 1;
    TYPEC2_SFRS->TYPECCTL3.RESETPHY = 1;

    // Short wait after coming out of reset
    for( u32_Dummy = 0; u32_Dummy < 1000; u32_Dummy++ );
  
    // Apply trims from flash
    
    // CC Port RD
    TYPEC1_SFRS->AFETRIM1.TRIMRD1 = NVM_CCRESISTANCE_TRIM.VALUE; // RD 
    TYPEC1_SFRS->AFETRIM1.TRIMRD2 = NVM_CCRESISTANCE_TRIM.VALUE;
    TYPEC2_SFRS->AFETRIM1.TRIMRD1 = NVM_CCRESISTANCE_TRIM.VALUE;
    TYPEC2_SFRS->AFETRIM1.TRIMRD2 = NVM_CCRESISTANCE_TRIM.VALUE;
    
    // P1 and P2 pullup
    TYPEC1_SFRS->AFETRIM3.TRIMHCUR = NVM_CCCURRENT_TRIM.VALUE;
    TYPEC2_SFRS->AFETRIM3.TRIMHCUR =  NVM_CCCURRENT_TRIM.VALUE;
    
#else
#pragma message("!!! PD not enabled, do not access PD related ports !!!")
    
#endif
       
    // 1.5V digital supply trim 
    TRIMMEMORY_SFRS->TRIMDATA1.DIGREGR3V = Chip_Info.u8_Cal_Version == 4 ? V_DIG_SUPPLY_TRIM.VALUE : DIG_SUPPLY_1p5_DEFAULT;  

#ifdef USB_TRIMS
    if( Chip_Info.u8_Cal_Version == 4 )
    {
        // Squelch
        USBAFE_UP->HSTRIM.HSSQLCHTRIM = SQUELCH_UFP_TRIM.VALUE;           
        USBAFE_DN1->HSTRIM.HSSQLCHTRIM = SQUELCH_DFP1_TRIM.VALUE; 
        USBAFE_DN2->HSTRIM.HSSQLCHTRIM = SQUELCH_DFP2_TRIM.VALUE; 
        USBAFE_DN3->HSTRIM.HSSQLCHTRIM = SQUELCH_DFP3_TRIM.VALUE; 

        // Disconnect threshold
        USBAFE_UP->HSTRIM.HSDSCNTRIM = DISCONNECT_DET_UFP_TRIM.VALUE; 
        USBAFE_DN1->HSTRIM.HSDSCNTRIM = DISCONNECT_DET_DFP1_TRIM.VALUE; 
        USBAFE_DN2->HSTRIM.HSDSCNTRIM = DISCONNECT_DET_DFP2_TRIM.VALUE;  
        USBAFE_DN3->HSTRIM.HSDSCNTRIM = DISCONNECT_DET_DFP3_TRIM.VALUE; 

        // CDP voltage setting
        USBAFE_DN1->FSLSTRIM.FSLSSE0FLTPWTRIM = CDP_V_DFP1_TRIM.VALUE;  
        USBAFE_DN2->FSLSTRIM.FSLSSE0FLTPWTRIM = CDP_V_DFP2_TRIM.VALUE;
        USBAFE_DN3->FSLSTRIM.FSLSSE0FLTPWTRIM = CDP_V_DFP3_TRIM.VALUE;
        
        // FSLS Falling edge trim
        USBAFE_UP->FSLSTRIM.FSLSSE0FLTFETRIM = FSTS_FE_UFP_TRIM.VALUE;  
        USBAFE_DN1->FSLSTRIM.FSLSSE0FLTFETRIM = FSTS_FE_DFP1_TRIM.VALUE;  
        USBAFE_DN2->FSLSTRIM.FSLSSE0FLTFETRIM = FSTS_FE_DFP2_TRIM.VALUE;
        USBAFE_DN3->FSLSTRIM.FSLSSE0FLTFETRIM = FSTS_FE_DFP3_TRIM.VALUE;

        // CDR Sync
        USBAFE_UP->CDRTRIM.CDRSYNCTRIM = CDR_SYNC_TRIM_1.VALUE;           
        USBAFE_DN1->CDRTRIM.CDRSYNCTRIM = CDR_SYNC_TRIM_2.VALUE;
        USBAFE_DN2->CDRTRIM.CDRSYNCTRIM = CDR_SYNC_TRIM_3.VALUE;
        USBAFE_DN3->CDRTRIM.CDRSYNCTRIM = CDR_SYNC_TRIM_4.VALUE;

        // Fast acq UFP
        USBAFECDR_SFRS->UFPTRIM.FASTAQ = FAST_ACQ_UFP_TRIM.VALUE;          
        USBAFECDR_SFRS->DFP1TRIM.FASTAQ = FAST_ACQ_DFP1_TRIM.VALUE;
        USBAFECDR_SFRS->DFP2TRIM.FASTAQ = FAST_ACQ_DFP2_TRIM.VALUE;
        USBAFECDR_SFRS->DFP3TRIM.FASTAQ = FAST_ACQ_DFP3_TRIM.VALUE;

        // Extended Packet Sync
        USBHUB->PHYTRIM.UP.SYNCEXT = EXT_RP_SYNC_UFP_TRIM.VALUE;
        USBHUB->PHYTRIM.DN1.SYNCEXT = EXT_RP_SYNC_DFP1_TRIM.VALUE;
        USBHUB->PHYTRIM.DN2.SYNCEXT = EXT_RP_SYNC_DFP2_TRIM.VALUE;
        USBHUB->PHYTRIM.DN3.SYNCEXT = EXT_RP_SYNC_DFP3_TRIM.VALUE;
    }
    else
    {
         // Squelch
        USBAFE_UP->HSTRIM.HSSQLCHTRIM = SQUELCH_DETECTOR_DEFAULT;           
        USBAFE_DN1->HSTRIM.HSSQLCHTRIM = SQUELCH_DETECTOR_DEFAULT; 
        USBAFE_DN2->HSTRIM.HSSQLCHTRIM = SQUELCH_DETECTOR_DEFAULT; 
        USBAFE_DN3->HSTRIM.HSSQLCHTRIM = SQUELCH_DETECTOR_DEFAULT; 

        // Disconnect threshold
        USBAFE_UP->HSTRIM.HSDSCNTRIM = DISCONNECT_DETECTOR_DEFAULT; 
        USBAFE_DN1->HSTRIM.HSDSCNTRIM = DISCONNECT_DETECTOR_DEFAULT; 
        USBAFE_DN2->HSTRIM.HSDSCNTRIM = DISCONNECT_DETECTOR_DEFAULT;  
        USBAFE_DN3->HSTRIM.HSDSCNTRIM = DISCONNECT_DETECTOR_DEFAULT; 

        // CDP voltage setting
        USBAFE_DN1->FSLSTRIM.FSLSSE0FLTPWTRIM = CDP_TX_VOLTAGE_DEFAULT;  
        USBAFE_DN2->FSLSTRIM.FSLSSE0FLTPWTRIM = CDP_TX_VOLTAGE_DEFAULT;
        USBAFE_DN3->FSLSTRIM.FSLSSE0FLTPWTRIM = CDP_TX_VOLTAGE_DEFAULT;
        
        // FSLS Falling edge trim
        USBAFE_UP->FSLSTRIM.FSLSSE0FLTFETRIM = FSLS_FE_DEFAULT;  
        USBAFE_DN1->FSLSTRIM.FSLSSE0FLTFETRIM = FSLS_FE_DEFAULT;  
        USBAFE_DN2->FSLSTRIM.FSLSSE0FLTFETRIM = FSLS_FE_DEFAULT;
        USBAFE_DN3->FSLSTRIM.FSLSSE0FLTFETRIM = FSLS_FE_DEFAULT;

        // CDR Sync
        USBAFE_UP->CDRTRIM.CDRSYNCTRIM = CDR_SYNC_TRIM_DEFAULT;           
        USBAFE_DN1->CDRTRIM.CDRSYNCTRIM = CDR_SYNC_TRIM_DEFAULT;
        USBAFE_DN2->CDRTRIM.CDRSYNCTRIM = CDR_SYNC_TRIM_DEFAULT;
        USBAFE_DN3->CDRTRIM.CDRSYNCTRIM = CDR_SYNC_TRIM_DEFAULT;
        // Fast acq UFP
        USBAFECDR_SFRS->UFPTRIM.FASTAQ = FAST_ACQ_DEFAULT;          
        USBAFECDR_SFRS->DFP1TRIM.FASTAQ = FAST_ACQ_DEFAULT;
        USBAFECDR_SFRS->DFP2TRIM.FASTAQ = FAST_ACQ_DEFAULT;
        USBAFECDR_SFRS->DFP3TRIM.FASTAQ = FAST_ACQ_DEFAULT;

        // Extended Packet Sync
        USBHUB->PHYTRIM.UP.SYNCEXT = EXT_REP_SYNC_DEFAULT;
        USBHUB->PHYTRIM.DN1.SYNCEXT = EXT_REP_SYNC_DEFAULT;
        USBHUB->PHYTRIM.DN2.SYNCEXT = EXT_REP_SYNC_DEFAULT;
        USBHUB->PHYTRIM.DN3.SYNCEXT = EXT_REP_SYNC_DEFAULT;
    }
      
#endif
    
    if( Chip_Info.u8_Chip_Revision == CHIP_REVISION_C0 )
    {
      v_Apply_C0_USB_Fixes();
    }
}

/** @brief Default Trims for CDR
 *        
 *
 *  These are written first and establish defaults for the CDR. Some of these values may be 
 *  overridden by values saved in non-volatile trim memory.
 *
 *  @param None
 *  @return Void.
 */
void v_Default_CDR_Setup( void )
{
  //////////////////////////////////////////////////////////////////////////////
  //AFE Performance Trim Settings:
  //Configured to use the reccomended values for AFE optimal performance on all ports
  //SET CDR, EXTENDED CDR AND PHY TRIMs
  //CDR: CDRTXMSKBYP=0, CDRDRBLTRIM=0, CDRSYNCTRIM=01, CDRSYNCTRIM=0, 0, 0, CDRIDLMONENA=1
  //Extended CDR: SQLMSK=0, FASTACQ=1 
  //PHY: EOI=01, SYNCEXT=01, 0, 0, WRAPDET=1, SORPFLT=1
  //////////////////////////////////////////////////////////////////////////////
  
  //Upstream Facing Port
  USBAFE_setCDRTrims(0);
  
  //Downstream Facing Port 1
  USBAFE_setCDRTrims(1);
  
  //Downstream Facing Port 2
  USBAFE_setCDRTrims(2);
  
  //Downstream Facing Port 3
  USBAFE_setCDRTrims(3);
  
}

/** @brief Wait for PLL to lock
 *        
 *
 *  Has timeout due to erratic behavior of lock indicator
 *
 *  @param None
 *  @return Void.
 */
static void v_PLL_Wait( void )
{
  volatile uint32_t u32_PLL_Lock_Count = 0;
  
  for( u32_PLL_Lock_Count = 0; u32_PLL_Lock_Count < 5000; u32_PLL_Lock_Count++ )
  {
    if( PMU->CLKCSR.PLLCLKUSABLE )
      break;
  }
}

/** @brief Program PLL.
 *        
 *
 *  Set up R, C values for PLL either from non-volatile memory stored at
 *  ATE or with hard-coded default values.
 *
 *  @param None
 *  @return Void.
 */
static void v_Program_PLL(void)
{
  // Grab the OST from NVR
  uint32_t u32_NVM_Pll_Bias_Trim = NVM_PLLBIAS_TRIM.VALUE; 
  u32_NVM_Pll_Bias_Trim &= 0x0F;
  u32_NVM_Pll_Bias_Trim |= 0xB0;
  PLL_SFRS->CCR0.BYTE = u32_NVM_Pll_Bias_Trim; 
  
  #ifdef USB_PLL_FLASH_VALUES
#pragma message("***PLL values initialized from flash***")
  PLL_SFRS->CCR0.BYTE = *(volatile uint8_t *)0x27F35;
  PLL_SFRS->CCR0.BYTE = *(volatile uint8_t *)0x27F36;
  PLL_SFRS->CCR0.BYTE = *(volatile uint8_t *)0x27F37;
  PLL_SFRS->CCR0.BYTE = *(volatile uint8_t *)0x27F38;
#else
#pragma message("***PLL values initialized with hard-coded defaults***")
  PLL_SFRS->CCR0.BYTE = 0x52;
  PLL_SFRS->CCR0.BYTE = 0x63;
  PLL_SFRS->CCR0.BYTE = 0x73;
  PLL_SFRS->CCR0.BYTE = 0x83;
#endif 
}

/** @brief PLL Calibration
 *        
 *
 *  Run the hardware calibration sequence for the PLL. At the end, the high speed
 * (30 MHz) clock is available. Because the timer is used, it is necessary to 
 *  make sure it isn't used if this routine is run at a time other than early boot.
 *
 *  @param None
 *  @return Void.
 */
void v_PLL_Calibration( void ) 
{
 
  NVIC_DisableIRQ( Timer0_IRQn ); 
  
  PMU_CLKCSR_t     clksrc;

  // clear PLL force if set
  _PMU->CLKCSR.PLLFORCE = 0;
 
  // Apply the ANAV1P5 to power PLL
  // CRG Registers:  PLL:  PLL_ENV1P5REG=1   
  CRG_SFRS->PLL.PLL_ENV1P5REG = 0x1;
 
  // Wait 5ms @4MHz for power up
  TIMER0->CFG &= ~TIMER_CFG_ENA;             // stop the general purpose timer
  TIMER0->COUNT = -(5 * 4000);               // load with value for 5 msec delay @ 4 MHz, count up
  TIMER0->CFG |= TIMER_CFG_ENA;              // turn on the general purpose timer. used for timeouts
  while( (int32_t)TIMER0->COUNT < 0  ) {}    // wait for the timeout value
 
  // Deassert PLL_RESET
  // This action results in the PLL being enabled with
  // an autocalibration based on the reset value of OST (0x5)
  _USBHUB->PORCTRL.PLLRST = 0;
 
  // Wait for PLL to lock
   v_PLL_Wait();
  
  // At this point we have a 30MHz clock
 
  // Disable auto-calibration upon suspend-exit
  PLL_SFRS->CCR0.BYTE = 0x30;   
 
  // Toggle Suspend to deassert PLL_EN and then assert PLL_EN
  USBHUB->SUSPENDFORCE = 0x1; 
  USBHUB->SUSPENDFORCE = 0x0;
 
  // At this point we have a 4MHz clock
   
  // Wait time for XTAL to settle
  TIMER0->CFG &= ~TIMER_CFG_ENA;           // turn off the general purpose timer
  TIMER0->COUNT = -(1 * 4000);             // load counter for 1 msec delay @ 4 MHz
  TIMER0->CFG |= TIMER_CFG_ENA;            // turn on the general purpose timer. used for timeouts              // setup a 1ms timeout using the 4MHz clock
  while( (int32_t)TIMER0->COUNT < 0) {}    // wait for the timeout value
 
  // OST, R and C values from flash map, should be default values for now.
  v_Program_PLL();

  // Perform auto-calibration
  PLL_SFRS->CCR0.BYTE = 0xF0;   
  
  // At this point we have a 4MHz clock which transitions to a 30MHz clock
  // once calibration is complete
  
  // Wait period for the auto-calibration to complete
  TIMER0->CFG &= ~TIMER_CFG_ENA;                         // turn off the general purpose timer
  TIMER0->COUNT = -(1 * 30000);                          // clear the counter desired counter value
  TIMER0->CFG |= TIMER_CFG_ENA;                          // turn on the general purpose timer. used for timeouts               // setup a 1ms timeout using the 4MHz/30Mhz clock
  while( (int32_t)TIMER0->COUNT < 0 ) {}                // wait for the timeout value
 
  // Wait for PLL to lock 
  v_PLL_Wait();

  // At this point we have a 30MHz clock
  
  // set the PLL force
  clksrc.BYTE     = 0;
  clksrc.PLLFORCE = 1;
  PMU->CLKCSR     = clksrc;
  
  SystemCoreClock = 30000000;
  
}


/** @brief Bug Fixes for C0
 *        
 *
 *  Settings for the bug fix control register for C0. 
 *
 * 1) Set the value of register SUP_C0_BUGFIX_CTRL (0x50013310) with 0xDF
 *    TT_DISCONNECT_FIX = 1 on all ports
 *    WUCONN_FIX_DISABLE = 0 Fix for SUPR-91 is enabled
 *    CHANGE_BIT_CLEAR_FIX_DISABLE = 1 Fix for SUPR-87 is disabled
 *    PATCH_REMOTE_WAKEUP_N = 1 Disables fix for SUPR-134
 *   
 * 2) Set the value of register WUCONNTRIM (0x50013064) with 0x1
 * 3) Set the value of register TDCNN_TRIM (0x50013314) with 0x6
 *    Connect event time for down facing ports = 7.99 us.
 *
 *  @param None
 *  @return Void.
 */
void v_Apply_C0_USB_Fixes(void)
{
    USBHUB_BUGFIX_CTRL->WORD = 0xdf;
    USBHUB->WUCONN = 1;
    *USBHUB_TDCNN_TRIM_CTRL = 0x6;
}

