/** @file debug_print.c
 *  @brief Functions to use Segger RTT for debug print access.
 *
 *  
 *  
 *
 *  @author Ian Board
 *  @bug No known bugs.
 */

#include <stdint.h>
#include "debug_print.h"

/** @brief Initialize Segger RTT
 *        
 *  @param None
 *  @return Void.
 */
void debug_init(void)
{
  SEGGER_RTT_ConfigUpBuffer(0, NULL, NULL, 0, SEGGER_RTT_MODE_BLOCK_IF_FIFO_FULL );
}



  