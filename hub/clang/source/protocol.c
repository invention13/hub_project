#include "superior.h"
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "queue.h"
#include <string.h>
#include "events.h"
#include "protocol.h"
#include "PD_Port.h"
#include "debug_gpio.h"
#include "pdo.h"

void v_Update_Protocol_State( PD_Port_t *p_Context, Event_t *event );

void   v_Init_Protocol( void  *p_Owning_Port, void *p_Init_Args )
{
  PD_Port_t *p_Port = (PD_Port_t *)p_Owning_Port;
  PD_Port_Init_t *p_Init = (PD_Port_Init_t *)p_Init_Args;
     // Event queue
   p_Port->Protocol.Event_Queue = xQueueCreate( PD_EVENT_QUEUE_LENGTH, sizeof(Event_t) );
   if( p_Port->Protocol.Event_Queue == NULL )
   { // Error condition - can't create queue
   }
   
   xTaskCreate( v_Protocol_Task,
                "Protocol",
                PROTOCOL_STACK_SIZE,
                p_Port,   // reference back to port that owns task
                tskIDLE_PRIORITY,
                &p_Port->Protocol_Task );
}

// Multiple copies of this thread may exist - must be re-entrant
void v_Protocol_Task( void *args )
{
#pragma diag_suppress = Pe177
    PD_Port_t *self = (PD_Port_t *)args;
    Event_t Event;
    uint32_t u32_Msg_Cnt = 0;
    
    // Initialization done here
    
    // Wait for all threads to be ready before proceeding.
    v_Announce_Ready( self, PROTOCOL_READY );
    v_Wait_Until_Ready( self, ALL_READY );

     while(1)
     {
       // Block indefinitely while waiting for events
       if( xQueueReceive( self->Protocol.Event_Queue, &Event, portMAX_DELAY ) )
       {
           // Check for requests and other events that are not state specific
           v_Update_Protocol_State( self, &Event );
           ++u32_Msg_Cnt;
       }
     }
 }

void v_Post_Protocol_Event( void *p_Owning_Port, Event_t *p_Event )
{
    PD_Port_t *p_Port = (PD_Port_t *)p_Owning_Port;
    xQueueSend( p_Port->Protocol.Event_Queue, ( void * )p_Event, ( TickType_t ) 0 );
}