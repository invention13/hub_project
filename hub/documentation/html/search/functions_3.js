var searchData=
[
  ['v_5fapply_5ftrims_5ffrom_5fflash',['v_Apply_Trims_From_Flash',['../cal_8c.html#a742eae585f857f4188d27ca7dbae5177',1,'cal.c']]],
  ['v_5fapply_5fusb_5ffixes',['v_Apply_USB_Fixes',['../cal_8c.html#ae9eb0d8bd71279d91a2aedffa7524465',1,'cal.c']]],
  ['v_5fbasic_5ftrims_5ffrom_5fflash',['v_Basic_Trims_From_Flash',['../cal_8c.html#a4e3170d02fd8bc1e3fbf57c17bc66d5c',1,'cal.c']]],
  ['v_5fdump_5ftrim_5fmemory',['v_Dump_Trim_Memory',['../cal_8c.html#ae3bd6306d87217e8ce34037cd6f96833',1,'cal.c']]],
  ['v_5fhub_5ftask',['v_Hub_Task',['../hub_8c.html#abd626b9120fc80b07a21935afdbc9673',1,'v_Hub_Task(void *args):&#160;hub.c'],['../main_8c.html#abd626b9120fc80b07a21935afdbc9673',1,'v_Hub_Task(void *args):&#160;hub.c']]],
  ['v_5finit_5fhub',['v_Init_Hub',['../hub_8c.html#aa6e908b98f9f5ead956be6d4de6b4a7b',1,'hub.c']]],
  ['v_5fpll_5fcalibration',['v_PLL_Calibration',['../cal_8c.html#a2f80b58575ad5296cb43105c684e716a',1,'cal.c']]]
];
